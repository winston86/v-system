<?php

/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - register()
 * - login()
 * - logout()
 * - getUsersInGroup()
 * - getUserGroupId()
 * - getGroups()
 * - getGroupsSelect()
 * - getGroupTitle()
 * - getUserId()
 * - getUserNickname()
 * - getUserFirstAndLastname()
 * - getUserLogin()
 * - getUserEmail()
 * - isUserActive()
 * - isLoged()
 * - isAdmin()
 * - is_user_can()
 * - getUserRights()
 * - getUser()
 * - getUsers()
 * - getUsersIdLoginArray()
 * - getUserImg()
 * - isMyProfile()
 * - getUserSearchFields()
 * Classes list:
 * - Users extends Db
 */
namespace core;

use classes\Logger;
use core\Db;

class Users extends Db
{

    public function __construct()
    {

        Logger::log('Creating users object... ');

        $this->user_id = @$_SESSION['user_straps']['id'] ? @$_SESSION['user_straps']['id'] : 0;

        $this->user_straps = @$_SESSION['user_straps']['straps'];

        parent::__construct();

    }

    public function register($userData)
    {

        global $Core;

        if ($new_user_id = $this->insert('users', $userData)) {

            $Core->sysMessage($this->lang['REGISTER_SUCCESS'], 'success');

        }

        return $new_user_id;

    }

    public function login($login, $pass)
    {

        global $Core;

        $user_id = $this->getUserId($login);

        if (!$this->isLoged()) {
            $this->delete('users_insys', array(
                'user_id' => $this->getUserId($login),
            ));
        }

        if (!$user_id) {
            $Core->sysMessage($this->lang['USER_DOES_NOT_EXIST'], 'error');
            $Core->back();
        }

        $user_pass = $this->selectField('users', 'password', array(
            'id'        => $user_id,
            'published' => 1,
        ));

        if ($user_pass == $pass) {

            $random = md5(rand(1, 1000000) . $login . $pass);

            $_SESSION['user_straps']['straps'] = $random;

            $_SESSION['user_straps']['id'] = $user_id;

            $this->insert('users_insys', array(
                'user_id'  => $user_id,
                'straps'   => $random,
                'log_date' => date("Y-m-d H:i:s"),
            ));

        } else {

            $Core->sysMessage($this->lang['WRONG_LOGIN_OR_PASS'], 'error');

            $Core->back();

        }

    }

    public function logout()
    {

        $this->delete('users_insys', array(
            'user_id' => $this->user_id,
        ));

        unset($_SESSION['user_straps']);

    }

    public function getUsersInGroup($g_id)
    {

        $params = array(
            'group_id' => $g_id,
        );

        $users = $this->selectRows('users', '*', $params);

        return $users;

    }

    public function getUserGroupId($user_id = false)
    {

        if (empty($user_id)) {
            $user_id = $this->user_id;
        }

        if (!empty($user_id)) {

            return $this->selectField('users', 'group_id', array(
                'id' => $user_id,
            ));

        } else {

            $group = $this->getDefaultGroup();

            return $group['id'];

        }

    }

    public function getGroups($params)
    {

        return $this->selectRows('user_groups', '*', $params);

    }

    public function getGroupsSelect($params = array())
    {

        $groups = $this->getGroups($params);

        $groups_select = array();

        foreach ($groups as $key => $value) {

            $groups_select[$value['id']] = $value['title'];

        }

        return $groups_select;

    }

    public function getGroupTitle($group_id)
    {

        return $this->selectField('user_groups', 'title', array(
            'id' => $group_id,
        ));

    }

    public function getUserId($login)
    {

        return $this->selectField('users', 'id', array(
            'login' => $login,
        ));

    }

    public function getUserNickname($id = null, $login = '')
    {

        if (!$id) {
            $id = $this->user_id;
        }

        if ($id > 0) {

            return $this->selectField('users', 'nickname', array(
                'id' => $id,
            ));

        } else {

            return $this->selectField('users', 'nickname', array(
                'login' => $login,
            ));

        }

    }

    public function getUserFirstAndLastname($id = 0, $login = '')
    {

        if ($id == 0) {
            $id = $this->user_id;
        }

        if ($id > 0) {
            return $this->selectField('users', 'firstname', array(
                'id' => $id,
            )) . ' ' . $this->selectField('users', 'lastname', array(
                'id' => $id,
            ));
        } else {
            return $this->selectField('users', 'firstname', array(
                'login' => $login,
            )) . ' ' . $this->selectField('users', 'lastname', array(
                'login' => $login,
            ));
        }

    }

    public function getUserLogin($id = false, $login = false)
    {

        if (!$id) {
            $id = $this->user_id;
        }

        return $this->selectField('users', 'login', array(
            'id' => $id,
        ));

    }

    public function getUserEmail($id = false, $login = false)
    {

        if ($id) {

            return $this->selectField('users', 'email', array(
                'id' => $id,
            ));

        } else {

            return $this->selectField('users', 'email', array(
                'login' => $login,
            ));

        }

    }

    public function isUserActive($id = false, $login = false)
    {

    }

    public function isLoged()
    {

        $log_data = $this->selectFields('users_insys', 'straps', array(
            'user_id' => $this->user_id,
        ));

        if ($this->user_straps == $log_data['straps'] && is_array($log_data) && isset($this->user_straps)) {

            return true;

        } else {

            return false;

        }

    }

    public function isAdmin()
    {

        return $this->getUserGroupId($this->user_id) == 1;

    }

    public function is_user_can($event)
    {

        $user_rights = $this->getUserRights();

        if (in_array($event, $user_rights)) {

            return true;

        } elseif ($user_rights == 'all' || $user_rights == array(
            'all',
        )) {

            return true;

        } else {

            return false;

        }

    }

    public function getDefaultGroup()
    {
        return $this->selectFields('user_groups', '*', array(
                'default_group' => 1,
            ));
    }

    public function getUserRights()
    {

        $user_group_id = $this->getUserGroupId($this->user_id);

        if (empty($user_group_id)) {
            return [];
        }

        $group_rights_str = $this->selectField('user_groups', 'permissions', array(
            'id' => $user_group_id,
        ));

        $group_rights = explode('||', $group_rights_str);

        if (in_array('all', $group_rights)) {
            return array(
                'all',
            );
        }

        $params = ['alias' => $group_rights];

        $result = $this->selectRows('permissions', 'events', $params);

        $user_rights = array();

        foreach ($result as $right) {

            $user_rights = array_merge($user_rights, explode('||', $right['events']));

        }

        return $user_rights;

    }

    public function getUser($where = array())
    {

        $join['g'] = array(
            'user_groups',
            'title as `group`',
            array(
                'id' => 'main.group_id',
            ),
        );

        $this->setJoin($join);

        $user = $this->selectFields('users', '*', $where);

        return $user;

    }

    public function getUsers($where = array(), $limit)
    {

        $join['g'] = array(
            'user_groups',
            'title as `group`',
            array(
                'id' => 'main.group_id',
            ),
        );

        $this->setJoin($join);

        $users = $this->selectRows('users', 'id,login,email,nickname,image,reg_date,group_id', $where, $limit);

        return $users;

    }

    public function getUsersIdLoginArray($where = [])
    {

        $res = $this->selectRows('users', 'id,login', $where);

        $users = [];

        foreach ($res as $u) {
            $users[$u['id']] = $u['login'];
        }

        return $users;

    }

    public function getUserImg($where)
    {

        return $this->selectField('users', 'image', $where);

    }

    public function isMyProfile($login)
    {

        return $this->selectField('users', 'id', array(
            'login' => $login,
        )) == $this->user_id;

    }

    public function getUserSearchFields($user_html, $disabled)
    {

        return array(

            'users[login]'    => array(
                'type'       => 'text',
                'attrs'      => array(
                    'placeholder' => $this->lang['LOGIN'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'      => $this->lang['LOGIN'],
                'disabled'   => $disabled,
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div id="users-block"><br><label>' . $this->lang['USER_BLOCK'] . '</label>' . $this->lang['USER_BLOCK_INFO'] . '<div  style="margin: 10 10 10 0" class="shop-discount-add form-inline">%block%',
            ),
            'users[nickname]' => array(
                'type'       => 'text',
                'attrs'      => array(
                    'placeholder' => $this->lang['NICKNAME'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'      => $this->lang['NICKNAME'],
                'disabled'   => $disabled,
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '%block%',
            ),
            'users[email]'    => array(
                'type'       => 'text',
                'attrs'      => array(
                    'placeholder' => $this->lang['EMAIL'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'      => $this->lang['EMAIL'],
                'disabled'   => $disabled,
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '%block%',
            ),
            'users[quantity]' => array(
                'type'       => 'select',
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'options'    => array(
                    '5'  => '5',
                    '10' => '10',
                    '15' => '15',
                    '20' => '20',
                    '25' => '25',
                    '30' => '30',
                    '40' => '40',
                ),
                'label'      => $this->lang['QUANTITY'],
                'disabled'   => $disabled,
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '%block%',
            ),
            'users[submit]'   => array(
                'type'       => 'button',
                'attrs'      => array(
                    'class' => 'shop-input  form-control btn-primary users-submit',
                ),
                'label'      => $this->lang['SUBMIT'],
                'disabled'   => $disabled,
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '%block%</div><div id="users-list">' . $user_html . '</div></div>',
            ),
        );
    }

}
