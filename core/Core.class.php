<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - execute()
 * - initSubObjects()
 * - getQueryStr()
 * - addErrorMessage()
 * - checkFields()
 * - sessionUnset()
 * - sessionPut()
 * - sessionGet()
 * - inSession()
 * - setLanguage()
 * - getLanguage()
 * - getLangsAvailable()
 * - getRequest()
 * - getUrlVars()
 * - getRoute()
 * - getPageRoute()
 * - coreInit()
 * - dbInit()
 * - renderInit()
 * - pageInit()
 * - usersInit()
 * - executeApp()
 * - viewContent()
 * - isAllowedPhpScript()
 * - loadView()
 * - loadModel()
 * - loadClass()
 * - redirect()
 * - back()
 * - getUri()
 * - inRequest()
 * - getLang()
 * - sysMessage()
 * - getSysMessages()
 * - getSysMessagesCount()
 * - print_rec()
 * - getAdminPart()
 * - getTplPosiotions()
 * - addEvent()
 * - clearTags()
 * - removeOnAttrs()
 * - removeEventAttributesFromTag()
 * - getAppSettings()
 * - updateAppSettings()
 * - getExtensionSettings()
 * - updateExtensionSettings()
 * - saveFiles()
 * - saveFile()
 * - getValEqLabelArr()
 * - getFilterParams()
 * - getFilterParamsVal()
 * - clearValueFromHash()
 * - itemForm()
 * - response()
 * - proccessMassiveActions()
 * Classes list:
 * - Core
 */
namespace core;

use classes\Logger;
use core\Db;
use core\Page;
use core\Render;
use core\Users;

class Core
{

    private static $instance;

    public function __construct()
    {

        global $_CONFIG;

        $this->route = static::getRoute(explode("/", str_replace("?" . $_SERVER['QUERY_STRING'], '', self::getUri())), $_CONFIG);

        $this->pageRoute = static::getPageRoute($_CONFIG);

        $this->clearPath = ROOT_DIR . implode('/', $this->route);

        $this->clearRoute = implode('/', $this->route);

        $_SERVER['SCRIPT_FILENAME'] = $this->clearPath;

        @chdir(dirname(ROOT_DIR . implode('/', $this->route)));

        $this->lang = static::setLanguage($_CONFIG);

        $this->langVars = static::getLang('global');

    }

    public function execute()
    {

        $this->viewContent();

    }

    public function initSubObjects()
    {

        if (empty($this->render)) {
            $this->render = static::renderInit();
        }

        if (empty($this->page)) {
            $this->page = static::pageInit();
        }

        if (empty($this->users)) {
            $this->users = static::usersInit();
            //exception for users lang
            $this->users->lang = $this->getLang('applications/users');
        }

        $this->db = static::dbInit();

    }

    public function getQueryStr()
    {
        return $_SERVER['QUERY_STRING'];
    }

    public function addErrorMessage($type, $lang)
    {

        $msg = '';

        if (!empty($lang['ERROR_' . strtoupper($type)])) {
            $msg .= $lang['ERROR_' . strtoupper($type)];
        } else {
            foreach ($this->getLangsAvailable() as $lang) {
                $msg .= $lang['ERROR_' . strtoupper($type)];
            }
        }

        $msg = !empty($msg) ? $msg : 'error ' . $type;

        $this->sysMessage($msg, 'error');

    }

    public function checkFields($fields, $lang)
    {
        $errors = array();
        foreach ($this->getRequest('required') as $req) {
            $req_arr = explode("[", $req);
            if (is_array($req_arr)) {
                $tmp = array();
                foreach ($req_arr as $key => $r) {
                    if ($key == 0) {
                        continue;
                    }

                    if (!empty($tmp)) {
                        $tmp = $tmp[trim($r, "]")];
                    } else {
                        $tmp = $fields[trim($r, "]")];
                    }
                }
                $type  = trim($r, "]");
                $value = $tmp;
                if ($type == 'email' || $type == 'mail') {
                    if (!preg_match("/^(.*)@(.*)\.(.*)$/i", $value)) {
                        $this->addErrorMessage($type, $lang);
                        $errors[] = $req;
                    }
                } else {
                    if (empty($value)) {
                        $this->addErrorMessage($type, $lang);
                        $errors[] = $req;
                    } else {

                    }
                }
            } else {
                if ($req == 'email' || $req == 'mail') {
                    if (!isset($fields[$req]) || !preg_match("/^(.*)@(.*)\.(.*)$/ig", $fields[$req])) {
                        $this->addErrorMessage($type, $lang);
                        $errors[] = $req;
                    }
                } else {
                    if (!isset($fields[$req]) || empty($fields[$req])) {
                        $this->addErrorMessage($type, $lang);
                        $errors[] = $req;
                    }
                }
            }

        }

        return $errors;
    }

    public function sessionUnset($var)
    {
        unset($_SESSION[$var]);
    }

    public function sessionPut($values)
    {
        foreach ($values as $key => $value) {
            $_SESSION[$key] = $value;
        }
        return;
    }

    public function sessionGet($var, $default = false)
    {
        return $this->inSession($var) ? $_SESSION[$var] : $default;
    }

    public function inSession($var)
    {
        return isset($_SESSION[$var]);
    }

    public function setLanguage($_CONFIG)
    {

        if ($this->inRequest('lang')) {
            $lang                     = $this->getRequest('lang');
            $_SESSION['user']['lang'] = $lang;
            $this->back();
        } elseif (isset($_SESSION['user']['lang'])) {
            $lang = $_SESSION['user']['lang'];
        } else {
            $lang = $_CONFIG['LANG'];
        }
        $_SESSION['user']['lang'] = $lang;
        return $lang;

    }

    public function getLanguage($params)
    {

        return $this->db->selectFields('languages', '*', $params);

    }

    public function getLangsAvailable()
    {
        $params['published'] = 1;

        return $this->db->selectRows('languages', '*', $params);

    }

    public function getRequest($var, $type = 'str')
    {

        if ($type == 'str') {

            return isset($_REQUEST[$var]) ? $_REQUEST[$var] : (isset($this->url_vars[$var]) ? $this->url_vars[$var] : '');

        }

        if ($type == 'int') {

            return isset($_REQUEST[$var]) ? (int) $_REQUEST[$var] : (isset($this->url_vars[$var]) ? (int) $this->url_vars[$var] : 0);

        }

        if ($type == 'array') {

            return isset($_REQUEST[$var]) ? (array) $_REQUEST[$var] : (isset($this->url_vars[$var]) ? (array) $this->url_vars[$var] : array());

        }

        return false;

    }

    public function getUrlVars()
    {

        if (!file_exists(ROOT_DIR . "/frontend/applications/" . $this->pageRoute . "/routes.php")) {
            return;
        }

        include ROOT_DIR . "/frontend/applications/" . $this->pageRoute . "/routes.php";

        $variables = array();

        foreach ($routes as $route => $vars) {

            if (preg_match($route, implode('/', $this->route), $matches)) {

                foreach ($vars as $key => $var) {

                    $variables[$var] = isset($matches[$key]) ? $matches[$key] : $key;

                }

            }

        }

        return $variables;

    }

    private function getRoute($route_array, $cfg)
    {

        return $route_array[1] ? $route_array : explode("/", str_replace("?" . $_SERVER['QUERY_STRING'], '', $cfg['HOME']));

    }

    private function getPageRoute($cfg)
    {

        return $this->route[1] ? $this->route[1] : $cfg['HOME'];

    }

    public static function coreInit()
    {

        if (!isset(self::$instance)) {

            self::$instance = new self;

        }

        return self::$instance;
    }

    public function dbInit()
    {

        return new \core\Db;

    }

    public function renderInit()
    {

        return new \core\Render;

    }

    public function pageInit()
    {

        return new \core\Page;

    }

    public function usersInit()
    {

        return new \core\Users;

    }

    public function executeApp($app)
    {

        switch ($app['type']) {

            case 'mod':

                $app_path = 'modules';

                break;

            case 'app':

                $app_path = 'applications';

                break;

            default:

                $app_path = 'applications';

        }

        include_once ROOT_DIR . "/frontend/{$app_path}/{$app['route']}/controller.php";

        $route = ucfirst($app['route']);

        $app_controller = "{$route}Controller";

        $ns = "frontend\\{$app_path}\\{$app['route']}\\{$app_controller}";

        $app_obj = new $ns;

        return $app_obj->execute($app);

    }

    public function viewContent()
    {

        Logger::log('Start of creating response for ' . $this->clearPath);

        if ($this->pageRoute == 'admin' && (!file_exists($this->clearPath) || is_dir($this->clearPath))) {

            $this->getAdminPart();

        }

        if (file_exists($this->clearPath) && !is_dir($this->clearPath)) {

            $mime = mime_content_type($this->clearPath);

            if (!$this->isAllowedMime($mime)) {

                die('Error. Forbidden type ' . $mime . ' due security policy.');

            }

            header('Content-Type: ' . $mime);

            if (preg_match("/^image\/(.*)$/i", $mime)) {
                echo file_get_contents($this->clearPath);
            } elseif (preg_match("/^video\/(.*)$/i", $mime)) {
                echo file_get_contents($this->clearPath);
            } elseif (preg_match("/^audio\/(.*)$/i", $mime)) {
                echo file_get_contents($this->clearPath);
            } elseif (preg_match("/^application\/(.*)$/i", $mime)) {
                echo file_get_contents($this->clearPath);
            } else {
                // text/* case
                if ($mime == 'text/x-php') {
                    if ($this->isAllowedPhpScript($this->clearPath)) {
                        Logger::log('include allowed php script' . $this->clearPath);
                        $this->initSubObjects();
                        include $this->clearPath;
                    } else {
                        die('Error. Forbidden script due security policy.');
                    }
                } else {
                    echo file_get_contents($this->clearPath);
                }
            }

            die;

        }

        if ($this->pageRoute != 'admin') {

            $this->url_vars = $this->getUrlVars();

        }

        $this->initSubObjects();

        $content['main_content'] = $this->page->getPageContent();

        $content['apps_in_page'] = $this->page->getPageApps();

        if (!$content['main_content'] || is_dir($this->clearPath)) {

            $this->render->renderErrorInfo(404);

        } else {

            $this->render->renderPage($content);

        }

        return true;
    }

    private function isAllowedMime($mime){

        global $_ALLOWED_MIMES;

        return in_array($mime, $_ALLOWED_MIMES);

    }

    private function isAllowedPhpScript($script_path = '')
    {

        global $_ALLOWED_SCRIPTS, $_FORBIDDEN_SCRIPTS;

        if (file_exists($script_path) || is_dir($script_path)) {
            foreach ($_ALLOWED_SCRIPTS as $tpl) {
                if (preg_match("/^" . preg_quote($tpl, '/') . "(.*)$/", $script_path)) {
                    foreach ($_FORBIDDEN_SCRIPTS as $ftpl) {
                        if (preg_match("/^" . preg_quote($ftpl) . "(.*)$/", $script_path)) {
                            $forbidden = true;
                        }
                    }
                    if ($forbidden) {
                        continue;
                    }
                    return true;
                }
            }

            return false;

        } else {

            return false;

        }

    }

    public function loadView($app, $type = 'app')
    {

        switch ($type) {

            case 'mod':

                $app_path = 'modules';

                break;

            case 'app':

                $app_path = 'applications';

                break;

            default:

                $app_path = 'applications';

        }

        include_once ROOT_DIR . "/frontend/{$app_path}/{$app}/view.php";

        $view_name = ucfirst($app);

        $ns = "\\frontend\\{$app_path}\\{$app}\\{$view_name}View";

        return new $ns;

    }

    public function loadModel($app, $type = 'app')
    {

        switch ($type) {

            case 'mod':

                $app_path = 'modules';

                break;

            case 'app':

                $app_path = 'applications';

                break;

            default:

                $app_path = 'applications';

        }

        include_once ROOT_DIR . "/frontend/{$app_path}/{$app}/model.php";

        $model_name = ucfirst($app);

        $ns = "\\frontend\\{$app_path}\\{$app}\\{$model_name}Model";

        return new $ns;

    }

    public function loadClass($class_name, $class_path, $params = null)
    {

        include_once ROOT_DIR . "/classes/" . $class_path;

        return new $class_name($params);

    }

    public function redirect($address)
    {

        header("Location: " . $address);

        //die();

    }

    public function back()
    {

        $back = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/';

        header("Location: " . $back);

        die();

    }

    public static function getUri()
    {

        return $_SERVER['REQUEST_URI'];

    }

    public function inRequest($var)
    {

        return isset($_REQUEST[$var]) || isset($this->url_vars[$var]);

    }

    public function getLang($file)
    {

        $lang = [];

        if (file_exists(ROOT_DIR . "/languages/" . $this->lang . "/" . $file . '.lang.php')) {
            include ROOT_DIR . "/languages/" . $this->lang . "/" . $file . '.lang.php';
        }

        return $lang;

    }

    public function sysMessage($msg, $type = 'info')
    {
        if (empty($msg)) {
            return;
        }

        $_SESSION['sysMsgs'][] = '<div class="msg alert alert-' . $type . ' ' . $type . '">' . $msg . '<a class="close float-right glyphicon glyphicon-remove-circle"></a></div>';

    }

    public function getSysMessages()
    {

        $msgs = @$_SESSION['sysMsgs'];

        unset($_SESSION['sysMsgs']);

        return $msgs;

    }

    public function getSysMessagesCount()
    {

        return @count($_SESSION['sysMsgs']);

    }

    public function print_rec($str)
    {
        echo '<pre>';
        print_r($str);
        die('</pre>');
    }

    public function getAdminPart()
    {
        Logger::log('Getting admin part... ' . $this->clearPath);

        $this->users  = new Users();
        $this->render = new Render();

        if (!$this->users->isLoged()) {

            $this->redirect('/users/login');

        } elseif (!$this->users->isAdmin()) {

            $this->render->renderErrorInfo(403);

        }

        if (file_exists($this->clearPath) && !is_dir($this->clearPath) && $this->users->isLoged() && $this->users->isAdmin()) {

            include ROOT_DIR . str_replace("?" . $_SERVER['QUERY_STRING'], '', self::getUri());

            die;

        }

        if (preg_match("/^\/(.*)\.\w+$/", $this->clearPath)) {
            $this->render->renderErrorInfo(404);
        }

        Logger::log('Creating core subobjects for admin part... ' . $this->clearPath);

        $this->initSubObjects();

        $this->admin = new \admin\Admin();

    }

    public function getTplPosiotions()
    {

        return $this->db->selectFields("tpl_positions", "id, title");

    }

    public function addEvent($event, $params)
    {

        $sql = "SELECT * FROM {$this->db->prefix}extensions WHERE :event IN (events) AND published = 1 ";

        $extensions = $this->db->query($sql, array(
            ':event' => $event,
        ));

        foreach ($extensions as $extension) {

            include_once ROOT_DIR . "/extensions/{$extension['route']}/controller.php";

            $tmp = explode("_", $extension['route']);

            array_map("ucfirst", $tmp);

            $class_name = implode($tmp, "") . "Controller";

            $ns = "extensions\\{$extension['route']}\\$class_name";

            $ext = new $ns;

            $ext->execute($params);

        }

    }

    public function clearTags($text)
    {

        return strip_tags($text, '<p>
                        <a>
                        <div>
                        <span>
                        <b>
                        <strong>
                        <i>
                        <center>
                        <code>
                        <fieldset>
                        <h1>
                        <h2>
                        <h3>
                        <h4>
                        <h5>
                        <h6>
                        <hr>
                        <img>
                        <legend>
                        <ul>
                        <li>
                        <table>
                        <tbody>
                        <tr>
                        <td>
                        <th>
                        <thead>
                        <title>
                        <u>
                        <br>
                        <em>');

    }

    public function removeOnAttrs($html)
    {

        $redefs = '(?(DEFINE)
                (?<tagname> [a-z][^\s>/]*+    )
                (?<attname> [^\s>/][^\s=>/]*+    )  # first char can be pretty much anything, including =
                (?<attval>  (?>
                        "[^"]*+" |
                        \'[^\']*+\' |
                        [^\s>]*+            # unquoted values can contain quotes, = and /
                    )
                )
                (?<attrib>  (?&attname)
                    (?: \s*+
                        = \s*+
                        (?&attval)
                    )?+
                )
                (?<crap>    [^\s>]    )             # most crap inside tag is ignored, will eat the last / in self closing tags
                (?<tag>     <(?&tagname)
                    (?: \s*+                # spaces between attributes not required: <b/foo=">"style=color:red>bold red text</b>
                        (?>
                            (?&attrib) |    # order matters
                            (?&crap)        # if not an attribute, eat the crap
                        )
                    )*+
                    \s*+ /?+
                    \s*+ >
                )
            )';

        // removes onanything attributes from all matched HTML tags
        $re = '(?&tag)' . $redefs;

        return preg_replace_callback("~$re~xie", '$this->removeEventAttributesFromTag("$0",$redefs)', $html);

    }

    // removes onanything attributes from a single opening tag
    private function removeEventAttributesFromTag($tag, $redefs)
    {
        $re = '( ^ <(?&tagname) ) | \G \s*+ (?> ((?&attrib)) | ((?&crap)) )' . $redefs;

        return preg_replace("~$re~xie", '"$1$3"? "$0": (preg_match("/^on/i", "$2")? " ": "$0")', $tag);

    }

    public function getAppSettings($app)
    {

        global $Core;

        $where = array(
            'route' => $app,
        );

        $settings = json_decode($Core->db->selectField('applications', 'settings', $where));

        return (array) $settings;

    }

    public function updateAppSettings($settings, $app)
    {

        global $Core;

        $update = array(
            'settings' => json_encode($settings),
        );

        $where = array(
            'route' => $app,
        );

        return $Core->db->update('applications', $update, $where);

    }

    public function getExtensionSettings($extension)
    {

        global $Core;

        $where = array(
            'route' => $extension,
        );

        $settings = json_decode($Core->db->selectField('extensions', 'settings', $where), true);

        return is_array($settings) ? $settings : [];

    }

    public function updateExtensionSettings($settings, $extension)
    {

        global $Core;

        $update = array(
            'settings' => json_encode($settings),
        );

        $where = array(
            'route' => $extension,
        );

        return $Core->db->update('extensions', $update, $where);

    }

    public function saveFiles($input_name, $dest)
    {

        $dest = ROOT_DIR . $dest;

        if (isset($_FILES[$input_name])) {

            if (is_array($_FILES[$input_name])) {

                foreach ($_FILES[$input_name] as $key => $file) {

                    if (!$this->saveFile($file, $dest)) {

                        $this->sysMsgs($this->lang['ERROR_UPLOAD'] . ' ' . $file, 'error');

                    }

                }

            } else {

                if (!$this->saveFile($file, $dest)) {

                    $this->sysMsgs($this->lang['ERROR_UPLOAD'] . ' ' . $file, 'error');

                }

            }

        }

        return false;

    }

    public function saveFile($file, $dest)
    {

        $dest = ROOT_DIR . $dest;

        $file = !empty($_FILES[$file]) && file_exists($_FILES[$file]['tmp_name']) ? $_FILES[$file]['tmp_name'] : (file_exists($file) ? $file : '');

        if (!empty($file)) {

            if (move_uploaded_file($file, $dest)) {
                
                $this->sysMessage($this->lang['FILE_UPLOADED'], 'success');

                return $dest;

            } else {

                $this->sysMessage($this->lang[$_FILES[$file]['error']], 'error');

                return false;

            }

        }

        return false;

    }

    public function getValEqLabelArr($arr, $val, $label)
    {
        $ret = array();
        foreach ($arr as $key => $value) {
            $ret[$value[$val]] = $value[$label];
        }
        return $ret;

    }

    public function getFilterParams()
    {
        $params = $this->inRequest('filter') ? $this->getRequest('filter') : ($this->inSession('filter') ? $this->sessionGet('filter') : array());

        if ($this->inRequest('filter')) {
            $this->sessionPut(array(
                'filter' => $params,
            ));
        }

        foreach ($params as $key => $value) {

            if (empty($value)) {
                continue;
            }

            $operator = key($value);
            $value    = $value[$operator];

            switch ($key) {
                case 'title':
                case 'code':
                case 'description':
                case 'file':
                    $params[$key] = array(
                        'operator' => 'LIKE',
                        'value'    => "%$value%",
                    );
                    break;
                case 'published':
                    if ($value == 'pub') {
                        $params[$key] = 1;
                    } else if ($value == 'upub') {
                        $params[$key] = 0;
                    } else {
                        $params[$key] = null;
                    }
                    break;
                default:

                    if ($operator === 'LIKE') {
                        $params[$key] = array(
                            'operator' => 'LIKE',
                            'value'    => "%$value%",
                        );
                    } else {
                        $params[$key] = trim($value);
                    }

                    break;
            }

        }

        return $params;

    }

    /**
     * @param $params
     * @param string $param
     * @return string
     */
    public function getFilterParamsVal($params, $param = '')
    {
        if (empty($param)) {
            $ret = [];
            foreach ($params as $param => $val) {
                if (is_array($val)) {
                    $ret[$param] = isset($val) ? trim($val['value'], "%") : '';
                } else {
                    $ret[$param] = isset($val) ? $val : '';
                }
            }
        } else {
            if (is_array($params[$param])) {
                $ret = isset($params[$param]) ? trim($params[$param]['value'], "%") : '';
            } else {
                $ret = isset($params[$param]) ? $params[$param] : '';
            }
        }

        return $ret;

    }

    public function clearValueFromHash($value)
    {

        list($res, $trash) = explode("-", $value);

        return $res;

    }

    public function itemForm($inputs_html, $component)
    {
        return '<form enctype="multipart/form-data"  class="' . $component . '-form" method="post" action="">' . $inputs_html . '
        <input type="submit" class="btn btn-primary" name="submit" value="' . $this->langVars['SUBMIT'] . '" />
        <input type="hidden" id="item_id" name="item_id" value="' . $this->getRequest('id') . '" />
        </form>';
    }

    public function response($data, $contentType = 'JSON')
    {

        header('Content-Type: ' . $contentType);

        echo $data;

        exit();

    }

    public function setHeaders($headers)
    {

        foreach ($headers as $header => $value) {
            header("{$header}: {$value}");
        }

    }

    public function proccessMassiveActions($app = null)
    {
        $action = $this->getRequest('massive_action');

        if ($this->inRequest('massive_action') && !empty($action)) {

            if (!empty($app)) {
                switch ($action) {
                    case 'publish':
                        foreach ($this->getRequest('items_checked') as $item_id) {
                            $app->publishItem($item_id);
                        }
                        break;
                    case 'unpublish':
                        foreach ($this->getRequest('items_checked') as $item_id) {
                            $app->unpublishItem($item_id);
                        }
                        break;
                    case 'delete':
                        foreach ($this->getRequest('items_checked') as $item_id) {
                            $app->deleteItem($item_id);
                        }
                        break;
                    default:

                        break;
                }

                if (method_exists($app, 'proccessMassiveActions')) {
                    $app->proccessMassiveActions();
                }

            } else {

                $table = $this->getRequest('dir') == 'modules' ? 'applications' : $this->getRequest('dir');

                foreach ($this->getRequest('items_checked') as $item_id) {

                    if ($this->db->$action($table, ['id' => $item_id])) {
                        switch ($action) {
                            case 'publish':
                                $this->sysMessage($item_id . ' ' . $this->langVars['PUBLISHED'], 'success');
                                break;
                            case 'unpublish':
                                $this->sysMessage($item_id . ' ' . $this->langVars['UNPUBLISHED'], 'success');
                                break;
                            case 'delete':
                                $this->sysMessage($item_id . ' ' . $this->langVars['DELETED'], 'success');
                                break;
                        }

                    };

                }

            }

        }

        return;

    }

}
