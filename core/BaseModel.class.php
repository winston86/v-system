<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - init()
 * - one()
 * - all()
 * - save()
 * - insert()
 * - update()
 * - delete()
 * - select()
 * - toArray()
 * Classes list:
 * - BaseModel extends Db
 */
namespace core;

use core\Db;
use core\MultiLang;

class BaseModel extends Db
{

    public $modelTable;

    public $translation;

    public function __construct($table = '')
    {

        $this->modelTable = $table;

        $this->translation = new MultiLang();

        parent::__construct();

    }

    public function init(&$data)
    {

        foreach ($data as $field => $value) {

            $this->{$field} = $value;

        }

        return (clone $this);

    }

    public function one($params)
    {

        $res = $this->selectFields($this->modelTable, '*', $params);

        $res = $this->translation->getTranslations([$res], $this->modelTable);

        $res = array_shift($res);

        $model = $this->init($res);

        return $model;

    }

    public function all($params, $limit = false, $orderBy = '', $groupBy = '')
    {

        $res = $this->selectRows($this->modelTable, '*', $params, $limit, $orderBy, $groupBy);

        $res = $this->translation->getTranslations($res, $this->modelTable);

        $models = [];

        foreach ($res as $key => $item) {

            $models[$key] = $this->init($item);

        }

        return $models;

    }

    public function save()
    {

        $columns = $this->getFieldNames();

        foreach ($columns as $key => $field) {

            $fields[$field] = $this->{$field};

        }

        if (isset($this->id)) {

            $this->update($this->modelTable, $fields, ['id' => $this->id]);

        } else {

            $id = $this->insert($this->modelTable, $fields);

        }

        $this->one(['id' => $id]);

    }

    public function insert($table, $fields = array())
    {
        $nativeFields = $this->translation->getNativeFieldsValues($table, $fields);

        $row_id = parent::insert($table, $nativeFields);

        if (!empty($row_id)) {
            $this->translation->generateTranslations($table, $fields, ['id' => $row_id]);
        }

        return $row_id;

    }

    /**
     * @param $table
     * @param $fields
     * @param $params
     * @return bool
     */
    public function update($table, $fields, $params)
    {

        $this->translation->generateTranslations($table, $fields, $params);

        return parent::update($table, $fields, $params);
    }

    public function delete($table, $params = array())
    {

        $ids = [];

        foreach ($this->selectRows($table, 'id', $params) as $toDel) {

            $ids[] = $toDel['id'];

        }

        if (parent::delete($table, $params)) {

            $this->translation->deleteTranslations($table, $ids);

        }
    }

    public function select($table, $fields, $params = array(), $limit = false, $orderBy = '', $groupBy = '')
    {
        $res = parent::select($table, $fields, $params, $limit, $orderBy, $groupBy);

        $this->modelTable = $table;

        $translation = $this->translation->getTranslations($res, $this->modelTable);

        if (!empty($translation)) {
           $res = $translation;
        }

        return $res;
    }

    public function toArray()
    {

        $columns = $this->getFieldNames($this->modelTable);

        foreach ($columns as $key => $field) {

            $fields[$field] = $this->{$field};

        }

        return $fields;

    }

}
