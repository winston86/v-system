<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - getPageContent()
 * - getPageApps()
 * - getPosName()
 * - addHeadJs()
 * - addHeadCss()
 * - makeUrl()
 * Classes list:
 * - Page
 */
namespace core;

use classes\Logger;

class Page
{

    public function __construct()
    {

        Logger::log('Creating page object... ');

        $this->content = array();

        $this->additional_links = '';

    }

    public function getPageContent()
    {

        global $Core;

        return $Core->db->selectFields("applications", "*", array(
            'route'     => $Core->pageRoute,
            'published' => 1,
        ));

    }

    public function getPageApps()
    {

        global $Core;
        if ($Core->getUri() != '/') {
            $attached_at = $Core->clearRoute != '/'
            && !is_file($Core->clearPath)
            && !is_dir($Core->clearPath)
            ? urldecode($Core->clearRoute) : $Core->pageRoute;
        } else {
            $attached_at = '/';
        }

        $tmp = explode("/", $attached_at);

        $com_rout = $tmp[1];
        
        if ($attached_at != '/' && empty($Core->db->countRows('applications', array('route' => $com_rout, 'published' => 1, 'type' => 'app')))) {
            return array();
        }

        $sql = "SELECT a_i_p.* FROM {$Core->db->prefix}apps_in_page  AS a_i_p

        LEFT JOIN {$Core->db->prefix}applications AS apps ON(apps.id = app_id)

        WHERE ( a_i_p.page_route = :attached_at OR ( :attached_at LIKE CONCAT(a_i_p.page_route, '%')

        AND a_i_p.sub_pages = 1) )

        AND a_i_p.published = 1

        AND apps.published = 1

        ORDER BY a_i_p.priority ASC";

        $apps = $Core->db->query($sql, array(
            ':attached_at' => $attached_at,
        ));

        if (!$apps) {
            return false;
        }

        $page_apps = array();

        foreach ($apps as $app) {

            $position = $this->getPosName($app['position_id']);

            $application = $Core->db->selectFields("applications", "*", array(
                'id'        => $app['app_id'],
                'published' => 1,
            ));

            $application['mod_settings'] = $app['settings'];

            $application['title'] = $app['title'];

            $page_apps[$position][] = $application;

        }

        return $page_apps;

    }

    public function getPosName($pos_id)
    {

        global $Core;

        return $Core->db->selectField('tpl_positions', 'title', array(
            'id' => $pos_id,
        ));

    }

    public function addHeadJs($url)
    {

        $this->additional_links .= !strstr($this->additional_links, '<script src="' . $url . '" type="text/javascript"></script>') ? '<script src="' . $url . '" type="text/javascript"></script>' : '';

    }

    public function addHeadCss($url)
    {

        $this->additional_links .= !strstr($this->additional_links, '<link href="' . $url . '" rel="stylesheet" type="text/css" />') ? '<link href="' . $url . '" rel="stylesheet" type="text/css" />' : '';

    }

    public function makeUrl($url, $params = array())
    {

        $parsed_url = parse_url($url);

        $path = $parsed_url['path'];

        parse_str($parsed_url['query'], $query);

        foreach ($params as $key => $value) {
            if (!empty($value)) {
                $query[$key] = $value;
            } else {
                unset($query[$key]);
            }

        }

        $new_url = $path . '?' . http_build_query($query);

        return $new_url;

    }

}
