<?php

namespace core;

class AppBaseController{

    public function __construct()
    {

    }

	public function execute($app)
	{
		global $Core;

		$this->model = $Core->loadModel($app['route'], $app['type']);

        $this->view = $Core->loadView($app['route'], $app['type']);
		
		if ($app['type'] == 'app') {
			
			$path = $Core->getRequest('path');

			list($action, $pre_action) = $this->getAction($path);

			if(method_exists($this, $action)){
				return $this->runAction($action, $pre_action);
			}else{
				list($action, $pre_action) = $this->getAction('default');
				return $this->runAction($action, $pre_action);
			}
		}
		
	}

	private function runAction($action, $pre_action = ''){
		if (method_exists($this, $pre_action)) {
			$this->$pre_action();
		}
		return $this->$action();
	}

	private function getAction($path){
		$tmp = static::$pathes[$path];
		if (is_array($tmp)) {
			$action = $tmp[0];
			$pre_action = $tmp[1];
		}else{
			$action = $tmp;
		}
		return [$action, $pre_action];
	}

}