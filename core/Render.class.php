<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - renderPage()
 * - replaceAdditionalLinks()
 * - printMainContent()
 * - getAppsHtml()
 * - getAppHtml()
 * - getErrorMsg()
 * - renderErrorInfo()
 * - eventPopUp()
 * - getSysMessagesCount()
 * - getSysMessages()
 * - addTwigFunctions()
 * - (()
 * - (()
 * - (()
 * - renderTpl()
 * - createInput()
 * - renderInputs()
 * - renderAdminMenu()
 * - getAdminHead()
 * - getModal()
 * - renderAdminList()
 * - getPagination()
 * - txtToASCII()
 * - resizeImg()
 * Classes list:
 * - Render
 */
namespace core;

use classes\Logger;

class Render
{

    public function __construct()
    {

        Logger::log('Creating render object... ');

        global $_CONFIG;

        $this->site_template = $_CONFIG['TEMPLATE'];

        $this->site_name = $_CONFIG['SITE_NAME'];

        $this->site_keywords = $_CONFIG['SITE_KEYWORDS'];

        $this->site_description = $_CONFIG['SITE_DESCRIPTION'];

        $this->tplDir = "/templates/" . $_CONFIG['TEMPLATE'];

    }

    public function renderPage($content)
    {

        global $Core;

        Logger::log('Rendering page...');

        $this->apps_in_page = $content['apps_in_page'];

        $this->main_content = $content['main_content'];

        ob_start();

        include ROOT_DIR . "/templates/" . $this->site_template . "/main_template.php";

        $html = ob_get_contents();

        ob_end_clean();

        $main_content_html = $this->printMainContent();

        $modules_html = $this->getAppsHtml();

        $html = str_replace('{*main_content*}', $main_content_html, $html);

        foreach ($modules_html as $pos => $module_html) {

            $tmp = '';

            foreach ($module_html as $mod) {
                $tmp .= $mod;
            }

            $html = str_replace("{*app.$pos*}", $tmp, $html);

        }

        if ($Core->getSysMessagesCount() > 0) {
            $msgs = '';
            foreach ($Core->getSysMessages() as $msg) {
                $msgs .= $msg;
            }
            $html = str_replace('{*sys_messages*}', $msgs, $html);
        } else {
            $html = str_replace('{*sys_messages*}', '', $html);
        }

        $html = $this->replaceAdditionalLinks($html);

        $html = preg_replace("/\{\*app\.(.*)\*\}/", '', $html);

        echo $html;

    }

    public function replaceAdditionalLinks($html)
    {

        global $Core;

        return str_replace('{*additional_links*}', $Core->page->additional_links, $html);

    }

    private function printMainContent()
    {

        global $Core;

        $app_content = $Core->executeApp($this->main_content, $this->main_content['type']);

        return $this->getAppHtml($app_content);

    }

    private function getAppsHtml()
    {

        global $Core;

        foreach ($this->apps_in_page as $app_pos => $modules) {

            foreach ($modules as $app) {
                $app_content               = $Core->executeApp($app);
                $modules_htmls[$app_pos][] = $this->getAppHtml($app_content, $app['type']);
            }

        }

        return $modules_htmls;

    }

    public function getAppHtml($app_content, $type = '')
    {
        return '<div class="' . $type . '">' . $app_content . '</div>';

    }

    public function getErrorMsg($code)
    {

        global $Core;

        return $Core->langVars['ERROR_' . $code];

    }

    public function renderErrorInfo($code)
    {

        include ROOT_DIR . "/templates/" . $this->site_template . "/additional/error.php";

        die;

    }

    public function eventPopUp($event, $params)
    {

        global $Core;

        $Core->addEvent($event, $params);

    }

    private function getSysMessagesCount()
    {

        global $Core;

        return $Core->getSysMessagesCount();

    }

    private function getSysMessages()
    {

        global $Core;

        return $Core->getSysMessages();

    }

    public function addTwigFunctions($twig)
    {

        $function = new \Twig_SimpleFunction('eventPopUp', function ($event, $params) {
            $this->eventPopUp($event, $params);
        });

        $twig->addFunction($function);

        $function = new \Twig_SimpleFunction('md5', function ($str) {
            return md5($str);
        });

        $twig->addFunction($function);

        $function = new \Twig_SimpleFunction('_t', function ($arr) {

            global $Core;

            return $arr[$Core->lang];

        });

        $twig->addFunction($function);

        return $twig;

    }

    public function renderTpl($file, $data = array(), $shortcode = false)
    {

        $tmp = explode(".", $file);

        $ext = array_pop($tmp);

        if ($ext == 'twig') {

            $template_path = pathinfo(ROOT_DIR . $file, PATHINFO_DIRNAME);

            $template_name = pathinfo(ROOT_DIR . $file, PATHINFO_FILENAME) . '.' . $ext;

            $loader = new \Twig_Loader_Filesystem($template_path);

            $twig = new \Twig_Environment($loader);

            $twig = $this->addTwigFunctions($twig);

            $return = $twig->render($template_name, $data);

        } else if ($ext == 'php') {

            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $$key = $value;
                }
            }

            ob_start();

            include ROOT_DIR . $file;

            $return = ob_get_contents();

            ob_end_clean();

        }

        list($trash, $base_path, $trash) = explode("/", Core::getUri());

        if (preg_match_all("/{\*file\*}(.*){\*\/file\*}/i", $return, $additional_files) && !stristr("admin", $base_path) && $shortcode) {

            foreach ($additional_files[1] as $key => $file) {

                ob_start();

                include ROOT_DIR . $file;

                $return = str_ireplace($additional_files[0][$key], ob_get_contents(), $return);

                ob_end_clean();

            }
        }

        return $return;

    }

    /* $inputs = array(
    'settings[enable_chicks]' => array( //name of input
    'type'=>'checkbox', // type of input
    'options'=>array( //options for select
    'enable'=>$this->lang['enable'], // value -> title
    'disable'=>$this->lang['disable']
    ),
    'multiple' => true, //multiple select
    'value'=> isset($this->settings['enable_chicks'])?$this->settings['enable_chicks']:0,  //current value
    'default_value'=>1, //default value. In case input is checkbox - value
    'attrs'=>array('class'=>'shop-setting'), // input atributes
    'required'=>true, //is required
    'label'  => $this->lang['enable_chicks'], // title
    'name_wrap'=>'<div class="inline"></div>', // title wrapper
    'input_wrap'=>'<div class="inline"></div>', // input wrapper
    'block_wrap'=>'<div class="setting-block"></div>', // block wrapper
    ),
    )
     */

    public function createInput($name, $params, $front, $lang = '')
    {

        global $Core;

        if (empty($lang)) {

            $lang = $Core->getLanguage(['str_id' => $Core->lang]);

        }

        $html = '';

        $params['block_wrap'] = isset($params['block_wrap']) ? explode("%block%", $params['block_wrap']) : array(
            0 => '',
            1 => '',
        );
        $params['name_wrap'] = isset($params['name_wrap']) ? explode("%name%", $params['name_wrap']) : array(
            0 => '',
            1 => '',
        );
        $params['input_wrap'] = isset($params['input_wrap']) ? explode("%input%", $params['input_wrap']) : array(
            0 => '',
            1 => '',
        );
        $params['type']          = isset($params['type']) ? $params['type'] : 'text';
        $params['required']      = isset($params['required']) ? $params['required'] : false;
        $params['default_value'] = isset($params['default_value']) ? $params['default_value'] : '';
        $params['attrs']         = isset($params['attrs']) ? $params['attrs'] : array();
        $params['value']         = isset($params['value']) ? $params['value'] : '';

        if (!empty($params['multilang']) && $params['multilang'] === true) {
            $tmp = explode("]", (isset($params['name']) ? $params['name'] : $name));
            $tmp[0] .= '_' . $lang['str_id'];
            $params['name'] = $name = implode("]", $tmp);
        } else {
            $params['name'] = $name = isset($params['name']) ? $params['name'] : $name;
        }

        if ($params['type'] == 'select') {
            $params['options']  = isset($params['options']) ? $params['options'] : array();
            $params['multiple'] = isset($params['multiple']) ? $params['multiple'] : false;
        } elseif ($params['type'] == 'multiselect_two_sides') {
            if ($front) {
                $Core->page->addHeadCss("/templates/" . $this->site_template . "/css/multiselect_two_sides.css");
            } else {
                $Core->page->addHeadCss("/admin/css/multiselect_two_sides.css");
            }
            $Core->page->addHeadJs("/js/multiselect_two_sides.min.js");
        } elseif ($params['type'] == 'counter') {
            if ($front) {
                $Core->page->addHeadCss("/templates/" . $this->site_template . "/css/bootstrap-spinner.css");
            } else {
                $Core->page->addHeadCss("/admin/css/bootstrap-spinner.css");
            }
            $Core->page->addHeadJs("/js/jquery.spinner.min.js");
        } elseif ($params['type'] == 'date') {
            if ($front) {
                $Core->page->addHeadCss("/templates/" . $this->site_template . "/css/datepicker.css");
            } else {
                $Core->page->addHeadCss("/admin/css/datepicker.css");
            }
            $Core->page->addHeadJs("/js/datepicker.js");
        } elseif ($params['type'] == 'images') {
            $Core->page->addHeadJs('/js/kcfinder.js');
        } elseif ($params['type'] == 'autocomplete_chosen') {
            $params['attrs']['class'] .= ' chosen-select';
            if ($front) {
                $Core->page->addHeadCss("/templates/" . $this->site_template . "/css/chosen.css");
            } else {
                $Core->page->addHeadCss("/admin/css/chosen.css");
            }
            $Core->page->addHeadJs("/js/chosen.jquery.js");
        }

        if ($front) {
            $html .= $this->renderTpl('/templates/' . $this->site_template . '/additional/' . $params['type'] . '.twig', array(
                'name'   => $name,
                'params' => $params,
                'errors' => $params['errors'],
            ));
        } else {
            $html .= $this->renderTpl('/admin/tpls/additional/' . $params['type'] . '.twig', array(
                'name'   => $name,
                'params' => $params,
                'errors' => $params['errors'],
            ));
        }
        if ($params['required']) {
            $html .= '<input type="hidden" name="required[]" value="' . $name . '" />';
        }

        return $html;

    }

    public function renderInputs($inputs, $front = true, $errors = [])
    {

        global $Core;

        $html = '';

        foreach ($inputs as $name => $params) {

            $value            = $params['value'];
            $params['errors'] = $errors;

            $tab_bookmarks = '';

            $tab_content = '';

            if (!empty($params['multilang']) && $front === false) {

                //tab bootstrap
                $tab_bookmarks = '<ul class="nav nav-tabs">';

                $tab_content = '<div class="tab-content">';

                foreach ($Core->getLangsAvailable() as $key => $lang) {

                    $tab_bookmarks .= '<li class="' . ($Core->lang == $lang['str_id'] ? 'active' : '') . '"><a data-toggle="tab" href="#' . md5($name . '-' . $lang['str_id']) . '">' . $lang['title'] . '</a></li>';

                    $tab_content .= '<div id="' . md5($name . '-' . $lang['str_id']) . '" class="tab-pane ' . ($Core->lang == $lang['str_id'] ? 'active' : ' fade') . '">';

                    if (is_array($value)) {
                        $params['value'] = $value[$lang['str_id']];
                    }

                    $tab_content .= $this->createInput($name, $params, $front, $lang);

                    $tab_content .= '</div>';

                }

                $tab_content .= '</div>';

                //tab bookmarks
                $tab_bookmarks .= '</ul>';

            } else {

                $tab_content .= $this->createInput($name, $params, $front, $lang);

            }

            $html .= $tab_bookmarks . $tab_content;

        }

        return $tab_bookmarks . $html;

    }

    public function renderAdminMenu($links)
    {
        global $Core;
        $menu = '<ul class="admin-sub-menu nav nav-tabs">';
        parse_str($Core->getQueryStr(), $url_vars);
        $url_vars['path']   = isset($url_vars['path']) ? $url_vars['path'] : '';
        $url_vars['action'] = !isset($url_vars['action']) || $url_vars['action'] == 'publish' || $url_vars['action'] == 'unpublish' ? 'view' : $url_vars['action'];
        foreach ($links as $link) {
            $url = '/admin?dir=' . $link['dir'] . '&component=' . $link['component'] . '&path=' . $link['path'] . '&action=' . $link['action'];
            if ($url_vars['dir'] == $link['dir'] && $url_vars['component'] == $link['component']) {
                if ($url_vars['path'] == $link['path']) {
                    if ($link['action'] == $url_vars['action']) {
                        $active = true;
                    } else {
                        $active = false;
                    }
                } else {
                    $active = false;
                }
            } else {
                $active = false;
            }

            $menu .= '<li role="presentation"  class="' . ($active ? 'active' : '') . '"><a class="' . (isset($link['class']) ? $link['class'] : '') . '" href="' . $url . '">' . (isset($link['img']) ? '<img class="admin-menu-img" src = "' . $link['img'] . '" title = "' . $link['title'] . '" />' : $link['title']) . '</a></li>';

        }

        return $menu . '</ul>';

    }

    private function getAdminHead($component = '')
    {

        global $Core;

        $lang = $Core->getLang('global');

        $data = array();

        $data['items']['applications']['title'] = $lang['APPLICATIONS'];
        $data['items']['applications']['href']  = '/admin?dir=applications';
        $data['items']['applications']['items'] = $Core->db->selectRows('applications', '*', array(
            'type' => 'app',
        ));

        $data['items']['modules']['title'] = $lang['MODULES'];
        $data['items']['modules']['href']  = '/admin?dir=modules';
        $data['items']['modules']['items'] = $Core->db->selectRows('applications', '*', array(
            'type' => 'mod',
        ));

        $data['items']['extensions']['title'] = $lang['EXTENSIONS'];
        $data['items']['extensions']['href']  = '/admin?dir=extensions';
        $data['items']['extensions']['items'] = $Core->db->selectRows('extensions', '*');

        $data['items']['settings']['title'] = $lang['SYS_SETTINGS'];
        $data['items']['settings']['href']  = '/admin?dir=settings';
        $data['items']['settings']['items'] = $Core->db->selectRows('settings', '*');

        $data['component'] = $component;

        $data['msgs'] = $Core->getSysMessages();

        return $Core->render->renderTpl('/admin/tpls/admin_head.tpl.php', $data);

    }

    public function getModal($data)
    {

        return $this->renderTpl("/admin/tpls/additional/modal.twig", $data);

    }

    public function renderAdminList($list, $path = '')
    {

        global $Core;

        $html = '<table class="table table-striped table-hover table-responsive">';

        $html .= '<tr>';

        $html .= '<th style="text-align:center">#</th>';

        foreach ($list['keys'] as $key) {

            $html .= '<th style="text-align:center;width:' . @$list['width'][$key] . 'px">' . $list['head'][$key] . '</th>';

        }

        $html .= '</tr>';

        if (isset($list['filter'])) {

            $html .= '<tr class="form-inline">';

            $html .= '<td style="text-align:center">-</td>';

            foreach ($list['keys'] as $k) {
                $operator = isset($list['filter'][$k]['operator']) ? $list['filter'][$k]['operator'] : '';
                $type     = $list['filter'][$k]['type'];
                if (is_array($list['filter'][$k]) && is_array($list['filter'][$k]['options']) && $type == 'select') {
                    $html .= '<td class="text-center">
                            <select style="width:' . $list['width'][$k] . 'px !important" class="for_filter form-control" name="filter[' . $k . '][' . $operator . ']">';
                    foreach ($list['filter'][$k]['options'] as $v => $opt) {
                        $html .= '<option value="' . $v . '" ' . ($list['filter'][$k]['value'] == $v ? 'selected="selected"' : '') . '>' . $opt . '</option>';
                    }
                    $html .= '</select>
                    </td>';
                } else {
                    $val = (is_array($list['filter'][$k]) ? $list['filter'][$k]['value'] : $list['filter'][$k]);
                    if ($k == 'tools') {
                        $html .= '<td class="text-center"><a class="glyphicon glyphicon-search" href="javascript:$(\'#filter\').submit()" title="' . $Core->langVars['SEARCH'] . '"></a></td>';
                    } elseif ($k == 'published' || $k == 'is_active') {
                        $html .= '<td class="text-center">
                            <select style="width:' . $list['width'][$k] . 'px !important" class="for_filter form-control" name="filter[' . $k . '][' . $operator . ']">
                                <option value="all" >' . $Core->langVars['ALL'] . '</option>
                                <option ' . ($val == 1 ? 'selected="selected"' : '') . ' value="pub">' . $Core->langVars['PUBLISHED'] . '</option>
                                <option ' . ($val === 0 ? 'selected="selected"' : '') . ' value="upub">' . $Core->langVars['NOT_PUBLISHED'] . '</option>
                            </select>
                        </td>';
                    } elseif ($type == 'date') {
                        $Core->page->addHeadCss("/admin/css/datepicker.css");
                        $Core->page->addHeadJs("/js/datepicker.js");
                        $html .= '<td class="text-center">
                                <input  class="for_filter form-control" type="text" id="date-picker-filter-' . $k . '" name="filter[date][' . $operator . ']" value="' . $val . '" />
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $(\'#date-picker-filter-' . $k . '\').datepicker({
                                            format: "yyyy-mm-dd"
                                        });
                                    });
                                </script>
                            </td>';
                    } elseif ($val == '-') {
                        $html .= '<td class="text-center"> - </td>';
                    } else {
                        $html .= '<td class="text-center"><input style="width:' . $list['width'][$k] . 'px !important" class="for_filter form-control" name="filter[' . $k . '][' . $operator . ']" value="' . $val . '"  /></td>';
                    }
                }

            }

            $html .= '</tr>';

            $html .= '<tr>';

            $html .= '<td style="display:none" colspan="' . (count($list['keys'])) . '" ><form id="filter" action="" method="POST" >';
            foreach ($list['keys'] as $k) {
                if ($k == 'tools') {
                    continue;
                }

                $val      = (is_array($list['filter'][$k]) ? $list['filter'][$k]['value'] : $list['filter'][$k]);
                $operator = isset($list['filter'][$k]['operator']) ? $list['filter'][$k]['operator'] : '';
                $html .= '<input name="filter[' . $k . '][' . $operator . ']" value="' . $val . '"  />';
            }
            $html .= '</form></td></tr>';
        }

        if (!isset($list['items']) || empty($list['items'])) {
            return $html . '<tr><td class="list-not-found" colspan="' . (count($list['head']) + 1) . '">' . $Core->langVars['ITEMS_NOT_FOUND'] . '</td></tr></table>';
        };

        foreach ($list['items'] as $k => $item) {
            $html .= '<tr><td style="text-align:center"><input ' . (@in_array($item['id'], $Core->getRequest('items_checked')) ? 'checked="checked"' : '') . ' ' . (@$item['disabled'] ? 'disabled="disabled"' : '') . ' type="checkbox" class="" name="items_checked[]" value="' . $item['id'] . '" /></td>';
            foreach ($list['keys'] as $key) {
                if ($key == 'published' || $key == 'is_active') {
                    if ($item[$key] == 1) {
                        $html .= '<td style="text-align:center"><a class="published glyphicon glyphicon-ok-circle" href="' . $path . '&action=unpublish&amp;id=' . $item['id'] . '"></a></td>';
                    } else {
                        $html .= '<td style="text-align:center"><a class="unpublished glyphicon glyphicon-remove-circle" href="' . $path . '&action=publish&amp;id=' . $item['id'] . '"></a></td>';
                    }

                } elseif ($key == 'tools') {
                    $html .= '<td style="text-align:center;min-width:215px">';
                    $html .= '<a class="edit glyphicon glyphicon-pencil" title="' . $Core->langVars['EDIT'] . '" href="' . $path . '&action=edit&amp;id=' . $item['id'] . '"></a>';
                    $html .= '<a class="delete glyphicon glyphicon-remove" title="' . $Core->langVars['DELETE'] . '" href="' . $path . '&action=delete&amp;id=' . $item['id'] . '"></a>';
                    $html .= '<a class="copy glyphicon glyphicon-duplicate" title="' . $Core->langVars['COPY'] . '" href="' . $path . '&action=add&amp;id=' . $item['id'] . '"></a>';
                    $html .= '</td>';
                } else {
                    $html .= '<td style="text-align:center">' . (!is_array($item[$key]) ? $item[$key] : strip_tags($item[$key][$Core->lang])) . '</td>';
                }
            }
            $html .= '</tr>';
        }

        $mass_actions = ['--- Select ---', 'delete' => $Core->langVars['DELETE'], 'publish' => $Core->langVars['PUBLISH'], 'unpublish' => $Core->langVars['UNPUBLISH']];

        $mass_actions = array_merge($mass_actions, !empty($list['mass_actions']) ? $list['mass_actions'] : []);

        $html .= '<tr><td colspan="' . (count($list['keys']) + 1) . '"> <form class="form-group form-inline" action="" method="post" id="massive_action_form">';
        $html .= '<div class="input-group"><span class="input-group-addon">' . $Core->langVars['MASSIVE_ACTION'] . '</span>';
        $html .= '<select class="form-control" name="massive_action" id="massive_action">';
        foreach ($mass_actions as $key => $action) {
            $html .= '<option value="' . $key . '">' . $action . '</option>';
        }
        $html .= '</select> </div>';
        $html .= '<div class="input-group"><input class="btn btn-primary" type="submit" name="submit" value="' . $Core->langVars['SUBMIT'] . '" /></div>';
        $html .= '</td></td></tr>';

        $html .= '</table>';

        return $html;

    }

    /*
    Повертає хтмл пагінації
    Замінює {page} в $url на номер сторінки і відповідно формує урл будь-якого типу
     */
    public function getPagination($items_count, $limit_on_page, $page, $url)
    {

        global $Core;

        if (empty($items_count) || $items_count <= $limit_on_page) {
            return '';
        }

        $pages_count = ceil($items_count / $limit_on_page);

        $links_html = '<nav><ul class="pagination">';

        if ($page - 3 > 0) {

            $links_html .= '<li><a class="previous" aria-label="Previous" href="' . str_replace("{page}", $page - 4 > 0 ? $page - 4 : 1, $url) . '"> <span aria-hidden="true"><b><<<<</b></span></a></li>';

        }
        if ($page > 1) {
            $links_html .= '<li><a class="previous" aria-label="Previous" href="' . str_replace("{page}", $page - 1 > 0 ? $page - 1 : 1, $url) . '"><span aria-hidden="true"><</span></a></li>';
        }
        for ($i = $page - 3 > 0 ? $page + 3 > $pages_count ? $pages_count - 6 : $page - 3 : 1; $i < $page && $i > 0; $i++) {

            $links_html .= '<li><a class="pagination-link" href="' . str_replace("{page}", $i, $url) . '">' . $i . '</a></li>';

        }

        $links_html .= '<li><a class="pagination-active" >' . $page . '</a></li>';

        for ($i = $page + 1; $i <= ($page <= 3 ? 7 : $page + 3) && $i <= $pages_count; $i++) {

            $links_html .= '<li><a class="pagination-link" href="' . str_replace("{page}", $i, $url) . '">' . $i . '</a></li>';

        }
        if ($page < $pages_count) {
            $links_html .= '<li><a aria-label="Next" class="next" href="' . str_replace("{page}", $page + 1, $url) . '"><span aria-hidden="true">></span></a></li>';
        }
        if ($page + 3 < $pages_count) {

            $links_html .= '<li><a aria-label="Next" class="next" href="' . str_replace("{page}", $page + 4, $url) . '"><span aria-hidden="true"><b>>>>></b></span></a></li>';

        }

        $links_html .= '</ul><div class="page-info">' . $Core->langVars['ON_PAGE'] . ': ' . ($page * $limit_on_page - $limit_on_page + 1) . ' - ' . ($page * $limit_on_page) . ' ' . $Core->langVars['FROM'] . ' ' . $items_count . '</div></nav>';

        return $links_html;

    }

    public function txtToASCII($text)
    {

        return htmlentities($text);

    }
    //$src = /images/img_for_resize; $path = /images/path_for_save; $width = (int)px; $height=(int)px; $scale=(int)%;
    public function resizeImg($src, $path, $width = 'auto', $height = 'auto', $scale = 50)
    {

        global $Core;

        $src = ROOT_DIR . $src;

        $path = ROOT_DIR . $path;

        $image = $Core->loadClass('classes\\SimpleImage', 'SimpleImage.class.php');

        $image->load($src);

        if ($width > 0 and $height == 'auto') {

            $image->resizeToWidth($width);

        } elseif ($width == 'auto' and $height > 0) {

            $image->resizeToHeight($height);

        } elseif ($width > 0 and $height > 0) {

            $image->resize($width, $height);

        } elseif ($width == 'auto' and $height == 'auto' and $scale > 0) {

            $image->scale($scale);

        }

        if (!file_exists($path)) {

            $new_file = fopen($path, 'w');

            fclose($new_file);

        }

        $image->save($path);

    }

}
