<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - __destruct()
 * - escapeStr()
 * - prepareSelect()
 * - prepareDelete()
 * - prepareInsert()
 * - prepareUpdate()
 * - prepareParams()
 * - getDbPrefix()
 * - setTableName()
 * - getSysTables()
 * - isTableExist()
 * - setSelectFields()
 * - isFieldExist()
 * - setInsertFields()
 * - setUpdateFields()
 * - getFieldNames()
 * - refreshColumnsList()
 * - setWhereStatment()
 * - setLImit()
 * - setOrderBy()
 * - setGroupBy()
 * - selectRows()
 * - selectFields()
 * - selectField()
 * - setJoin()
 * - select()
 * - delete()
 * - insert()
 * - update()
 * - placeArray()
 * - query()
 * - placeParams()
 * - countRows()
 * - selectMax()
 * - publish()
 * - unpublish()
 * Classes list:
 * - Db
 */
namespace core;

use classes\Logger;

class Db
{

    public function __construct()
    {

        global $_CONFIG;

        Logger::log("access to DB for " . $_SERVER['REQUEST_URI']);

        $this->pdo = new \PDO("mysql:host={$_CONFIG['DB_HOST']};dbname={$_CONFIG['DB_DATABASE']};charset=" . $_CONFIG['DB_ENC'], $_CONFIG['DB_USER'], $_CONFIG['DB_PASS']);

        $this->pdo->query("SET NAMES 'utf8'");

        $this->fields = "";

        $this->table = "";

        $this->join = "";

        $this->joinFields = "";

        $this->where = "";

        $this->limit = "";

        $this->prefix = $this->getDbPrefix();

        $this->sys_tables = $this->getSysTables();

    }

    public function __destruct()
    {

        $this->pdo = null;

    }

    public function escapeStr($str)
    {

        return $this->pdo->quote($str);

    }

    private function prepareSelect()
    {

        return $this->pdo->prepare("
                SELECT $this->fields FROM $this->table  as main
                $this->join
                $this->where
                $this->orderBy
                $this->groupBy
                $this->limit
                ");

    }

    private function prepareDelete()
    {

        return $this->pdo->prepare("
                DELETE FROM $this->table
                $this->where
                ");

    }

    private function prepareInsert()
    {

        $field_names = $this->getFieldNames();

        $first_field = $field_names[0];

        return $this->pdo->prepare("
                INSERT INTO $this->table
                ($this->insertFields) VALUES ($this->insertValues)
                ON DUPLICATE KEY UPDATE `$first_field` = $first_field
                ");

    }

    private function prepareUpdate()
    {

        return $this->pdo->prepare("
                UPDATE $this->table
                SET $this->updateFields
                $this->where
                ");

    }

    private function prepareParams($params = array())
    {

        if (count($params) > 0 && is_array($params)) {

            foreach ($params as $key => $param) {

                $params[':' . $key] = is_array($param) ? "IN('" . implode(",", $param) . "')" : $param;

                unset($params[$key]);

            }

        }

        return $params;

    }

    public function getDbPrefix()
    {

        global $_CONFIG;

        return $_CONFIG['DB_PREFIX'];
    }

    private function setTableName($table)
    {

        if (!$this->isTableExist($table)) {
            return false;
        }

        $this->table = $this->prefix . $table;

    }

    private function getSysTables()
    {

        global $_CONFIG;

        $sql = "SELECT `TABLE_NAME`, `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA` = '{$_CONFIG['DB_DATABASE']}' ";

        $res = $this->query($sql);

        $tables = array();

        foreach ($res as $key => $value) {

            $tables[$value['TABLE_NAME']][] = $value['COLUMN_NAME'];

        }

        return $tables;

    }

    private function isTableExist($table = '')
    {

        if (!preg_match("/^" . $this->prefix . "/", $table)) {

            $table = $this->prefix . $table;

        }

        return array_key_exists($table, $this->sys_tables);

    }

    private function setSelectFields($fields)
    {

        if (!is_array($fields)) {
            $tmp = explode(",", $fields);
        } else {
            $tmp = $fields;
        }

        foreach ($tmp as $key => $value) {
            if (!$this->isFieldExist($value)) {
                $tmp[$key] = trim($value);
            }

        }

        $fields = "main." . implode(", main.", $tmp);

        $this->fields = $fields . $this->joinFields;

    }

    private function isFieldExist($field, $table = '')
    {

        if (empty($table)) {

            foreach ($this->sys_tables as $table => $fields) {

                if (in_array($field, $fields)) {

                    return true;

                }

            }

            return false;

        }

        return in_array($field, $this->sys_tables[$table]);

    }

    private function setInsertFields($fields_array)
    {

        $this->insertFields = '';

        $this->insertValues = '';

        $fields_in_table = $this->getFieldNames();

        foreach ($fields_in_table as $key => $field) {

            if (isset($fields_array[$field])) {

                $this->insertFields .= "`{$field}`,";

                $this->insertValues .= ":{$field},";

                $fields[$field] = $fields_array[$field];
            }

        }

        $this->insertFields = trim($this->insertFields, ", ");

        $this->insertValues = trim($this->insertValues, ", ");

        return $fields;

    }

    public function setUpdateFields($fields_array)
    {

        $this->updateFields = '';

        $fields_in_table = $this->getFieldNames();

        foreach ($fields_in_table as $key => $field) {

            if (isset($fields_array[$field])) {

                $this->updateFields .= "`{$field}` = :{$field}, ";

                $fields[$field] = $fields_array[$field];
            }

        }

        $this->updateFields = trim($this->updateFields, ", ");

        return isset($fields) ? $fields : array();

    }

    protected function getFieldNames($table = '')
    {

        $table = empty($table) ? $this->table : $this->prefix . $table;

        if (isset($this->sys_tables[$table])) {
            return $this->sys_tables[$table];
        }

        $result = $this->query("SHOW COLUMNS FROM `{$table}` ");

        foreach ($result as $col) {
            $this->sys_tables[$table][] = $col['Field'];
        }

        return $this->sys_tables[$table];

    }

    protected function refreshColumnsList($table)
    {

        $table = empty($table) ? $this->table : $this->prefix . $table;

        $result = $this->query("SHOW COLUMNS FROM `{$table}` ");

        $this->sys_tables[$table] = [];

        foreach ($result as $col) {

            $this->sys_tables[$table][] = $col['Field'];

        }

        return $this->sys_tables[$table];
    }

    private function setWhereStatment($params = null, $pre = '')
    {

        if (!isset($params) || empty($params) || !is_array($params)) {
            $this->where = '';
            return;
        };

        $where = "WHERE 1 AND ";

        $columns = array();

        foreach ($this->getFieldNames() as $key => $value) {
            $columns[] = $value;
        }

        foreach ($params as $key => $param) {

            if (!in_array($key, $columns)) {
                continue;
            }

            if (isset($param['operator'])) {

                if (is_array($param['value']) && count($param['value']) > 0) {

                    $where .= (!empty($pre) ? $pre . '.' : '') . "$key {$param['operator']} (" . $this->placeArray($param['value']) . ") AND ";

                } elseif (!empty($param['value'])) {

                    $where .= (!empty($pre) ? $pre . '.' : '') . "$key {$param['operator']} " . $this->escapeStr($param['value']) . " AND ";

                }
                continue;

            }

            if (is_array($param) && !empty($param)) {

                $where .= (!empty($pre) ? $pre . '.' : '') . "$key IN(" . $this->placeArray($param) . ") AND ";

            } elseif (in_array($key, $columns) && (!empty($param) || $param === 0)) {

                $where .= (!empty($pre) ? $pre . '.' : '') . "$key = " . $this->escapeStr($param) . " AND ";

            }

        }

        $where = trim($where, "AND ");

        $this->where = $where;

    }

    private function setLImit($limit)
    {

        $this->limit = $limit > 0 || strlen($limit) > 0 ? " LIMIT " . $limit : '';

    }

    private function setOrderBy($orderBy = '')
    {

        $this->orderBy = $orderBy != '' ? 'ORDER BY ' . $orderBy : '';

    }

    private function setGroupBy($groupBy = '')
    {

        $this->groupBy = !empty($groupBy) ? 'GROUP BY ' . $groupBy : '';

    }

    public function selectRows($table, $fields = "*", $params = array(), $limit = false, $orderBy = '', $groupBy = '')
    {

        return $this->select($table, $fields, $params, $limit, $orderBy, $groupBy);

    }

    public function selectFields($table, $fields = '*', $params = array(), $limit = 1, $orderBy = '', $groupBy = '')
    {

        $result = $this->select($table, $fields, $params, $limit, $orderBy, $groupBy);

        if (empty($result)) {
            return false;
        }

        return $limit > 1 ? $result : $result[0];

    }

    public function selectField($table, $field, $params = array())
    {

        $result = $this->select($table, $field, $params);

        if (count($result) > 1) {

            $res = array();

            foreach ($result as $key => $value) {

                $res[] = $value[$field];

            }

            return $res;

        } else {

            return isset($result[0][$field]) ? $result[0][$field] : false;

        }

    }

    public function setJoin($join = array(), $type = "LEFT")
    {

        $this->join       = "";
        $this->joinFields = "";
        foreach ($join as $key => $value) {

            $f = explode(",", $value[1]);

            foreach ($f as $k => $v) {
                $f[$k] = trim($v);
                $this->joinFields .= ", t{$key}.{$v}";
            }

            $on = " ";

            foreach ($value[2] as $k => $v) {
                if (isset($v['operator']) && isset($v['value']) && (!empty($v['value']) || $v['value'] == 0)) {
                    if (!is_array($v['value'])) {
                        $on .= "t" . $key . "." . $k . " " . $v['operator'] . " " . $v['value'] . " AND ";
                    } else {
                        $on .= "t" . $key . "." . $k . " " . $v['operator'] . "('" . implode("','", $v['value']) . ") AND ";
                    }
                } else {
                    if (!is_array($v)) {
                        $on .= "t" . $key . "." . $k . " = " . $v . " AND ";
                    } else {
                        $on .= "t" . $key . "." . $k . " IN('" . implode("','", $v) . ") AND ";
                    }
                }
            }

            $on = trim($on, "AND ");

            $this->join .= " {$type} JOIN $this->prefix{$value[0]} as t$key ON $on";

        }

        return;

    }

    public function select($table, $fields, $params = array(), $limit = false, $orderBy = '', $groupBy = '')
    {

        $this->setTableName($table);

        $this->setSelectFields($fields);

        $this->setLImit($limit);

        $this->setOrderBy($orderBy);

        $this->setGroupBy($groupBy);

        $this->setWhereStatment($params, 'main');

        $query_obj = $this->prepareSelect();
        //$params = $this->prepareParams($params);

        $query_obj->execute();

        $this->fields = "";

        $this->table = "";

        $this->join = "";

        $this->joinFields = "";

        $this->where = "";

        $this->limit = "";

        return is_object($query_obj) ? $query_obj->fetchAll() : false;

    }

    public function delete($table, $params = array())
    {

        $this->setTableName($table);

        $this->setWhereStatment($params);

        $query_obj = $this->prepareDelete();

        $params = $this->prepareParams($params);

        $query_obj->execute($params);

        return is_object($query_obj);

    }

    public function insert($table, $fields = array())
    {

        $this->setTableName($table);

        $fields = $this->setInsertFields($fields);

        $query_obj = $this->prepareInsert();

        $fields = $this->prepareParams($fields);

        $query_obj->execute($fields);

        return is_object($query_obj) ? $this->pdo->lastInsertId() : false;

    }

    public function update($table, $fields, $params)
    {

        $this->setTableName($table);

        $this->setWhereStatment($params);

        $fields = $this->setUpdateFields($fields);

        $query_obj = $this->prepareUpdate();

        $fields = $this->prepareParams($fields);

        $query_obj->execute($fields);

        return is_object($query_obj);

    }

    public function placeArray($arr)
    {

        array_map(array(
            $this,
            'escapeStr',
        ), $arr);

        return "'" . implode($arr, "','") . "'";

    }

    public function query($sql, $params = array())
    {

        $query_obj = $this->pdo->prepare($sql);

        $query_obj->execute($params);

        return is_object($query_obj) ? $query_obj->fetchAll() : false;

    }

    public function placeParams($sql, $params)
    {
        foreach ($params as $key => $param) {
            $sql = str_replace(":" . $key, "{$this->escapeStr($param)}", $sql);
        }
        return $sql;
    }

    public function countRows($table, $where = array())
    {

        $this->setTableName($table);

        $this->setWhereStatment($where);

        $sql = "SELECT COUNT(*) as count FROM $this->table  $this->where";

        $result = $this->query($sql);

        return $result ? $result[0]['count'] : 0;

    }

    public function selectMax($table, $col)
    {

        $this->setTableName($table);

        $sql = "SELECT MAX(`$col`) as max_value FROM $this->table";

        $result = $this->query($sql);

        return $result[0]['max_value'];

    }

    public function publish($table, $row_id)
    {

        return $this->update($table, array(
            'published' => 1,
        ), array(
            'id' => $row_id,
        ));

    }

    public function unpublish($table, $row_id)
    {

        return $this->update($table, array(
            'published' => 0,
        ), array(
            'id' => $row_id,
        ));

    }

}
