<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - getValues()
 * - getTranslations()
 * - saveTranslation()
 * - generateTranslations()
 * - deleteTranslations()
 * - getMultilangValue()
 * - getValuesForCurrentLang()
 * Classes list:
 * - Multilang extends Db
 */
namespace core;

use core\Db;

class Multilang extends Db
{

    public function __construct()
    {
        global $Core;

        $raw_translations = $Core->db->selectRows('translations', '*');

        foreach ($raw_translations as $key => $transaltion) {

            $this->translations[$transaltion['table_name']][$transaltion['row_id']][$transaltion['field_name']][$transaltion['lang_str_id']] = $transaltion['value'];

        }

        parent::__construct();

    }

    public function getValues($field, $values)
    {

        global $Core;

        $translation = [];

        if (is_array($values[$field])) {
            return $values[$field];
        }elseif (!empty($values[$field])) {
            $value = $values[$field];
            foreach ($Core->getLangsAvailable() as $k => $lang) {
                $translation[$lang['str_id']] = $value;
            }
            return $translation;
        }

        foreach ($Core->getLangsAvailable() as $lang) {

            $translation[$lang['str_id']] = $values[$field . '_' . $lang['str_id']];

        }

        $translation = empty($translation) ? $values[$field] : $translation;

        return $translation;
    }

    public function getTranslations($res, $table)
    {

        if (empty($table) || !is_array($res)) {
            return $res;
        }

        foreach ($res as $key => $row) {

            foreach ($row as $field => $value) {

                if (!empty($this->translations[$table][$row['id']][$field])) {

                    $res[$key][$field] = $this->translations[$table][$row['id']][$field];

                } /*else{

            $translations = $this->selectRows('translations', '*', [
            'table_name' => $table,
            'row_id' => $row['id'],
            'field_name' => $field,
            ]);

            if (count($translations) > 0){

            foreach ($translations as $k => $translation){

            $translations[$translation['lang_str_id']] = $translation['value'];

            unset($translations[$k]);

            }

            $res[$key][$field] = $translations;

            }else{

            continue;

            }

            $this->translations[$table][$row['id']][$field] = $translations;

            }*/

            }

        }

        return $res;

    }

    public function saveTranslation($table, $field, $row_id, $lang_str_id, $lang_id, $value)
    {
        $row = ['table_name' => $table, 'field_name' => $field, 'row_id' => $row_id, 'lang_str_id' => $lang_str_id, 'lang_id' => $lang_id, 'value' => $value];

        $where = ['table_name' => $table, 'field_name' => $field, 'row_id' => $row_id, 'lang_id' => $lang_id];

        if ($this->countRows('translations', $where) > 0) {

            parent::update('translations', $row, $where);

        } else {

            parent::insert('translations', $row);

        }

    }

    public function generateTranslations($table, &$fields, $params)
    {
        global $Core;

        $item_id = $this->selectField($table, 'id', $params);

        if (empty($item_id)) {
            return false;
        }

        foreach ($fields as $key => $value) {

            foreach ($this->getFieldNames($table) as $field) {

                $langs_str = '';

                $languages = $this->selectRows('languages');

                foreach ($languages as $k => $lang) {

                    $langs_str .= (empty($k) ? '' : '|') . $lang['str_id'];

                    unset($languages[$k]);

                    $languages[$lang['str_id']] = $lang;

                }

                if (preg_match("/^{$field}_({$langs_str})$/", $key)) {

                    $tmp = explode("_", $key);

                    $lang_str_id = array_pop($tmp);

                    $field_name = implode("_",$tmp);

                    $fields[$field_name] = $fields[$field_name . '_' . $Core->lang];

                    $lang_id = $languages[$lang_str_id]['id'];

                    $this->saveTranslation($table, $field_name, $item_id, $lang_str_id, $lang_id, $value);

                    $this->translations[$table][$item_id][$field_name][$lang_str_id] = $value;

                }

            }

        }

    }

    public function getNativeFieldsValues($table, $fields)
    {
        global $Core;

        foreach ($fields as $key => $value) {

            foreach ($this->getFieldNames($table) as $field) {

                $langs_str = '';

                $languages = $this->selectRows('languages');

                foreach ($languages as $k => $lang) {

                    $langs_str .= (empty($k) ? '' : '|') . $lang['str_id'];

                    unset($languages[$k]);

                    $languages[$lang['str_id']] = $lang;

                }

                if (preg_match("/^{$field}_({$langs_str})$/", $key)) {

                    $tmp = explode("_", $key);

                    $lang_str_id = array_pop($tmp);

                    $field_name = implode("_",$tmp);

                    $fields[$field_name] = $fields[$field_name . '_' . $Core->lang];

                }
            }
        }
        return $fields;
    }

    public function deleteTranslations($table, $ids)
    {

        return parent::delete('translations', ['table_name' => $table, 'row_id' => $ids]);

    }

    public static function getMultilangValue($field, $values)
    {

        global $Core;

        $langs = $Core->getLangsAvailable();

        $multilangValues = [];

        foreach ($langs as $lang) {

            $multilangValues[$lang['str_id']] = !empty($values[$field . '_' . $lang['str_id']]) ? $values[$field . '_' . $lang['str_id']] : $values[$field];

        }

        return $multilangValues;

    }

    public function getValuesForCurrentLang(&$items = [])
    {

        global $Core;

        foreach ($items as $key => $item) {

            foreach ($item as $field => $value) {

                if (is_array($value) && isset($value[$Core->lang])) {
                    $items[$key][$field] = $value[$Core->lang];
                }

            }
        }
    }

}
