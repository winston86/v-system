<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>404 NOT FOUND</title>
	<meta name="keywords" content="<?php echo $this->site_keywords; ?>">
	<meta name="description" content="<?php echo $this->site_description; ?>">
	<meta name="generator" content="victor-system">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="/templates/<?php echo $this->site_template ; ?>/css/reset.css" rel="stylesheet" type="text/css">
	<link href="/templates/<?php echo $this->site_template ; ?>/css/text.css" rel="stylesheet" type="text/css">
	<link href="/templates/<?php echo $this->site_template ; ?>/css/styles.css" rel="stylesheet" type="text/css">
</head>

<body>

<div class="error-<?php echo $code; ?>"><?php echo $this->getErrorMsg($code); ?></div>

<script type="text/javascript">
	setTimeout("history.go(-1)", 2000);
</script>
</body>
</html>