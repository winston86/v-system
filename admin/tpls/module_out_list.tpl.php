<h1><?php echo $data['LANG']['APP_TITLE']; ?></h1>
<table class="table table-striped table-hover table-responsive">
<tr class="admin-component">
    <td class="bordered">
		<span class="mod-out-N">
			#
		</span>
    </td>
    <td class="bordered">
		<span class="mod-out-id">
			<?php echo $data['LANG']['ID_SHORT']; ?>
		</span>
    </td>
	<td class="bordered">
		<span class="mod-out-title">
			<?php echo $data['LANG']['TITLE']; ?>
		</span>
	</td>
	<td class="bordered">
		<span class="mod-out-route">
			<?php echo $data['LANG']['PAGE_ROUTE']; ?>
		</span>
	</td>
	<td class="bordered">
		<span class="mod-out-position">
			<?php echo $data['LANG']['POSITION']; ?>
		</span>
	</td>
	<td style="text-align: center" class="bordered">
		<span class="mod-out-pub">
			<?php echo $data['LANG']['PUBLISHED']; ?>
		</span>
	</td>
	<td style="text-align: center" class="bordered">
		<span class="mod-out-prio">
			<?php echo $data['LANG']['PRIORITY']; ?>
		</span>
	</td>
	<td style="text-align: center" class="bordered">
		<span class="mod-out-del">
			<?php echo $data['LANG']['ACTIONS']; ?>
		</span>
	</td>
	
</tr>
	<tr class="form-inline">
        <td class="text-center">
            <div class="">
                -
            </div>
        </td>
		<td class="text-center">
			<div class="">
				<input name="filter[id][]" type="text" class="for_filter form-control mod-filter-id" value="<?php echo $filter['id']; ?>">
			</div>
		</td>
		<td class="text-center">
            <div class="">
                <input name="filter[title][]" type="text" class="for_filter form-control mod-filter-title" value="<?php echo $filter['title']; ?>">
            </div>
        </td>
		<td class="text-center">
            <div class="">
                <input name="filter[page_route][]" type="text" class="for_filter form-control mod-filter-route" value="<?php echo $filter['page_route']; ?>">
            </div>
        </td>
		<td class="text-center">
            <div class="">
                <select name="filter[position_id][]" id="" class="for_filter form-control mod-filter-pos">
                    <option value=""> ---- </option>
                    <?php foreach ($positions as $pos): ?>
                        <option value="<?php echo $pos['id']; ?>" <?php echo $pos['id'] == $filter['position_id']?'selected="selected"':''; ?>><?php echo $pos['title']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </td>
		<td class="text-center">
            <div class="">
                <select name="filter[published][]" id="" class="for_filter form-control mod-filter-pub">
                    <option value="all"><?php echo $LANG['ALL']; ?></option>
                    <option value="pub"<?php echo $filter['published'] == '1'?'selected="selected"':''; ?>><?php echo $LANG['PUBLISHED']; ?></option>
                    <option value="upub"<?php echo $filter['published'] == '0'?'selected="selected"':''; ?>><?php echo $LANG['MOD_NOT_PUBLISHED']; ?></option>
                </select>
            </div>
        </td>
		<td class="text-center">
            <div class="">
                <input name="filter[priority][]" type="text" class="for_filter form-control mod-filter-prio" value="<?php echo $filter['priority']; ?>">
            </div>
        </td>
		<td class="text-center">
            <a class="glyphicon glyphicon-search" href="javascript:$('#filter').submit()" title="Шукати"></a>
        </td>
	</tr>
    <tr style="display: none">
        <td>
            <form id="filter" action="" method="POST">
                <input name="filter[id][]" value="<?php echo $filter['id']; ?>">
                <input name="filter[title][]" value="<?php echo $filter['title']; ?>">
                <input name="filter[page_route][]" value="<?php echo $filter['page_route']; ?>">
                <input name="filter[position_id][]" value="<?php echo $filter['position_id']; ?>">
                <input name="filter[published][]" value="<?php echo $filter['published']; ?>">
                <input name="filter[priority][]" value="<?php echo $filter['priority']; ?>">
            </form>
        </td>
    </tr>
<?php foreach($data['out-list'] as $out){ ?>
<tr>
    <td>
        <div class="">
            <input type="checkbox" class="" name="items_checked[]" value="<?php echo $out['id']; ?>">
        </div>
    </td>
    <td>
		<span class="mod-out-id badge">
			<?php echo $out['id']; ?>
		</span>
    </td>
	<td>
		<span class="mod-out-title">
			<a href="/admin/?dir=modules&component=<?php echo $out['component']; ?>&path=out-edit&action=edit&id=<?php echo $out['id']; ?>">
				<?php echo $out['title']; ?>
			</a>
		</span>
	</td>
	<td>
		<span class="mod-out-route">
			<?php echo $out['page_route']; ?>
		</span>
	</td>
	<td>
		<span class="mod-out-pos badge">
			<?php echo $out['position']; ?>
		</span>
	</td>
	<td style="text-align: center">
		<span class="mod-out-pub">
			<?php echo $out['published']
			?
			'<a class="published glyphicon glyphicon-ok-circle" href="/admin/?dir=modules&component='.$data['component'].'&path=out-list&action=unpublish&id='.$out['id'].'"></a>'
			:
			'<a class="unpublished glyphicon glyphicon-remove-circle" href="/admin/?dir=modules&component='.$data['component'].'&path=out-list&action=publish&id='.$out['id'].'"></a>'
			?>
		</span>
	</td>
	<td style="text-align: center">
		<span class="mod-out-prio">
			<span class="cur-priority badge"><?php echo $out['priority']; ?></span>
			<a class="plus glyphicon glyphicon-chevron-up"  href="/admin/?dir=modules&component=<?php echo $data['component']; ?>&path=out-list&action=up-priority&id=<?php echo $out['id']; ?>"></a>
			<a class="minus glyphicon glyphicon-chevron-down"  href="/admin/?dir=modules&component=<?php echo $data['component']; ?>&path=out-list&action=down-priority&id=<?php echo $out['id']; ?>"></a>
		</span>
	</td>
	<td style="text-align: center">
		<span class="mod-out-del">
			<a class="del-link delete glyphicon glyphicon-remove"  href="/admin/?dir=modules&component=<?php echo $data['component']; ?>&path=out-list&action=delete&id=<?php echo $out['id']; ?>"></a>
		</span>
	</td>
</tr>
<?php } ?>
    <tr>
        <td colspan="8">
            <form class="form-group form-inline" action="" method="post" id="massive_action_form">
                <div class="input-group">
                    <span class="input-group-addon"><?php echo $LANG['MASSIVE_ACTION']; ?></span>
                    <select
                        class="form-control" name="massive_action" id="massive_action">
                        <option value="0">--- Select ---</option>
                        <option value="delete"><?php echo $LANG['DELETE']; ?></option>
                        <option value="publish"><?php echo $LANG['PUBLISH']; ?></option>
                        <option value="unpublish"><?php echo $LANG['UNPUBLISH']; ?></option>
                    </select>
                </div>
                <div class="input-group">
                    <input class="btn btn-primary" type="submit" name="submit" value="<?php echo $LANG['SEND']; ?>">
                </div>
            </form>
        </td>
    </tr>
	<tr>
		<td colspan="8">
			<?php echo $pagination; ?>
		</td>
	</tr>
</table>
