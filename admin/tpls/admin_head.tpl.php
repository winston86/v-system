<div class="well">
<h1><?php echo $this->site_name; ?></h1>
<ul class="admin-menu list-inline list-group">
	<?php foreach($items as $item){ ?>

		<li class="admin-menu-item list-group-item">
			<span class="badge"><?php echo count($item['items']); ?></span>
			<a href="<?php echo $item['href']; ?>"><?php echo $item['title']; ?></a>
			<?php if($item['items']){ ?>
				<ul class="list-group">
					<?php foreach($item['items'] as $sub_item){ ?>
						
						<li class="sub_item list-group-item <?php echo $component == $sub_item['route'] ? 'active' : '';  ?>">
							<a href="<?php echo $item['href']; ?>&component=<?php echo $sub_item['route']; ?>">
								<?php echo  $sub_item['title']; ?>
							</a>
						</li>
			
					<?php } ?>
				</ul>
			<?php } ?>
		</li>

	<?php } ?>
</ul>
<?php if(!empty($msgs)){ ?>
<div class="sys-messages">
<?php echo implode(" ", $msgs); ?>
</div>
<?php	} ?>
</div>