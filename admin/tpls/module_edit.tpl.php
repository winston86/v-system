<h1><?php echo $data['LANG']['APP_TITLE']; ?></h1>
<form action="" method="POST">
    <div class="row well input-group">
        <label class="pre-text input-group-addon"><?php echo $data['LANG']['TITLE'] ?>:</label>
        <input placeholder="<?php echo $data['LANG']['TITLE'] ?>" class="mod-edit-input form-control" type="text" name="out[title]" value="<?php echo $data['title']; ?>"/>
    </div>
    <div class="row well input-group">
        <label class="pre-text input-group-addon"><?php echo $data['LANG']['ATTACHE_TO_URL'] ?>:</label>
        <input placeholder="<?php echo $data['LANG']['ATTACHE_TO_URL'] ?>" class="mod-edit-input form-control" type="text" name="out[attach]" value="<?php echo $data['attach']; ?>"/>
    </div>
    <div class="row well input-group">
        <label class="pre-text input-group-addon"><?php echo $data['LANG']['POSITION'] ?>: </label>
        <select class="mod-edit-select form-control" name="out[position_id]">
            <?php foreach ($data['positions'] as $position) { ?>
                <option <?php echo $position['id'] == $data['position_id'] ? ' selected="selected"' : ''; ?>
                    value="<?php echo $position['id']; ?>"><?php echo $position['title']; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="row well">
        <input type="checkbox" name="out[sub_pages]" <?php echo $data['sub_pages'] == 1 ? 'checked="checked"' : ''; ?> >
        <label class="pre-text"><?php echo $data['LANG']['SUB_PAGES']; ?></label>
    </div>
    <?php echo $data['settings']; ?>
    <div class="row well">
        <input type="checkbox" value="1" name="out[published]" <?php echo $data['published'] == 1 ? 'checked="checked"' : ''; ?> />
        <label class="pre-text"><?php echo $data['LANG']['PUBLISHED']; ?></label>
    </div>
    <input class="btn btn-primary" type="submit" name="submit" value="<?php echo $data['LANG']['SEND']; ?>"/>
    <input
        class="btn btn-default" type="button"
        onclick="window.location = '<?php echo $data['shortlink']; ?>'"
        name="back"
        value="<?php echo $data['LANG']['BACK']; ?>">
</form>
