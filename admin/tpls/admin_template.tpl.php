<!DOCTYPE HTML  "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/html11/DTD/xhtml">
<head>
	<title><?php echo $this->site_name; ?></title>
	<meta name="keywords" content="<?php echo $this->site_keywords; ?>"/>
	<meta name="description" content="<?php echo $this->site_description; ?>"/>
	<meta name="generator" content="victor-system"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<script src="/vendor/bower-asset/jquery/dist/jquery.min.js" type="text/javascript"></script>
	<script src="/vendor/twbs/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="/admin/js/ai.js" type="text/javascript"></script>
	<script src="/js/ui.js" type="text/javascript"></script>
	<link href="/vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="/admin/css/styles.css" rel="stylesheet" type="text/css"/>
	{*additional_links*}
</head>

<body>

<div id="wrapper">
<div class="admin-head-menu">
	<?php
	ini_set('display_errors',1);
	echo $this->getAdminHead($component); //!!!!!head!!!!! ?>
</div>
<div class="admin-main-content">
	<?php   if($this->getSysMessagesCount() > 0){ ?>
			<div class="sys-info">
				<?php
					foreach($this->getSysMessages()as $msg){
						echo $msg;
					}
				?>
			</div>
	<?php } ?>
	
	<?php echo $content; ?>
</div>
</div>
<div id="modal-window" class="modal fade" role="dialog">
</div>
</body>
</html>