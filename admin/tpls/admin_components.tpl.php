<table>
<?php foreach($data['items'] as $item){ ?>

<tr class="admin-component">
	<td class="bordered">
		<span class="admin-component-title">
			<a href="/admin?dir=<?php echo $data['dir']; ?>&component=<?php echo  $item['route']; ?>">
				<?php echo $item['title']; ?>
			</a>
		</span>
	</td>
	<td class="bordered">
		<span class="admin-component-desc">
			<?php echo $item['description']; ?>
		</span>
	</td>
	<td class="bordered">
		<span class="admin-components-pub">
			<?php echo $item['published']
			?
			'<a class="published" href="/admin/?dir='.$data['dir'].'&action=unpublish&id='.$item['id'].'">V</a>' /*$data['LANG']['PUBLISHED']*/ 
			:
			'<a class="unpublished" href="/admin/?dir='.$data['dir'].'&action=publish&id='.$item['id'].'">X</a>'  /*$data['LANG']['NOT_PUBLISHED']*/  
			?>				
		</span>
	</td>
</tr>


<?php } ?>
</table>