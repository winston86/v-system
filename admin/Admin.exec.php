<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - execute()
 * - renderPage()
 * - adminHome()
 * - listComponents()
 * - renderAdmin()
 * - editModuleOut()
 * - renderModuleList()
 * - upPriority()
 * - downPriority()
 * - publish()
 * - unpublish()
 * - delete()
 * Classes list:
 * - Admin
 */
namespace admin;

use classes\Logger;

class Admin
{

    public function __construct()
    {

        global $Core;

        if (!$Core->users->isAdmin()) {
            $Core->render->renderErrorInfo(403);
        }

        $this->dir = $Core->getRequest('dir');

        $this->action = $Core->getRequest('action');

        $this->component = $Core->getRequest('component');

        $this->lang = $Core->getLang('global');

        $this->execute();

    }

    private function execute()
    {

        Logger::log('Initialization of admin part... ');

        if (!$this->dir) {

            $this->adminHome();

        }

        if (!$this->component) {

            $this->listComponents();

        }

        include_once ROOT_DIR . "/admin/" . $this->dir . "/" . $this->component . "/backend.php";

        $parts = explode("_", $this->component);

        array_map('ucfirst', $parts);

        $com_class_name = implode("", $parts) . "Backend";

        $com = new $com_class_name;

        $this->renderPage($com);

        exit;

    }

    private function renderPage($com)
    {

        global $Core;

        $Core->proccessMassiveActions($com);

        $content = $com->execute();

        $this->renderAdmin($content);

    }

    private function adminHome()
    {

        $content = '<h1>This Is Admin Home</h1>';

        $this->renderAdmin($content);

        exit;

    }

    private function listComponents()
    {

        global $Core;

        $table = $this->dir;

        if ($this->dir == 'applications') {

            $where = array(
                'type' => 'app',
            );

        } elseif ($this->dir == 'modules') {

            $where = array(
                'type' => 'mod',
            );

            $table = 'applications';

        } else {

            $where = array();

        }

        if ($this->action == 'publish') {

            $this->publish($table);

        }

        if ($this->action == 'unpublish') {

            $this->unpublish($table);

        }

        $Core->proccessMassiveActions();

        $page    = $Core->inRequest('page') ? $Core->getRequest('page') : 1;
        $on_page = 10;

        $limit = (($page - 1) * $on_page) . ', ' . $on_page;

        $items_count = $Core->db->countRows($table, $where);

        $items = $Core->db->selectRows($table, '*', $where, $limit);

        $list['items'] = $items;
        $list['keys']  = array(
            'id',
            'title',
            'description',
            'published',
        );
        $list['head'] = array(
            'id'          => $this->lang['ID'],
            'title'       => $this->lang['TITLE_PARSING'],
            'description' => $this->lang['DESCRIPTION'],
            'published'   => $this->lang['PUBLISHED'],
        );

        $current_path = '/admin?dir=' . $this->dir;

        $list_html = $Core->render->renderAdminList($list, $current_path);

        $url = '/admin?dir=applications&page={page}';

        $pagination_html = $Core->render->getPagination($items_count, $on_page, $page, $url);

        $content = $list_html . $pagination_html;

        $this->renderAdmin($content);

        exit;

    }

    public function renderAdmin($content)
    {

        global $Core;

        $html = $Core->render->renderTpl("/admin/tpls/admin_template.tpl.php", ['content' => $content, 'component' => $this->component]);

        $html = str_replace('{*additional_links*}', $Core->page->additional_links, $html);

        echo $html;

    }

    public function editModuleOut($module)
    {

        global $Core;

        $shortlink = '/admin?dir=' . $Core->getRequest('dir') . '&component=' . $Core->getRequest('component');

        $out_id = $Core->getRequest('id');

        if ($Core->inRequest('submit')) {

            $out = $Core->getRequest('out');

            $out['app_id'] = $module->com_id;

            $out['published'] = isset($out['published']) ? 1 : 0;

            $out['sub_pages'] = isset($out['sub_pages']) ? 1 : 0;

            $out['settings'] = serialize($Core->getRequest('settings'));

            $out['page_route'] = $out['attach'];

            unset($out['attach']);

        }

        if ($Core->inRequest('submit') && !empty($out_id)) {

            $Core->db->update('apps_in_page', $out, array(
                'id' => $out_id,
            ));

            $Core->sysMessage($Core->langVars['SAVED'], 'success');

            $Core->redirect($shortlink);

        } elseif ($Core->inRequest('submit') && empty($out_id)) {

            $Core->db->insert('apps_in_page', $out);

            $Core->sysMessage($Core->langVars['SAVED'], 'success');

            $Core->redirect($shortlink);

        }

        $data['settings'] = $module->getSettings($out_id);

        if (!empty($out_id)) {

            $data['attach'] = $Core->db->selectField('apps_in_page', 'page_route', array(
                'id' => $out_id,
            ));

            $data['title'] = $Core->db->selectField('apps_in_page', 'title', array(
                'id' => $out_id,
            ));

        }

        $data['position_id'] = $Core->db->selectField('apps_in_page', 'position_id', array(
            'id' => $out_id,
        ));

        $data['positions'] = $Core->db->selectRows('tpl_positions', '*');

        $data['published'] = $Core->db->selectField('apps_in_page', 'published', array(
            'id' => $out_id,
        ));

        $data['sub_pages'] = $Core->db->selectField('apps_in_page', 'sub_pages', array(
            'id' => $out_id,
        ));

        $data['LANG'] = $Core->langVars;

        $data['shortlink'] = $shortlink;

        return $Core->render->renderTpl("/admin/tpls/module_edit.tpl.php", $data);

    }

    public function renderModuleList($mod)
    {

        global $Core;

        $data['LANG'] = $mod->lang;

        if ($mod->action == 'view') {

            $params = $Core->getFilterParams();

            $data['filter'] = $Core->getFilterParamsVal($params);

            $data['positions'] = $Core->db->selectRows('tpl_positions', '*');

            $params['app_id'] = $mod->com_id;

            $data['out-list'] = $Core->db->selectRows('apps_in_page', '*', $params, $mod->limit, 'priority');

            foreach ($data['out-list'] as $key => $out) {

                $data['out-list'][$key]['position'] = $Core->db->selectField('tpl_positions', 'title', array(
                    'id' => $out['position_id'],
                ));

                $data['out-list'][$key]['component'] = $Core->db->selectField('applications', 'route', array(
                    'id' => $out['app_id'],
                ));

            }

            $data['component'] = $mod->component;

            $url = '/admin?dir=modules&component=' . $mod->component . '&path=' . $mod->path . '&page={page}';

            $data['pagination'] = $Core->render->getPagination(count($data['out-list']), $mod->on_page, $mod->page, $url);

            return $Core->render->renderTpl("/admin/tpls/module_out_list.tpl.php", $data);

        }
    }

    public function upPriority($table, $row_id = null)
    {

        global $Core;

        if (empty($row_id)) {

            $row_id = $Core->getRequest('id');

        }

        $sql = "UPDATE {$Core->db->prefix}$table SET priority = priority + 1 WHERE id = '{$row_id}'";

        $Core->db->query($sql);

        $Core->sysMessage($row_id . ' ' . $Core->langVars['SAVED'], 'success');

        if (!$Core->inRequest('massive_action')) {
            $Core->back();
        }

    }

    public function downPriority($table, $row_id = null)
    {

        global $Core;

        if (empty($row_id)) {

            $row_id = $Core->getRequest('id');

        }

        $sql = "UPDATE {$Core->db->prefix}$table SET priority = priority - 1 WHERE id = '{$row_id}'";

        $Core->db->query($sql);

        $Core->sysMessage($row_id . ' ' . $Core->langVars['SAVED'], 'success');

        if (!$Core->inRequest('massive_action')) {
            $Core->back();
        }

    }

    public function publish($table, $row_id = null)
    {

        global $Core;

        if (empty($row_id)) {

            $row_id = $Core->getRequest('id');

        }

        $Core->db->publish($table, $row_id);

        $Core->sysMessage($row_id . ' ' . $Core->langVars['PUBLISHED'], 'success');

        if (!$Core->inRequest('massive_action')) {
            $Core->back();
        }

    }

    public function unpublish($table, $row_id = null)
    {

        global $Core;

        if (empty($row_id)) {

            $row_id = $Core->getRequest('id');

        }

        $Core->db->unpublish($table, $row_id);

        $Core->sysMessage($row_id . ' ' . $Core->langVars['UNPUBLISHED'], 'success');

        if (!$Core->inRequest('massive_action')) {
            $Core->back();
        }

    }

    public function delete($table, $row_id = null)
    {

        global $Core;

        if (empty($row_id)) {

            $row_id = $Core->getRequest('id');

        }

        $Core->db->delete($table, array(
            'id' => $row_id,
        ));

        $Core->sysMessage($row_id . ' ' . $Core->langVars['DELETED'], 'success');

        if (!$Core->inRequest('massive_action')) {
            $Core->back();
        }

    }

}
