<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - deleteItem()
 * - publishItem()
 * - unpublishItem()
 * - execute()
 * Classes list:
 * - Extension
 */
namespace admin;

use admin\Admin;

class Extension
{

    public function __construct()
    {
        global $Core;

        $this->dir = $Core->getRequest('dir');

        $this->component = $Core->getRequest('component');

        $this->path = $Core->getRequest('path');

        $this->action = $Core->inRequest('action') ? $Core->getRequest('action') : 'view';

        $this->lang = array_merge($Core->getLang("extensions/{$this->component}"), $Core->getLang('global'));

        $this->comLink = "/admin?dir={$this->dir}&component={$this->component}";

        $this->on_page = 10;

        $this->page = $Core->inRequest('filter') ? 1 : ($Core->inRequest('page') ? $Core->getRequest('page') : 1);

        $this->limit = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;

        $this->id = $Core->getRequest('id');

        $this->settings = $Core->getExtensionSettings($this->component);

    }

    public function deleteItem($id = null)
    {
        $id = empty($id) ? $this->id : $id;
        if (empty($id)) {
            return false;
        }

        Admin::delete($this->getPathTable(), $id);
    }

    public function publishItem($id = null)
    {
        $id = empty($id) ? $this->id : $id;
        if (empty($id)) {
            return false;
        }

        Admin::publish($this->getPathTable(), $id);
    }

    public function unpublishItem($id = null)
    {
        $id = empty($id) ? $this->id : $id;
        if (empty($id)) {
            return false;
        }

        Admin::unpublish($this->getPathTable(), $id);
    }

    protected function execute()
    {

        if ($this->action == 'delete') {

            $this->deleteItem();

        }

        if ($this->action == 'publish') {

            $this->publishItem();

        }

        if ($this->action == 'unpublish') {

            $this->unpublishItem();

        }

    }

}
