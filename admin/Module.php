<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - getPathTable()
 * - viewOutList()
 * - viewAddForm()
 * - execute()
 * Classes list:
 * - Module
 */
namespace admin;

use admin\Admin;

class Module
{

    public function __construct()
    {

        global $Core;

        $this->dir = $Core->getRequest('dir');

        $this->component = $Core->getRequest('component');

        $this->path = $Core->getRequest('path');

        $this->action = $Core->getRequest('action') ? $Core->getRequest('action') : 'view';

        $this->lang = array_merge($Core->getLang('modules/' . $this->component . ''), $Core->getLang('global'));

        $this->com_id = $Core->db->selectField('applications', 'id', array(
            'route' => $this->component,
        ));

        if (!$this->com_id) {

            $Core->sysMessage($this->lang['NOT_INSTALLED'], 'error');

            $Core->back();

        }

        $this->on_page = 10;

        $this->page = $Core->inRequest('filter') ? 1 : ($Core->inRequest('page') ? $Core->getRequest('page') : 1);

        $this->limit = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;

    }

    public function getPathTable()
    {
        return 'apps_in_page';
    }

    public function viewOutList()
    {

        return Admin::renderModuleList($this);

    }

    public function viewAddForm()
    {

        return Admin::editModuleOut($this);

    }

    protected function execute()
    {

        if ($this->action == 'edit') {

            return Admin::editModuleOut($this);

        }

        if ($this->action == 'delete') {

            Admin::delete('apps_in_page');

        }

        if ($this->action == 'publish') {

            Admin::publish('apps_in_page');

        }

        if ($this->action == 'unpublish') {

            Admin::unpublish('apps_in_page');

        }

        if ($this->action == 'up-priority') {

            Admin::upPriority('apps_in_page');

        }

        if ($this->action == 'down-priority') {

            Admin::downPriority('apps_in_page');

        }

    }

}
