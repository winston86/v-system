<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - execute()
 * - getPathTable()
 * - viewList()
 * - viewForm()
 * Classes list:
 * - LandingBackend extends Application
 */
use admin\Application;
use core\BaseModel;
use core\MultiLang;

class LandingBackend extends Application
{

    public function __construct()
    {

        global $Core;

        $Core->page->addHeadJs('/js/kcfinder.js');

        $this->model = new BaseModel('landing');

        parent::__construct();

    }

    public function execute()
    {

        global $Core;

        $menu_links = array();

        $menu_links[0]['dir']       = $this->dir;
        $menu_links[0]['component'] = $this->component;
        $menu_links[0]['path']      = 'list';
        $menu_links[0]['action']    = 'view';
        //$menu_links[0]['img'] = "/images/lang-ua.png";
        $menu_links[0]['title'] = $this->lang['LIST'];

        $sub_menu_links[0]['dir']       = $this->dir;
        $sub_menu_links[0]['component'] = $this->component;
        $sub_menu_links[0]['path']      = 'form';
        $sub_menu_links[0]['action']    = 'add';
        //$sub_menu_links[0]['img'] = "/images/lang-ua.png";
        $sub_menu_links[0]['title'] = $this->lang['ADD'];

        $menu     = $Core->render->renderAdminMenu($menu_links);
        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        switch ($this->path) {

            case 'list':
                $content = $this->viewList();
                break;

            default:
                $content = $this->viewList();
                break;

        }

        parent::execute();

        return $menu . $sub_menu . $content;

    }

    public function getPathTable()
    {
        return 'landing';
    }

    private function viewList()
    {

        global $Core;

        if ($this->action == 'view') {

            $params = array();

            $limit = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;

            $items_count = $this->model->countRows('landing', $params);

            $items = $this->model->selectRows('landing', '*', $params, $limit);

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'        => $this->lang['ID'],
                'title'     => $this->lang['TITLE'],
                'published' => $this->lang['PUBLISHED'],
                'tools'     => $this->lang['TOOLS'],
            );

            $list_html = $Core->render->renderAdminList($list, $Core->page->makeUrl($this->pathLink, ['path' => 'list']));

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $this->url);

            return $list_html . $pagination_html;

        } elseif ($this->action == 'edit' || $this->action == 'add') {

            return $this->viewForm();

        }

    }

    private function viewForm()
    {

        global $Core;

        $block_id = $Core->inRequest('id') ? $Core->getRequest('id') : 0;

        if ($Core->inRequest('submit')) {
            $block = $Core->getRequest('block');

            $block['published'] = isset($block['published']) ? 1 : 0;

            $errors = $Core->checkFields($block, $this->lang);

        }

        $redirect_to = $Core->page->makeUrl($this->pathLink, ['path' => 'list']);

        if ($Core->inRequest('submit') && $this->action == 'edit' && count($errors) == 0) {

            $this->model->update('landing', $block, array(
                'id' => $block_id,
            ));

            $Core->sysMessage($this->lang['SAVED'], 'success');

            $Core->redirect($redirect_to);

        } elseif ($Core->inRequest('submit') && $this->action == 'add' && count($errors) == 0) {

            $this->model->insert('landing', $block);

            $Core->sysMessage($this->lang['SAVED'], 'success');

            $Core->redirect($redirect_to);

        }

        $data = $this->model->selectFields('landing', '*', array(
            'id' => $block_id,
        ));

        $fields_to_fill = array(
            'block[title]'     => array(
                'type'          => 'text',
                'multilang'     => true,
                'default_value' => '',
                'value'         => MultiLang::getValues('title', $data),
                'attrs'         => array(
                    'placeholder' => $this->lang['TITLE'],
                    'class'       => 'landing-input  form-control',
                ),
                'label'         => $this->lang['TITLE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="landing-block-form ">%block%</div>',
            ),
            'block[content]'   => array(
                'type'       => 'editor',//'code_editor',
                'editor_type'=> 'custom',
                'multilang'  => true,
                'value'      => MultiLang::getValues('content', $data),
                'attrs'      => array(
                    'class' => 'landing-input',
                ),
                'label'      => $this->lang['CONTENT'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="landing-block-form">%block%</div>',
            ),
            'block[published]' => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'landing-input',
                ),
                'value'         => @$data['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="landing-block-form checkbox">%block%</div>',
            ),
        );

        $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
        return $Core->itemForm($inputs_html, 'content');

    }

}
