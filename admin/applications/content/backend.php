<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - execute()
 * - getPathTable()
 * - viewSettings()
 * - viewCats()
 * - viewPublications()
 * Classes list:
 * - ContentBackend extends Application
 */
use \admin\Application;

class ContentBackend extends Application
{

    public function __construct()
    {

        global $Core;

        $Core->page->addHeadJs('/js/kcfinder.js');

        parent::__construct();

    }

    public function execute()
    {

        global $Core;

        $menu_links = array();

        $menu_links[0]['dir']       = $this->dir;
        $menu_links[0]['component'] = $this->component;
        $menu_links[0]['path']      = 'publications';
        $menu_links[0]['action']    = 'view';
        //$menu_links[0]['img'] = "/images/lang-ua.png";
        $menu_links[0]['title'] = $this->lang['PUBLICATIONS'];

        $menu_links[1]['dir']       = $this->dir;
        $menu_links[1]['component'] = $this->component;
        $menu_links[1]['path']      = 'categories';
        $menu_links[1]['action']    = 'view';
        //$menu_links[1]['img'] = "/images/lang-ua.png";
        $menu_links[1]['title'] = $this->lang['CATEGORIES'];

        $menu_links[2]['dir']       = $this->dir;
        $menu_links[2]['component'] = $this->component;
        $menu_links[2]['path']      = 'settings';
        $menu_links[2]['action']    = 'view';
        //$menu_links[2]['img'] = "/images/lang-ua.png";
        $menu_links[2]['title'] = $this->lang['PARSING'];

        $menu = $Core->render->renderAdminMenu($menu_links);

        switch ($this->path) {

            case 'publications':
                $content = $this->viewPublications();
                break;

            case 'settings':
                $content = $this->viewSettings();
                break;

            case 'categories':
                $content = $this->viewCats();
                break;

            default:
                $content = $this->viewPublications();
                break;

        }

        parent::execute();

        return $menu . $content;

    }

    public function getPathTable()
    {
        switch ($this->path) {

            case 'publications':
                return 'publications';
                break;

            case 'settings':
                return 'parser_rules';
                break;

            case 'categories':
                return 'publication_cats';
                break;
            default:
                return 'parser_rules';
                break;

        }
    }

    private function viewSettings()
    {

        global $Core;

        $data['LANG'] = $this->lang;

        $data['backLink'] = $this->backLink;

        $menu_links[1]['dir']       = $this->dir;
        $menu_links[1]['component'] = $this->component;
        $menu_links[1]['path']      = 'settings';
        $menu_links[1]['action']    = 'add';
        // $menu_links[1]['img'] = "/images/lang-ua.png";
        $menu_links[1]['title'] = $this->lang['ADD'];

        $submenu = $Core->render->renderAdminMenu($menu_links);

        if ($this->action == 'view') {

            $params = array();

            $limit = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;

            $items_count = $Core->db->countRows('parser_rules', $params);

            $items = $Core->db->selectRows('parser_rules', '*', $params, $limit);

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'description',
                'catalog_url',
                'p_href_rule',
                'p_title_rule',
                'p_content_rule',
                'p_image_rule',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'             => $this->lang['ID'],
                'title'          => $this->lang['TITLE_PARSING'],
                'description'    => $this->lang['DESCRIPTION'],
                'catalog_url'    => $this->lang['CATALOG_URL'],
                'p_href_rule'    => $this->lang['P_HREF_RULE'],
                'p_title_rule'   => $this->lang['P_TITLE_RULE'],
                'p_content_rule' => $this->lang['P_CONTENT_RULE'],
                'p_image_rule'   => $this->lang['P_IMAGE_RULE'],
                'published'      => $this->lang['PUBLISHED'],
                'tools'          => $this->lang['TOOLS'],
            );

            $current_path = '/admin?dir=applications&component=content&path=settings';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $this->url);

            return $submenu . $list_html . $pagination_html;

            # code...

        }

        if ($this->action == 'edit' || $this->action == 'add') {

            $rule_id = $Core->inRequest('id') ? $Core->getRequest('id') : 0;

            if ($Core->inRequest('submit')) {

                $rule = $Core->getRequest('rule');

                $rule['published'] = isset($rule['published']) ? 1 : 0;

                $errors = $Core->checkFields($rule, $this->lang);

            }

            if ($Core->inRequest('submit') && $this->action == 'edit' && count($errors) == 0) {

                $Core->db->update('parser_rules', $rule, array(
                    'id' => $rule_id,
                ));

                $Core->sysMessage($data['LANG']['SAVED'], 'success');

                $Core->redirect('/admin/?dir=applications&component=content&path=settings&action=view');

            } elseif ($Core->inRequest('submit') && $this->action == 'add' && count($errors) == 0) {

                $Core->db->insert('parser_rules', $rule);

                $Core->sysMessage($data['LANG']['SAVED'], 'success');

                $Core->redirect('/admin/?dir=applications&component=content&path=settings&action=view');

            }

            $data = $Core->db->selectFields('parser_rules', '*', array(
                'id' => $rule_id,
            ));

            $fields_to_fill = array(
                'rule[title]'          => array(
                    'type'          => 'text',
                    'default_value' => '',
                    'value'         => @$data['title'],
                    'attrs'         => array(
                        'placeholder' => $this->lang['TITLE_PARSING'],
                        'class'       => 'content-input  form-control',
                    ),
                    'label'         => $this->lang['TITLE_PARSING'],
                    'required'      => true,
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="content-rule-add ">%block%</div>',
                ),
                'rule[description]'    => array(
                    'type'       => 'editor',
                    'editor_type'=> 'custom',
                    'value'      => @$data['description'],
                    'attrs'      => array(
                        'class' => 'content-input',
                    ),
                    'label'      => $this->lang['DESCRIPTION'],
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div class="content-rule-add">%block%</div>',
                ),
                'rule[catalog_url]'    => array(
                    'type'          => 'text',
                    'default_value' => '',
                    'value'         => @$data['catalog_url'],
                    'attrs'         => array(
                        'placeholder' => $this->lang['CATALOG_URL'],
                        'class'       => 'content-input  form-control',
                    ),
                    'label'         => $this->lang['CATALOG_URL'],
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="content-rule-add ">%block%</div>',
                ),
                'rule[p_href_rule]'    => array(
                    'type'          => 'text',
                    'default_value' => '',
                    'value'         => @$data['p_href_rule'],
                    'attrs'         => array(
                        'placeholder' => $this->lang['P_HREF_RULE'],
                        'class'       => 'content-input  form-control',
                    ),
                    'label'         => $this->lang['P_HREF_RULE'],
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="content-rule-add ">%block%</div>',
                ),
                'rule[p_title_rule]'   => array(
                    'type'          => 'text',
                    'default_value' => '',
                    'value'         => @$data['p_title_rule'],
                    'attrs'         => array(
                        'placeholder' => $this->lang['P_TITLE_RULE'],
                        'class'       => 'content-input  form-control',
                    ),
                    'label'         => $this->lang['P_TITLE_RULE'],
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="content-rule-add ">%block%</div>',
                ),
                'rule[p_content_rule]' => array(
                    'type'          => 'text',
                    'default_value' => '',
                    'value'         => @$data['p_content_rule'],
                    'attrs'         => array(
                        'placeholder' => $this->lang['P_CONTENT_RULE'],
                        'class'       => 'content-input  form-control',
                    ),
                    'label'         => $this->lang['P_CONTENT_RULE'],
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="content-rule-add ">%block%</div>',
                ),
                'rule[p_image_rule]'   => array(
                    'type'          => 'text',
                    'default_value' => '',
                    'value'         => @$data['p_image_rule'],
                    'attrs'         => array(
                        'placeholder' => $this->lang['P_IMAGE_RULE'],
                        'class'       => 'content-input  form-control',
                    ),
                    'label'         => $this->lang['P_IMAGE_RULE'],
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="content-order_status-add ">%block%</div>',
                ),
                'rule[published]'      => array(
                    'type'          => 'checkbox',
                    'attrs'         => array(
                        'class' => 'content-input',
                    ),
                    'value'         => @$data['published'],
                    'label'         => $this->lang['PUBLISHED'],
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="content-rule-add checkbox">%block%</div>',
                ),
            );

            $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
            return $Core->itemForm($inputs_html, 'content');

        }

    }

    private function viewCats()
    {

        global $Core;

        $data['LANG'] = $this->lang;

        $data['backLink'] = $this->backLink;

        $menu_links[1]['dir']       = $this->dir;
        $menu_links[1]['component'] = $this->component;
        $menu_links[1]['path']      = 'categories';
        $menu_links[1]['action']    = 'add';
        // $menu_links[1]['img'] = "/images/lang-ua.png";
        $menu_links[1]['title'] = $this->lang['ADD'];

        $submenu = $Core->render->renderAdminMenu($menu_links);

        if ($this->action == 'view') {

            $params = array();

            $limit = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;

            $items_count = $Core->db->countRows('publication_cats', $params);

            $items = $Core->db->selectRows('publication_cats', '*', $params, $limit);

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'description',
                'search_tag',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'          => $this->lang['ID'],
                'title'       => $this->lang['TITLE_PARSING'],
                'description' => $this->lang['DESCRIPTION'],
                'search_tag'  => $this->lang['SEARCH_TAG'],
                'published'   => $this->lang['PUBLISHED'],
                'tools'       => $this->lang['TOOLS'],
            );

            $current_path = '/admin?dir=applications&component=content&path=categories';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $this->url);

            return $submenu . $list_html . $pagination_html;

            # code...

        }

        if ($this->action == 'edit' || $this->action == 'add') {

            $cat_id = $Core->inRequest('id') ? $Core->getRequest('id') : 0;

            if ($Core->inRequest('submit')) {

                $cat = $Core->getRequest('cat');

                $cat['published'] = !empty($cat['published']) ? 1 : 0;

                $errors = $Core->checkFields($cat, $this->lang);

            }

            if ($Core->inRequest('submit') && $this->action == 'edit' && count($errors) == 0) {

                $Core->db->update('publication_cats', $cat, array(
                    'id' => $cat_id,
                ));

                $Core->sysMessage($data['LANG']['SAVED'], 'success');

                $Core->redirect('/admin/?dir=applications&component=content&path=categories&action=view');

            } elseif ($Core->inRequest('submit') && $this->action == 'add' && count($errors) == 0) {

                $Core->db->insert('publication_cats', $cat);

                $Core->sysMessage($data['LANG']['SAVED'], 'success');

                $Core->redirect('/admin/?dir=applications&component=content&path=categories&action=view');

            }

            $data = $Core->db->selectFields('publication_cats', '*', array(
                'id' => $cat_id,
            ));

            $fields_to_fill = array(
                'cat[title]'       => array(
                    'type'          => 'text',
                    'default_value' => '',
                    'value'         => @$data['title'],
                    'attrs'         => array(
                        'placeholder' => $this->lang['TITLE_PARSING'],
                        'class'       => 'content-input  form-control',
                    ),
                    'label'         => $this->lang['TITLE_PARSING'],
                    'required'      => true,
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="content-cat-edit ">%block%</div>',
                ),
                'cat[description]' => array(
                    'type'       => 'editor',
                    'editor_type'=> 'custom',
                    'value'      => @$data['description'],
                    'attrs'      => array(
                        'class' => 'content-input',
                    ),
                    'label'      => $this->lang['DESCRIPTION'],
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div class="content-cat-edit">%block%</div>',
                ),
                'cat[search_tag]'  => array(
                    'type'          => 'text',
                    'default_value' => '',
                    'value'         => @$data['search_tag'],
                    'attrs'         => array(
                        'placeholder' => $this->lang['SEARCH_TAG'],
                        'class'       => 'content-input  form-control',
                    ),
                    'label'         => $this->lang['SEARCH_TAG'],
                    'required'      => true,
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="content-cat-edit ">%block%</div>',
                ),
                'cat[published]'   => array(
                    'type'          => 'checkbox',
                    'attrs'         => array(
                        'class' => 'content-input',
                    ),
                    'value'         => @$data['published'],
                    'label'         => $this->lang['PUBLISHED'],
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="content-cat-edit checkbox">%block%</div>',
                ),
            );

            $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
            return $Core->itemForm($inputs_html, 'content');

        }
    }

    private function viewPublications()
    {

        global $Core;

        $data['LANG'] = $this->lang;

        $data['backLink'] = $this->backLink;

        $menu_links[1]['dir']       = $this->dir;
        $menu_links[1]['component'] = $this->component;
        $menu_links[1]['path']      = 'publications';
        $menu_links[1]['action']    = 'add';
        // $menu_links[1]['img'] = "/images/lang-ua.png";
        $menu_links[1]['title'] = $this->lang['ADD'];

        $submenu = $Core->render->renderAdminMenu($menu_links);

        if ($this->action == 'view') {

            $params = array();

            $limit = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;

            $items_count = $Core->db->countRows('publications', $params);

            $items = $Core->db->selectRows('publications', '*', $params, $limit);

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'description',
                'image',
                'pub_url',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'          => $this->lang['ID'],
                'title'       => $this->lang['TITLE_PARSING'],
                'description' => $this->lang['DESCRIPTION'],
                'image'       => $this->lang['IMAGE'],
                'pub_url'     => $this->lang['PUB_URL'],
                'published'   => $this->lang['PUBLISHED'],
                'tools'       => $this->lang['TOOLS'],
            );

            $current_path = '/admin?dir=applications&component=content&path=publications';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $this->url);

            return $submenu . $list_html . $pagination_html;

            # code...

        }

        if ($this->action == 'edit' || $this->action == 'add') {

            $pub_id = $Core->inRequest('id') ? $Core->getRequest('id') : 0;

            if ($Core->inRequest('submit')) {

                $pub = $Core->getRequest('publication');

                $pub['published'] = !empty($pub['published']) ? 1 : 0;

                $errors = $Core->checkFields($pub, $this->lang);

            }

            if ($Core->inRequest('submit') && $this->action == 'edit' && count($errors) == 0) {

                $Core->db->update('publications', $pub, array(
                    'id' => $pub_id,
                ));

                $Core->sysMessage($data['LANG']['SAVED'], 'success');

                $Core->redirect('/admin/?dir=applications&component=content&path=publications&action=view');

            } elseif ($Core->inRequest('submit') && $this->action == 'add' && count($errors) == 0) {

                $Core->db->insert('publications', $pub);

                $Core->sysMessage($data['LANG']['SAVED'], 'success');

                $Core->redirect('/admin/?dir=applications&component=content&path=publications&action=view');

            }

            $data = $Core->db->selectFields('publications', '*', array(
                'id' => $pub_id,
            ));

            $fields_to_fill = array(
                'publication[title]'       => array(
                    'type'          => 'text',
                    'default_value' => '',
                    'value'         => @$data['title'],
                    'attrs'         => array(
                        'placeholder' => $this->lang['TITLE_PARSING'],
                        'class'       => 'content-input  form-control',
                    ),
                    'label'         => $this->lang['TITLE_PARSING'],
                    'required'      => true,
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="content-publication-edit ">%block%</div>',
                ),
                'publication[description]' => array(
                    'type'       => 'editor',
                    'editor_type'=> 'custom',
                    'value'      => @$data['description'],
                    'attrs'      => array(
                        'class' => 'content-input',
                    ),
                    'label'      => $this->lang['DESCRIPTION'],
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div class="content-publication-edit">%block%</div>',
                ),
                'publication[content]'     => array(
                    'type'       => 'editor',
                    'editor_type'=> 'custom',
                    'value'      => @$data['content'],
                    'attrs'      => array(
                        'class' => 'content-input',
                    ),
                    'label'      => $this->lang['CONTENT'],
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div class="content-publication-edit">%block%</div>',
                ),
                'publication[image]'       => array(
                    'type'       => 'images',
                    'attrs'      => array(
                        'class' => 'content-input btn btn-success',
                    ),
                    'value'      => @$data['image'],
                    'label'      => $this->lang['IMAGE'],
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div class="content-publication-edit">%block%</div>',
                ),
                'publication[pub_url]'     => array(
                    'type'          => 'text',
                    'default_value' => '',
                    'value'         => @$data['pub_url'],
                    'attrs'         => array(
                        'placeholder' => $this->lang['PUB_URL'],
                        'class'       => 'content-input  form-control',
                    ),
                    'label'         => $this->lang['PUB_URL'],
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="content-publication-edit ">%block%</div>',
                ),
                'publication[published]'   => array(
                    'type'          => 'checkbox',
                    'attrs'         => array(
                        'class' => 'content-input',
                    ),
                    'value'         => @$data['published'],
                    'label'         => $this->lang['PUBLISHED'],
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="content-publication-edit checkbox">%block%</div>',
                ),
            );

            $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
            return $Core->itemForm($inputs_html, 'content');

        }
    }

}
