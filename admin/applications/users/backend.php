<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - execute()
 * - viewUsers()
 * - viewGroups()
 * - viewRights()
 * - getPathTable()
 * - saveUser()
 * Classes list:
 * - UsersBackend extends Application
 */
use admin\Application;

class UsersBackend extends Application
{

    public function __construct()
    {

        parent::__construct();

    }

    public function execute()
    {

        global $Core;

        $menu_links = array();

        $menu_links[0]['dir']       = $this->dir;
        $menu_links[0]['component'] = $this->component;
        $menu_links[0]['path']      = 'users';
        $menu_links[0]['action']    = 'view';
        // $menu_links[0]['img'] = "/images/lang-ua.png";
        $menu_links[0]['title'] = $this->lang['USERS'];

        $menu_links[1]['dir']       = $this->dir;
        $menu_links[1]['component'] = $this->component;
        $menu_links[1]['path']      = 'groups';
        $menu_links[1]['action']    = 'view';
        // $menu_links[1]['img'] = "/images/lang-ua.png";
        $menu_links[1]['title'] = $this->lang['GROUPS'];

        $menu_links[2]['dir']       = $this->dir;
        $menu_links[2]['component'] = $this->component;
        $menu_links[2]['path']      = 'rights';
        $menu_links[2]['action']    = 'view';
        // $menu_links[2]['img'] = "/images/lang-ua.png";
        $menu_links[2]['title'] = $this->lang['RIGHTS'];

        $menu = $Core->render->renderAdminMenu($menu_links);

        switch ($this->path) {

            case 'users':
                $content = $this->viewUsers();
                break;

            case 'groups':
                $content = $this->viewGroups();
                break;

            case 'rights':
                $content = $this->viewRights();
                break;

            default:
                $content = $this->viewUsers();
                break;

        }

        parent::execute();

        return $menu . $content;

    }

    private function viewUsers()
    {

        global $Core;

        if ($this->action == 'view') {

            $menu_links[1]['dir']       = $this->dir;
            $menu_links[1]['component'] = $this->component;
            $menu_links[1]['path']      = 'users';
            $menu_links[1]['action']    = 'add';
            // $menu_links[1]['img'] = "/images/lang-ua.png";
            $menu_links[1]['title'] = $this->lang['ADD'];

            $submenu = $Core->render->renderAdminMenu($menu_links);

            $params = array();

            $limit = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;

            $items_count = $Core->db->countRows('users', $params);

            $items = $Core->db->selectRows('users', '*', $params, $limit);

            foreach ($items as $key => $user) {
                $groups                     = $Core->users->getGroupTitle($user['group_id']);
                $groups                     = is_array($groups) ? implode("<br>", $groups) : $groups;
                $items[$key]['group_title'] = $groups;

            }

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'login',
                'nickname',
                'email',
                'group_title',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'          => $this->lang['ID'],
                'login'       => $this->lang['LOGIN'],
                'nickname'    => $this->lang['NICKNAME'],
                'email'       => $this->lang['EMAIL'],
                'group_title' => $this->lang['GROUP'],
                'published'   => $this->lang['IS_ACTIVE'],
                'tools'       => $this->lang['TOOLS'],
            );

            $current_path = '/admin?dir=applications&component=users';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $this->url);

            return $submenu . $list_html . $pagination_html;

        }

        if ($this->action == 'edit' || $this->action == 'add') {

            $user_id = $Core->getRequest('id') > 0 ? $Core->getRequest('id') : 0;

            if ($Core->inRequest('submit')) {

                $user = $Core->getRequest('user');

                $user['published'] = isset($user['published']) ? 1 : 0;

                $errors = $Core->checkFields($user, $this->lang);

            }

            if ($Core->inRequest('submit') && count($errors) == 0) {

                $this->saveUser($user);

            }

            $user = !empty($user) ? $user : $Core->db->selectFields('users', '*', array(
                'id' => $user_id,
            ));

            $groups = $Core->db->selectRows('user_groups', '*', ['published' => 1]);

            $fields_to_fill = array(
                'user[login]'     => array(
                    'type'       => 'text',
                    'value'      => @$user['login'],
                    'attrs'      => array(
                        'placeholder' => $this->lang['LOGIN'],
                        'class'       => 'users-input  form-control',
                    ),
                    'label'      => $this->lang['LOGIN'],
                    'required'   => true,
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div class="users-input ">%block%</div>',
                ),
                'user[nickname]'  => array(
                    'type'       => 'text',
                    'value'      => @$user['nickname'],
                    'attrs'      => array(
                        'placeholder' => $this->lang['NICKNAME'],
                        'class'       => 'users-input  form-control',
                    ),
                    'label'      => $this->lang['NICKNAME'],
                    'required'   => true,
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div class="users-input ">%block%</div>',
                ),
                'user[email]'     => array(
                    'type'       => 'text',
                    'value'      => @$user['email'],
                    'attrs'      => array(
                        'placeholder' => $this->lang['EMAIL'],
                        'class'       => 'users-input  form-control',
                    ),
                    'label'      => $this->lang['EMAIL'],
                    'required'   => true,
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div class="users-input ">%block%</div>',
                ),
                'user[group_id]'  => array(
                    'type'       => 'select',
                    'value'      => @$user['group_id'],
                    'attrs'      => array(
                        'class' => 'users-input  form-control',
                    ),
                    'options'    => $Core->getValEqLabelArr($groups, 'id', 'title'),
                    'label'      => $this->lang['GROUP'],
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '%block%',
                ),
                'user[image]'     => array(
                    'type'       => 'images',
                    'attrs'      => array(
                        'class' => 'users-input btn btn-success',
                    ),
                    'value'      => @$user['image'],
                    'label'      => $this->lang['IMAGE'],
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div class="users-field">%block%</div>',
                ),
                'user[published]' => array(
                    'type'          => 'checkbox',
                    'attrs'         => array(
                        'class' => 'users-input',
                    ),
                    'value'         => @$user['published'],
                    'label'         => $this->lang['IS_ACTIVE'],
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="users-input checkbox">%block%</div>',
                ),
            );

            $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
            return $Core->itemForm($inputs_html, 'users');

        }

    }

    private function viewGroups()
    {

        global $Core;

        $data['LANG'] = $this->lang;

        $data['backLink'] = $this->backLink;

        if ($this->action == 'view') {

            $menu_links[1]['dir']       = $this->dir;
            $menu_links[1]['component'] = $this->component;
            $menu_links[1]['path']      = 'groups';
            $menu_links[1]['action']    = 'add';
            // $menu_links[1]['img'] = "/images/lang-ua.png";
            $menu_links[1]['title'] = $this->lang['ADD'];

            $submenu = $Core->render->renderAdminMenu($menu_links);

            $limit_on_page = 10;

            $page = $Core->inRequest('page') ? $Core->getRequest('page') : 1;

            $limit = ($page * $limit_on_page - $limit_on_page) . ", " . $limit_on_page;

            $params = array();

            $groups_count = $Core->db->countRows('user_groups', $params);

            $items = $Core->db->selectRows('user_groups', '*', $params, $limit, 'id');

            foreach ($items as $key => $group) {

                $items[$key]['permissions'] = str_replace("||", "<br>", $group['permissions']);

            }

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'description',
                'permissions',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'          => $this->lang['ID'],
                'title'       => $this->lang['TITLE'],
                'description' => $this->lang['DESCRIPTION'],
                'permissions' => $this->lang['PERMISSIONS'],
                'published'   => $this->lang['PUBLISHED'],
                'tools'       => $this->lang['TOOLS'],
            );

            $current_path = '/admin?dir=applications&component=users&path=groups';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($groups_count, $limit_on_page, $page, '/admin/?dir=applications&component=users&path=groups&action=view&page={page}');

            return $submenu . $list_html . $pagination_html;

        }

        if ($this->action == 'edit' || $this->action == 'add') {

            $group_id = $Core->inRequest('id') ? $Core->getRequest('id') : false;

            if ($Core->inRequest('submit')) {

                $group = $Core->getRequest('group');

                $group['permissions'] = implode('||', $group['permissions']);

                $errors = $Core->checkFields($group, $this->lang);

            }

            if ($Core->inRequest('submit') && $this->action == 'edit' && count($errors) == 0) {

                $Core->db->update('user_groups', $group, array(
                    'id' => $group_id,
                ));

                $Core->sysMessage($data['LANG']['SAVED'], 'success');

                $Core->redirect('/admin/?dir=applications&component=users&path=groups&action=view');

            } elseif ($Core->inRequest('submit') && $this->action == 'add' && count($errors) == 0) {

                $Core->db->insert('user_groups', $group);

                $Core->sysMessage($data['LANG']['SAVED'], 'success');

                $Core->redirect('/admin/?dir=applications&component=users&path=groups&action=view');

            }
            $permissions = $Core->db->selectRows('permissions', '*', ['published' => 1]);

            if (!empty($group_id)) {

                $group = $Core->db->selectFields('user_groups', '*', array(
                    'id' => $group_id,
                ));

                $group_permissions = explode('||', $group['permissions']);

            }

            $fields_to_fill = array(
                'group[title]'       => array(
                    'type'       => 'text',
                    'value'      => @$group['title'],
                    'attrs'      => array(
                        'placeholder' => $this->lang['GROUP'],
                        'class'       => 'users-input  form-control',
                    ),
                    'label'      => $this->lang['GROUP'],
                    'required'   => true,
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div class="users-input ">%block%</div>',
                ),
                'group[description]' => array(
                    'type'       => 'text',
                    'value'      => @$group['description'],
                    'attrs'      => array(
                        'placeholder' => $this->lang['DESCRIPTION'],
                        'class'       => 'users-input  form-control',
                    ),
                    'label'      => $this->lang['DESCRIPTION'],
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div class="users-input ">%block%</div>',
                ),
                'group[permissions]' => array(
                    'type'       => 'select',
                    'options'    => @$Core->getValEqLabelArr($permissions, 'alias', 'title'),
                    'multiple'   => true,
                    'value'      => @$group_permissions,
                    'attrs'      => array(
                        'class' => 'users-input form-control',
                    ),
                    'label'      => $this->lang['PERMISSIONS'],
                    'required'   => true,
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div class="users-input ">%block%</div>',
                ),
                'group[default_group]'   => array(
                    'type'          => 'checkbox',
                    'attrs'         => array(
                        'class' => 'users-input',
                    ),
                    'default_value' => 0,
                    'value'         => @$group['default_group'],
                    'label'         => $this->lang['DEFAULT_GROUP'],
                    'disabled'      => (!empty($Core->users->getDefaultGroup())&&empty($group['default_group'])
                                        ?
                                        true:false),
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="users-input checkbox">%block%</div>',
                ),
                'group[published]'   => array(
                    'type'          => 'checkbox',
                    'attrs'         => array(
                        'class' => 'users-input',
                    ),
                    'default_value' => 1,
                    'value'         => @$group['published'],
                    'label'         => $this->lang['PUBLISHED'],
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div class="users-input checkbox">%block%</div>',
                ),
            );

            $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
            return $Core->itemForm($inputs_html, 'users');

        }

    }

    private function viewRights()
    {

        global $Core;

        $data['LANG'] = $this->lang;

        $data['backLink'] = $this->backLink;

        if ($this->action == 'view') {

            $menu_links[1]['dir']       = $this->dir;
            $menu_links[1]['component'] = $this->component;
            $menu_links[1]['path']      = 'rights';
            $menu_links[1]['action']    = 'add';
            // $menu_links[1]['img'] = "/images/lang-ua.png";
            $menu_links[1]['title'] = $this->lang['ADD'];

            $submenu = $Core->render->renderAdminMenu($menu_links);

            $limit_on_page = 10;

            $page = $Core->inRequest('page') ? $Core->getRequest('page') : 1;

            $limit = ($page * $limit_on_page - $limit_on_page) . ", " . $limit_on_page;

            $params = array();

            $rights_count = $Core->db->countRows('permissions', $params);

            $items = $Core->db->selectRows('permissions', '*', $params, $limit, 'id');

            foreach ($items as $key => $pr) {

                $items[$key]['events'] = str_replace("||", "<br>", $pr['events']);

            }

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'description',
                'events',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'        => $this->lang['ID'],
                'title'     => $this->lang['TITLE'],
                'alias'     => $this->lang['ALIAS'],
                'events'    => $this->lang['EVENTS'],
                'published' => $this->lang['PUBLISHED'],
                'tools'     => $this->lang['TOOLS'],
            );

            $current_path = '/admin?dir=applications&component=users&path=rights';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($rights_count, $limit_on_page, $page, '/admin/?dir=applications&component=users&path=rights&action=view&page={page}');

            return $submenu . $list_html . $pagination_html;

        }

        if ($this->action == 'edit' || $this->action == 'add') {

            $permission_id = $Core->inRequest('id') ? $Core->getRequest('id') : false;

            if ($Core->inRequest('submit')) {

                $permission = $Core->getRequest('permission');

                $permission['events'] = implode('||', $permission['events']);

                $errors = $Core->checkFields($permission, $this->lang);

            }

            if ($Core->inRequest('submit') && $this->action == 'edit' && count($errors) == 0) {

                $Core->db->update('permissions', $permission, array(
                    'id' => $permission_id,
                ));

                $Core->sysMessage($data['LANG']['SAVED'], 'success');

                $Core->redirect('/admin/?dir=applications&component=users&path=rights&action=view');

            } elseif ($Core->inRequest('submit') && $this->action == 'add' && count($errors) == 0) {

                $Core->db->insert('permissions', $permission);

                $Core->sysMessage($data['LANG']['SAVED'], 'success');

                $Core->redirect('/admin/?dir=applications&component=users&path=rights&action=view');

            }

            $events = $Core->db->selectRows('events', '*');

            if (!empty($permission_id)) {

                $permission = $Core->db->selectFields('permissions', '*', array(
                    'id' => $permission_id,
                ));

                $permission_events = explode('||', $permission['events']);

            }

            $fields_to_fill = array(
                'permission[title]'  => array(
                    'type'       => 'text',
                    'value'      => @$permission['title'],
                    'attrs'      => array(
                        'placeholder' => $this->lang['RIGHT_NAME'],
                        'class'       => 'users-input  form-control',
                    ),
                    'label'      => $this->lang['RIGHT_NAME'],
                    'required'   => true,
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div class="users-input ">%block%</div>',
                ),
                'permission[alias]'  => array(
                    'type'       => 'text',
                    'value'      => @$permission['alias'],
                    'attrs'      => array(
                        'placeholder' => $this->lang['ALIAS_EXPLENATION'],
                        'class'       => 'users-input  form-control',
                    ),
                    'label'      => $this->lang['ALIAS'],
                    'required'   => true,
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div class="users-input ">%block%</div>',
                ),
                'permission[events]' => array(
                    'type'       => 'select',
                    'options'    => @$Core->getValEqLabelArr($events, 'event', 'event'),
                    'multiple'   => true,
                    'value'      => @$permission_events,
                    'attrs'      => array(
                        'class' => 'users-input form-control',
                    ),
                    'label'      => $this->lang['EVENTS'],
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div class="users-input ">%block%</div>',
                ),
            );

            $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
            return $Core->itemForm($inputs_html, 'users');

        }

    }

    public function getPathTable()
    {
        switch ($this->path) {
            case 'users':
                return 'users';
                break;
            case 'groups':
                return 'user_groups';
                break;
            case 'rights':
                return 'permissions';
            default:
                return 'users';
                break;
        };
    }

    private function saveUser($user)
    {

        global $Core;

        $user_id = $Core->getRequest('id');

        if ($this->action == 'edit') {

            $saved = $Core->db->update('users', $user, array(
                'id' => $user_id,
            ));

        } else {

            $saved = $Core->db->insert('users', $user);
        }

        if ($saved) {

            $Core->sysMessage($this->lang['SAVED'], 'success');

            $Core->back();

        } else {

            return false;

        }

    }

}
