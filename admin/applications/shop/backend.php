<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - execute()
 * - settings()
 * - products()
 * - renderProdFields()
 * - renderPVariants()
 * - renderSVariants()
 * - cats()
 * - renderCatFields()
 * - manufacturers()
 * - renderManFields()
 * - features()
 * - renderFeatureFields()
 * - featureVariants()
 * - renderFeatureVariantFields()
 * - deliveries()
 * - renderDeliveryFields()
 * - payments()
 * - renderPaymentFields()
 * - warehouses()
 * - renderWarehouseFields()
 * - sales()
 * - renderSaleFields()
 * - discounts()
 * - renderDiscountFields()
 * - currencies()
 * - renderCurrencyFields()
 * - orders()
 * - renderOrderFieldsStep2()
 * - renderStep2Form()
 * - renderOrderFields()
 * - renderStep1Form()
 * - orderStatuses()
 * - renderOrderStatusFields()
 * - getAjaxResponse()
 * - renderProductRow()
 * - renderCheckedProductVariantsInOrders()
 * - renderProductVariantsModal()
 * - getPathTable()
 * Classes list:
 * - ShopBackend extends Application
 */
use admin\Application;
use core\MultiLang;

class ShopBackend extends Application
{

    public function __construct()
    {

        global $Core;

        $this->shopcore = $Core->loadClass('classes\\ShopCore', 'Shopcore.class.php');

        $Core->page->addHeadCss('/admin/css/shop.css');

        parent::__construct();

    }

    public function execute()
    {

        global $Core;

        $this->settings = $Core->getAppSettings('shop');

        $menu_links = array();

        $menu_links[0]['dir']       = $this->dir;
        $menu_links[0]['component'] = $this->component;
        $menu_links[0]['path']      = 'settings';
        $menu_links[0]['action']    = 'view';
        $menu_links[0]['class']     = 'glyphicon glyphicon-cog';
        //$menu_links[0]['img'] = "/images/lang-ua.png";
        $menu_links[0]['title'] = $this->lang['SETTINGS'];

        $menu_links[1]['dir']       = $this->dir;
        $menu_links[1]['component'] = $this->component;
        $menu_links[1]['path']      = 'products';
        $menu_links[1]['action']    = 'view';
        $menu_links[1]['class']     = 'glyphicon glyphicon-list-alt';
        //$menu_links[1]['img'] = "/images/lang-ua.png";
        $menu_links[1]['title'] = $this->lang['ITEMS'];

        $menu_links[2]['dir']       = $this->dir;
        $menu_links[2]['component'] = $this->component;
        $menu_links[2]['path']      = 'categories';
        $menu_links[2]['action']    = 'view';
        $menu_links[2]['class']     = 'glyphicon glyphicon-tasks';
        //$menu_links[2]['img'] = "/images/lang-ua.png";
        $menu_links[2]['title'] = $this->lang['CATEGORIES'];

        if (!empty($this->settings['enable_manufacturers'])) {
            $menu_links[3]['dir']       = $this->dir;
            $menu_links[3]['component'] = $this->component;
            $menu_links[3]['path']      = 'manufacturers';
            $menu_links[3]['action']    = 'view';
            $menu_links[3]['class']     = 'glyphicon glyphicon-barcode';
            //$menu_links[3]['img'] = "/images/lang-ua.png";
            $menu_links[3]['title'] = $this->lang['MANUFACTURERS'];
        }

        $menu_links[4]['dir']       = $this->dir;
        $menu_links[4]['component'] = $this->component;
        $menu_links[4]['path']      = 'warehouses';
        $menu_links[4]['action']    = 'view';
        $menu_links[4]['class']     = 'glyphicon glyphicon-object-align-bottom';
        //$menu_links[4]['img'] = "/images/lang-ua.png";
        $menu_links[4]['title'] = $this->lang['WAREHOUSES'];

        if (!empty($this->settings['enable_product_variants'])) {
            $menu_links[5]['dir']       = $this->dir;
            $menu_links[5]['component'] = $this->component;
            $menu_links[5]['path']      = 'features';
            $menu_links[5]['action']    = 'view';
            $menu_links[5]['class']     = 'glyphicon glyphicon-sort-by-attributes';
            //$menu_links[5]['img'] = "/images/lang-ua.png";
            $menu_links[5]['title'] = $this->lang['FEATURES'];

            $menu_links[6]['dir']       = $this->dir;
            $menu_links[6]['component'] = $this->component;
            $menu_links[6]['path']      = 'feature_variants';
            $menu_links[6]['action']    = 'view';
            $menu_links[6]['class']     = 'glyphicon glyphicon-filter';
            //$menu_links[6]['img'] = "/images/lang-ua.png";
            $menu_links[6]['title'] = $this->lang['FEATURE_VARIANTS'];
        }

        $menu_links[7]['dir']       = $this->dir;
        $menu_links[7]['component'] = $this->component;
        $menu_links[7]['path']      = 'orders';
        $menu_links[7]['action']    = 'view';
        $menu_links[7]['class']     = 'glyphicon glyphicon-shopping-cart';
        //$menu_links[7]['img'] = "/images/lang-ua.png";
        $menu_links[7]['title'] = $this->lang['ORDERS'];

        $menu_links[71]['dir']       = $this->dir;
        $menu_links[71]['component'] = $this->component;
        $menu_links[71]['path']      = 'order_statuses';
        $menu_links[71]['action']    = 'view';
        $menu_links[71]['class']     = 'glyphicon glyphicon-list-alt';
        //$menu_links[71]['img'] = "/images/lang-ua.png";
        $menu_links[71]['title'] = $this->lang['STATUSES'];

        if (!empty($this->settings['enable_delivery'])) {
            $menu_links[8]['dir']       = $this->dir;
            $menu_links[8]['component'] = $this->component;
            $menu_links[8]['path']      = 'delivery_services';
            $menu_links[8]['action']    = 'view';
            $menu_links[8]['class']     = 'glyphicon glyphicon-send';
            //$menu_links[8]['img'] = "/images/lang-ua.png";
            $menu_links[8]['title'] = $this->lang['DELIVERIES'];
        }

        if (!empty($this->settings['enable_payments'])) {
            $menu_links[9]['dir']       = $this->dir;
            $menu_links[9]['component'] = $this->component;
            $menu_links[9]['path']      = 'payments';
            $menu_links[9]['action']    = 'view';
            $menu_links[9]['class']     = 'glyphicon glyphicon-credit-card';
            //$menu_links[9]['img'] = "/images/lang-ua.png";
            $menu_links[9]['title'] = $this->lang['PAYMENTS'];

        }

        if (!empty($this->settings['enable_discounts'])) {
            $menu_links[10]['dir']       = $this->dir;
            $menu_links[10]['component'] = $this->component;
            $menu_links[10]['path']      = 'discounts';
            $menu_links[10]['action']    = 'view';
            $menu_links[10]['class']     = 'glyphicon glyphicon glyphicon-user';
            //$menu_links[10]['img'] = "/images/lang-ua.png";
            $menu_links[10]['title'] = $this->lang['DISCOUNTS'];

        }

        if (!empty($this->settings['enable_sales'])) {
            $menu_links[11]['dir']       = $this->dir;
            $menu_links[11]['component'] = $this->component;
            $menu_links[11]['path']      = 'sales';
            $menu_links[11]['action']    = 'view';
            $menu_links[11]['class']     = 'glyphicon glyphicon-circle-arrow-down';
            //$menu_links[11]['img'] = "/images/lang-ua.png";
            $menu_links[11]['title'] = $this->lang['SALES'];
        }

        /*
        $menu_links[12]['dir'] = $this->dir;
        $menu_links[12]['component'] = $this->component;
        $menu_links[12]['path'] = 'glyphicon glyphicon-tree-conifer';
        $menu_links[12]['action'] = 'view';
        $menu_links[12]['class'] = 'glyphicon glyphicon-tree-conifer';
        //$menu_links[12]['img'] = "/images/lang-ua.png";
        $menu_links[12]['title'] =  $this->lang['TAXES'];

         */
        if (!empty($this->settings['enable_multiple_curencies'])) {
            $menu_links[13]['dir']       = $this->dir;
            $menu_links[13]['component'] = $this->component;
            $menu_links[13]['path']      = 'currencies';
            $menu_links[13]['action']    = 'view';
            $menu_links[13]['class']     = 'glyphicon glyphicon-usd';
            //$menu_links[13]['img'] = "/images/lang-ua.png";
            $menu_links[13]['title'] = $this->lang['CURRENCIES'];
        }

        if ($this->action == 'publish') {
            $Core->db->update('shop_' . $this->path, array(
                'published' => 1,
            ), array(
                'id' => $Core->getRequest('id'),
            ));
            $this->action = 'view';
        } elseif ($this->action == 'unpublish') {
            $Core->db->update('shop_' . $this->path, array(
                'published' => 0,
            ), array(
                'id' => $Core->getRequest('id'),
            ));
            $this->action = 'view';
        }

        $menu = $Core->render->renderAdminMenu($menu_links);

        switch ($this->path) {

            case 'products':
                $content = $this->products();
                break;

            case 'settings':
                $content = $this->settings();
                break;

            case 'categories':
                $content = $this->cats();
                break;

            case 'manufacturers':
                if (!empty($this->settings['enable_manufacturers'])) {
                    $content = $this->manufacturers();
                }
                break;

            case 'warehouses':
                if (!empty($this->settings['enable_warehouses'])) {
                    $content = $this->warehouses();
                }
                break;

            case 'features':
                if (!empty($this->settings['enable_product_variants'])) {
                    $content = $this->features();
                }
                break;

            case 'feature_variants':
                if (!empty($this->settings['enable_product_variants'])) {
                    $content = $this->featureVariants();
                }
                break;

            case 'orders':
                $content = $this->orders();
                break;

            case 'delivery_services':
                if (!empty($this->settings['enable_delivery'])) {
                    $content = $this->deliveries();
                }
                break;

            case 'payments':
                if (!empty($this->settings['enable_payments'])) {
                    $content = $this->payments();
                }
                break;

            case 'discounts':
                if (!empty($this->settings['enable_discounts'])) {
                    $content = $this->discounts();
                }
                break;

            case 'sales':
                if (!empty($this->settings['enable_sales'])) {
                    $content = $this->sales();
                }
                break;
            /*
            case 'taxes':
            $content = $this->taxes();
            break;*/

            case 'currencies':
                if (!empty($this->settings['enable_multiple_curencies'])) {
                    $content = $this->currencies();
                }
                break;

            case 'order_statuses':
                $content = $this->orderStatuses();
                break;

            case 'ajax':
                die($this->getAjaxResponse());
                break;

            default:
                $content = $this->settings();
                break;

        }

        if (empty($content)) {
            $content = $this->lang['NOTHING_FOUND'];
        }

        return $menu . $content;

    }

    //////////////////////////////////////
    /*private function taxes(){}
     */
    /////////////////////////////////////
    private function settings()
    {

        global $Core;

        if ($Core->inRequest('submit')) {
            if ($Core->updateAppSettings($Core->getRequest('settings'), 'shop')) {
                $Core->sysMessage($this->lang['SAVED'], 'success');
            }
            $this->settings = $Core->getRequest('settings');
        }

        $prepere_settings = array(
            'settings[enable_component]'             => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_component']) ? $this->settings['enable_component'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_component'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_filters]'               => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_filters']) ? $this->settings['enable_filters'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_filters'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_sort]'                  => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_sort']) ? $this->settings['enable_sort'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_sort'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_discounts]'             => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_discounts']) ? $this->settings['enable_discounts'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_discounts'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_payments]'              => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_payments']) ? $this->settings['enable_payments'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_payments'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_delivery]'              => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_delivery']) ? $this->settings['enable_delivery'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_delivery'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_manufacturers]'         => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_manufacturers']) ? $this->settings['enable_manufacturers'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_manufacturers'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_products_in_root_cats]' => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_products_in_root_cats']) ? $this->settings['enable_products_in_root_cats'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_products_in_root_cats'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            /*'settings[enable_taxes]' => array(
            'type'=>'checkbox',
            'value'=> isset($this->settings['enable_taxes'])?$this->settings['enable_taxes']:null,
            'default_value' => 1,
            'attrs'=>array('class'=>'shop-setting'),
            'label'  => $this->lang['enable_taxes'],
            'name_wrap'=>'%name%',
            'input_wrap'=>'%input%',
            'block_wrap'=>'<div class="setting-block checkbox">%block%</div>'
            ),*/
            'settings[enable_partners]'              => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_partners']) ? $this->settings['enable_partners'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_partners'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_sales]'                 => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_sales']) ? $this->settings['enable_sales'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_sales'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_reviews]'               => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_reviews']) ? $this->settings['enable_reviews'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_reviews'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_buy_one_click]'         => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_buy_one_click']) ? $this->settings['enable_buy_one_click'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_buy_one_click'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_compare]'               => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_compare']) ? $this->settings['enable_compare'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_compare'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_quick_buy]'             => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_quick_buy']) ? $this->settings['enable_quick_buy'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_quick_buy'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_warehouses]'            => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_warehouses']) ? $this->settings['enable_warehouses'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_warehouses'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_multiple_curencies]'    => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_multiple_curencies']) ? $this->settings['enable_multiple_curencies'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_multiple_curencies'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_product_rating]'        => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_product_rating']) ? $this->settings['enable_product_rating'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_product_rating'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_visit_rating]'          => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_visit_rating']) ? $this->settings['enable_visit_rating'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_visit_rating'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[enable_product_variants]'      => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['enable_product_variants']) ? $this->settings['enable_product_variants'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['enable_product_variants'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[user_mail_notification]'       => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['user_mail_notification']) ? $this->settings['user_mail_notification'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['user_mail_notification'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[owner_mail_notification]'      => array(
                'type'          => 'checkbox',
                'value'         => isset($this->settings['owner_mail_notification']) ? $this->settings['owner_mail_notification'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting',
                ),
                'label'         => $this->lang['owner_mail_notification'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block checkbox">%block%</div>',
            ),
            'settings[shop_owner_email]'             => array(
                'type'          => 'text',
                'value'         => isset($this->settings['shop_owner_email']) ? $this->settings['shop_owner_email'] : null,
                'default_value' => 'admin@admin.admin',
                'attrs'         => array(
                    'class' => 'shop-setting form-control',
                ),
                'label'         => $this->lang['shop_owner_email'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block">%block%</div>',
            ),
            'settings[shop_name]'                    => array(
                'type'          => 'text',
                'value'         => isset($this->settings['shop_name']) ? $this->settings['shop_name'] : null,
                'default_value' => 'myShop',
                'attrs'         => array(
                    'class' => 'shop-setting form-control',
                ),
                'label'         => $this->lang['shop_name'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block">%block%</div>',
            ),
            'settings[min_order_price]'              => array(
                'type'          => 'text',
                'value'         => isset($this->settings['min_order_price']) ? $this->settings['min_order_price'] : null,
                'default_value' => 0,
                'attrs'         => array(
                    'class' => 'shop-setting form-control',
                ),
                'label'         => $this->lang['min_order_price'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block">%block%</div>',
            ),
            'settings[currency]'                     => array(
                'type'       => 'select',
                'value'      => isset($this->settings['currency']) ? $this->settings['currency'] : null,
                'options'    => $this->shopcore->getCurSelect(),
                'attrs'      => array(
                    'class' => 'shop-setting form-control',
                ),
                'label'      => $this->lang['DEFAULT_CURRENCY'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="setting-block">%block%</div>',
            ),
            'settings[managers_group]'               => array(
                'type'       => 'select',
                'value'      => isset($this->settings['managers_group']) ? $this->settings['managers_group'] : null,
                'options'    => $Core->users->getGroupsSelect(),
                'attrs'      => array(
                    'class' => 'shop-setting form-control',
                ),
                'label'      => $this->lang['MANAGE_GROUP'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="setting-block">%block%</div>',
            ),
            'settings[real_shop_address]'            => array(
                'type'          => 'text',
                'value'         => isset($this->settings['real_shop_address']) ? $this->settings['real_shop_address'] : null,
                'default_value' => 'Rivne',
                'attrs'         => array(
                    'class' => 'shop-setting form-control',
                ),
                'label'         => $this->lang['real_shop_address'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block">%block%</div>',
            ),
            'settings[contact_phones]'               => array(
                'type'          => 'text',
                'value'         => isset($this->settings['contact_phones']) ? $this->settings['contact_phones'] : null,
                'default_value' => '+380668419584,+380111111111',
                'attrs'         => array(
                    'class' => 'shop-setting form-control',
                ),
                'label'         => $this->lang['contact_phones'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block">%block%</div>',
            ),
            'settings[product_thumb_pic_size]'       => array(
                'type'          => 'text',
                'value'         => isset($this->settings['product_thumb_pic_size']) ? $this->settings['product_thumb_pic_size'] : null,
                'default_value' => '150X150',
                'attrs'         => array(
                    'class' => 'shop-setting form-control',
                ),
                'label'         => $this->lang['product_thumb_pic_size'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block">%block%</div>',
            ),
            'settings[product_thumb_medium_size]'    => array(
                'type'          => 'text',
                'value'         => isset($this->settings['product_thumb_medium_size']) ? $this->settings['product_thumb_medium_size'] : null,
                'default_value' => '350X350',
                'attrs'         => array(
                    'class' => 'shop-setting form-control',
                ),
                'label'         => $this->lang['product_thumb_medium_size'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block">%block%</div>',
            ),
            'settings[product_thumb_large_size]'     => array(
                'type'          => 'text',
                'value'         => isset($this->settings['product_thumb_large_size']) ? $this->settings['product_thumb_large_size'] : null,
                'default_value' => '1024X1024',
                'attrs'         => array(
                    'class' => 'shop-setting form-control',
                ),
                'label'         => $this->lang['product_thumb_large_size'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block">%block%</div>',
            ),
            'settings[enable_watch_dogs]'            => array(
                'type'          => 'select',
                'options'       => array(
                    'enable'  => $this->lang['ENABLE'],
                    'disable' => $this->lang['DISABLE'],
                ),
                'value'         => isset($this->settings['enable_watch_dogs']) ? $this->settings['enable_watch_dogs'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting form-control',
                ),
                'label'         => $this->lang['enable_watch_dogs'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block">%block%</div>',
            ),
            'settings[enable_chicks]'                => array(
                'type'          => 'select',
                'options'       => array(
                    'enable'  => $this->lang['ENABLE'],
                    'disable' => $this->lang['DISABLE'],
                ),
                'value'         => isset($this->settings['enable_chicks']) ? $this->settings['enable_chicks'] : null,
                'default_value' => 1,
                'attrs'         => array(
                    'class' => 'shop-setting form-control',
                ),
                'label'         => $this->lang['enable_chicks'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="setting-block">%block%</div>',
            ),
        );

        $inputs_html = $Core->render->renderInputs($prepere_settings, false, $errors);
        return $Core->itemForm($inputs_html, 'shop');

    }

    private function products()
    {

        global $Core;

        ///////////////////////////////////
        $current_path = '/admin?dir=applications&component=shop&path=products';

        if ($this->action == 'getFVariants') {

            $variants = $this->shopcore->getFeatureVariants(array(
                'feature_id' => $Core->getRequest('feature_id'),
            ));
            $product_variants_ids = array();
            if ($product_variants = $this->shopcore->getProductVariants(array(
                'product_id' => $Core->getRequest('id'),
            ))) {
                foreach ($product_variants as $key => $value) {
                    $product_variants_ids[]         = $value['id'];
                    $product_variants[$value['id']] = $value['quantity'];
                }
            }
            $prepere_variants = array();
            foreach ($variants as $variant) {
                $prepere_variants["product[variants][{$variant['id']}]"] = array(
                    'type'       => 'counter',
                    'attrs'      => array(
                        'class'  => 'form-control variants',
                        'style'  => 'width:40px;float:right',
                        'v_id'   => $variant['id'],
                        'v_name' => $variant['title'],
                    ),
                    'value'      => in_array($variant['id'], $product_variants_ids) ? $product_variants[$variant['id']] : 0,
                    'label'      => $variant['title'],
                    'name_wrap'  => '<td>%name%</td>',
                    'input_wrap' => '<td>%input%</td>',
                    'block_wrap' => '<tr class="form-group" style="width:50%">%block%</tr>',
                );
            }
            $data = array(
                'data'       => '
                <table class="table  table-striped">
                <thead><tr><th>' . $this->lang['VARIANTS'] . '</th><th>' . $this->lang['QUANTITY'] . '</th></tr></thead>
                <tbody>
                ' . $Core->render->renderInputs($prepere_variants, false, $errors) . '
                </tbody>
                </table>',
                'title'      => $this->lang['SELECT_VARIANTS'],
                'LANG'       => $this->lang,
                'form_attrs' => array(
                    'id'   => 'product-variants-select',
                    'f_id' => $Core->getRequest('feature_id'),
                ),
            );

            $html = $Core->render->getModal($data);

            $Core->page->addHeadJs('/js/jquery.spinner.min.js');
            $Core->page->addHeadCss('/admin/css/bootstrap-spinner.css');

            $html = $Core->render->replaceAdditionalLinks($html);

            $Core->response($html, 'text/html');

        }

        $sub_menu_links[10]['dir']       = $this->dir;
        $sub_menu_links[10]['component'] = $this->component;
        $sub_menu_links[10]['path']      = 'products';
        $sub_menu_links[10]['action']    = 'add';
        //$sub_menu_links[10]['img'] = "/images/lang-ua.png";
        $sub_menu_links[10]['title'] = $this->lang['ADD_PRODUCT'];

        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        $errors = array();

        if ($this->action == "view") {

            $params = $Core->getFilterParams();

            //////////////////////////////////////////
            $limit       = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;
            $items       = $this->shopcore->getProducts($params, $limit);
            $items_count = $this->shopcore->getProductsCount($params);
            $url         = $this->backLink . '&page={page}';

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'code',
                'title',
                'price',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'        => $this->lang['ID'],
                'code'      => $this->lang['CODE'],
                'title'     => $this->lang['TITLE'],
                'price'     => $this->lang['PRICE'],
                'published' => $this->lang['PUBLISHED'],
                'tools'     => $this->lang['TOOLS'],
            );

            $list['width'] = array(
                'id'        => 70,
                'code'      => 100,
                'title'     => 250,
                'price'     => 100,
                'published' => 160,
            );
            /////////////filter table
            $list['filter'] = array(
                'id'        => $Core->getFilterParamsVal($params, 'id'),
                'code'      => $Core->getFilterParamsVal($params, 'code'),
                'title'     => $Core->getFilterParamsVal($params, 'title'),
                'price'     => $Core->getFilterParamsVal($params, 'price'),
                'published' => $Core->getFilterParamsVal($params, 'published'),
            );

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $url);

            return $sub_menu . $list_html . $pagination_html;

        } elseif ($this->action == "add") {
            if ($Core->inRequest('submit')) {
                $product = $Core->getRequest('product');
                $errors  = $Core->checkFields($product, $this->lang);
                if (count($errors) == 0) {
                    if ($product['id'] = $this->shopcore->addProduct($product)) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($current_path . '&id=' . $product['id']);
                }
            }
            $values = $Core->inRequest('product') ? $Core->getRequest('product') : ($Core->inRequest('id') ? $this->shopcore->getProducts(array(
                'id' => $Core->getRequest('id'),
            ), 1) : array());
            return $sub_menu . $this->renderProdFields($values, $errors);
        } elseif ($this->action == "edit") {
            if ($Core->inRequest('submit')) {
                $product = $Core->getRequest('product');
                $errors  = $Core->checkFields($product, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->updateProduct($product, array(
                        'id' => $Core->getRequest('id'),
                    ))) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }
                }

            }
            $values = $this->shopcore->getProducts(array(
                'id' => $Core->getRequest('id'),
            ), 1);
            return $sub_menu . $this->renderProdFields($values, $errors);
        } elseif ($this->action == "delete") {
            if ($this->shopcore->deleteProduct(array(
                'id' => $Core->getRequest('id'),
            ))) {
                $Core->sysMessage($this->lang['DELETED'], 'success');
            }

            $Core->back();
        }

    }

    private function renderProdFields($values = array(), $errors = array())
    {

        global $Core;

        $lang_variants = array();

        foreach ($Core->getLangsAvailable() as $key => $lang) {
            $lang_variants[$lang['id']] = $lang['title'];
        }

        $cat_select = $this->shopcore->getCatSelect();

        $warehouses_select = $this->shopcore->getWarehousesSelect();

        $manufacturers_select = $this->shopcore->getManufacturersSelect();

        foreach ($values['manufacturers'] as $key => $value) {
            unset($values['manufacturers'][$key]);
            $values['manufacturers'][] = $value['manufacturer_id'];
        }

        foreach ($values['categories'] as $key => $value) {
            unset($values['categories'][$key]);
            $values['categories'][] = $value['category_id'];
        }

        foreach ($values['warehouses'] as $key => $value) {
            unset($values['warehouses'][$key]);
            $values['warehouses'][] = $value['warehouse_id'];
        }

        $fields_to_fill = array(
            'product[code]'             => array(
                'type'          => 'text',
                'default_value' => '',
                'value'         => @$values['code'],
                'attrs'         => array(
                    'placeholder' => $this->lang['CODE'],
                    'class'       => 'shop-input form-control',
                ),
                'label'         => $this->lang['CODE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-product-field ">%block%</div>',
            ),
            'product[title]'            => array(
                'type'          => 'text',
                'default_value' => '',
                'value'         => MultiLang::getValues('title', $values),
                'multilang'     => true,
                'attrs'         => array(
                    'placeholder' => $this->lang['TITLE'],
                    'class'       => 'shop-input form-control',
                ),
                'label'         => $this->lang['TITLE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-product-field ">%block%</div>',
            ),
            'product[description]'      => array(
                'type'       => 'editor',
                'editor_type'=> 'custom',
                'editor_type' => 'custom',
                'value'      => MultiLang::getValues('description', $values),
                'multilang'  => true,
                'attrs'      => array(
                    'class' => 'shop-input',
                ),
                'label'      => $this->lang['DESCRIPTION'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-product-field">%block%</div>',
            ),
            'product[categories]'       => array(
                'type'       => 'multiselect_two_sides',
                'options'    => $cat_select,
                'value'      => @$values['categories'],
                'attrs'      => array(
                    'class' => 'shop-input form-control',
                ),
                'label'      => $this->lang['CATEGORIES'],
                'id'         => 'product_cats',
                'required'   => true,
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-product-field ">%block%</div>',
            ),
            'product[warehouses]'       => array(
                'type'       => 'multiselect_two_sides',
                'options'    => $warehouses_select,
                'value'      => @$values['warehouses'],
                'attrs'      => array(
                    'class' => 'shop-input form-control',
                ),
                'label'      => $this->lang['WAREHOUSES'],
                'id'         => 'product_warehouses',
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-product-field">%block%</div>',
            ),
            'product[manufacturers]'    => array(
                'type'       => 'multiselect_two_sides',
                'options'    => $manufacturers_select,
                'value'      => @$values['manufacturers'],
                'attrs'      => array(
                    'class' => 'shop-input form-control',
                ),
                'label'      => $this->lang['MANUFACTURERS'],
                'id'         => 'product_manufacturers',
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-product-field">%block%</div>',
            ),
            'product[price]'            => array(
                'type'          => 'text',
                'default_value' => 0,
                'value'         => @$values['price'],
                'attrs'         => array(
                    'class'       => 'shop-input form-control',
                    'placeholder' => $this->lang['PRICE'],
                ),
                'label'         => $this->lang['PRICE'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-product-field">%block%</div>',
            ),
            'product[sale_price]'       => array(
                'type'          => 'text',
                'default_value' => 0,
                'value'         => @$values['sale_price'],
                'attrs'         => array(
                    'class'       => 'shop-input form-control',
                    'placeholder' => $this->lang['SALE_PRICE'],
                ),
                'label'         => $this->lang['SALE_PRICE'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-product-field">%block%</div>',
            ),
            'product[purchase_price]'   => array(
                'type'          => 'text',
                'default_value' => 0,
                'value'         => @$values['purchase_price'],
                'attrs'         => array(
                    'class'       => 'shop-input form-control',
                    'placeholder' => $this->lang['PURCHASE_PRICE'],
                ),
                'label'         => $this->lang['PURCHASE_PRICE'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-product-field">%block%</div>',
            ),
            'product[images]'           => array(
                'type'       => 'images',
                'attrs'      => array(
                    'class' => 'shop-input btn btn-success',
                ),
                'value'      => @$values['images'],
                'label'      => $this->lang['PRODUCT_IMAGES'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-product-field">%block%</div>',
            ),
            'product[meta_keywords]'    => array(
                'type'       => 'textarea',
                'attrs'      => array(
                    'class' => 'shop-input form-control',
                ),
                'value'      => MultiLang::getValues('meta_keywords', $values),
                'multilang'  => true,
                'label'      => $this->lang['META_KEYWORDS'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-product-field">%block%</div>',
            ),
            'product[meta_description]' => array(
                'type'       => 'textarea',
                'attrs'      => array(
                    'class' => 'shop-input form-control',
                ),
                'value'      => MultiLang::getValues('meta_description', $values),
                'multilang'  => true,
                'label'      => $this->lang['META_DESCRIPTION'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-product-field">%block%</div>',
            ),
            /*'product[quantity]' => array(
            'type'=>'text',
            'attrs'=>array('placeholder'=>$this->lang['QUANTITY'],'class'=>'shop-input form-control'),
            'default_value' => 0,
            'value' => @$values['quantity'],
            'label'  => $this->lang['QUANTITY'],
            'name_wrap'=>'%name%',
            'input_wrap'=>'%input%',
            'block_wrap'=>'<div class="shop-product-field">%block%</div>'
            ),*/
            'product[published]'        => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'default_value' => 1,
                'value'         => @$values['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '<input type="hidden" name="product[published]" value="0" />%input%',
                'block_wrap'    => '<div class="shop-product-field  checkbox">%block%</div>',
            ),
        );

        $inputs_html = $Core->render->renderInputs(array_merge(array_slice($fields_to_fill, 0, 7), $this->renderPVariants($values['id']), array_slice($fields_to_fill, 7, count($fields_to_fill) - 1)), false, $errors);
        return $Core->itemForm($inputs_html, 'shop');

    }

    private function renderPVariants($p_id, $field = 'product')
    {
        foreach ($this->shopcore->getFeatures() as $key => $feature) {
            $fields_to_fill[$field . "[feature_{$feature['id']}]"] = array(
                'type'       => 'button',
                'attrs'      => array(
                    'class'             => 'btn btn-primary feature',
                    'data-loading-text' => 'Wait! loading.',
                    'autocomplete'      => 'off',
                    'feature_id'        => $feature['id'],
                ),
                'label'      => $feature['title'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-product-field">%block%<div id="product_variants_' . $feature['id'] . '">' . $this->renderSVariants($p_id, $feature['id']) . '</div></div>',
            );
        }

        return $fields_to_fill;

    }

    private function renderSVariants($p_id, $f_id)
    {

        $variants = $this->shopcore->getProductVariants(array(
            'product_id' => $p_id,
            'feature_id' => $f_id,
        ));

        $html = '';

        foreach ($variants as $key => $value) {
            $html .= '<kbd><kbd>' . $value['title'] . '</kbd><span class="badge">' . $value['quantity'] . '</span><input type="hidden" value="' . $value['quantity'] . '" name="product[variants][' . $value['id'] . ']"></kbd>';
        }

        return $html;

    }

    private function cats()
    {

        global $Core;

        $sub_menu_links[10]['dir']       = $this->dir;
        $sub_menu_links[10]['component'] = $this->component;
        $sub_menu_links[10]['path']      = 'categories';
        $sub_menu_links[10]['action']    = 'add';
        //$sub_menu_links[10]['img'] = "/images/lang-ua.png";
        $sub_menu_links[10]['title'] = $this->lang['ADD_CATEGORY'];

        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        $errors = array();

        if ($this->action == "view") {

            $params = $Core->getFilterParams();

            $parent_id        = 0;
            $limit            = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;
            $categories       = $this->shopcore->getCategories(array(), $limit);
            $categories_count = $this->shopcore->getCategoriesCountByParentId($parent_id);
            $url              = $this->backLink . '&page={page}';

            $list['items'] = $categories;
            $list['keys']  = array(
                'id',
                'title',
                'seo_url',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'        => $this->lang['ID'],
                'title'     => $this->lang['TITLE'],
                'seo_url'   => $this->lang['SEO_URL'],
                'published' => $this->lang['PUBLISHED'],
                'tools'     => $this->lang['TOOLS'],
            );
            /////////////filter table
            $list['filter'] = array(
                'id'        => $Core->getFilterParamsVal($params, 'id'),
                'title'     => $Core->getFilterParamsVal($params, 'title'),
                'seo_url'   => $Core->getFilterParamsVal($params, 'seo_url'),
                'published' => $Core->getFilterParamsVal($params, 'published'),
                'tools'     => $Core->getFilterParamsVal($params, 'tools'),
            );

            $current_path = '/admin?dir=applications&component=shop&path=categories';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($categories_count, $this->on_page, $this->page, $url);

            return $sub_menu . $list_html . $pagination_html;

        } elseif ($this->action == "add") {
            if ($Core->inRequest('submit')) {
                $category = $Core->getRequest('category');
                $errors   = $Core->checkFields($category, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->addCategory($category)) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }
                }

            }
            $values = $Core->inRequest('category') ? $Core->getRequest('category') : ($Core->inRequest('id') ? $this->shopcore->getCategories(array(
                'id' => $Core->getRequest('id'),
            ), 1) : array());
            return $sub_menu . $this->renderCatFields($values, $errors);
        } elseif ($this->action == "edit") {
            if ($Core->inRequest('submit')) {
                $category = $Core->getRequest('category');
                $errors   = $Core->checkFields($category, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->updateCategory($category, array(
                        'id' => $Core->getRequest('id'),
                    ))) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }
                }

            }
            $values = $Core->inRequest('category') ? $Core->getRequest('category') : $this->shopcore->getCategories(array(
                'id' => $Core->getRequest('id'),
            ), 1);
            return $sub_menu . $this->renderCatFields($values, $errors);
        } elseif ($this->action == "delete") {
            if ($this->shopcore->deleteCategories(array(
                'id' => $Core->getRequest('id'),
            ))) {
                $Core->sysMessage($this->lang['DELETED'], 'success');
            }

            $Core->back();
        }

    }

    private function renderCatFields($values, $errors = array())
    {

        global $Core;

        foreach ($Core->getLangsAvailable() as $key => $lang) {
            $lang_variants[$lang['id']] = $lang['title'];
        }

        $cat_select = array();

        foreach ($this->shopcore->getCategories() as $cat) {
            $cat_select[$cat['id']] = $cat['title'];
        }

        unset($cat_select[$Core->getRequest('id')]);

        $fields_to_fill = array(
            'category[title]'            => array(
                'type'          => 'text',
                'default_value' => '',
                'value'      => MultiLang::getValues('title', $values),
                'multilang'  => true,
                'attrs'         => array(
                    'placeholder' => $this->lang['TITLE'],
                    'class'       => 'shop-input form-control',
                ),
                'label'         => $this->lang['TITLE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-category-field ">%block%</div>',
            ),
            'category[description]'      => array(
                'type'       => 'editor',
                'editor_type'=> 'custom',
                'editor_type' => 'custom',
                'value'      => MultiLang::getValues('description', $values),
                'multilang'  => true,
                'attrs'      => array(
                    'class' => 'shop-input',
                ),
                'label'      => $this->lang['DESCRIPTION'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-category-field">%block%</div>',
            ),
            'category[parent_id]'        => array(
                'type'       => 'select',
                'options'    => $cat_select,
                'multiple'   => false,
                'value'      => @$values['parent_id'],
                'attrs'      => array(
                    'class' => 'shop-input form-control',
                ),
                'label'      => $this->lang['PARENT_CAT'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-product-field ">%block%</div>',
            ),
            'category[images]'           => array(
                'type'       => 'images',
                'attrs'      => array(
                    'class' => 'shop-input btn btn-success',
                ),
                'value'      => @$values['images'],
                'label'      => $this->lang['PRODUCT_IMAGES'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-category-field">%block%</div>',
            ),
            'category[seo_url]'          => array(
                'type'          => 'text',
                'default_value' => '',
                'value'         => @$values['seo_url'],
                'attrs'         => array(
                    'placeholder' => $this->lang['SEO_URL'],
                    'class'       => 'shop-input form-control',
                ),
                'label'         => $this->lang['SEO_URL'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-category-field ">%block%</div>',
            ),
            'category[meta_keywords]'    => array(
                'type'       => 'textarea',
                'attrs'      => array(
                    'class' => 'shop-input form-control',
                ),
                'value'      => MultiLang::getValues('meta_keywords', $values),
                'multilang'  => true,
                'label'      => $this->lang['META_KEYWORDS'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-category-field">%block%</div>',
            ),
            'category[meta_description]' => array(
                'type'       => 'textarea',
                'attrs'      => array(
                    'class' => 'shop-input form-control',
                ),
                'value'      => MultiLang::getValues('meta_description', $values),
                'multilang'  => true,
                'label'      => $this->lang['META_DESCRIPTION'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-category-field">%block%</div>',
            ),
            'category[published]'        => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'default_value' => 1,
                'value'         => @$values['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '<input type="hidden" name="category[published]" value="0" />%input%',
                'block_wrap'    => '<div class="shop-category-field checkbox">%block%</div>',
            ),
        );

        $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
        return $Core->itemForm($inputs_html, 'shop');

    }

    private function manufacturers()
    {

        global $Core;

        $sub_menu_links[10]['dir']       = $this->dir;
        $sub_menu_links[10]['component'] = $this->component;
        $sub_menu_links[10]['path']      = 'manufacturers';
        $sub_menu_links[10]['action']    = 'add';
        //$sub_menu_links[10]['img'] = "/images/lang-ua.png";
        $sub_menu_links[10]['title'] = $this->lang['ADD_MANUFACTURER'];

        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        $errors = array();

        if ($this->action == 'view') {

            $params      = $Core->getFilterParams();
            $limit       = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;
            $items       = $this->shopcore->getManufacturers($params, $limit);
            $items_count = $this->shopcore->getManufacturersCount($params);
            $url         = $this->backLink . '&page={page}';

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'description',
                'country',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'          => $this->lang['ID'],
                'title'       => $this->lang['TITLE'],
                'description' => $this->lang['DESCRIPTION'],
                'country'     => $this->lang['COUNTRY'],
                'published'   => $this->lang['PUBLISHED'],
                'tools'       => $this->lang['TOOLS'],
            );
            /////////////filter table
            $list['filter'] = array(
                'id'          => $Core->getFilterParamsVal($params, 'id'),
                'title'       => $Core->getFilterParamsVal($params, 'title'),
                'description' => $Core->getFilterParamsVal($params, 'description'),
                'country'     => $Core->getFilterParamsVal($params, 'country'),
                'published'   => $Core->getFilterParamsVal($params, 'published'),
                'tools'       => $Core->getFilterParamsVal($params, 'tools'),
            );

            $current_path = '/admin?dir=applications&component=shop&path=manufacturers';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $url);

            return $sub_menu . $list_html . $pagination_html;
        } elseif ($this->action == 'add') {
            if ($Core->inRequest('submit')) {
                $manufacturer = $Core->getRequest('manufacturer');
                $errors       = $Core->checkFields($manufacturer, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->addManufacturer($manufacturer)) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }
                }

            }
            $values = $Core->inRequest('manufacturer') ? $Core->getRequest('manufacturer') : ($Core->inRequest('id') ? $this->shopcore->getManufacturers(array(
                'id' => $Core->getRequest('id'),
            ), 1) : array());
            return $sub_menu . $this->renderManFields($values, $errors);
        } elseif ($this->action == 'edit') {
            if ($Core->inRequest('submit')) {
                $manufacturer = $Core->getRequest('manufacturer');
                $errors       = $Core->checkFields($manufacturer, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->updateManufacturer($manufacturer, array(
                        'id' => $Core->getRequest('id'),
                    ))) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }
                }

            }
            $values = $Core->inRequest('manufacturer') ? $Core->getRequest('manufacturer') : $this->shopcore->getManufacturers(array(
                'id' => $Core->getRequest('id'),
            ), 1);
            return $sub_menu . $this->renderManFields($values, $errors);
        } elseif ($this->action == 'delete') {
            if ($this->shopcore->deleteManufacturers(array(
                'id' => $Core->getRequest('id'),
            ))) {
                $Core->sysMessage($this->lang['DELETED'], 'success');
            }

            $Core->back();
        }

    }

    private function renderManFields($values, $errors = array())
    {

        global $Core;

        foreach ($Core->getLangsAvailable() as $key => $lang) {
            $lang_variants[$lang['id']] = $lang['title'];
        }

        $fields_to_fill = array(
            'manufacturer[title]'            => array(
                'type'          => 'text',
                'default_value' => '',
                'value'      => MultiLang::getValues('title', $values),
                'multilang'  => true,
                'attrs'         => array(
                    'placeholder' => $this->lang['TITLE'],
                    'class'       => 'shop-input form-control',
                ),
                'label'         => $this->lang['TITLE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-manufacturer-field ">%block%</div>',
            ),
            'manufacturer[description]'      => array(
                'type'       => 'editor',
                'editor_type'=> 'custom',
                'value'      => MultiLang::getValues('description', $values),
                'multilang'  => true,
                'attrs'      => array(
                    'class' => 'shop-input',
                ),
                'label'      => $this->lang['DESCRIPTION'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-manufacturer-field">%block%</div>',
            ),
            'manufacturer[images]'           => array(
                'type'       => 'images',
                'attrs'      => array(
                    'class' => 'shop-input btn btn-success',
                ),
                'value'      => @$values['images'],
                'label'      => $this->lang['PRODUCT_IMAGES'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-manufacturer-field">%block%</div>',
            ),
            'manufacturer[seo_url]'          => array(
                'type'          => 'text',
                'default_value' => '',
                'value'         => @$values['seo_url'],
                'attrs'         => array(
                    'placeholder' => $this->lang['SEO_URL'],
                    'class'       => 'shop-input form-control',
                ),
                'label'         => $this->lang['SEO_URL'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-manufacturer-field ">%block%</div>',
            ),
            'manufacturer[meta_keywords]'    => array(
                'type'       => 'textarea',
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'value'      => MultiLang::getValues('meta_keywords', $values),
                'multilang'  => true,
                'label'      => $this->lang['META_KEYWORDS'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-manufacturer-field">%block%</div>',
            ),
            'manufacturer[meta_description]' => array(
                'type'       => 'textarea',
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'value'      => MultiLang::getValues('meta_description', $values),
                'multilang'  => true,
                'label'      => $this->lang['META_DESCRIPTION'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-manufacturer-field">%block%</div>',
            ),
            'manufacturer[country]'          => array(
                'type'          => 'text',
                'default_value' => '',
                'value'         => @$values['seo_url'],
                'attrs'         => array(
                    'placeholder' => $this->lang['COUNTRY'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'         => $this->lang['COUNTRY'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-manufacturer-field ">%block%</div>',
            ),
            'manufacturer[published]'        => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'value'         => @$values['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-manufacturer-field checkbox">%block%</div>',
            ),
        );

        $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
        return $Core->itemForm($inputs_html, 'shop');
    }

    private function features()
    {

        global $Core;

        $sub_menu_links[10]['dir']       = $this->dir;
        $sub_menu_links[10]['component'] = $this->component;
        $sub_menu_links[10]['path']      = 'features';
        $sub_menu_links[10]['action']    = 'add';
        //$sub_menu_links[10]['img'] = "/images/lang-ua.png";
        $sub_menu_links[10]['title'] = $this->lang['ADD_FEATURE'];

        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        $errors = array();

        if ($this->action == "view") {

            $params = $Core->getFilterParams();

            $limit       = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;
            $items       = $this->shopcore->getFeatures($params, $limit);
            $items_count = $this->shopcore->getFeaturesCount($params);

            $url = $this->backLink . '&page={page}';

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'description',
                'type',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'          => $this->lang['ID'],
                'title'       => $this->lang['TITLE'],
                'description' => $this->lang['DESCRIPTION'],
                'type'        => $this->lang['TYPE'],
                'published'   => $this->lang['PUBLISHED'],
                'tools'       => $this->lang['TOOLS'],
            );
            /////////////filter table
            $list['filter'] = array(
                'id'          => $Core->getFilterParamsVal($params, 'id'),
                'title'       => $Core->getFilterParamsVal($params, 'title'),
                'description' => $Core->getFilterParamsVal($params, 'description'),
                'type'        => $Core->getFilterParamsVal($params, 'type'),
                'published'   => $Core->getFilterParamsVal($params, 'published'),
                'tools'       => $Core->getFilterParamsVal($params, 'tools'),
            );

            $current_path = '/admin?dir=applications&component=shop&path=features';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $url);

            return $sub_menu . $list_html . $pagination_html;

        } elseif ($this->action == "add") {
            if ($Core->inRequest('submit')) {
                $feature = $Core->getRequest('feature');
                $errors  = $Core->checkFields($feature, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->addFeature($feature)) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }
                }

            }
            $values = $Core->inRequest('feature') ? $Core->getRequest('feature') : ($Core->inRequest('id') ? $this->shopcore->getFeatures(array(
                'id' => $Core->getRequest('id'),
            ), 1) : array());
            return $sub_menu . $this->renderFeatureFields($values, $errors);
        } elseif ($this->action == "edit") {
            if ($Core->inRequest('submit')) {
                $feature = $Core->getRequest('feature');
                $errors  = $Core->checkFields($feature, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->updateFeature($feature, array(
                        'id' => $Core->getRequest('id'),
                    ))) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }
                }

            }
            $values = $Core->inRequest('feature') ? $Core->getRequest('feature') : $this->shopcore->getFeatures(array(
                'id' => $Core->getRequest('id'),
            ), 1);
            return $sub_menu . $this->renderFeatureFields($values, $errors);
        } elseif ($this->action == "delete") {
            if ($this->shopcore->deleteFeatures(array(
                'id' => $Core->getRequest('id'),
            ))) {
                $Core->sysMessage($this->lang['DELETED'], 'success');
            }

            $Core->back();
        }

    }

    private function renderFeatureFields($values, $errors = array())
    {

        global $Core;

        $cat_select = array();

        foreach ($this->shopcore->getCategories() as $cat) {
            $cat_select[$cat['id']] = $cat['title'];
        }

        /*
        $manufacturers_select = array();

        foreach ($this->shopcore->getManufacturers() as $mf) {
        $manufacturers_select[$mf['id']] = $mf['title'];
        }

        $warehouses_select = array();

        foreach($this->shopcore->getWarehouses() as $wh){
        $warehouses_select[$wh['id']] = $wh['title'];
        }
         */
        $feature_types = array(
            'checkbox' => 'checkbox',
            'select'   => 'select',
            'radio'    => 'radio',
        );

        $fields_to_fill = array(
            'feature[title]'       => array(
                'type'          => 'text',
                'default_value' => '',
                'value'      => MultiLang::getValues('title', $values),
                'multilang'  => true,
                'attrs'         => array(
                    'placeholder' => $this->lang['TITLE'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'         => $this->lang['TITLE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-feature-field ">%block%</div>',
            ),
            'feature[description]' => array(
                'type'       => 'editor',
                'editor_type'=> 'custom',
                'value'      => MultiLang::getValues('description', $values),
                'multilang'  => true,
                'attrs'      => array(
                    'class' => 'shop-input',
                ),
                'label'      => $this->lang['DESCRIPTION'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-feature-field">%block%</div>',
            ),
            'feature[type]'        => array(
                'type'       => 'select',
                'options'    => $feature_types,
                'multiple'   => false,
                'value'      => @$values['type'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'label'      => $this->lang['TYPE'],
                'required'   => true,
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-feature-field ">%block%</div>',
            ),
            'feature[images]'      => array(
                'type'       => 'images',
                'attrs'      => array(
                    'class' => 'shop-input btn btn-success',
                ),
                'value'      => @$values['image'],
                'label'      => $this->lang['IMAGE'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-feature-field ">%block%</div>',
            ),
            'feature[priority]'    => array(
                'type'          => 'text',
                'default_value' => '',
                'value'         => @$values['priority'],
                'attrs'         => array(
                    'placeholder' => $this->lang['PRIORITY'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'         => $this->lang['PRIORITY'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-feature-field ">%block%</div>',
            ),
            'feature[required]'    => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'default_value' => 1,
                'value'         => @$values['required'],
                'label'         => $this->lang['REQUIRED_FIELD'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-feature-field checkbox">%block%</div>',
            ),
            'feature[filter]'      => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'default_value' => 1,
                'value'         => @$values['required'],
                'label'         => $this->lang['SHOW_IN_FILTER'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-feature-field checkbox">%block%</div>',
            ),
            'feature[published]'   => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'value'         => @$values['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-feature-field checkbox">%block%</div>',
            ),
        );

        $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
        return $Core->itemForm($inputs_html, 'shop');
    }

    private function featureVariants()
    {

        global $Core;

        $sub_menu_links[10]['dir']       = $this->dir;
        $sub_menu_links[10]['component'] = $this->component;
        $sub_menu_links[10]['path']      = 'feature_variants';
        $sub_menu_links[10]['action']    = 'add';
        //$sub_menu_links[10]['img'] = "/images/lang-ua.png";
        $sub_menu_links[10]['title'] = $this->lang['ADD_FEATURE_VARIANT'];

        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        $errors = array();

        if ($this->action == "view") {

            $params = $Core->getFilterParams();

            $limit       = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;
            $items       = $this->shopcore->getFeatureVariants($params, $limit);
            $items_count = $this->shopcore->getFeatureVariantsCount($params);

            $url = $this->backLink . '&page={page}';

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'feature',
                'title',
                'description',
                'type',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'          => $this->lang['ID'],
                'feature'     => $this->lang['FEATURE'],
                'title'       => $this->lang['TITLE'],
                'description' => $this->lang['DESCRIPTION'],
                'type'        => $this->lang['TYPE'],
                'published'   => $this->lang['PUBLISHED'],
                'tools'       => $this->lang['TOOLS'],
            );
            /////////////filter table
            $list['filter'] = array(
                'id'          => $Core->getFilterParamsVal($params, 'id'),
                'feature'     => $Core->getFilterParamsVal($params, 'feature'),
                'title'       => $Core->getFilterParamsVal($params, 'title'),
                'description' => $Core->getFilterParamsVal($params, 'description'),
                'type'        => $Core->getFilterParamsVal($params, 'type'),
                'published'   => $Core->getFilterParamsVal($params, 'published'),
                'tools'       => $Core->getFilterParamsVal($params, 'tools'),
            );

            $current_path = '/admin?dir=applications&component=shop&path=feature_variants';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $url);

            return $sub_menu . $list_html . $pagination_html;

        } elseif ($this->action == "add") {
            if ($Core->inRequest('submit')) {
                $featurevariant = $Core->getRequest('featurevariant');
                $errors         = $Core->checkFields($featurevariant, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->addFeatureVariant($featurevariant)) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $Core->inRequest('featurevariant') ? $Core->getRequest('featurevariant') : ($Core->inRequest('id') ? $this->shopcore->getFeatureVariants(array(
                'id' => $Core->getRequest('id'),
            ), 1) : array());
            return $sub_menu . $this->renderFeatureVariantFields($values, $errors);
        } elseif ($this->action == "edit") {
            if ($Core->inRequest('submit')) {
                $featurevariant = $Core->getRequest('featurevariant');
                $errors         = $Core->checkFields($featurevariant, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->updateFeatureVariant($featurevariant, array(
                        'id' => $Core->getRequest('id'),
                    ))) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $this->shopcore->getFeatureVariants(array(
                'id' => $Core->getRequest('id'),
            ), 1);
            return $sub_menu . $this->renderFeatureVariantFields($values, $errors);
        } elseif ($this->action == "delete") {
            if ($this->shopcore->deleteFeatureVariants(array(
                'id' => $Core->getRequest('id'),
            ))) {
                $Core->sysMessage($this->lang['DELETED'], 'success');
            }

            $Core->back();
        }

    }

    private function renderFeatureVariantFields($values, $errors = array())
    {

        global $Core;

        $feature_select = array();

        foreach ($this->shopcore->getFeatures() as $f) {
            $feature_select[$f['id']] = $f['title'];
        }

        $fields_to_fill = array(
            'featurevariant[title]'       => array(
                'type'          => 'text',
                'default_value' => '',
                'value'      => MultiLang::getValues('title', $values),
                'multilang'  => true,
                'attrs'         => array(
                    'placeholder' => $this->lang['TITLE'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'         => $this->lang['TITLE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-featurevariant-field ">%block%</div>',
            ),
            'featurevariant[description]' => array(
                'type'       => 'editor',
                'editor_type'=> 'custom',
                'value'      => MultiLang::getValues('description', $values),
                'multilang'  => true,
                'attrs'      => array(
                    'class' => 'shop-input',
                ),
                'label'      => $this->lang['DESCRIPTION'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-featurevariant-field">%block%</div>',
            ),
            'featurevariant[mod_value]'   => array(
                'type'          => 'counter',
                'value'         => @$values['mod_value'],
                'default_value' => 0,
                'attrs'         => array(
                    'class' => 'shop-input  form-control',
                ),
                'label'         => $this->lang['MOD_VALUE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-featurevariant-field  form-inline" style="margin:10px;margin-left:0">%block%',
            ),
            'featurevariant[mod_type]'    => array(
                'type'       => 'select',
                'value'      => @$values['mod_type'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'options'    => array(
                    '%'        => $this->lang['PERCENTAGE'],
                    'currency' => $this->lang['CURRENCY_VALUES'],
                ),
                'label'      => $this->lang['MOD_TYPE'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '%block%</div>',
            ),
            'featurevariant[feature_id]'  => array(
                'type'       => 'select',
                'options'    => $feature_select,
                'multiple'   => false,
                'value'      => @$values['feature_id'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'label'      => $this->lang['FEATURES'],
                'required'   => true,
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-featurevariant-field ">%block%</div>',
            ),
            'featurevariant[published]'   => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'value'         => @$values['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-featurevariant-field checkbox">%block%</div>',
            ),
        );

        $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
        return $Core->itemForm($inputs_html, 'shop');
    }

    private function deliveries()
    {

        global $Core;

        $sub_menu_links[10]['dir']       = $this->dir;
        $sub_menu_links[10]['component'] = $this->component;
        $sub_menu_links[10]['path']      = 'delivery_services';
        $sub_menu_links[10]['action']    = 'add';
        //$sub_menu_links[10]['img'] = "/images/lang-ua.png";
        $sub_menu_links[10]['title'] = $this->lang['ADD_DELIVERY_SERVICE'];

        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        $errors = array();

        if ($this->action == "view") {

            $params = $Core->getFilterParams();

            $limit       = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;
            $items       = $this->shopcore->getDeliveryServices($params, $limit);
            $items_count = $this->shopcore->getDeliveryServicesCount($params);

            $url = $this->backLink . '&page={page}';

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'description',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'          => $this->lang['ID'],
                'title'       => $this->lang['TITLE'],
                'description' => $this->lang['DESCRIPTION'],
                'published'   => $this->lang['PUBLISHED'],
                'tools'       => $this->lang['TOOLS'],
            );

            /////////////filter table
            $list['filter'] = array(
                'id'          => $Core->getFilterParamsVal($params, 'id'),
                'title'       => $Core->getFilterParamsVal($params, 'title'),
                'description' => $Core->getFilterParamsVal($params, 'description'),
                'published'   => $Core->getFilterParamsVal($params, 'published'),
                'tools'       => $Core->getFilterParamsVal($params, 'tools'),
            );

            $current_path = '/admin?dir=applications&component=shop&path=delivery_services';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $url);

            return $sub_menu . $list_html . $pagination_html;

        } elseif ($this->action == "add") {
            if ($Core->inRequest('submit')) {
                $delivery = $Core->getRequest('delivery');
                $errors   = $Core->checkFields($delivery, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->addDeliveryService($delivery)) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $Core->inRequest('delivery') ? $Core->getRequest('delivery') : ($Core->inRequest('id') ? $this->shopcore->getDeliveryServices(array(
                'id' => $Core->getRequest('id'),
            ), 1) : array());
            return $sub_menu . $this->renderDeliveryFields($values, $errors);
        } elseif ($this->action == "edit") {
            if ($Core->inRequest('submit')) {
                $delivery = $Core->getRequest('delivery');
                $errors   = $Core->checkFields($delivery, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->updateDeliveryServices($delivery, array(
                        'id' => $Core->getRequest('id'),
                    ))) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $Core->inRequest('delivery') ? $Core->getRequest('delivery') : $this->shopcore->getDeliveryServices(array(
                'id' => $Core->getRequest('id'),
            ), 1);
            return $sub_menu . $this->renderDeliveryFields($values, $errors);
        } elseif ($this->action == "delete") {
            if ($this->shopcore->deleteDeliveries(array(
                'id' => $Core->getRequest('id'),
            ))) {
                $Core->sysMessage($this->lang['DELETED'], 'success');
            }

            $Core->back();
        }

    }

    private function renderDeliveryFields($values, $errors = array())
    {

        global $Core;

        $script_checkbox = $Core->render->renderInputs(array(
            "delivery[use_script]" => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'value'         => @$values['use_script'],
                'label'         => $this->lang['DELIVERY_USE_SCRIPT'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-delivery-field checkbox">%block%</div>',
            ),
        ));

        $fields_to_fill = array(
            'delivery[title]'       => array(
                'type'          => 'text',
                'default_value' => '',
                'value'      => MultiLang::getValues('title', $values),
                'multilang'  => true,
                'attrs'         => array(
                    'placeholder' => $this->lang['TITLE'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'         => $this->lang['TITLE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-delivery-field ">%block%</div>',
            ),
            'delivery[description]' => array(
                'type'       => 'editor',
                'editor_type'=> 'custom',
                'value'      => MultiLang::getValues('description', $values),
                'multilang'  => true,
                'attrs'      => array(
                    'class' => 'shop-input',
                ),
                'label'      => $this->lang['DESCRIPTION'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-delivery-field">%block%</div>',
            ),
            'delivery[prices]'      => array(
                'type'       => 'textarea',
                'value'      => @$values['prices'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'label'      => $this->lang['DELIVERY_PRICES'] . '<br>' . $this->lang['DELIVERY_PRICES_INFO'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-delivery-field">%block%</div>',
            ),
            'delivery[script_path]' => array(
                'type'       => 'text',
                'value'      => @$values['script_path'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'label'      => $this->lang['DELIVERY_SCRIPT_PATH'],
                'name_wrap'  => '%name%',
                'input_wrap' => $script_checkbox . '%input%',
                'block_wrap' => '<div class="shop-delivery-field">%block%</div>',
            ),
            'delivery[published]'   => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'value'         => @$values['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-delivery-field checkbox">%block%</div>',
            ),
        );
        $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
        return $Core->itemForm($inputs_html, 'shop');
    }

    private function payments()
    {

        global $Core;

        $sub_menu_links[10]['dir']       = $this->dir;
        $sub_menu_links[10]['component'] = $this->component;
        $sub_menu_links[10]['path']      = 'payments';
        $sub_menu_links[10]['action']    = 'add';
        //$sub_menu_links[10]['img'] = "/images/lang-ua.png";
        $sub_menu_links[10]['title'] = $this->lang['ADD_PAYMENT_SERVICE'];

        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        $errors = array();

        if ($this->action == "view") {

            $params = $Core->getFilterParams();

            $limit       = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;
            $items       = $this->shopcore->getPayments($params, $limit);
            $items_count = $this->shopcore->getPaymentsCount($params);

            $url = $this->backLink . '&page={page}';

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'description',
                'file',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'          => $this->lang['ID'],
                'title'       => $this->lang['TITLE'],
                'description' => $this->lang['DESCRIPTION'],
                'file'        => $this->lang['FILE'],
                'published'   => $this->lang['PUBLISHED'],
                'tools'       => $this->lang['TOOLS'],
            );
            /////////////filter table
            $list['filter'] = array(
                'id'          => $Core->getFilterParamsVal($params, 'id'),
                'title'       => $Core->getFilterParamsVal($params, 'title'),
                'description' => $Core->getFilterParamsVal($params, 'description'),
                'file'        => $Core->getFilterParamsVal($params, 'file'),
                'published'   => $Core->getFilterParamsVal($params, 'published'),
                'tools'       => $Core->getFilterParamsVal($params, 'tools'),
            );

            $current_path = '/admin?dir=applications&component=shop&path=payments';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $url);

            return $sub_menu . $list_html . $pagination_html;

        } elseif ($this->action == "add") {
            if ($Core->inRequest('submit')) {
                $payment = $Core->getRequest('payment');
                $errors  = $Core->checkFields($payment, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->addPayment($payment)) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $Core->inRequest('payment') ? $Core->getRequest('payment') : ($Core->inRequest('id') ? $this->shopcore->getPayments(array(
                'id' => $Core->getRequest('id'),
            ), 1) : array());
            return $sub_menu . $this->renderPaymentFields($values, $errors);
        } elseif ($this->action == "edit") {
            if ($Core->inRequest('submit')) {
                $payment = $Core->getRequest('payment');
                $errors  = $Core->checkFields($payment, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->updatePayments($payment, array(
                        'id' => $Core->getRequest('id'),
                    ))) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $Core->inRequest('payment') ? $Core->getRequest('payment') : $this->shopcore->getPayments(array(
                'id' => $Core->getRequest('id'),
            ), 1);
            return $sub_menu . $this->renderPaymentFields($values, $errors);
        } elseif ($this->action == "delete") {
            if ($this->shopcore->deletePayments(array(
                'id' => $Core->getRequest('id'),
            ))) {
                $Core->sysMessage($this->lang['DELETED'], 'success');
            }

            $Core->back();
        }

    }

    private function renderPaymentFields($values, $errors = array())
    {

        global $Core;

        $fields_to_fill = array(
            'payment[title]'       => array(
                'type'          => 'text',
                'default_value' => '',
                'value'      => MultiLang::getValues('title', $values),
                'multilang'  => true,
                'attrs'         => array(
                    'placeholder' => $this->lang['TITLE'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'         => $this->lang['TITLE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-payment-field ">%block%</div>',
            ),
            'payment[description]' => array(
                'type'       => 'editor',
                'editor_type'=> 'custom',
                'value'      => MultiLang::getValues('description', $values),
                'multilang'  => true,
                'attrs'      => array(
                    'class' => 'shop-input',
                ),
                'label'      => $this->lang['DESCRIPTION'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-payment-field">%block%</div>',
            ),
            'payment[file]'        => array(
                'type'       => 'text',
                'value'      => @$values['file'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'label'      => $this->lang['PAYMENT_FILE'] . '<br>' . $this->lang['PAYMENT_FILE_INFO'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-payment-field">%block%</div>',
            ),
            'payment[published]'   => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'value'         => @$values['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-payment-field checkbox">%block%</div>',
            ),
        );

        $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
        return $Core->itemForm($inputs_html, 'shop');
    }

    private function warehouses()
    {

        global $Core;

        $sub_menu_links[10]['dir']       = $this->dir;
        $sub_menu_links[10]['component'] = $this->component;
        $sub_menu_links[10]['path']      = 'warehouses';
        $sub_menu_links[10]['action']    = 'add';
        //$sub_menu_links[10]['img'] = "/images/lang-ua.png";
        $sub_menu_links[10]['title'] = $this->lang['ADD'];

        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        $errors = array();

        if ($this->action == "view") {

            $params = $Core->getFilterParams();

            $limit       = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;
            $items       = $this->shopcore->getWarehouses($params, $limit);
            $items_count = $this->shopcore->getWarehousesCount($params);

            $url = $this->backLink . '&page={page}';

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'description',
                'work_hours',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'          => $this->lang['ID'],
                'title'       => $this->lang['TITLE'],
                'description' => $this->lang['DESCRIPTION'],
                'work_hours'  => $this->lang['WORK_HOURS'],
                'published'   => $this->lang['PUBLISHED'],
                'tools'       => $this->lang['TOOLS'],
            );
            /////////////filter table
            $list['filter'] = array(
                'id'          => $Core->getFilterParamsVal($params, 'id'),
                'title'       => $Core->getFilterParamsVal($params, 'title'),
                'description' => $Core->getFilterParamsVal($params, 'description'),
                'work_hours'  => $Core->getFilterParamsVal($params, 'work_hours'),
                'published'   => $Core->getFilterParamsVal($params, 'published'),
                'tools'       => $Core->getFilterParamsVal($params, 'tools'),
            );

            $current_path = '/admin?dir=applications&component=shop&path=warehouses';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $url);

            return $sub_menu . $list_html . $pagination_html;

        } elseif ($this->action == "add") {
            if ($Core->inRequest('submit')) {
                $warehouse = $Core->getRequest('warehouse');
                $errors    = $Core->checkFields($warehouse, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->addWarehouse($warehouse)) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $Core->inRequest('warehouse') ? $Core->getRequest('warehouse') : ($Core->inRequest('id') ? $this->shopcore->getWarehouses(array(
                'id' => $Core->getRequest('id'),
            ), 1) : array());
            return $sub_menu . $this->renderWarehouseFields($values, $errors);
        } elseif ($this->action == "edit") {
            if ($Core->inRequest('submit')) {
                $warehouse = $Core->getRequest('warehouse');
                $errors    = $Core->checkFields($warehouse, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->updateWarehouses($warehouse, array(
                        'id' => $Core->getRequest('id'),
                    ))) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $Core->inRequest('warehouse') ? $Core->getRequest('warehouse') : $this->shopcore->getWarehouses(array(
                'id' => $Core->getRequest('id'),
            ), 1);
            return $sub_menu . $this->renderWarehouseFields($values, $errors);
        } elseif ($this->action == "delete") {
            if ($this->shopcore->deleteWarehouses(array(
                'id' => $Core->getRequest('id'),
            ))) {
                $Core->sysMessage($this->lang['DELETED'], 'success');
            }

            $Core->back();
        }

    }

    private function renderWarehouseFields($values, $errors = array())
    {

        global $Core;

        $fields_to_fill = array(
            'warehouse[title]'       => array(
                'type'          => 'text',
                'default_value' => '',
                'value'         => @$values['title'],
                'attrs'         => array(
                    'placeholder' => $this->lang['TITLE'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'         => $this->lang['TITLE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-warehouse-field ">%block%</div>',
            ),
            'warehouse[description]' => array(
                'type'       => 'editor',
                'editor_type'=> 'custom',
                'value'      => @$values['description'],
                'attrs'      => array(
                    'class' => 'shop-input',
                ),
                'label'      => $this->lang['DESCRIPTION'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-warehouse-field">%block%</div>',
            ),
            'warehouse[work_hours]'  => array(
                'type'       => 'textarea',
                'value'      => @$values['work_hours'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'label'      => $this->lang['WORK_HOURS'] . '<br>' . $this->lang['WORK_HOURS_INFO'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-warehouse-field">%block%</div>',
            ),
            'warehouse[published]'   => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'value'         => @$values['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-warehouse-field checkbox">%block%</div>',
            ),
        );

        $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
        return $Core->itemForm($inputs_html, 'shop');
    }

    private function sales()
    {

        global $Core;

        $sub_menu_links[10]['dir']       = $this->dir;
        $sub_menu_links[10]['component'] = $this->component;
        $sub_menu_links[10]['path']      = 'sales';
        $sub_menu_links[10]['action']    = 'add';
        //$sub_menu_links[10]['img'] = "/images/lang-ua.png";
        $sub_menu_links[10]['title'] = $this->lang['ADD'];

        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        $errors = array();

        if ($this->action == "view") {

            $params = $Core->getFilterParams();

            $limit       = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;
            $items       = $this->shopcore->getSales($params, $limit);
            $items_count = $this->shopcore->getSalesCount($params);

            $url = $this->backLink . '&page={page}';

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'value',
                'type',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'        => $this->lang['ID'],
                'title'     => $this->lang['TITLE'],
                'value'     => $this->lang['VALUE'],
                'type'      => $this->lang['TYPE'],
                'published' => $this->lang['PUBLISHED'],
                'tools'     => $this->lang['TOOLS'],
            );
            /////////////filter table
            $list['filter'] = array(
                'id'        => $Core->getFilterParamsVal($params, 'id'),
                'title'     => $Core->getFilterParamsVal($params, 'title'),
                'value'     => $Core->getFilterParamsVal($params, 'value'),
                'type'      => $Core->getFilterParamsVal($params, 'type'),
                'published' => $Core->getFilterParamsVal($params, 'published'),
                'tools'     => $Core->getFilterParamsVal($params, 'tools'),
            );

            $current_path = '/admin?dir=applications&component=shop&path=sales';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $url);

            return $sub_menu . $list_html . $pagination_html;

        } elseif ($this->action == "add") {
            if ($Core->inRequest('submit')) {
                $sale   = $Core->getRequest('sale');
                $errors = $Core->checkFields($sale, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->addSale($sale)) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $Core->inRequest('sale') ? $Core->getRequest('sale') : ($Core->inRequest('id') ? $this->shopcore->getSales(array(
                'id' => $Core->getRequest('id'),
            ), 1) : array());
            return $sub_menu . $this->renderSaleFields($values, $errors);
        } elseif ($this->action == "edit") {
            if ($Core->inRequest('submit')) {
                $sale   = $Core->getRequest('sale');
                $errors = $Core->checkFields($sale, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->updateSales($sale, array(
                        'id' => $Core->getRequest('id'),
                    ))) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $Core->inRequest('sale') ? $Core->getRequest('sale') : $this->shopcore->getSales(array(
                'id' => $Core->getRequest('id'),
            ), 1);
            return $sub_menu . $this->renderSaleFields($values, $errors);
        } elseif ($this->action == "delete") {
            if ($this->shopcore->deleteSales(array(
                'id' => $Core->getRequest('id'),
            ))) {
                $Core->sysMessage($this->lang['DELETED'], 'success');
            }

            $Core->back();
        }

    }

    private function renderSaleFields($values, $errors = array())
    {

        global $Core;

        $categories_select    = $this->shopcore->getCatSelect();
        $manufacturers_select = $this->shopcore->getManufacturersSelect();
        $warehouses_select    = $this->shopcore->getWarehousesSelect();
        $groups_select        = $Core->users->getGroupsSelect();

        //$Core->print_rec($groups_select);
        $fields_to_fill = array(
            'sale[title]'         => array(
                'type'          => 'text',
                'default_value' => '',
                'value'      => MultiLang::getValues('title', $values),
                'multilang'  => true,
                'attrs'         => array(
                    'placeholder' => $this->lang['TITLE'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'         => $this->lang['TITLE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-sale-field ">%block%</div>',
            ),
            'sale[categories]'    => array(
                'type'       => 'multiselect_two_sides',
                'options'    => $categories_select,
                'value'      => @$values['categories'],
                'attrs'      => array(
                    'class' => 'shop-input form-control',
                ),
                'label'      => $this->lang['CATEGORIES'],
                'id'         => 'sale_cats',
                //'required' => true,
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-sale-field ">%block%</div>',
            ),
            'sale[manufacturers]' => array(
                'type'       => 'multiselect_two_sides',
                'options'    => $manufacturers_select,
                'value'      => @$values['manufacturers'],
                'attrs'      => array(
                    'class' => 'shop-input form-control',
                ),
                'label'      => $this->lang['MANUFACTURERS'],
                'id'         => 'sale_manufacturers',
                //'required' => true,
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-sale-field ">%block%</div>',
            ),
            'sale[warehouses]'    => array(
                'type'       => 'multiselect_two_sides',
                'options'    => $warehouses_select,
                'value'      => @$values['warehouses'],
                'attrs'      => array(
                    'class' => 'shop-input form-control',
                ),
                'label'      => $this->lang['WAREHOUSES'],
                'id'         => 'sale_warehouses',
                //'required' => true,
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-sale-field ">%block%</div>',
            ),
            'sale[groups]'        => array(
                'type'       => 'multiselect_two_sides',
                'options'    => $groups_select,
                'value'      => @$values['groups'],
                'attrs'      => array(
                    'class' => 'shop-input form-control',
                ),
                'label'      => $this->lang['GROUPS'],
                'id'         => 'sale_groups',
                //'required' => true,
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-sale-field ">%block%</div>',
            ),
            'sale[value]'         => array(
                'type'          => 'counter',
                'value'         => @$values['value'],
                'default_value' => 0,
                'attrs'         => array(
                    'class' => 'shop-input  form-control',
                ),
                'label'         => $this->lang['SALE_VALUE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-sale-field form-inline" style="margin:10px;margin-left:0">%block%',
            ),
            'sale[type]'          => array(
                'type'       => 'select',
                'value'      => @$values['type'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'options'    => array(
                    '%'        => $this->lang['PERCENTAGE'],
                    'currency' => $this->lang['CURRENCY_VALUES'],
                ),
                'label'      => $this->lang['SALE_TYPE'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '%block%</div>',
            ),
            'sale[start]'         => array(
                'type'       => 'date',
                'value'      => @$values['start'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'id'         => 'start',
                'label'      => $this->lang['SALE_START'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-sale-field form-inline" style="margin:10px;margin-left:0">%block%',
            ),
            'sale[end]'           => array(
                'type'       => 'date',
                'value'      => @$values['end'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'id'         => 'end',
                'label'      => $this->lang['SALE_END'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '%block%</div>',
            ),
            'sale[published]'     => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'value'         => @$values['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-sale-field checkbox">%block%</div>',
            ),
        );
        $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
        return $Core->itemForm($inputs_html, 'shop');
    }

    private function discounts()
    {

        global $Core;

        if ($Core->inRequest('ajax')) {
            $u_filter        = $Core->getRequest('users');
            $params['login'] = !empty($u_filter['login']) ? array(
                'value'    => '%' . $u_filter['login'] . '%',
                'operator' => 'LIKE',
            ) : '';
            $params['email'] = !empty($u_filter['email']) ? array(
                'value'    => '%' . $u_filter['email'] . '%',
                'operator' => 'LIKE',
            ) : '';
            $params['nickname'] = !empty($u_filter['nickname']) ? array(
                'value'    => '%' . $u_filter['nickname'] . '%',
                'operator' => 'LIKE',
            ) : '';
            $params['id'] = $Core->inRequest('items_checked') ? array(
                'value'    => $Core->getRequest('items_checked'),
                'operator' => 'NOT IN',
            ) : '';

            $ref = parse_url($_SERVER['HTTP_REFERER']);

            parse_str($ref['query'], $vars);

            $list['items'] = $Core->users->getUsers($params, $u_filter['quantity']);

            $list['keys'] = array(
                'id',
                'nickname',
                'login',
                'email',
            );
            $list['head'] = array(
                'id'       => $this->lang['ID'],
                'nickname' => $this->lang['NICKNAME'],
                'login'    => $this->lang['LOGIN'],
                'email'    => $this->lang['EMAIL'],
            );

            $html = $Core->render->renderAdminList($list);

            $html = empty($html) ? $this->lang['NOTHING_FOUND'] : $html;

            die($html);

        }

        $sub_menu_links[10]['dir']       = $this->dir;
        $sub_menu_links[10]['component'] = $this->component;
        $sub_menu_links[10]['path']      = 'discounts';
        $sub_menu_links[10]['action']    = 'add';
        //$sub_menu_links[10]['img'] = "/images/lang-ua.png";
        $sub_menu_links[10]['title'] = $this->lang['ADD'];

        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        $errors = array();

        if ($this->action == "view") {

            $params = $Core->getFilterParams();

            $limit       = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;
            $items       = $this->shopcore->getDiscounts($params, $limit);
            $items_count = $this->shopcore->getDiscountsCount($params);

            $url = $this->backLink . '&page={page}';

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'description',
                'nickname',
                'value',
                'type',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'          => $this->lang['ID'],
                'title'       => $this->lang['TITLE'],
                'description' => $this->lang['DESCRIPTION'],
                'nickname'    => $this->lang['NICKNAME'],
                'value'       => $this->lang['DISCOUNT_VALUE'],
                'type'        => $this->lang['DISCOUNT_TYPE'],
                'published'   => $this->lang['PUBLISHED'],
                'tools'       => $this->lang['TOOLS'],
            );
            /////////////filter table
            $list['filter'] = array(
                'id'          => $Core->getFilterParamsVal($params, 'id'),
                'title'       => $Core->getFilterParamsVal($params, 'title'),
                'description' => $Core->getFilterParamsVal($params, 'description'),
                'nickname'    => $Core->getFilterParamsVal($params, 'nickname'),
                'value'       => $Core->getFilterParamsVal($params, 'value'),
                'type'        => $Core->getFilterParamsVal($params, 'type'),
                'published'   => $Core->getFilterParamsVal($params, 'published'),
                'tools'       => $Core->getFilterParamsVal($params, 'tools'),
            );

            $current_path = '/admin?dir=applications&component=shop&path=discounts';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $url);

            return $sub_menu . $list_html . $pagination_html;

        } elseif ($this->action == "add") {
            if ($Core->inRequest('submit')) {
                $discount = $Core->getRequest('discount');
                $errors   = $Core->checkFields($discount, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->addDiscount($discount)) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $Core->inRequest('discount') ? $Core->getRequest('discount') : ($Core->inRequest('id') ? $this->shopcore->getDiscounts(array(
                'id' => $Core->getRequest('id'),
            ), 1) : array());
            return $sub_menu . $this->renderDiscountFields($values, $errors);
        } elseif ($this->action == "edit") {
            if ($Core->inRequest('submit')) {
                $discount = $Core->getRequest('discount');
                $errors   = $Core->checkFields($discount, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->updateDiscounts($discount, array(
                        'id' => $Core->getRequest('id'),
                    ))) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $Core->inRequest('discount') ? $Core->getRequest('discount') : $this->shopcore->getDiscounts(array(
                'id' => $Core->getRequest('id'),
            ), 1);
            return $sub_menu . $this->renderDiscountFields($values, $errors);
        } elseif ($this->action == "delete") {
            if ($this->shopcore->deleteDiscounts(array(
                'id' => $Core->getRequest('id'),
            ))) {
                $Core->sysMessage($this->lang['DELETED'], 'success');
            }

            $Core->back();
        }

    }

    private function renderDiscountFields($values, $errors = array())
    {

        global $Core;

        $user_html = '';

        if (isset($values['user_id'])) {
            $list['keys'] = array(
                'id',
                'nickname',
                'login',
                'email',
            );
            $list['head'] = array(
                'id'       => $this->lang['ID'],
                'nickname' => $this->lang['NICKNAME'],
                'login'    => $this->lang['LOGIN'],
                'email'    => $this->lang['EMAIL'],
            );

            $list['items'] = array(
                0 => array(
                    'id'       => $values['user_id'],
                    'login'    => $values['login'],
                    'nickname' => $values['nickname'],
                    'email'    => $values['email'],
                    'checked'  => true,
                    'disabled' => true,
                ),
            );

            $user_html = $Core->render->renderAdminList($list, $this->backLink);
        }

        $fields_to_fill = array(
            'discount[title]'       => array(
                'type'          => 'text',
                'default_value' => '',
                'value'      => MultiLang::getValues('title', $values),
                'multilang'  => true,
                'attrs'         => array(
                    'placeholder' => $this->lang['TITLE'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'         => $this->lang['TITLE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-discount-field ">%block%</div>',
            ),
            'discount[description]' => array(
                'type'       => 'editor',
                'editor_type'=> 'custom',
                'value'      => MultiLang::getValues('description', $values),
                'multilang'  => true,
                'attrs'      => array(
                    'class' => 'shop-input',
                ),
                'label'      => $this->lang['DESCRIPTION'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-discount-field">%block%</div>',
            ),
            'discount[user_id]'     => array(
                'type'                 => 'autocomplete_chosen',
                'default_value'        => '',
                'value'                => [$values['user_id']],
                'attrs'                => array(
                    'data-placeholder'  => $this->lang['USER'],
                    'class'             => 'comment-input  form-control',
                    'query-table'       => 'users',
                    'query-value-field' => 'id',
                    'query-title-field' => 'login',
                    'query-params'      => "published=1",
                    'query-limit'       => 100,
                ),
                'options'              => [$values['user_id'] => $Core->users->getUserLogin($values['user_id'])],
                'label'                => $this->lang['USER'],
                'max_selected_options' => 1,
                'multiple'             => true,
                'allowed_fields'       => array(
                    'login',
                    'nickname',
                    'email',
                    'id',
                ),
                'name_wrap'            => '%name%',
                'input_wrap'           => '%input%',
                'block_wrap'           => '<div class="shop-discount-field">%block%</div>',
            ),
            'discount[value]'       => array(
                'type'          => 'counter',
                'value'         => @$values['value'],
                'default_value' => 0,
                'attrs'         => array(
                    'class' => 'shop-input  form-control',
                ),
                'label'         => $this->lang['DISCOUNT_VALUE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-discount-field form-inline" style="margin:10px;margin-left:0">%block%',
            ),
            'discount[type]'        => array(
                'type'       => 'select',
                'value'      => @$values['type'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'options'    => array(
                    '%'        => $this->lang['PERCENTAGE'],
                    'currency' => $this->lang['CURRENCY_VALUES'],
                ),
                'label'      => $this->lang['DISCOUNT_TYPE'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '%block%</div>',
            ),
            'discount[start]'       => array(
                'type'       => 'date',
                'value'      => @$values['start'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'id'         => 'start',
                'label'      => $this->lang['DISCOUNT_START'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-discount-field form-inline" style="margin:10px;margin-left:0">%block%',
            ),
            'discount[end]'         => array(
                'type'       => 'date',
                'value'      => @$values['end'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'id'         => 'end',
                'label'      => $this->lang['DISCOUNT_END'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '%block%</div>',
            ),
            'discount[published]'   => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'value'         => @$values['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-discount-field checkbox">%block%</div>',
            ),
        );

        $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
        return $Core->itemForm($inputs_html, 'shop');
    }

    private function currencies()
    {

        global $Core;

        $sub_menu_links[10]['dir']       = $this->dir;
        $sub_menu_links[10]['component'] = $this->component;
        $sub_menu_links[10]['path']      = 'currencies';
        $sub_menu_links[10]['action']    = 'add';
        //$sub_menu_links[10]['img'] = "/images/lang-ua.png";
        $sub_menu_links[10]['title'] = $this->lang['ADD'];

        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        $errors = array();

        if ($this->action == "view") {

            $params = $Core->getFilterParams();

            $limit       = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;
            $items       = $this->shopcore->getCurrencies($params, $limit);
            $items_count = $this->shopcore->getCurrenciesCount($params);

            $url = $this->backLink . '&page={page}';

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'symbol',
                'rate',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'        => $this->lang['ID'],
                'title'     => $this->lang['TITLE'],
                'symbol'    => $this->lang['SYMBOL'],
                'rate'      => $this->lang['RATE'],
                'published' => $this->lang['PUBLISHED'],
                'tools'     => $this->lang['TOOLS'],
            );
            /////////////filter table
            $list['filter'] = array(
                'id'        => $Core->getFilterParamsVal($params, 'id'),
                'title'     => $Core->getFilterParamsVal($params, 'title'),
                'symbol'    => $Core->getFilterParamsVal($params, 'symbol'),
                'rate'      => $Core->getFilterParamsVal($params, 'rate'),
                'published' => $Core->getFilterParamsVal($params, 'published'),
                'tools'     => $Core->getFilterParamsVal($params, 'tools'),
            );

            $current_path = '/admin?dir=applications&component=shop&path=currencies';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $url);

            return $sub_menu . $list_html . $pagination_html;

        } elseif ($this->action == "add") {
            if ($Core->inRequest('submit')) {
                $currency = $Core->getRequest('currency');
                $errors   = $Core->checkFields($currency, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->addCurrency($currency)) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $Core->inRequest('currency') ? $Core->getRequest('currency') : ($Core->inRequest('id') ? $this->shopcore->getCurrencies(array(
                'id' => $Core->getRequest('id'),
            ), 1) : array());
            return $sub_menu . $this->renderCurrencyFields($values, $errors);
        } elseif ($this->action == "edit") {
            if ($Core->inRequest('submit')) {
                $currency = $Core->getRequest('currency');
                $errors   = $Core->checkFields($currency, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->updateCurrencies($currency, array(
                        'id' => $Core->getRequest('id'),
                    ))) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $Core->inRequest('currency') ? $Core->getRequest('currency') : $this->shopcore->getCurrencies(array(
                'id' => $Core->getRequest('id'),
            ), 1);
            return $sub_menu . $this->renderCurrencyFields($values, $errors);
        } elseif ($this->action == "delete") {
            if ($this->shopcore->deleteCurrencies(array(
                'id' => $Core->getRequest('id'),
            ))) {
                $Core->sysMessage($this->lang['DELETED'], 'success');
            }

            $Core->back();
        }

    }

    private function renderCurrencyFields($values, $errors = array())
    {

        global $Core;

        $fields_to_fill = array(
            'currency[title]'     => array(
                'type'          => 'text',
                'default_value' => '',
                'value'      => MultiLang::getValues('title', $values),
                'multilang'  => true,
                'attrs'         => array(
                    'placeholder' => $this->lang['TITLE'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'         => $this->lang['TITLE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-currency-field ">%block%</div>',
            ),
            'currency[symbol]'    => array(
                'type'       => 'text',
                'value'      => @$values['symbol'],
                'attrs'      => array(
                    'placeholder' => $this->lang['SYMBOL'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'      => $this->lang['SYMBOL'],
                'required'   => true,
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-currency-field">%block%</div>',
            ),
            'currency[literal]'   => array(
                'type'       => 'text',
                'value'      => @$values['literal'],
                'attrs'      => array(
                    'placeholder' => $this->lang['CURRENCY_LITERAL'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'      => $this->lang['CURRENCY_LITERAL'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-currency-field">%block%</div>',
            ),
            'currency[rate]'      => array(
                'type'       => 'text',
                'value'      => @$values['rate'],
                'attrs'      => array(
                    'placeholder' => $this->lang['RATE'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'      => $this->lang['RATE'] . '<br>' . $this->lang['RATE_INFO'],
                'required'   => true,
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-currency-field">%block%</div>',
            ),
            'currency[published]' => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'value'         => @$values['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-currency-field checkbox">%block%</div>',
            ),
        );

        $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
        return $Core->itemForm($inputs_html, 'shop');
    }

    private function orders()
    {

        global $Core;

        $sub_menu_links[10]['dir']       = $this->dir;
        $sub_menu_links[10]['component'] = $this->component;
        $sub_menu_links[10]['path']      = 'orders';
        $sub_menu_links[10]['action']    = 'add';
        //$sub_menu_links[10]['img'] = "/images/lang-ua.png";
        $sub_menu_links[10]['title'] = $this->lang['ORDER_ADD'];

        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        $errors = array();

        if ($Core->inRequest('id')) {
            $id = $Core->getRequest('id');
        }

        if ($this->action == "view") {

            $params = $Core->getFilterParams();

            $limit       = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;
            $items       = $this->shopcore->getOrders($params, $limit, 'id DESC');
            $items_count = $this->shopcore->getOrdersCount($params);

            $url = $this->backLink . '&page={page}';

            $items = $items_count > 0 ? $items : array();

            foreach ($items as $item) {
                $item['user']     = $Core->users->getUserFirstAndLastname($item['user_id']);
                $item['delivery'] = $Core->db->selectField('shop_delivery_services', 'title', array(
                    'id' => $item['delivery_id'],
                ));
                $item['payment'] = $Core->db->selectField('shop_payments', 'title', array(
                    'id' => $item['payment_id'],
                ));
                $item['status'] = $Core->db->selectField('shop_order_statuses', 'title', array(
                    'id' => $item['status_id'],
                ));

                $item['sum'] = $this->shopcore->getOrderSum($item['id']);

                $list['items'][] = $item;
            }
            $list['keys'] = array(
                'id',
                'user',
                'delivery',
                'payment',
                'status',
                'date_add',
                'date_mod',
                'comment',
                'sum',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'        => $this->lang['ID'],
                'user'      => $this->lang['USER'],
                'delivery'  => $this->lang['DELIVERY'],
                'payment'   => $this->lang['PAYMENT'],
                'status'    => $this->lang['STATUS'],
                'date_add'  => $this->lang['DATE_ADD'],
                'date_mod'  => $this->lang['DATE_MOD'],
                'comment'   => $this->lang['COMMENT'],
                'sum'       => $this->lang['SUM'],
                'published' => $this->lang['PUBLISHED'],
                'tools'     => $this->lang['TOOLS'],
            );

            /////////////filter table
            $list['filter'] = array(
                'id'        => $Core->getFilterParamsVal($params, 'id'),
                'user'      => $Core->getFilterParamsVal($params, 'user'),
                'delivery'  => $Core->getFilterParamsVal($params, 'delivery'),
                'payment'   => $Core->getFilterParamsVal($params, 'payment'),
                'status'    => $Core->getFilterParamsVal($params, 'status'),
                'date_add'  => $Core->getFilterParamsVal($params, 'date_add'),
                'date_mod'  => $Core->getFilterParamsVal($params, 'date_mod'),
                'comment'   => $Core->getFilterParamsVal($params, 'comment'),
                'sum'       => $Core->getFilterParamsVal($params, 'sum'),
                'published' => $Core->getFilterParamsVal($params, 'published'),
                'tools'     => $Core->getFilterParamsVal($params, 'tools'),
            );

            $current_path = '/admin?dir=applications&component=shop&path=orders';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $url);

            return $sub_menu . $list_html . $pagination_html;

        } elseif ($this->action == "add") {
            if ($Core->inRequest('step') && $Core->getRequest('step') == 1) {
                if ($Core->inRequest('submit')) {
                    $order  = $Core->getRequest('order');
                    $errors = $Core->checkFields($order, $this->lang);
                    if (count($errors) == 0) {
                        $o_id = $this->shopcore->addOrder($order);
                        parse_str(parse_url($Core->getUri(), PHP_URL_QUERY), $params);
                        $params['id']   = $o_id;
                        $params['step'] = 2;
                        $uri = parse_url($Core->getUri(), PHP_URL_PATH) . "?" . http_build_query($params);
                        $Core->redirect($uri);
                    } else {
                        $Core->back();
                    }
                }
            }
            if ($Core->inRequest('step') && $Core->getRequest('step') == 2) {
                if ($Core->inRequest('submit')) {
                    $order = $this->shopcore->getOrders(array(
                        'id' => $id,
                    ), 1);
                    $order_items = $Core->getRequest('order_items');
                    $this->shopcore->addOrderItems($order, $order_items);
                    $this->shopcore->makeOrderInfoShot($order['id'], $order, $order_items);
                    $Core->sessionUnset('shop_order');
                    $Core->redirect('/admin?dir=applications&component=shop&path=orders&id=' . $id);
                }
            }
            $values = $Core->inRequest('order') ? $Core->getRequest('order') : ($Core->inRequest('id') ? $this->shopcore->getOrders(array(
                'id' => $id,
            ), 1) : array());

            if ($Core->inRequest('step') && $Core->getRequest('step') == 2) {
                return $sub_menu . $this->renderOrderFieldsStep2($values, $errors);
            }
            return $sub_menu . $this->renderOrderFields($values, $errors);
        } elseif ($this->action == "edit") {
            if ($Core->inRequest('step') && $Core->getRequest('step') == 1) {
                if ($Core->inRequest('submit')) {
                    $order       = $Core->getRequest('order');
                    $order['id'] = $id;
                    $errors      = $Core->checkFields($order, $this->lang);
                    if (count($errors) == 0) {
                        $order['date_mod'] = date("Y-m-d H:i:s");
                        $this->shopcore->updateOrder($order, array(
                            'id' => $id,
                        ));
                        $go_to_next_step = true;
                    } else {
                        $Core->back();
                    }
                }
            }
            if ($Core->inRequest('step') && $Core->getRequest('step') == 2) {
                if ($Core->inRequest('submit')) {
                    $order_items = $Core->getRequest('order_items');
                    $errors      = $Core->checkFields($order_items, $this->lang);
                    if (count($errors) == 0) {
                        $order = $this->shopcore->getOrders(array(
                            'id' => $Core->getRequest('id'),
                        ), 1);
                        $this->shopcore->updateOrderItems($order, $order_items);
                        $Core->redirect('/admin?dir=applications&component=shop&path=orders&action=edit&id=' . $id);
                    } else {
                        $Core->back();
                    }

                }
            }
            $values = $Core->inRequest('order') ? $Core->getRequest('order') : ($Core->inRequest('id') ? $this->shopcore->getOrders(array(
                'id' => $id,
            ), 1) : array());

            if (($Core->inRequest('step') && $Core->getRequest('step') == 2) || $go_to_next_step) {
                return $sub_menu . $this->renderOrderFieldsStep2($values, $errors);
            }
            return $sub_menu . $this->renderOrderFields($values, $errors);
        } elseif ($this->action == "delete") {
            if ($this->shopcore->deleteOrders(array(
                'id' => $id,
            ))) {
                $Core->sysMessage($this->lang['DELETED'], 'success');
            }

            $Core->back();
        }

    }

    public function renderOrderFieldsStep2()
    {

        global $Core;

        if ($Core->inRequest('id')) {
            $id = $Core->getRequest('id');
        }

        $Core->page->addHeadJs('/js/jquery.spinner.min.js');
        $Core->page->addHeadCss('/admin/css/bootstrap-spinner.css');

        $fields_to_fill = array();

        $order_items = $this->shopcore->getOrderItems(array(
            'order_id' => $id,
        ));

        $selected_items = array();

        foreach ($order_items as $key => $item) {
            $selected_items[] = $item['hashed_id'];
        }

        $fields_to_fill['search'] = array(
            'type'           => 'autocomplete_chosen',
            'value'          => $selected_items,
            'attrs'          => array(
                'data-placeholder'  => $this->lang['TYPE_PRODUCTS'],
                'class'             => 'shop-input  form-control',
                'query-table'       => 'shop_products',
                'query-value-field' => 'id',
                'query-title-field' => 'title',
                'query-params'      => "published=1",
                'query-limit'       => 1000,
            ),
            'options'        => $Core->getValEqLabelArr($order_items, 'hashed_id', 'title'),
            'label'          => $this->lang['PRODUCTS'] . $this->lang['TYPE_PRODUCTS_INFO'],
            'multiple'       => true,
            'allowed_fields' => array(
                'code',
                'title',
                'description',
                'id',
            ),
            'name_wrap'      => '%name%',
            'input_wrap'     => '%input%',
            'block_wrap'     => '<div class="shop-order-product-field">%block%</div>',
        );

        parse_str(parse_url($Core->getUri(), PHP_URL_QUERY), $params);
        $params['step']   = 2;
        $uri2             = parse_url($Core->getUri(), PHP_URL_PATH) . "?" . http_build_query($params);
        $params['step']   = 1;
        $params['action'] = 'edit';
        $uri              = parse_url($Core->getUri(), PHP_URL_PATH) . "?" . http_build_query($params);

        $rows = '';

        foreach ($order_items as $key => $product) {

            $rows .= $this->renderProductRow($product['hashed_id'], $product['order_id']);

        }

        $params = ['uri2' => $uri2, 'uri' => $uri, 'rows' => $rows];

        return $this->renderStep2Form($fields_to_fill, $params);

    }

    private function renderStep2Form($fields_to_fill, $params)
    {
        global $Core;

        return '<form class="shop-form" method="post" action="' . $params['uri2'] . '">' . $Core->render->renderInputs($fields_to_fill, false, $errors) . '<div class="container-fluid" id="order-products" style="">' . $params['rows'] . '</div>
        <input type="submit" class="btn btn-primary" name="submit" value="' . $this->lang['SAVE'] . '" />
        <a class="btn btn-info pull-right order-nav">' . $this->lang['ORDER_STEP_TWO'] . '</a>
        <a href="' . $params['uri'] . '"  class="btn btn-success pull-right order-nav">' . $this->lang['ORDER_STEP_ONE'] . '</a>
        </form>';
    }

    private function renderOrderFields($values, $errors)
    {

        global $Core;

        $fields_to_fill = array(
            'order[user_id]'          => array(
                'type'                 => 'autocomplete_chosen',
                'value'                => @$values['user_id'],
                'attrs'                => array(
                    'data-placeholder'  => $this->lang['TYPE_USER'],
                    'class'             => 'shop-input  form-control',
                    'query-table'       => 'users',
                    'query-value-field' => 'id',
                    'query-title-field' => 'login',
                    'query-params'      => '',
                    'query-limit'       => 1000,
                ),
                'options'              => array(
                    $values['user_id'] => $Core->users->getUserLogin($values['user_id']),
                ),
                'max_selected_options' => 1,
                'label'                => $this->lang['USER'] . $this->lang['TYPE_USER_INFO'],
                'multiple'             => true,
                'required'             => true,
                'allowed_fields'       => array(
                    'nickname',
                    'login',
                    'firstname',
                    'lastname',
                    'email',
                    'id',
                ),
                'name_wrap'            => '%name%',
                'input_wrap'           => '%input%',
                'block_wrap'           => '<div class="shop-order-field ">%block%</div>',
            ),
            'order[delivery_id]'      => array(
                'type'       => 'select',
                'value'      => @$values['delivery_id'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'options'    => $Core->getValEqLabelArr($this->shopcore->getDeliveryServices(), 'id', 'title'),
                'label'      => $this->lang['DELIVERY'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '%block%',
            ),
            'order[delivery_address]' => array(
                'type'       => 'textarea',
                'attrs'      => array(
                    'class' => 'shop-input form-control',
                ),
                'value'      => @$values['delivery_address'],
                'label'      => $this->lang['DELIVERY_ADDRESS'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-order-field">%block%</div>',
            ),
            'order[payment_id]'       => array(
                'type'       => 'select',
                'value'      => @$values['payment_id'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'options'    => $Core->getValEqLabelArr($this->shopcore->getPayments(), 'id', 'title'),
                'label'      => $this->lang['PAYMENT'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '%block%',
            ),
            'order[payment_details]'  => array(
                'type'       => 'textarea',
                'attrs'      => array(
                    'class' => 'shop-input form-control',
                ),
                'value'      => @$values['payment_details'],
                'label'      => $this->lang['PAYMENT_DETAILS'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-order-field">%block%</div>',
            ),
            'order[status_id]'        => array(
                'type'       => 'select',
                'value'      => @$values['status_id'],
                'attrs'      => array(
                    'class' => 'shop-input  form-control',
                ),
                'options'    => $Core->getValEqLabelArr($this->shopcore->getOrderStatuses(), 'id', 'title'),
                'required'   => true,
                'label'      => $this->lang['STATUS'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-order-field ">%block%</div>',
            ),
            'order[comment]'          => array(
                'type'       => 'textarea',
                'attrs'      => array(
                    'class' => 'shop-input form-control',
                ),
                'value'      => @$values['comment'],
                'label'      => $this->lang['COMMENT'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-order-field">%block%</div>',
            ),
            'order[published]'        => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'value'         => @$values['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-order-field checkbox">%block%</div>',
            ),
        );

        parse_str(parse_url($Core->getUri(), PHP_URL_QUERY), $params);
        $params['step'] = 1;
        if ($params['action'] == 'add' && isset($params['id'])) {
            unset($params['id']);
        }
        $uri = parse_url($Core->getUri(), PHP_URL_PATH) . "?" . http_build_query($params);

        if (isset($params['id'])) {
            $params['step'] = 2;
            $uri2           = parse_url($Core->getUri(), PHP_URL_PATH) . "?" . http_build_query($params);
        }

        $params = ['uri2' => $uri2, 'uri' => $uri];

        return $this->renderStep1Form($fields_to_fill, $params);

    }

    private function renderStep1Form($fields_to_fill, $params)
    {
        global $Core;

        return '<form class="shop-form" method="post" action="' . $params['uri'] . '">' . $Core->render->renderInputs($fields_to_fill, false, $errors) . '
        <input type="submit" class="btn btn-primary" name="submit" value="' . $this->lang['SAVE'] . '" />
        ' . (!empty($params['uri2']) ? '<a href="' . $params['uri2'] . '" class="btn btn-success pull-right order-nav">' . $this->lang['ORDER_STEP_TWO'] . '</a>
            <a  class="btn btn-info pull-right order-nav">' . $this->lang['ORDER_STEP_ONE'] . '</a>' : '') . '
        </form>';

    }

    private function orderStatuses()
    {

        global $Core;

        $sub_menu_links[10]['dir']       = $this->dir;
        $sub_menu_links[10]['component'] = $this->component;
        $sub_menu_links[10]['path']      = 'order_statuses';
        $sub_menu_links[10]['action']    = 'add';
        //$sub_menu_links[10]['img'] = "/images/lang-ua.png";
        $sub_menu_links[10]['title'] = $this->lang['ADD'];

        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        $errors = array();

        if ($this->action == "view") {
            $params      = $Core->getFilterParams();
            $limit       = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;
            $items       = $this->shopcore->getOrderStatuses($params, $limit);
            $items_count = $this->shopcore->getOrderStatusesCount($params);

            $url = $this->backLink . '&page={page}';

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'description',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'          => $this->lang['ID'],
                'title'       => $this->lang['TITLE'],
                'description' => $this->lang['DESCRIPTION'],
                'published'   => $this->lang['PUBLISHED'],
                'tools'       => $this->lang['TOOLS'],
            );
            $list['filter'] = array(
                'id'          => $Core->getFilterParamsVal($params, 'id'),
                'title'       => $Core->getFilterParamsVal($params, 'title'),
                'description' => $Core->getFilterParamsVal($params, 'description'),
                'published'   => $Core->getFilterParamsVal($params, 'published'),
            );

            $current_path = '/admin?dir=applications&component=shop&path=order_statuses';

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $url);

            return $sub_menu . $list_html . $pagination_html;

        } elseif ($this->action == "add") {
            if ($Core->inRequest('submit')) {
                $order_status = $Core->getRequest('order_status');
                $errors       = $Core->checkFields($order_status, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->addOrderStatus($order_status)) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $Core->inRequest('order_status') ? $Core->getRequest('order_status') : ($Core->inRequest('id') ? $this->shopcore->getOrderStatuses(array(
                'id' => $Core->getRequest('id'),
            ), 1) : array());
            return $sub_menu . $this->renderOrderStatusFields($values, $errors);
        } elseif ($this->action == "edit") {
            if ($Core->inRequest('submit')) {
                $order_status = $Core->getRequest('order_status');
                $errors       = $Core->checkFields($order_status, $this->lang);
                if (count($errors) == 0) {
                    if ($this->shopcore->updateOrderStatuses($order_status, array(
                        'id' => $Core->getRequest('id'),
                    ))) {
                        $Core->sysMessage($this->lang['SAVED'], 'success');
                    }

                    $Core->redirect($this->pathLink);
                }

            }
            $values = $Core->inRequest('order_status') ? $Core->getRequest('order_status') : $this->shopcore->getOrderStatuses(array(
                'id' => $Core->getRequest('id'),
            ), 1);
            return $sub_menu . $this->renderOrderStatusFields($values, $errors);
        } elseif ($this->action == "delete") {
            if ($this->shopcore->deleteOrderStatuses(array(
                'id' => $Core->getRequest('id'),
            ))) {
                $Core->sysMessage($this->lang['DELETED'], 'success');
            }

            $Core->back();
        }

    }

    private function renderOrderStatusFields($values, $errors)
    {

        global $Core;

        $fields_to_fill = array(
            'order_status[title]'       => array(
                'type'          => 'text',
                'default_value' => '',
                'value'      => MultiLang::getValues('title', $values),
                'multilang'  => true,
                'attrs'         => array(
                    'placeholder' => $this->lang['TITLE'],
                    'class'       => 'shop-input  form-control',
                ),
                'label'         => $this->lang['TITLE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-order_status-field ">%block%</div>',
            ),
            'order_status[description]' => array(
                'type'       => 'editor',
                'editor_type'=> 'custom',
                'value'      => MultiLang::getValues('description', $values),
                'multilang'  => true,
                'attrs'      => array(
                    'class' => 'shop-input',
                ),
                'label'      => $this->lang['DESCRIPTION'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="shop-order_status-field">%block%</div>',
            ),
            'order_status[published]'   => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'shop-input',
                ),
                'value'         => @$values['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="shop-order_status-field checkbox">%block%</div>',
            ),
        );

        $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
        return $Core->itemForm($inputs_html, 'shop');

    }

    public function getAjaxResponse()
    {
        global $Core;
        switch ($this->action) {
            case 'getOrderProductRow':
                return $this->renderProductRow($Core->getRequest('id'));
                break;
            case 'getOrderProductVariantsModal':
                return $this->renderProductVariantsModal();
                break;

            default:
                # code...

                break;
        }
    }

    public function renderProductRow($hashed_id, $o_id = false)
    {
        global $Core;

        list($id, $dump) = explode("-", $hashed_id);
        $data            = $this->shopcore->getProducts(array(
            'id' => $id,
        ), 1);
        $data['LANG'] = $this->lang;
        $images       = $this->shopcore->getProductImages($id, $size = false);
        if (count($images[$id]) > 0) {
            $data['image'] = $images[$id][0];
        } else {
            $data['image'] = "/admin/img/no-image.png";
        }

        if ($o_id > 0) {
            $order_item = $this->shopcore->getOrderItems(array(
                'order_id'   => $o_id,
                'product_id' => $id,
                'hashed_id'  => $hashed_id,
            ), 1);
            $data['order_item'] = $order_item;
        }

        $fields_to_fill = array();

        $fields_to_fill['order_items[' . $hashed_id . '][manager_user_id]'] = array(
            'type'       => 'select',
            'value'      => @$order_item['manager_user_id'],
            'attrs'      => array(
                'class' => 'shop-input  form-control',
            ),
            'options'    => $Core->getValEqLabelArr($Core->users->getUsersInGroup($this->settings['managers_group']), 'id', 'login'),
            'label'      => $this->lang['MANAGER'],
            'name_wrap'  => '<div class="manager-label-wrapper">%name%</div>',
            'input_wrap' => '%input%',
            'block_wrap' => '%block%',
        );
        $data['manager_field'] = $Core->render->renderInputs($fields_to_fill, false, $errors);

        $fields_to_fill = array();

        $fields_to_fill['order_items[' . $hashed_id . '][quantity]'] = array(
            'type'       => 'counter',
            'attrs'      => array(
                'class' => 'form-control quantity',
                'style' => 'width:40px;float:right',
            ),
            'value'      => $order_item['quantity'],
            'label'      => $this->lang['QTY'],
            'name_wrap'  => '<div class="qty-label-wrapper">%name%</div>',
            'input_wrap' => '%input%',
            'block_wrap' => '%block%',
        );
        $data['quantity_field']        = $Core->render->renderInputs($fields_to_fill, false, $errors);
        $data['checked_variants_html'] = $this->renderCheckedProductVariantsInOrders($id, $hashed_id);
        $data['hashed_id']             = $hashed_id;
        return $Core->render->renderTpl('/admin/tpls/applications/shop_order_product_row.twig', $data);
    }

    public function renderCheckedProductVariantsInOrders($product_id, $hashed_id)
    {
        global $Core;
        $checked_v = array();
        if ($Core->inRequest('id')) {
            $order_id   = $Core->getRequest('id');
            $order_item = $this->shopcore->getOrderItems(array(
                'product_id' => $product_id,
                'order_id'   => $order_id,
                'hashed_id'  => $hashed_id,
            ), 1);
            $checked_v = explode(",", $order_item['variants_id']);
        }
        //new render
        $product_features = $this->shopcore->getProductFeatures($product_id);
        
        foreach ($product_features as $feature) {
            $product_variants = $this->shopcore->getProductVariants(['product_id' => $product_id, 'feature_id' => $feature['id']]);
            switch ($feature['type']) {
                case 'checkbox':
                    foreach ($product_variants as $key => $v) {
                        $fields_to_fill['order_items[' . $hashed_id . '][variants][' . $v['variant_id'] . ']'] = array(
                            'type'          => 'checkbox',
                            'default_value' => 1,
                            'value'         => in_array($v['variant_id'], $checked_v) ? 1 : '0',
                            'attrs'         => array(
                                'class' => 'form-control shop-variant',
                            ),
                            'label'         => $v['title'],
                            'name_wrap'     => '%name%',
                            'input_wrap'    => '%input%<br>',
                            'block_wrap'    => $key == 0 ? '<div class="clearfix"></div><div class="order-feature-title">' . $feature['title'] . '</div><div  class="col-md-2 text-center">%block%</div>' : '<div  class="col-md-2 text-center">%block%</div>',
                        );

                    }
                    break;
                case 'select':
                    $options = $Core->getValEqLabelArr($product_variants, 'id', 'title');
                    foreach ($options as $oid => $op) {
                        if (in_array($oid, $checked_v)) {
                            $val = $oid;
                        }
                    }
                    $fields_to_fill['order_items[' . $hashed_id . '][feature_variant][' . $feature['id'] . ']'] = array(
                        'type'       => 'select',
                        'value'      => @$val,
                        'options'    => $options,
                        'attrs'      => array(
                            'class' => 'form-control shop-variant',
                            'style' => 'width:100%',
                        ),
                        'name_wrap'  => '%name%',
                        'input_wrap' => '%input%',
                        'block_wrap' => '<div class="clearfix"></div><div class="order-feature-title">' . $feature['title'] . '</div><div  class="col-md-12 text-center">%block%</div>',
                    );
                    break;
                case 'radio':
                    foreach ($product_variants as $key => $v) {
                        $fields_to_fill['order_items[' . $hashed_id . '][variants][' . $v['variant_id'] . ']'] = array(
                            'type'          => 'radio',
                            'group'         => $hashed_id,
                            'default_value' => $v['variant_id'],
                            'value'         => in_array($v['variant_id'], $checked_v) ? $v['variant_id'] : '0',
                            'attrs'         => array(
                                'class' => 'form-control shop-variant',
                            ),
                            'label'         => $v['title'],
                            'name_wrap'     => '%name%',
                            'input_wrap'    => '%input%',
                            'block_wrap'    => $key == 0 ? '<div class="clearfix"></div><div class="order-feature-title">' . $feature['title'] . '</div><div  class="col-md-2 text-center">%block%</div>' : '<div  class="col-md-2 text-center">%block%</div>',
                        );
                    }
                    break;
                default:
                    break;
            }
        }
        return $Core->render->renderInputs($fields_to_fill, false, $errors);
    }

    public function renderProductVariantsModal()
    {
        global $Core;
        $checked_variants_html = $this->renderCheckedProductVariantsInOrders($Core->getRequest('product_id'), $Core->getRequest('hash'));
        $data                  = array(
            'data'       => $checked_variants_html,
            'title'      => $this->lang['SELECT_VARIANTS'],
            'LANG'       => $this->lang,
            'form_attrs' => array(
                'id' => 'product-variants',
            ),
        );

        $html = $Core->render->renderTpl("/admin/tpls/additional/modal.twig", $data);
        $html = $Core->render->replaceAdditionalLinks($html);

        die($html);
    }

    public function getPathTable()
    {
        return 'shop_' . $this->path;
    }

}
