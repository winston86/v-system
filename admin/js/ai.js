var filled_fields = [];
$(document).ready(function(){
  $('.feature').click( function () {
    var $mybtn = $(this).button('loading');
    if($('#product-variants-select').attr('f_id') != $(this).attr('feature_id')){

        if($('#product-variants-select').html()){
            filled_fields[$('#product-variants-select').attr('f_id')] = $('#product-variants-select');
            $('#product-variants-select').detach();
        }

        if(filled_fields[$(this).attr('feature_id')]){
            $('#modal-window').append(filled_fields[$(this).attr('feature_id')]).modal();
            $mybtn.button('reset');
            listenForPFSubmit();
            return;
        }
        $.ajax({
            url:'/admin/?dir=applications&component=shop&path=products&action=getFVariants&feature_id='+$(this).attr('feature_id')+'&id='+$('#item_id').val(),
            success:function(data){
                $('#modal-window').html(data).modal();
                $mybtn.button('reset');
                listenForPFSubmit();
            },
            error:function(error){
                alert(error)
            }
        });
    }else{
       $('#modal-window').modal();
       $mybtn.button('reset');
       listenForPFSubmit();
    }
  })
});

function jq( selector ) {
 
    return selector.replace( /(:|\.|\[|\]|,)/g, "\\$1" );
 
}

function listenForPFSubmit(){
    var vars = '';
    $('#product-variants-select').submit(function(){ 
        $('#cancel-modal').trigger('click');
        $('#product-variants-select input.variants').each(function(i,v){
            if(parseInt($(v).val()) > 0){
             vars += '<kbd><kbd>'+$(v).attr('v_name')+'</kbd><span class="badge">'+$(v).val()+'</span><input type="hidden" name="'+$(v).attr('name')+'" value="'+$(v).val()+'" /></kbd>';
            }
        });
        v = $('#product-variants-select input.variants',0).closest('form');
        $('#product_variants_'+$(v).attr('f_id')).html(vars);
        return false;
    });
}

$(document).ready(
    function(){$('.for_filter').on('change',
        function(){
            $('#filter').find('input[name='+jq($(this).attr('name'))+']').val($(this).val());
        }) ;
})

$(document).ready(function(){
    $('.users-submit').on('click',function(){
        var data = $('#users-block input, #users-block select').serialize();
        var list_data = $('#users-list').serialize();
        $.get(
            '/admin?dir=applications&component=shop&path=discounts&ajax=1&'+data+'&'+list_data,
            function(d){
                var tmp = $('#users-list').find('input:checked').closest('tr');
                $('#users-list').html(d);
                $('#users-list table').append(tmp);
            }
        );
        return false;
    })
})

$(document).ready(function(){
    $("*[query-table]").on('chosen:no_results',function(){
        obj = $(this);
        if(obj.attr('query-title-field') == '')return;
        search_inpt = obj.parent().find('.search-field input')//obj.chosen().search_field;
        search = search_inpt.val();
        in_select = $(this).find('option:selected');
        exc = [];
        in_select.each(function(){
            exc.push($(this).val());
        })
        var fields_to_search = '';
        obj.parent().find('*[query-input-link][disabled!=disabled]').each(function(){
            fields_to_search += $(this).val()+',';
        });
        $.ajax({
            type: "post",
            url: "?dir=ajax&component=admin_ajax&path=autocomplete-field",
            data: {
                table:  obj.attr('query-table'),
                value_field: obj.attr('query-value-field'),
                title_field:  obj.attr('query-title-field'), 
                params: obj.attr('query-params'), 
                fields_to_search: fields_to_search.slice(0,-1),
                value:  search,
                limit:  obj.attr('query-limit'),
                exception: exc
            },
            success:function(data){
                if(data == '')return;
                obj.find('option').not(':selected').remove();
                obj.prepend(data);
                setTimeout(function(){
                        jQuery.unique(obj.find('option'));
                        obj.trigger("chosen:updated");
                        search_inpt.val(search);
                },1000);
            },
            error: function(data){

            }
        })
    });

    $('.allowed_fields').click(function(){
        select = $('#'+ $(this).attr('query-input-link') );
        if($(this).hasClass('active')){
            $(this).toggleClass( 'active' );
            $('input[query-input-link='+jq($(this).attr('query-input-link'))+']').removeAttr('disabled');
            select.attr('query-title-field','');
            select.find('option').not(':selected').remove();
        }else{
            $('input[query-input-link='+jq($(this).attr('query-input-link'))+']').attr('disabled','disabled');
            $(this).toggleClass( 'active' );
            $(this).removeAttr('disabled');
            select.attr('query-title-field',$(this).val());
        }
        select.trigger("chosen:updated");
    })
    $("*[query-table]").on('chosen:maxselected',function(){
        return false;
    })

});
$(document).ready(function(){
    $('select[name="search\[\]"]').change(function(evt, params){
        console.log(evt);
        console.log(params);
        if(params.deselected !== undefined){
            $('#product-'+params.deselected).remove();
            return;
        }
        $.ajax({
            url:'?dir=applications&component=shop&path=ajax&action=getOrderProductRow',
            type: 'post',
            data: {id:params.selected},
            success: function(data){
                $('#order-products').append(data);
                jQuery('.quantity');
            },
            error: function(data){
                console.log(data);
            }
        });
    })
});
function choseProductVariants(id,order_id,hash){
    $.ajax({
        url:'?dir=applications&component=shop&path=ajax&action=getOrderProductVariantsModal',
        type: 'post',
        data: {product_id:id,id:order_id,hash: hash},
        success: function(data){
            var tmp = $('<div></div>').append(data);
            tmp.find('input').removeAttr('checked');
            $('#product-variants-container-'+hash).find('.shop-variant[checked="checked"]').each(function(){
                var item = $(this);
                tmp.find('input[name="'+jq(item.attr('name'))+'"]').each(function(){
                    this.checked = "checked";
                })
                tmp.find('select[name="'+jq(item.attr('name'))+'"]').each(function(){
                    this.value = item.val();
                })
            });

            $('#modal-window').html(tmp).modal();
            $('#product-variants').submit(function(){
                $('#cancel-modal').trigger('click');
                $('#product-variants-container-'+hash).html($(this).find('input:checked').hide().attr('checked','checked'));
                $(this).find('select').each(function(){
                    $('#product-variants-container-'+hash).append(
                        $('<input checked="checked" type="hidden" class="shop-variant" />').val($(this).find('option:selected').val()).attr('name',$(this).attr('name'))
                    );
                });
                return false;
            });
        },
        error: function(data){
            console.log(data);
        },
    });
    return false;
}

$(document).ready(function(){
    $('body').on('click','input:radio',function(){
        var radiogroup = $(this).attr('group');
        $('input:radio[group="'+radiogroup+'"]').removeAttr('checked');
        $(this).prop( "checked", true );
    });
});

$(document).ready(function () {
    $('#massive_action_form').submit(function () {
        $(this).hide().prepend($('input[name="'+jq("items_checked[]")+'"]:checked'));
    });
});