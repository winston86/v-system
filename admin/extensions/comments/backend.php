<?php
/**
 * Class and Function List:
 * Function list:
 * - execute()
 * - getPathTable()
 * - viewEditForm()
 * - viewList()
 * - updateItemStatus()
 * - proccessMassiveActions()
 * - publishItem()
 * Classes list:
 * - CommentsBackend extends Extension
 */
use admin\Extension;

class CommentsBackend extends Extension
{

    public function execute()
    {

        global $Core;

        $menu_links = array();

        $menu_links[0]['dir']       = $this->dir;
        $menu_links[0]['component'] = $this->component;
        $menu_links[0]['path']      = 'all';
        $menu_links[0]['action']    = 'view';
        //      $menu_links[0]['img'] = "/images/lang-ua.png";
        $menu_links[0]['title'] = $this->lang['ALL_COMMENTS'];

        $menu_links[1]['dir']       = $this->dir;
        $menu_links[1]['component'] = $this->component;
        $menu_links[1]['path']      = 'new';
        $menu_links[1]['action']    = 'view';
        //      $menu_links[1]['img'] = "/images/lang-ua.png";
        $menu_links[1]['title'] = $this->lang['NEW_COMMENTS'];

        $menu = $Core->render->renderAdminMenu($menu_links);

        switch ($this->action) {

            case 'add':
            case 'edit':
                $content = $this->viewEditForm();
                break;
            case 'set_new_status':
                $this->updateItemStatus(1);
                break;
            case 'unset_new_status':
                $this->updateItemStatus(0);
                break;
            default:
                $content = $this->viewList();
                break;

        }

        parent::execute();

        return $menu . $content;

    }

    public function getPathTable()
    {

        return 'comments';

    }

    private function viewEditForm()
    {

        global $Core;

        $item_id = $Core->getRequest('id');

        if ($Core->inRequest('submit')) {

            $comment = $Core->getRequest('comment');

            $comment['user_id'] = $Core->clearValueFromHash($comment['user_id'][0]);

            $errors = $Core->checkFields($comment, $this->lang);

            if (count($errors) > 0) {

                $Core->back();

            }

            if ($this->action == 'add') {

                $item_id = $Core->db->insert('comment', $comment);

                $Core->sysMessage($item_id . ' ' . $this->lang['SAVED'], 'success');

            } else {

                $Core->db->update('comments', $comment, array(
                    'id' => $item_id,
                ));

                $Core->sysMessage($item_id . ' ' . $this->lang['SAVED'], 'success');

            }

        }

        $values = $comment;

        if (empty($values) && !empty($item_id)) {
            $values = $Core->db->selectFields('comments', '*', ['id' => $item_id]);
        }

        $fields_to_fill = array(
            'comment[page_title]' => array(
                'type'          => 'text',
                'default_value' => '',
                'value'         => @$values['page_title'],
                'attrs'         => array(
                    'placeholder' => $this->lang['PAGE_TITLE'],
                    'class'       => 'comment-field  form-control',
                ),
                'label'         => $this->lang['PAGE_TITLE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="comment-block ">%block%</div>',
            ),
            'comment[text]'       => array(
                'type'       => 'editor',
                'editor_type'=> 'custom',
                'value'      => @$values['text'],
                'attrs'      => array(
                    'class' => 'comment-field',
                ),
                'label'      => $this->lang['COMMENT'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="comment-block">%block%</div>',
            ),
            'comment[user_id]'    => array(
                'type'                 => 'autocomplete_chosen',
                'default_value'        => '',
                'value'                => [$values['user_id']],
                'attrs'                => array(
                    'data-placeholder'  => $this->lang['USER'],
                    'class'             => 'comment-input  form-control',
                    'query-table'       => 'users',
                    'query-value-field' => 'id',
                    'query-title-field' => 'login',
                    'query-params'      => "published=1",
                    'query-limit'       => 100,
                ),
                'options'              => [$values['user_id'] => $Core->users->getUserLogin($values['user_id'])],
                'label'                => $this->lang['USER'],
                'max_selected_options' => 1,
                'multiple'             => true,
                'allowed_fields'       => array(
                    'login',
                    'nickname',
                    'email',
                    'id',
                ),
                'name_wrap'            => '%name%',
                'input_wrap'           => '%input%',
                'block_wrap'           => '<div class="shop-order-product-add">%block%</div>',
            ),
            'comment[published]'  => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'comment-field',
                ),
                'default_value' => 1,
                'value'         => @$values['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '<input type="hidden" name="comment[published]" value="0" />%input%',
                'block_wrap'    => '<div class="comment-block checkbox">%block%</div>',
            ),
        );

        $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors) . '<input type="hidden" name="comment[new]" value="0" />';
        return $Core->itemForm($inputs_html, 'comment');

    }

    private function viewList()
    {

        global $Core;

        $current_path = "/admin?dir={$this->dir}&component={$this->component}&path={$this->path}";

        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        if ($this->action == "view") {

            $params            = $Core->getFilterParams();
            $params['user_id'] = $params['user'];

            $limit = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;

            if ($this->path == 'new') {
                $params['new'] = 1;
            } else {
                $params['new'] = ['operator' => '>=', 'value' => 0];
            }

            $items = $Core->db->selectRows('comments', '*', $params, $limit);

            $items_count = $Core->db->countRows('comments', $params);

            $url = $current_path . '&page={page}';

            foreach ($items as $k => $i) {
                $items[$k]['user'] = $Core->users->getUserLogin($i['user_id']);
                $items[$k]['new']  = '<a href="' . $current_path . '&action=' . ($i['new'] == 1 ? 'unset_new_status' : 'set_new_status') . '&id=' . $i['id'] . '" class="glyphicon glyphicon-eye-' . ($i['new'] == 1 ? 'close' : 'open') . '"></a>';
            }

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'page_title',
                'text',
                'user',
                'date',
                'new',
                'published',
                'tools',
            );
            $list['width'] = array(
                'id'         => 50,
                'page_title' => 200,
                'text'       => 200,
                'user'       => 100,
                'date'       => 50,
                'new'        => 70,
            );
            $list['head'] = array(
                'id'         => $this->lang['ID_SHORT'],
                'page_title' => $this->lang['PAGE_TITLE'],
                'text'       => $this->lang['TEXT'],
                'user'       => $this->lang['USER'],
                'date'       => $this->lang['DATE'],
                'new'        => $this->lang['NEW_COMMENTS'],
                'published'  => $this->lang['PUBLISHED'],
                'tools'      => $this->lang['TOOLS'],
            );

            $users_array[0] = $this->lang['ALL'];
            $users_array += $Core->users->getUsersIdLoginArray();

            $list['filter'] = array(
                'id'         => $Core->getFilterParamsVal($params, 'id'),
                'page_title' => $Core->getFilterParamsVal($params, 'page_title'),
                'text'       => ['value' => $Core->getFilterParamsVal($params, 'text'),
                    'operator'               => 'LIKE'],
                'user'       => ['value' => $Core->getFilterParamsVal($params, 'user'),
                    'options'                => $users_array,
                    'type'                   => 'select'],
                'date'       => ['value' => $Core->getFilterParamsVal($params, 'date'),
                    'type'                   => 'date',
                    'operator'               => 'LIKE'],
                'new'        => '-',
                'published'  => $Core->getFilterParamsVal($params, 'published'),
            );

            $list['mass_actions'] = ['set_new_status' => $this->lang['SET_NEW_STATUS'], 'unset_new_status' => $this->lang['UNSET_NEW_STATUS']];

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $url);

            return $sub_menu . $list_html . $pagination_html;
        }

    }

    private function updateItemStatus($new, $params = [])
    {

        global $Core;

        if (empty($params)) {
            $params = ['id' => $Core->getRequest('id')];
        }

        if ($Core->db->update('comments', ['new' => $new], $params)) {

            if ($new == 1) {
                $Core->sysMessage($this->lang['SET_NEW_STATUS_SUCCESS'], 'success');
            } else {
                $Core->sysMessage($this->lang['UNSET_NEW_STATUS_SUCCESS'], 'success');
            }

        }

        if (!$Core->inRequest('mass_action') && $this->action != 'publish') {
            $Core->back();
        }

    }

    public function proccessMassiveActions()
    {

        global $Core;

        $action = $Core->getRequest('massive_action');

        switch ($action) {
            case 'set_new_status':
                $this->updateItemStatus(1, ['id' => $Core->getRequest('items_checked')]);
                break;
            case 'unset_new_status':
            case 'publish':
                $this->updateItemStatus(0, ['id' => $Core->getRequest('items_checked')]);
                break;
        }

    }

    public function publishItem()
    {

        global $Core;

        $item_status = $Core->db->selectField('comments', 'new', ['id' => $Core->getRequest('id')]);

        if ($item_status == 1) {
            $this->updateItemStatus(0, ['id' => $Core->getRequest('id')]);
        }

        parent::publishItem(); // TODO: Change the autogenerated stub

    }

}
