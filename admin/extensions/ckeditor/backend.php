<?php
/**
 * Class and Function List:
 * Function list:
 * - execute()
 * - getPathTable()
 * - viewSettingsByGroup()
 * Classes list:
 * - CkeditorBackend extends Extension
 */
use admin\Extension;

class CkeditorBackend extends Extension
{

    static $toolbars = ['document' => ['Source', 'Save', 'NewPage', 'Preview', 'Print', 'Templates'], 'clipboard' => ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', 'Undo', 'Redo'], 'editing' => ['Find', 'Replace', 'SelectAll', 'Scayt'], 'Forms' => ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'], 'basicstyles' => ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'RemoveFormat'], 'paragraph' => ['NumberedList', 'BulletedList', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'BidiLtr', 'BidiRtl', 'Language'], 'links' => ['Link', 'Unlink', 'Anchor'], 'insert' => ['Image', 'Flash', 'Table', 'Smiley', 'HorizontalRule', 'SpecialChar', 'PageBreak', 'Iframe'], 'styles' => ['Styles', 'Format', 'Font', 'FontSize'], 'colors' => ['TextColor', 'BGColor'], 'tools' => ['Maximize', 'ShowBlocks'], 'about' => ['About']];

    public function execute()
    {

        global $Core;

        $menu_links = array();

        $groups = $Core->db->selectRows('user_groups', 'id, title');

        $group_title = empty($this->path) ? $groups[0]['title'] : $Core->db->selectField('user_groups', 'title', ['id' => $this->path]);

        $this->title = '<h1>' . $group_title . '</h1>';

        $this->path = empty($this->path) ? $groups[0]['id'] : $this->path;

        foreach ($groups as $k => $g) {

            $menu_links[$k]['dir']       = $this->dir;
            $menu_links[$k]['component'] = $this->component;
            $menu_links[$k]['path']      = $g['id'];
            $menu_links[$k]['action']    = 'view';
            //      $menu_links[0]['img'] = "/images/lang-ua.png";
            $menu_links[$k]['title'] = $g['title'];

        }

        $menu = $Core->render->renderAdminMenu($menu_links);

        switch ($this->action) {

            default:
                $content = $this->viewSettingsByGroup();

                break;

        }

        return $menu . $content;

    }

    public function getPathTable()
    {

        return 'extensions';

    }

    private function viewSettingsByGroup()
    {

        global $Core;

        if ($Core->inRequest('submit')) {
            $settings       = $Core->getRequest('ckeditor');
            $settings       = array_merge($this->settings, $settings);
            $this->settings = $settings;
            $Core->updateExtensionSettings($settings, 'ckeditor');
        }

        $values = $this->settings['_' . $this->path];

        $fields_to_fill = [];

        foreach (static::$toolbars as $name => $tools) {

            foreach ($tools as $key => $tool) {

                $value = $values[$name][$tool];

                $attrs          = [];
                $attrs['class'] = 'ckeditor-settings-field';

                if ($tool == '-') {
                    //                    $attrs['disabled'] = 'disabled';
                    $attrs['class'] .= ' hidden';
                    $value = 1;
                }

                if ($key == 0) {

                    $block_title = "<br><label>{$name}</label><hr>";

                } else {

                    $block_title = '';

                }

                $fields_to_fill['ckeditor[_' . $this->path . '][' . $name . '][' . $tool . ']'] = array(
                    'type'          => 'checkbox',
                    'attrs'         => $attrs,
                    'value'         => @$value,
                    'label'         => $this->lang[$tool],
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => $block_title . '<div class="ckeditor-settings-block checkbox">%block%</div>',
                );

            }

        }

        $inputs_html = '<h1>' . $this->title . '</h1>' . $Core->render->renderInputs($fields_to_fill, false);

        return $Core->itemForm($inputs_html, 'ckeditor');

    }

}
