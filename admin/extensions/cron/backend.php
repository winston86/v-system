<?php
/**
 * Class and Function List:
 * Function list:
 * - execute()
 * - viewEditForm()
 * - viewList()
 * - getPathTable()
 * Classes list:
 * - CronBackend extends Extension
 */
use admin\Extension;

class CronBackend extends Extension
{

    public function execute()
    {

        global $Core;

        $menu_links = array();

        $menu_links[0]['dir']       = $this->dir;
        $menu_links[0]['component'] = $this->component;
        $menu_links[0]['path']      = 'default';
        $menu_links[0]['action']    = 'add';
        //$menu_links[0]['img'] = "/images/lang-ua.png";
        $menu_links[0]['title'] = $this->lang['ADD'];

        $menu = $Core->render->renderAdminMenu($menu_links);

        switch ($this->action) {

            case 'add':
            case 'edit':
                $content = $this->viewEditForm();
                break;
            case 'delete':
                $this->deleteItem();
                break;
            case 'publish':
                $this->publishItem();
                break;
            case 'unpublish':
                $this->unpublishItem();
                break;
            default:
                $content = $this->viewList();
                break;

        }

        return $menu . $content;

    }

    private function viewEditForm()
    {

        global $Core;

        $item_id = $Core->getRequest('id');

        if ($Core->inRequest('submit')) {

            $cron = $Core->getRequest('cron');

            $errors = $errors = $Core->checkFields($cron, $this->lang);

            if (count($errors) > 0) {

                $Core->back();

            }

            if ($this->action == 'add') {

                $item_id = $Core->db->insert('cron', $cron);

                $Core->sysMessage($item_id . ' ' . $this->lang['SAVED'], 'success');

            } else {

                $Core->db->update('cron', $cron, array(
                    'id' => $item_id,
                ));

                $Core->sysMessage($item_id . ' ' . $this->lang['SAVED'], 'success');

            }

        }

        $values = $cron;

        if (empty($values) && !empty($item_id)) {
            $values = $Core->db->selectFields('cron', '*', ['id' => $item_id]);
        }

        $time_options = ['60' => $this->lang['MINUTE'], '300' => $this->lang['5_MINUTE'], '3600' => $this->lang['HOUR'], '43200' => $this->lang['12_HOURS'], '86400' => $this->lang['DAY'], '604800' => $this->lang['WEEK']];

        $fields_to_fill = array(
            'cron[title]'       => array(
                'type'          => 'text',
                'default_value' => '',
                'value'         => @$values['title'],
                'attrs'         => array(
                    'placeholder' => $this->lang['CRON_TITLE'],
                    'class'       => 'cron-field  form-control',
                ),
                'label'         => $this->lang['CRON_TITLE'],
                'required'      => true,
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="cron-block ">%block%</div>',
            ),
            'cron[description]' => array(
                'type'       => 'editor',
                'editor_type'=> 'custom',
                'value'      => @$values['description'],
                'attrs'      => array(
                    'class' => 'cron-field',
                ),
                'label'      => $this->lang['DESCRIPTION'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="cron-block">%block%</div>',
            ),
            'cron[url]'         => array(
                'type'          => 'text',
                'default_value' => '',
                'value'         => @$values['url'],
                'attrs'         => array(
                    'placeholder' => $this->lang['CRON_URL'],
                    'class'       => 'cron-field  form-control',
                ),
                'label'         => $this->lang['CRON_URL'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '%input%',
                'block_wrap'    => '<div class="cron-block ">%block%</div>',
            ),
            'cron[i_val]'       => array(
                'type'       => 'select',
                'value'      => $values['i_val'],
                'options'    => $time_options,
                'attrs'      => array(
                    'class' => 'cron-field form-control',
                ),
                'label'      => $this->lang['I_VAL'],
                'name_wrap'  => '%name%',
                'input_wrap' => '%input%',
                'block_wrap' => '<div class="cron-block">%block%</div>',
            ),
            'cron[published]'   => array(
                'type'          => 'checkbox',
                'attrs'         => array(
                    'class' => 'cron-field',
                ),
                'default_value' => 1,
                'value'         => @$values['published'],
                'label'         => $this->lang['PUBLISHED'],
                'name_wrap'     => '%name%',
                'input_wrap'    => '<input type="hidden" name="cron[published]" value="0" />%input%',
                'block_wrap'    => '<div class="cron-block checkbox">%block%</div>',
            ),
        );

        $inputs_html = $Core->render->renderInputs($fields_to_fill, false, $errors);
        return $Core->itemForm($inputs_html, 'cron');

    }

    private function viewList()
    {

        global $Core;

        $sub_menu = $Core->render->renderAdminMenu($sub_menu_links);

        if ($this->action == "view") {
            $params      = $Core->getFilterParams();
            $limit       = (($this->page - 1) * $this->on_page) . ', ' . $this->on_page;
            $items       = $Core->db->selectRows('cron', '*', $params, $limit);
            $items_count = $Core->db->countRows('cron', '*');

            $url = $this->backLink . '&page={page}';

            $list['items'] = $items;
            $list['keys']  = array(
                'id',
                'title',
                'description',
                'url',
                'i_val',
                'published',
                'tools',
            );
            $list['head'] = array(
                'id'          => $this->lang['ID'],
                'title'       => $this->lang['TITLE'],
                'description' => $this->lang['DESCRIPTION'],
                'url'         => $this->lang['CRON_URL'],
                'i_val'       => $this->lang['I_VAL'],
                'published'   => $this->lang['PUBLISHED'],
                'tools'       => $this->lang['TOOLS'],
            );
            $list['filter'] = array(
                'id'          => $Core->getFilterParamsVal($params, 'id'),
                'title'       => $Core->getFilterParamsVal($params, 'title'),
                'description' => $Core->getFilterParamsVal($params, 'description'),
                'url'         => $Core->getFilterParamsVal($params, 'url'),
                'i_val'       => $Core->getFilterParamsVal($params, 'i_val'),
                'published'   => $Core->getFilterParamsVal($params, 'published'),
            );

            $current_path = "/admin?dir={$this->dir}&component={$this->component}&path={$this->path}";

            $list_html = $Core->render->renderAdminList($list, $current_path);

            $pagination_html = $Core->render->getPagination($items_count, $this->on_page, $this->page, $url);

            return $sub_menu . $list_html . $pagination_html;
        }

    }

    public function getPathTable()
    {

        return 'cron';

    }

}
