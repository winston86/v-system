<?php
/**
 * Class and Function List:
 * Function list:
 * - execute()
 * - getPathTable()
 * - viewSettings()
 * Classes list:
 * - AceEditorBackend extends Extension
 */
use admin\Extension;

class AceEditorBackend extends Extension
{

    static $params = ['theme' => ['ace/theme/ambiance' => 'ambiance', 'ace/theme/chaos' => 'chaos', 'ace/theme/chrome' => 'chrome', 'ace/theme/clouds' => 'clouds', 'ace/theme/cobalt' => 'cobalt', 'ace/theme/crimson_editor' => 'crimson_editor', 'ace/theme/dawn' => 'dawn', 'ace/theme/dreamweaver' => 'dreamweaver', 'ace/theme/eclipse' => 'eclipse', 'ace/theme/github' => 'github', 'ace/theme/gob' => 'github', 'ace/theme/gruvbox' => 'gruvbox', 'ace/theme/idle_fingers' => 'idle_fingers', 'ace/theme/iplastic' => 'iplastic', 'ace/theme/katzenmilch' => 'iplastic', 'ace/theme/kr_theme' => 'kr_theme', 'ace/theme/kuroir' => 'kuroir', 'ace/theme/merbivore' => 'merbivore', 'ace/theme/merbivore_soft' => 'merbivore_soft', 'ace/theme/mono_industrial' => 'mono_industrial', 'ace/theme/monokai' => 'monokai', 'ace/theme/pastel_on_dark' => 'pastel_on_dark', 'ace/theme/solarized_dark' => 'solarized_dark', 'ace/theme/solarized_light' => 'solarized_light', 'ace/theme/sql_server' => 'sql_server', 'ace/theme/terminal' => 'terminal', 'ace/theme/textmate' => 'textmate', 'ace/theme/tomorrow' => 'tomorrow', 'ace/theme/tomorrow_night' => 'tomorrow_night', 'ace/theme/tomorrow_night_blue' => 'tomorrow_night_blue', 'ace/theme/tomorrow_night_bright' => 'tomorrow_night_bright', 'ace/theme/tomorrow_night_eighties' => 'tomorrow_night_eighties', 'ace/theme/twilight' => 'twilight', 'ace/theme/vibrant_ink' => 'vibrant_ink', 'ace/theme/xcode' => 'xcode'], 'fontSize' => 12, 'setTabSize' => 4, 'setShowPrintMargin' => true, 'setHighlightActiveLine' => true, 'setUseWrapMode' => false, 'setUseSoftTabs' => false];

    public function execute()
    {

        switch ($this->action) {

            default:
                $content = $this->viewSettings();

                break;

        }

        return $content;

    }

    public function getPathTable()
    {

        return 'extensions';

    }

    public function viewSettings()
    {

        global $Core;

        if ($Core->inRequest('submit')) {
            $settings       = $Core->getRequest('aceeditor');
            $settings       = array_merge($this->settings, $settings);
            $this->settings = $settings;
            $Core->updateExtensionSettings($settings, 'aceeditor');
        }

        $values = $this->settings;

        $fields_to_fill = [];

        foreach (static::$params as $key => $param) {

            if (is_array($param)) {
                $fields_to_fill['aceeditor[' . $key . ']'] = array(
                    'type'       => 'select',
                    'value'      => !empty($values[$key]) ? $values[$key] : '',
                    'options'    => $param,
                    'attrs'      => array(
                        'class' => 'form-control aceeditor-param',
                        'style' => 'width:100%',
                    ),
                    'label'      => $this->lang[strtoupper($key)],
                    'name_wrap'  => '%name%',
                    'input_wrap' => '%input%',
                    'block_wrap' => '<div  class="col-md-12 text-center">%block%</div>',
                );
            } elseif (is_integer($param)) {
                $fields_to_fill['aceeditor[' . $key . ']'] = array(
                    'type'          => 'counter',
                    'value'         => @$values[$key],
                    'default_value' => $param,
                    'attrs'         => array(
                        'class' => 'aceeditor-param  form-control',
                    ),
                    'label'         => $this->lang[strtoupper($key)],
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div  class="col-md-12">%block%</div>',
                );
            } elseif (is_bool($param)) {
                $fields_to_fill['aceeditor[' . $key . ']'] = array(
                    'type'          => 'checkbox',
                    'default_value' => 1,
                    'value'         => $values[$key] ? 1 : 0,
                    'attrs'         => array(
                        'class' => 'aceeditor-param',
                    ),
                    'label'         => $this->lang[strtoupper($key)],
                    'name_wrap'     => '%name%',
                    'input_wrap'    => '%input%',
                    'block_wrap'    => '<div  class="col-md-12">%block%</div>',
                );
            }

        }

        $inputs_html = $Core->render->renderInputs($fields_to_fill, false);

        return $Core->itemForm($inputs_html, 'aceeditor');

    }

}
