<?php

class AdminAjaxBackend
{

    public function __construct()
    {
        global $Core;

        $this->dir = $Core->getRequest('dir');

        $this->component = $Core->getRequest('component');

        $this->path = $Core->getRequest('path');

        $this->action = $Core->inRequest('action') ? $Core->getRequest('action') : 'view';

        $this->backLink = '/admin?dir=' . $this->dir . '&component=' . $this->component . '&path=' . $this->path . '&action=' . $this->action;

        $this->comLink = '/admin?dir=' . $this->dir . '&component=' . $this->component;

        $this->pathLink = '/admin?dir=' . $this->dir . '&component=' . $this->component . '&path=' . $this->path . '&action=view';

        $this->lang = array_merge($Core->getLang('applications/shop'), $Core->getLang('global'));

    }

    public function execute()
    {

        global $Core;

        switch ($this->path) {
            case 'autocomplete-field':
                die($this->getAutocompleteData());
                break;

            default:
                break;
        }

    }

    public function getAutocompleteData()
    {

        global $Core;

        $title_field = $Core->getRequest('title_field');

        $value_field = $Core->getRequest('value_field');

        $value = $Core->getRequest('value');

        $table = $Core->getRequest('table');

        $limit = $Core->getRequest('limit', 'int');

        $exc = $Core->inRequest('exception') ? $Core->getRequest('exception') : array();

        $params = array();

        foreach (explode(",", $Core->getRequest('fields_to_search')) as $key => $field) {
            if ($field == 'id') {
                $ids          = array_diff(array($value), $exc);
                $params['id'] = $ids;
            } else {
                $params[$field] = array(
                    'operator' => 'LIKE',
                    'value'    => "%" . $value . "%",
                );
            }
        }

//        if($title_field != 'id' && $Core->inRequest('exception')  ){
        //            $params['id'] = array(
        //                'operator' => 'NOT IN',
        //                'value' => $exc
        //                );
        //        }

        parse_str($Core->getRequest('params'), $tmp);

        $params = array_merge($tmp, $params);

        $res = $Core->db->selectRows(
            $table,
            $value_field . ',' . $title_field,
            $params,
            $limit
        );

        $html = '';
        foreach ($Core->getValEqLabelArr($res, $value_field, $title_field) as $key => $value) {

            $html .= '<option value="' . $key . '-' . (md5(uniqid(rand(), true))) . '">' . $value . '</option>';

        }

        $Core->response($html, 'text/html');

    }

}
