<?php
/**
 * Class and Function List:
 * Function list:
 * - execute()
 * - getSettings()
 * Classes list:
 * - HtmlBackend extends Module
 */
use admin\Module;

class HtmlBackend extends Module
{

    public function execute()
    {

        global $Core;

        $menu_links = array();

        $menu_links[0]['dir']       = $this->dir;
        $menu_links[0]['component'] = $this->component;
        $menu_links[0]['path']      = 'out-list';
        $menu_links[0]['action']    = 'view';
        //        $menu_links[0]['img'] = "/images/lang-ua.png";
        $menu_links[0]['title'] = $this->lang['LIST'];

        $menu_links[1]['dir']       = $this->dir;
        $menu_links[1]['component'] = $this->component;
        $menu_links[1]['path']      = 'out-add';
        $menu_links[1]['action']    = 'out-add';
        //        $menu_links[1]['img'] = "/images/lang-ua.png";
        $menu_links[1]['title'] = $this->lang['ADD'];

        $menu = $Core->render->renderAdminMenu($menu_links);

        switch ($this->path) {

            case 'out-list':
                $content = $this->viewOutList();
                break;

            case 'out-add':
            case 'out-edit':
                $content = $this->viewAddForm();
                break;

            default:
                $content = $this->viewOutList();
                break;

        }

        parent::execute();

        return $menu . $content;

    }

    public function getSettings($out_id = null)
    {

        global $Core;

        if ($out_id > 0) {

            $settings = unserialize($Core->db->selectField('apps_in_page', 'settings', array(
                'id' => $out_id,
            )));

            $data['html'] = $settings['html'];

        } else {

            $data['html'] = '';

        }

        $data['LANG'] = $this->lang;

        return $Core->render->renderTpl('/admin/tpls/modules/html_settings.twig', $data);

    }

}
