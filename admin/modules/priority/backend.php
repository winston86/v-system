<?php
/**
 * Class and Function List:
 * Function list:
 * - execute()
 * - viewOutList()
 * Classes list:
 * - PriorityBackend extends Module
 */
use admin\Admin;
use admin\Module;

class PriorityBackend extends Module
{

    public function execute()
    {

        $content = $this->viewOutList();

        parent::execute();

        return $content;

    }

    public function viewOutList()
    {

        $this->com_id = ['operator' => "<", 'value' => '0'];

        return Admin::renderModuleList($this);

    }

}
