<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - execute()
 * Classes list:
 * - DesignBackend
 */
class DesignBackend
{

    public function __construct()
    {

        global $Core;

        $this->dir = $Core->getRequest('dir');

        $this->component = $Core->getRequest('component');

        $this->path = $Core->getRequest('path');

        $this->action = $Core->getRequest('action');

        $this->comLink = "/admin?dir=settings";

        $this->lang = array_merge($Core->getLang('settings/design'), $Core->getLang('global'));

    }

    public function execute()
    {

        global $Core;

        if ($Core->inRequest('submit')) {

            $settings = $Core->getRequest('settings');

            $new_positions = $settings['positions']['new'];

            $positions = $settings['positions'];

            unset($settings['positions']);

            foreach ($new_positions as $position) {

                if (!empty($position)) {

                    $Core->db->insert('tpl_positions', array(
                        'title' => $position,
                    ));

                }

            }

            foreach ($positions as $id => $position) {

                if (!empty($position)) {

                    $Core->db->update('tpl_positions', array(
                        'title' => $position,
                    ), array(
                        'id' => $id,
                    ));

                } else {

                    $Core->delete('tpl_positions', array(
                        'id' => $id,
                    ));

                }

            }

            if ($Core->inRequest('del_pos')) {

                $positions_for_del = trim($Core->getRequest('del_pos'), ",");

                $sql = "DELETE FROM {$Core->db->prefix}tpl_positions WHERE id IN({$positions_for_del})";

                $Core->db->query($sql);

            }

            $settings_str = serialize($settings);

            if ($Core->db->update('settings', array(
                'settings' => $settings_str,
            ), array(
                'route' => 'design',
            ))) {

                $Core->sysMessage($this->lang['SAVED'], 'success');

            }

        }

        $data = array();

        $data['LANG'] = $this->lang;

        $data['comLink'] = $this->comLink;

        $data['settings'] = unserialize($Core->db->selectField('settings', 'settings', array(
            'route' => 'design',
        )));

        $data['settings']['positions'] = $Core->db->selectRows('tpl_positions', '*');

        return $Core->render->renderTpl("/admin/tpls/settings/design.twig", $data);

    }

}
