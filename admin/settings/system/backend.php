<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - execute()
 * Classes list:
 * - SystemBackend
 */
class SystemBackend
{

    public function __construct()
    {

        global $Core;

        $this->dir = $Core->getRequest('dir');

        $this->component = $Core->getRequest('component');

        $this->path = $Core->getRequest('path');

        $this->action = $Core->getRequest('action');

        $this->comLink = "/admin?dir={$this->dir}&component={$this->component}";

        $this->lang = array_merge($Core->getLang('settings/system'), $Core->getLang('global'));

    }

    public function execute()
    {

        global $Core;

        global $_CONFIG;

        if ($Core->inRequest('submit')) {

            $_CONFIG_STR = "<?php \r\n \$_CONFIG = array( \r\n";

            $config = array_merge($_CONFIG, $Core->getRequest('config'));

            foreach ($config as $name => $value) {

                $_CONFIG_STR .= "'" . $name . "' => '" . $value . "',\r\n";

            }

            $_CONFIG_STR .= "); \r\n ?>";

            if (file_put_contents(ROOT_DIR . "/config.inc.php", $_CONFIG_STR)) {

                $Core->sysMessage($this->lang['SAVED'], 'success');

                $_CONFIG = $config;

            }

        }

        $data = array();

        $data['LANG'] = $this->lang;

        $data['comLink'] = $this->comLink;

        $data['settings'] = $_CONFIG;

        return $Core->render->renderTpl("/admin/tpls/settings/system.twig", $data);

    }

}
