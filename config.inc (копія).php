<?php
$_CONFIG = array(
'DB_HOST'			=> 	"localhost",
'DB_DATABASE'			=>	"system",
'DB_PREFIX'			=>	"sys_",
'DB_USER'			=> 	"root",
'DB_PASS'				=> 	"",
'DB_ENC'				=>	"utf8",
'TEMPLATE'			=>	"native",
'SITE_NAME'			=> 	"victor-system",
'SITE_KEYWORDS'		=>	"victor-system",
'SITE_DESCRIPTION'		=>	"victor-system",
'LANG'				=>	"ua",
'HOME'				=>	"content"
);
?>