<?php
// usage in order process
/*
include($delivery['script_path']);
$service = new array_pop(get_declared_classes());
$price_of_delivery = $service->getDeliveryPrice();
 */

class novaPoshta
{

    public function __construct()
    {
        global $Core;
        $this->api_key        = "3c79352a14f5bf60cc8521e9ac829f48";
        $this->cities_library = $_SERVER['DOCUMENT_ROOT'] . '/frontend/applications/shop/extensions/nova_poshta/cities_data.json';
        $this->api_url        = "http://api.novaposhta.ua/v2.0/json/";
    }

    /*params
    CitySenderId*    int[36]    id города отправителя (в апі використовується код)
    CityRecipientId*    int[36]    id города получателя (в апі використовується код)
    Weight*    int[36]    min - 0,1 Вес фактический
    ServiceType*    int[36]    Тип услуги
    Cost*    int[36]    Целое число, объявленная стоимость (если объявленная стоимость не указана, API автоматически подставит минимальную объявленную цену - 300.00
    CargoType    string[36]    Значение из справочника Тип груза
    SeatsAmount    string[36]    Целое число, количество мест отправления
    RedeliveryCalculate    string[36]    Обратная доставка
    Amount    int[36]    Целое число
     */

    public function getDeliveryPrice($CitySenderId, $CityRecipientId, $Weight, $ServiceType, $Cost, $CargoType = null, $SeatsAmount = null)
    {

        $json = '
			{
			   "modelName": "InternetDocument",
			   "calledMethod": "getDocumentPrice",
			   "methodProperties": {
			      "CitySender": "' . $this->getCityRef($CitySenderId) . '",
			      "CityRecipient": "' . $this->getCityRef($CityRecipientId) . '",
			      "Weight": "' . $Weight . '",
			      "ServiceType": "' . $ServiceType . '",
			      "Cost": "' . $Cost . '",
			      "CargoType": "' . $CargoType . '",
			      "SeatsAmount": "' . $SeatsAmount . '"
			   },
			   "apiKey": "' . $this->api_key . '"
			}
	 	';

        $params = $this->getQueryFromJson($json);

        $res = $this->sendRequest($params);

        //return cost
        return $res->data['Cost'];

    }

    public function getQueryFromJson($json)
    {

        $params_data = json_decode($json);

        $params = http_build_query($params_data);

        return $params;

    }

    //returns HTML for order page (frontend)
    public function getDeliveryBlock()
    {

    }

    public function getCityRef($id)
    {
        $city = $this->getCityById($id);
        return $city->Ref;
    }

    public function getCityById($id)
    {
        $cities = $this->getAllCities();
        return $cities[$id];
    }

    public function getAllCities()
    {
        $raw_data = json_decode(file_get_contents($this->cities_library));
        foreach ($raw_data->data as $key => $city) {
            $data[$city['id']] = $city;
        }
        return $data;
    }

    public function refreshData()
    {

        $json = '{
					"modelName": "Address",
					"calledMethod": "getCities",
					"apiKey": "' . $this->api_key . '"
				}';

        $params = $this->getQueryFromJson($json);

        $data = $this->sendRequest($params);

        /*if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/frontend/applications/shop/extensions/nova_poshta/cities_data.json')){
        //create file
        }*/

        return file_put_contents($this->cities_library, json_encode($data));

    }

    public function sendRequest($params)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $Tthis->api_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $data = curl_exec($ch);

        curl_close($ch);

        $data = json_decode($data);

        if ($data['statusCode'] == 429) {
            sleep(2);
            $data = $this->sendRequest($params);
        } else {
            return json_decode($data);
        }
    }

}
