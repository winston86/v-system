<?php

namespace frontend\applications\shop;

use \core\Render;

class ShopView extends Render
{

    public function renderList($data)
    {

        return $this->renderTpl("{$this->tplDir}/applications/shop/list.tpl.php", $data);

    }

    public function renderItem($data)
    {

        return $this->renderTpl("{$this->tplDir}/applications/shop/item.tpl.php", $data);

    }

    public function renderShopMain($data)
    {

    }

}
