<?php

/**
 * Class shop_controller
 */
class ShopController extends AppBaseController
{

    public static $pathes = [
        'category' => 'loadCategory',
        'product' => 'loadProduct',
        'manufacturer' => 'loadManufacturer',
        'manufacturer' => 'loadWarhouse',
        'default' => 'loadShopMain'
    ];

    protected function loadShopMain()
    {
        $main = [];
        return $this->view->renderShopMain($main);
    }

    protected function loadCategory()
    {
        global $Core;

        $id = $Core->getRequest('id');

        $category = $this->model->getCategory($id);

        return $this->view->renderCategory($category);

    }

    protected function loadManufacturer()
    {

    }

    protected function loadWarhouse()
    {

    }

}
