<?php

namespace frontend\applications\landing;

use core\Multilang;
use \core\Render;

class LandingView extends Render
{

    public function renderBlocks($blocks)
    {

        $multilang = new Multilang();

        $multilang->getValuesForCurrentLang($blocks);

        return $this->renderTpl("{$this->tplDir}/applications/landing/page.twig", ['blocks' => $blocks]);

    }

}
