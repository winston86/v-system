<?php
namespace frontend\applications\landing;

use core\AppBaseController;

class LandingController extends AppBaseController
{

    public static $pathes = [
        'default' => 'viewLandingPage'
    ];

    protected function viewLandingPage()
    {

        $blocks = $this->model->getBlocks(['published' => 1]);

        return $this->view->renderBlocks($blocks);

    }

}
