<?php

namespace frontend\applications\landing;

use core\BaseModel;

class LandingModel extends BaseModel
{

    public function getBlocks($params = [])
    {

        return $this->selectRows('landing', '*', $params);

    }

}
