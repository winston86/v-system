<?php

namespace frontend\applications\users;

use \core\Render;

class UsersView extends Render
{

    public function viewRegister($data)
    {

        return $this->renderTpl($this->tplDir . '/applications/users/register.tpl.php', $data);

    }

    public function viewLogin($data)
    {

        return $this->renderTpl($this->tplDir . '/applications/users/login.tpl.php', $data);

    }

    public function renderProfile($data)
    {

        return $this->renderTpl($this->tplDir . '/applications/users/profile.tpl.php', $data);

    }

    public function renderProfSettings($data)
    {

        return $this->renderTpl($this->tplDir . '/applications/users/profile_settings.tpl.php', $data);

    }

}
