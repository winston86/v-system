<?php

namespace frontend\applications\users;

use core\AppBaseController;

/**
 * Class UsersController
 * @package frontend\applications\users
 */
class UsersController extends AppBaseController
{

    public static $pathes = [
        'register' => 'viewRegister',
        'login' => 'viewLogin',
        'logout' => 'userLogout',
        'profile' => 'getProfile',
        'default' => 'viewLogin'
    ];

    /**
     * UsersController constructor.
     */
    public function __construct()
    {

        global $Core;

        $this->url_vars = $Core->getUrlVars();

        $this->data['LANG'] = array_merge($Core->getLang('global'), $Core->getLang('applications/users'));

        parent::__construct();

    }

    protected function viewLogin()
    {

        global $Core;

        if ($Core->inRequest('user_login')) {

            $login = $Core->getRequest('login');

            $pass = $Core->getRequest('pass');

            if ($Core->users->isLoged($login)) {
                $Core->sysMessage($data['LANG']['ALLREADY_LOGED_IN'], 'info');
                $Core->back();}

            if ($login == '' || $pass == '' || !isset($login) || !isset($pass)) {

                $Core->sysMessage($this->data['LANG']['WRONG_LOG_DATA'], 'error');
                $Core->back();
            }

            $this->model->userLogin($login, md5($pass));

        }

        return $this->view->viewLogin($this->data);
    }

    protected function viewRegister()
    {

        global $Core;

        if ($Core->inRequest('user_register')) {

            $login = $Core->getRequest('login');

            $nickname = $Core->getRequest('nickname');

            $firstname = $Core->getRequest('firstname');

            $lastname = $Core->getRequest('lastname');

            $email = $Core->getRequest('email');

            $pass = $Core->getRequest('pass');

            $pass2 = $Core->getRequest('pass2');

            $errors = false;

            if ($login == '' || !isset($login) || strlen($login) < 5) {

                $Core->sysMessage($this->data['LANG']['WRONG_LOGIN_DATA'], 'error');

                $errors = true;

            }

            if ($nickname == '' || !isset($nickname) || strlen($nickname) < 3) {

                $Core->sysMessage($this->data['LANG']['WRONG_NICKNAME_DATA'], 'error');

                $errors = true;

            }

            if ($email == '' || !isset($email)) {

                $Core->sysMessage($this->data['LANG']['WRONG_EMAIL_DATA'], 'error');

                $errors = true;

            } elseif (preg_match("/^(\w*)@(\w*).(\w*)$/", $email) == 0) {

                $Core->sysMessage($this->data['LANG']['WRONG_EMAIL_DATA'], 'error');

                $errors = true;

            }

            if ($pass == '' || !isset($pass) || strlen($pass) < 6 || preg_match("/^(\w*)$/", $pass) == 0) {

                $Core->sysMessage($this->data['LANG']['WRONG_PASS_DATA'], 'error');

                $errors = true;

            } elseif ($pass != $pass2) {

                $Core->sysMessage($this->data['LANG']['CHECK_PASS'], 'error');

                $errors = true;

            }

            if ($errors) {$Core->back();}

            $userData['login'] = $login;

            $userData['nickname'] = $nickname;

            $userData['firstname'] = $firstname;

            $userData['lastname'] = $lastname;

            $userData['email'] = $email;

            $userData['password'] = md5($pass);

            $userData['id'] = $user_id = $this->model->register($userData);

        }

        return $this->view->viewRegister($this->data);

    }

    protected function userLogout()
    {
        return $this->model->userLogout($this->data);
    }

    /**
     * @return mixed
     */
    protected function profileSettings()
    {

        global $Core;

        if (!$Core->users->isMyProfile($this->url_vars['profile_login']) || !$Core->users->isLoged()) {

            $Core->sysMessage($this->data['LANG']['EDIT_NOT_ALLOWED']);

            $Core->back;

        }

        if ($Core->inRequest('submit')) {

            $settings = $Core->getRequest('settings');

            $this->model->saveSettings($this->url_vars['profile_login'], $settings);

            $Core->redirect('/users/profile/' . $this->url_vars['profile_login']);

        } else {

            $this->data['user'] = $this->model->getUserInfo($Core->getRequest('profile_login'));

            return $this->view->renderProfSettings($this->data);

        }

    }

    /**
     * @return mixed
     */
    protected function getProfile()
    {

        global $Core;

        $this->data['user'] = $this->model->getUserInfo($Core->getRequest('profile_login'));

        switch ($Core->getRequest('action')) {

            case 'settings':

                return $this->profileSettings();

                break;

            default:

                return $this->view->renderProfile($this->data);

                break;

        }

    }

}
