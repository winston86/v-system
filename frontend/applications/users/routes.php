<?php

$routes = array();

$routes["/^(\/)?$/i"] = array(

    'login' => 'path',

);

$routes["/^\/users\/?$/i"] = array(

    'login' => 'path',

);

$routes["/^\/users\/login\/?$/i"] = array(

    'login' => 'path',

);

$routes["/^\/users\/logout\/?$/i"] = array(

    'logout' => 'path',

);

$routes["/^\/users\/register\/?$/i"] = array(

    'register' => 'path',

);

$routes["/^\/users\/profile\/(.*)\/?$/i"] = array(

    'profile' => 'path',
    1         => 'profile_login',

);

$routes["/^\/users\/profile\/(.*)\/(.*)\/?$/i"] = array(

    'profile' => 'path',
    1         => 'profile_login',
    2         => 'action',

);
