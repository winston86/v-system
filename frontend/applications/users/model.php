<?php

namespace frontend\applications\users;

use \core\Users;

class UsersModel extends Users
{

    public function userLogin($login, $pass)
    {

        global $Core;

        if (!$this->isLoged($login)) {

            $this->login($login, $pass);

            $Core->redirect('/');

        }

    }

    public function userLogout()
    {

        $this->logout();

    }

    public function register($userData)
    {

        global $Core;

        if ($this->getUserId($userData['login']) > 0) {

            $Core->sysMessage($this->lang['USER_ALLREADY_REGISTERED'], 'error');

            $Core->back();

        }

        return $this->register($userData);

    }

    public function getUserInfo($login)
    {

        $params = array('login' => $login);

        return $this->getUser($params);

    }

    public function saveSettings($login, $settings)
    {

        global $Core;

        if ($_FILES['settings']['name']['image']) {

            $curr_image = $this->getUserImg(array('login' => $login));

            $large_image = ROOT_DIR . '/images/users/' . $curr_image;

            if (file_exists($large_image)) {

                unlink($large_image);

                $small_image = ROOT_DIR . '/images/users/small/' . $curr_image;

                unlink($small_image);

            }

            if ($Core->saveFile($_FILES['settings']['tmp_name']['image'], '/images/users/' . $_FILES['settings']['name']['image'])) {

                $settings['image'] = $_FILES['settings']['name']['image'];

                $Core->render->resizeImg('/images/users/' . $_FILES['settings']['name']['image'], '/images/users/small/' . $_FILES['settings']['name']['image'], 200);

            } else {

                $settings['image'] = '';

            };

        }

        return $this->update('users', $settings, array('login' => $login));

    }

}
