<?php

$routes = array();

$routes["/^\/content\/list\/?$/i"] = array(

    'list' => 'path',

);
/*
$routes["/^\/content\/?$/i"] = array(

'list'        => 'path'

);

$routes["/^\/content\/list\/?$/i"] = array(

'list'        => 'path'

);*/

$routes["/^\/?  || \/content\/clear\/? || \/content\/?$/i"] = array(

    'clear' => 'path',

);

$routes["/^\/content\/list\/(.*?)\/?$/i"] = array(

    1      => 'needle',
    'list' => 'path',

);

$routes["/^\/content\/list\/(.*?)\/(.*?)\/?$/i"] = array(

    1      => 'needle',
    2      => 'limit',
    'list' => 'path',

);

$routes["/^\/content\/list\/(.*?)\/(.*?)\/(.*?)\/?$/i"] = array(

    1      => 'needle',
    2      => 'limit',
    3      => 'order',
    'list' => 'path',

);

$routes["/^\/content\/list\/(.*?)\/(.*?)\/(.*?)\/(.*?)\/?$/i"] = array(

    1      => 'needle',
    2      => 'limit',
    3      => 'order',
    4      => 'last_days',
    'list' => 'path',

);

$routes["/^\/content\/list\/(.*?)\/(.*?)\/(.*?)\/(.*?)\/(.*?)\/?$/i"] = array(

    1      => 'needle',
    2      => 'limit',
    3      => 'order',
    4      => 'last_days',
    5      => 'page',
    'list' => 'path',

);

$routes["/^\/content\/item\/(.*?)\/?$/i"] = array(

    1      => 'item_id',
    'item' => 'path',

);

$routes["/^\/content\/parser\/?$/i"] = array(

    'parser' => 'path',

);
