<?php

namespace frontend\applications\content;

use \core\Db;
use \Sunra\PhpSimple\HtmlDomParser;

class ContentModel extends Db
{

    public function getItems($needle = 'all', $limit = array(0, 10), $order = 'asc', $last_days = 1)
    {

        if ($needle !== 'all') {

            $this->updateCatViews($needle);

        }

        $needle = $needle == 'all' ? '' : $needle;

        $needle = urldecode($needle);

        $limit = $limit[0] . ',' . $limit[1];

        switch ($order) {

            case 'desc':

                $order = 'ORDER BY id DESC';

                break;

            default:

                $order = 'ORDER BY id ASC';

                break;

        }

        $sql = "SELECT * FROM

		{$this->prefix}publications

		WHERE published = '1'

		AND add_date >= ( CURDATE() - INTERVAL {$last_days} DAY )

		" . ($needle != '' ? " AND content LIKE '% {$needle} %'  " : "") . "

		{$order}

		LIMIT {$limit} ";
        
        return $this->query($sql);

    }

    public function getItemsCount($needle = 'all', $last_days = 1)
    {

        $needle = $needle == 'all' ? '' : $needle;

        $needle = urldecode($needle);

        $sql = "SELECT COUNT(id) as count FROM

		{$this->prefix}publications

		WHERE published = '1'

		AND add_date >= ( CURDATE() - INTERVAL {$last_days} DAY )

		" . ($needle != '' ? " AND content LIKE '% {$needle} %'  " : "");

        $result = $this->query($sql);

        return $result[0]['count'];

    }

    public function getCats()
    {

        return $this->selectRows('publication_cats', '*', array('published' => '1'));

    }

    public function getMaxCatViews()
    {

        return $this->selectMax('publication_cats', 'views');

    }

    public function updateCatViews($needle)
    {

        $needle = urldecode($needle);

        $sql = "UPDATE {$this->prefix}publication_cats SET views = views + 1 WHERE search_tag = :needle ";

        return $this->query($sql, array(':needle' => $needle));

    }

    public function getItem($item_id)
    {

        return $this->selectFields('publications', '*', array('id' => $item_id));

    }

    public function refreshDB()
    {

        $rules = $this->selectRows('parser_rules', '*', array('published' => 1));

        foreach ($rules as $rule) {

            $catalog = HtmlDomParser::str_get_html(file_get_contents($rule['catalog_url']));

            if (!is_object($catalog)) {
                continue;
            }
            $processed = [];
            foreach (array_reverse($catalog->find($rule['p_href_rule'])) as $item) {

                $query_data = array();

                if (stristr($item->href, 'http')) {

                    $item_href = $item->href;

                } else {

                    $tmp = explode('/', $rule['catalog_url']);

                    $domain_name = $tmp[0] . "//" . $tmp[2];

                    $item_href = $domain_name . $item->href;

                }

                if (in_array($item_href, $processed)) {
                    continue;
                }

                $item_page = HtmlDomParser::str_get_html(file_get_contents($item_href));

                if (is_object($item_page)) {

                    $processed[] = $item_href;

                    $query_data['description'] = substr($item_page->find($rule['p_content_rule'], 0)->plaintext, 0, 500) . "...";

                    $query_data['title'] = is_object($item_page->find($rule['p_title_rule'], 0)) ? $item_page->find($rule['p_title_rule'], 0)->plaintext : $query_data['description'];

                    $query_data['content'] = $item_page->find($rule['p_content_rule'], 0)->plaintext;

                    $query_data['image'] = stristr($item_page->find($rule['p_image_rule'], 0)->src, 'http') ? $item_page->find($rule['p_image_rule'], 0)->src : $domain_name . $item_page->find($rule['p_image_rule'], 0)->src;

                    $query_data['add_date'] = date("Y-m-d H:i:s");

                    $query_data['pub_url'] = $item_href;

                    $query_data['published'] = 1;

                    $query_data['views'] = 0;

                    if ($item_page->find($rule['p_image_rule'], 0)->src != '' && $query_data['content'] != '' && $query_data['title'] != '') {

                        $this->insert('publications', $query_data);

                    }

                }

            }

        }

    }

}
