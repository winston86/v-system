<?php
/*
Окрім фільтра можна використовувати ссилки по типу
/content/list/all/1/rand/2/сторінка
де
/компонент/шлях/тег пошуку(можна укр.)/кількість на сторінці/сортування(asc,desc,rand)/період в днях/сторінка

очищення фільтрів
/content/clear || / (головна сторінка) || /content

//парсер
/content/parser

 */

namespace frontend\applications\content;

use core\AppBaseController;

class ContentController extends AppBaseController
{

    public static $pathes = [
        'item' => 'viewItem',
        'list' => 'viewItemsList',
        'clear' => ['viewItemsList', 'clearFilter'],
        'parser' => 'parseItems',
        'default' => ['viewItemsList', 'clearFilter']
    ];

   protected function clearFilter()
    {
        global $Core;
        $Core->sessionUnset('content');
    }

    protected function viewItem()
    {

        global $Core;

        $data['LANG'] = array_merge($Core->getLang("applications/content"), $Core->getLang("global"));

        $item_id = $Core->getRequest('item_id');

        $data['item'] = $this->model->getItem($item_id);

        return $this->view->renderItem($data);

    }

    protected function viewItemsList()
    {

        global $Core;

        $filter = $Core->sessionGet('content');

        $data['LANG'] = array_merge($Core->getLang("applications/content"), $Core->getLang("global"));

        $data['needle'] = $needle = $filter['needle'] = $Core->inRequest('needle')
        ?
        $Core->getRequest('needle')
        :
        (
            isset($filter['needle'])
            ?
            $filter['needle']
            :
            'all'
        );

        $data['order'] = $order = $filter['order'] = $Core->inRequest('order')
        ?
        $Core->getRequest('order')
        :
        (
            isset($filter['order'])
            ?
            $filter['order']
            :
            'asc'
        );

        $data['last_days'] = $last_days = $filter['last_days'] = $Core->inRequest('last_days')
        ?
        $Core->getRequest('last_days')
        :
        (
            isset($filter['last_days'])
            ?
            $filter['last_days']
            :
            1
        );

        $data['on_page'] = $on_page = $filter['on_page'] = $Core->inRequest('limit')
        ?
        $Core->getRequest('limit')
        :
        (
            isset($filter['on_page'])
            ?
            $filter['on_page']
            :
            10
        );

        if ($Core->inRequest('needle') || $Core->inRequest('order') || $Core->inRequest('last_days') || $Core->inRequest('limit')) {
            $data['page'] = $page = $filter['page'] = 1;
        } else {
            $data['page'] = $page = $filter['page'] = $Core->inRequest('page')
            ?
            $Core->getRequest('page')
            :
            (
                isset($filter['page'])
                ?
                $filter['page']
                :
                10
            );
        }

        $data['items_count'] = $this->model->getItemsCount($needle, $last_days);

        $page = $Core->inRequest('page') ? $Core->getRequest('page') : 1;

        $range[0] = ($page - 1) * $on_page;

        $range[1] = $on_page;

        $data['items'] = $this->model->getItems($needle, $range, $order, $last_days);

        $url = "/content/list/" . $filter['needle'] . "/" . $on_page . "/" . $filter['order'] . "/" . $filter['last_days'] . "/{page}";

        $data['pagination'] = $Core->render->getPagination($data['items_count'], $on_page, $page, $url);

        $Core->sessionPut(array('content' => $filter));

        $data['cats'] = $this->model->getCats();

        $max_cat_views = $this->model->getMaxCatViews();

        foreach ($data['cats'] as $key => $cat) {

            $cat_font_size = ceil($cat['views'] / $max_cat_views * 9) + 21;

            $data['cats'][$key]['font_size'] = $cat_font_size;

        }

        return $this->view->renderList($data);

    }

    protected function parseItems()
    {

        $this->model->refreshDB();

        die('All done');

    }

}
