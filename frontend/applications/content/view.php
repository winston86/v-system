<?php

namespace frontend\applications\content;

use \core\Render;

class ContentView extends Render
{

    public function renderList($data)
    {

        return $this->renderTpl("{$this->tplDir}/applications/content/list.tpl.php", $data);

    }

    public function renderItem($data)
    {

        return $this->renderTpl("{$this->tplDir}/applications/content/item.tpl.php", $data);

    }

}
