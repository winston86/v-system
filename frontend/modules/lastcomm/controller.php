<?php

namespace frontend\modules\lastcomm;

use core\AppBaseController;

/**
 * Class LastcommController
 * @package frontend\modules\lastcomm
 */
class LastcommController extends AppBaseController
{

    /**
     * @param $app
     * @return mixed
     */
    public function execute($app)
    {
        global $Core;

        parent::execute($app);

        $settings = unserialize($app['mod_settings']);

        $data['comments'] = $this->model->getComments($settings);

        $data['title'] = $app['title'];

        $data['show_date'] = $settings['show_date'];

        $data['LANG'] = $Core->getLang('modules/lastcomm');

        return $this->view->renderComments($data);

    }

}
