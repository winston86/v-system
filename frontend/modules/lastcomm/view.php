<?php

namespace frontend\modules\lastcomm;

use \core\Render;

/**
 * Class LastcommView
 * @package frontend\modules\lastcomm
 */
class LastcommView extends Render
{

    /**
     * @param $data
     * @return mixed|string
     */
    public function renderComments($data)
    {

        return $this->renderTpl("{$this->tplDir}/modules/lastcomm/simple.tpl.php", $data);

    }

}
