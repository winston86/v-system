<?php

namespace frontend\modules\lastcomm;

use \core\Db;

/**
 * Class LastcommModel
 * @package frontend\modules\lastcomm
 */
class LastcommModel extends Db
{

    //for example
    /**
     * @param $settings
     * @return array
     */
    public function getComments($settings)
    {

        $left_join = ($settings['show_author'] == 1 ? "LEFT JOIN {$this->prefix}users u ON(u.id = c.user_id) " : '');

        $where = ($settings['by_url'] != '' ? "AND c.route = '{$settings['by_url']}' " : '');

        $sql = "SELECT c.*, u.nickname as u_name FROM {$this->prefix}comments c

		{$left_join}

		WHERE c.published = '1' {$where}

		ORDER BY id DESC LIMIT {$settings['count']}";

        $comments = $this->query($sql);

        return array_reverse($comments);

    }

}
