<?php

namespace frontend\modules\menu;

use \core\Render;

/**
 * Class MenuView
 * @package frontend\modules\menu
 */
class MenuView extends Render
{

    /**
     * @param $items
     * @param int $id
     * @return mixed|string
     */
    public function renderMenu($items, $id = 0)
    {

        $data['items'] = $items;

        $data['id'] = $id;

        return $this->renderTpl("{$this->tplDir}/modules/menu/menu.tpl.php", $data);

    }

}
