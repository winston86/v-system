<?php

namespace frontend\modules\menu;

use core\AppBaseController;

/**
 * Class MenuController
 * @package frontend\modules\menu
 */
class MenuController extends AppBaseController
{

    /**
     * @param $app
     * @return mixed
     */
    public function execute($app)
    {

        parent::execute($app);

        $app_id = $app['id'];

        $app = $this->model->getSettings($app);

        $items = $app['items'];

        return $this->view->renderMenu($items, $app_id);

    }

}
