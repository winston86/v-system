<?php

namespace frontend\modules\menu;

use \core\Db;

/**
 * Class MenuModel
 * @package frontend\modules\menu
 */
class MenuModel extends Db
{

    //for example
    /**
     * @param $app
     * @return mixed
     */
    public function getSettings($app)
    {

        $app = unserialize($app['mod_settings']);
        return $app;

    }

}
