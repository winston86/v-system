<?php

namespace frontend\modules\html;

use core\AppBaseController;

/**
 * Class HtmlController
 * @package frontend\modules\html
 */
class HtmlController extends AppBaseController
{

    /**
     * @param $app
     * @return mixed
     */
    public function execute($app)
    {
        global $Core;
        
        parent::execute($app);

        $app = $this->model->getSettings($app);

        $mod['html'] = $app['html'];

        $mod['title'] = $app['title'];

        return $this->view->renderHtml($mod);

    }

}
