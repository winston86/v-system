<?php

namespace frontend\modules\html;

use \core\Render;

/**
 * Class HtmlView
 * @package frontend\modules\html
 */
class HtmlView extends Render
{

    /**
     * @param $mod
     * @return mixed|string
     */
    public function renderHtml($mod)
    {

        return $this->renderTpl("{$this->tplDir}/modules/html/simple.twig", $mod, true);

    }

}
