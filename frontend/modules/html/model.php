<?php

namespace frontend\modules\html;

use \core\Db;

/**
 * Class HtmlModel
 * @package frontend\modules\html
 */
class HtmlModel extends Db
{

    /**
     * @param $app
     * @return mixed
     */
    public function getSettings($app)
    {

        $settings = unserialize($app['mod_settings']);

        $app['html'] = $settings['html'];

        return $app;

    }

}
