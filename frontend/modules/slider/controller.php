<?php

namespace frontend\modules\slider;

use core\AppBaseController;

/**
 * Class SliderController
 * @package frontend\modules\slider
 */
class SliderController extends AppBaseController
{

    /**
     * @param $app
     * @return mixed
     */
    public function execute($app)
    {

        global $Core;

        parent::execute($app);

        $app = $this->model->getSettings($app);

        $type = $app['type'];

        unset($app['type']);

        if ($type == 'camera1' || $type == 'camera2') {

            $Core->page->addHeadCss('/frontend/modules/slider/css/camera.css');

            $Core->page->addHeadJs('/frontend/modules/slider/js/jquery.mobile.customized.min.js');

            $Core->page->addHeadJs('/frontend/modules/slider/js/jquery.easing.1.3.js');

            $Core->page->addHeadJs('/frontend/modules/slider/js/camera.min.js');

        } else {

            $Core->page->addHeadCss('/frontend/modules/slider/css/style.css');

            $Core->page->addHeadJs('/frontend/modules/slider/js/slides.js');

        }

        $items = $app['items'];

        return $this->view->renderSlider($items, $type);

    }

}
