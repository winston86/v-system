<?php

namespace frontend\modules\slider;

use \core\Db;

/**
 * Class slider_model
 * @package frontend\modules\slider
 */
class SliderModel extends Db
{

    /**
     * @param $app
     * @return mixed
     */
    public function getSettings($app)
    {

        $app = unserialize($app['mod_settings']);

        return $app;

    }

}
