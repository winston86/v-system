<?php

namespace frontend\modules\slider;

use \core\Render;

class SliderView extends Render
{

    public function renderItems($items, $type)
    {

        $items_list_html = '';

        foreach ($items as $item) {

            $data = $item;

            $items_list_html .= $this->renderTpl("{$this->tplDir}/modules/slider/item{$type}.tpl.php", $data);

        }

        return $items_list_html;

    }

    public function renderSlider($items, $type = '')
    {

        $type = $type != '' ? '_' . $type : '';

        $data['items_list_html'] = $this->renderItems($items, $type);

        return $this->renderTpl("{$this->tplDir}/modules/slider/slider{$type}.tpl.php", $data);

    }

}
