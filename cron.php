<?php

global $Core;

$sql = "SELECT * FROM {$Core->db->prefix}cron WHERE published = '1' AND ( UNIX_TIMESTAMP( NOW() )  -  UNIX_TIMESTAMP( last_exe ) > i_val )";

$tasks = $Core->db->selectRows('cron', '*', array('published' => 1));

foreach ($tasks as $task) {

    if (file_get_contents($task['url'])) {

        $Core->db->update('cron', array('last_exe' => date("Y-m-d H:i:s")), array('id' => $task['id']));

    }

}
