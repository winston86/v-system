<?php

namespace extensions\ckeditor;

class CkeditorController
{

    public function __construct()
    {

    }

    public function execute($params)
    {

        global $Core;

        if (!in_array('viewEditor', $Core->users->getUserRights()) && !$Core->users->isAdmin()) {return;}

        $Core->page->addHeadJs("/extensions/ckeditor/ckeditor.js");

        $Core->page->addHeadJs("/extensions/ckeditor/adapters/jquery.js");

        $name = isset($params['name']) ? $params['name'] : '';

        $width = isset($params['width']) ? $params['width'] : '';

        $height = isset($params['height']) ? $params['height'] : '';

        $value = isset($params['value']) ? $params['value'] : '';

        $type = isset($params['type']) ? $params['type'] : 'light';

        $settings = $Core->getExtensionSettings('ckeditor');

        $user_group_id = $Core->users->getUserGroupId();

        $toolbars = [];

        if (is_array($user_group_id)) {

            foreach ($user_group_id as $g_id) {
                $toolbars = array_merge($tools, $settings['_' . $g_id]);
            }

        } else {

            $toolbars = $settings['_' . $user_group_id];

        }

        ?>

		<textarea name="<?php echo $name; ?>" ><?php echo $value; ?></textarea>

		<script type="text/javascript">

			jQuery(document).ready(function(){

				CKEDITOR.config.width = '<?php echo $width; ?>';

				CKEDITOR.config.height = '<?php echo $height; ?>';

		<?php if ($type == 'light') {?>

				CKEDITOR.config.toolbar = [
					{ name: 'document', items: [ '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
					[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
                    '/',																				// Line break - next group will be placed in new line.
					{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
				]

		<?php }?>

		<?php if ($type == 'custom'): ?>
				CKEDITOR.config.toolbar = [
                    <?php foreach ($toolbars as $section_name => $tools): ?>
						{name: '<?php echo $section_name; ?>',
						items:[
							<?php foreach ($tools as $key => $tool): ?>
								<?php echo !empty($tool)?"'".$key."',":''; ?>
							<?php endforeach;?>
						]},
                    <?php endforeach;?>
				];

		<?php endif;?>



				jQuery( "textarea[name="+ui.jq('<?php echo $name; ?>')+"]" ).ckeditor();

			})

		</script>

		<?php

    }

}

?>