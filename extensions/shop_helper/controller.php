<?php

namespace extensions\shop_helper;

class ShopHelperController
{

    public function __construct()
    {

        global $Core;

        $this->lang = $Core->getLang('extensions/shop_helper');

    }

    public function execute($params)
    {

        global $Core;

        if (!in_array('shop_helper', $Core->users->getUserRights()) && !$Core->users->isAdmin()) {return;}

        switch ($params['event']) {
            case 'chang_status':
                #code
                break;

            default:
                # code...
                break;
        }

    }
