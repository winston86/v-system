<?php

namespace extensions\aceeditor;

class AceEditorController
{

    public function __construct()
    {

    }

    public function execute($params)
    {

        global $Core;

        if (!in_array('viewCodeEditor', $Core->users->getUserRights()) && !$Core->users->isAdmin()) {return;}

        $Core->page->addHeadJs("/vendor/bower-asset/ace-builds/src-noconflict/ace.js");

        $name = $params['name'];

        unset($params['name']);

        $value = $params['value'];

        unset($params['value']);

        $settings = $Core->getExtensionSettings('aceeditor');

        ?>
		<style type="text/css" media="screen">
			#<?php echo str_replace(["[", "]"], ["\[", "\]"], $name); ?>{
				width:100%;
				height: 100%;
			}
		</style>
		<input type="hidden" name="<?php echo $name; ?>" value="<?php echo htmlentities($value); ?>"/>
		<div id="<?php echo $name; ?>"><?php echo htmlentities($value); ?></div>
		<script type="text/javascript">
			//available settings
//			editor.getSession().setMode("ace/mode/javascript");
//			editor.setTheme("ace/theme/twilight");
//			editor.getSession().setMode("ace/mode/javascript");
//			editor.setValue("the new text here");
//			editor.insert("Something cool");
//			editor.gotoLine(lineNumber);
//			editor.getSession().setTabSize(4);
//			editor.getSession().setUseSoftTabs(true);
//			document.getElementById('editor').style.fontSize='12px';
//			editor.getSession().setUseWrapMode(true);
//			editor.setHighlightActiveLine(false);
//			editor.setShowPrintMargin(false);
//			editor.setReadOnly(true);  // false to make it editable
//			editor.resize()
		<?php echo str_replace(["]", "["], "", $name); ?> = {
			editor: ace.edit("<?php echo $name; ?>"),
			init: function(){
				var _this = this;
				this.editor.getSession().setMode("ace/mode/html");
				this.editor.setTheme("<?php echo $settings['theme']; ?>");
				document.getElementById("<?php echo $name; ?>").style.fontSize='<?php echo $settings['fontSize']; ?>px';
				this.editor.getSession().setTabSize(<?php echo $settings['setTabSize']; ?>);
				this.editor.setShowPrintMargin(<?php echo !empty($settings['setShowPrintMargin']) ? 'true' : 'false'; ?>);
				this.editor.setHighlightActiveLine(<?php echo !empty($settings['setHighlightActiveLine']) ? 'true' : 'false'; ?>);
				this.editor.getSession().setUseWrapMode(<?php echo !empty($settings['setUseWrapMode']) ? 'true' : 'false'; ?>);
				this.editor.getSession().setUseSoftTabs(<?php echo !empty($settings['setUseSoftTabs']) ? 'true' : 'false'; ?>);
				<?php
foreach ($params as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $sub_key => $sub_value) {
                    ?>
							this.editor.<?php echo $key ?>().<?php echo $sub_key; ?>(<?php echo $sub_value; ?>);
							<?php
}
            } else {
                ?>
						this.editor.<?php echo $key ?>(<?php echo $value; ?>);
						<?php
}
        }
        ?>
				this.editor.getSession().on("change", function (e) {
					$('input[name="'+jq('<?php echo $name; ?>')+'"]').val(_this.editor.getSession().getValue());
				});
			}
		}
		<?php echo str_replace(["]", "["], "", $name); ?>.init();
		</script>
		<?php

    }

}

?>