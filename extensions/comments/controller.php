<?php

namespace extensions\comments;

class CommentsController
{

    public function __construct()
    {

        global $Core;

        $this->lang = $Core->getLang('extensions/comments');

    }

    public function execute($params)
    {

        global $Core;

        $rights = $Core->users->getUserRights();

        if (!in_array('comment', $rights) && !in_array('all', $rights) && !$Core->users->isAdmin()) {return;}

        if ($Core->inRequest('submit') && $Core->getRequest('comment') != '') {

            if ($Core->db->insert('comments',
                array(
                    'route'      => $Core->getUri(),
                    'page_title' => $Core->getRequest('page_title'),
                    'text'       => $Core->removeOnAttrs($Core->clearTags($Core->getRequest('comment'))),
                    'parent_id'  => $Core->getRequest('parent_id'),
                    'user_id'    => $Core->users->user_id,
                    'published'  => 0,
                )
            )) {
                $Core->sysMessage($this->lang['COMMENT_ADDED_WAIT_FOR_MODERATION'], 'success');
            }

        }

        $comments = $this->getComments(0);

        echo $comments != '' ? '<h3 class="comments">' . $this->lang['COMMENTS'] . '</h3>' . $comments : '';

        if (in_array('viewEditor', $rights) || $Core->users->isAdmin()) {
            ?>
			<div id="comment_wrapper">
				<div class="cur_answer"></div>
				<div id="main_answer">

					<form id="comment_form" action="" method="post">

						<?php $Core->addEvent('viewEditor', array('type' => 'custom', 'width' => '100%', 'height' => 200, 'name' => 'comment', 'value' => ''));?>

						<input id="parent_id" type="hidden" name="parent_id" value="0" />

						<input id="page_title" type="hidden" name="page_title" value="<?php echo $params['page_title']; ?>" />

						<input type="submit" name="submit" value="<?php echo $this->lang['SUBMIT']; ?>" />

						<input type="button" id="cancel_answer" style="display:none" onclick="cancelAnswer()" value="<?php echo $this->lang['CANCEL']; ?>" />

					</form>

				</div>
			</div>
		<?php
}

    }

    private function getComments($parent_id = 0, $html = '')
    {

        global $Core;

        $sql = "SELECT * FROM {$Core->db->prefix}comments WHERE parent_id = '{$parent_id}' AND route = '{$Core->getUri()}' AND published = '1'  ORDER BY id DESC ";

        $comments = $Core->db->query($sql);

        $tmp = '';

        foreach ($comments as $key => $comment) {

            $res = $this->getComments($comment['id'], $html);

            $tmp .= '<li class="comment">
				<span class="comment-date">' . $this->lang['DATE'] . ': ' . $comment['date'] . '</span>
				<span id="comment_' . $comment['id'] . '">' . $comment['text'] . '</span>
				</br>
				<a class="answer-button" onclick="setParentId(' . $comment['id'] . ')">' . $this->lang['ANSWER'] . '</a>
				</li>' . $res;

        }

        return !empty($tmp) ? $html . '<ul class="comment-ul">' . $tmp . '</ul>' : '';
    }

}

?>

<script type="text/javascript">



function	setParentId(id){

		jQuery('#parent_id').val(id);
		 jQuery('#comment_wrapper').find('.cur_answer').html(jQuery('#comment_'+id).html());
		jQuery('#cancel_answer').show();
		jQuery('.cur_answer').show();

}

function cancelAnswer(){
		jQuery('#parent_id').val('0');
		  jQuery('#cancel_answer').hide();
		  jQuery('#comment_wrapper').find('.cur_answer').empty();
		  jQuery('.cur_answer').hide();

}

</script>
