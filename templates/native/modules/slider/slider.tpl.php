<div id="slide_container" style="position:relative;">
	<div id="slides" >
		<?php echo $items_list_html; ?>
	</div>
</div>

<script>
		jQuery(function(){
			jQuery("#slides").slides({
				slide: 150,
				playInterval: 5000,
				play: 1000,
				randomize:true,
				width: 580,
				height: 303,
				preload:{active:true,image:'../img/loading.gif'},
				navigateStart: function( current ){
				jQuery('.slide_title').hide();
				},
				navigateEnd: function( current ){
				jQuery('.slide_title').slideDown(200);
				}
			});
			jQuery('#slides').bind('mouseenter',function(){
				if (window.hoverTimeout){clearTimeout(hoverTimeout)}
				jQuery("#slides").slides("stop");
			})
			jQuery('#slides').bind('mouseleave',function(){
			hoverTimeout = setTimeout(function(){ jQuery("#slides").slides("play"); },1000);
			});
			jQuery("#slides").slides("play");
		});
function showSlider(moduleId){jQuery(moduleId).attr('style','visibility:visible')}
jQuery(document).ready(function(){setTimeout('showSlider("#slide_container")', 1)})
</script>	