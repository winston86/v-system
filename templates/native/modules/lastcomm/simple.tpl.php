<div class="mod-lastcomm-title mod-title"><?php echo $title; ?></div>
<div class="mod-lastcomm-content">
<?php
foreach($comments as $comment){ ?>

<div class="lastcomm-item">
	<a href="<?php echo $comment['route'] ?>"><?php echo $comment['page_title']; ?></a>
	<div class="clear"></div>
	<?php if($comment['u_name']){  ?>
		<span class="lastcomm-author"><?php echo $LANG['AUTHOR'].": ".$comment['u_name']; ?></span>
	<?php } ?>
	
	<?php if($show_date == 1){ ?>
		<span class="lastcomm-date"><?php echo $LANG['DATE'].": ".$comment['date']; ?></span>
	<?php } ?>
	<div class="clear"></div>
	<div class="lastcomm-item-con">
	<?php echo $comment['text']; ?>
	</div>
</div>

<?php } ?>
</div>
