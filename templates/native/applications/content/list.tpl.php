<div class="content-filter">
	<form method="post" action="">
		<span class="in-line"><?php echo $LANG['PUB_CATS']; ?></span>
		<select name="needle" class="content-input in-line">
							<option value="all"><?php echo $LANG['ALL_PUBLICATIONS']; ?></option>
			<?php foreach($cats as $cat){ ?>
			
							<option <?php echo $needle == $cat['search_tag'] ? 'selected' : '' ; ?> value="<?php echo $cat['search_tag']; ?>"> <?php echo $cat['title']; ?></option>
						
						}
			<?php } ?>
		</select>
		<span  class="in-line"><?php echo $LANG['ON_PAGE']; ?></span>
		<select name="limit" class="content-input in-line">
			<?php for($i=1; $i <= 32; $i++){ ?>
				<option <?php echo $on_page == $i ? 'selected' : '' ; ?>  value="<?php echo $i; ?>"><?php echo $i; ?></option>
			<?php } ?>
		</select>
		<span  class="in-line"><?php echo $LANG['ORDER']; ?></span>
		<select name="order" class="content-input in-line">
			<option <?php echo $order == 'asc'? 'selected' : '' ; ?> value='asc'><?php echo $LANG['ASC']; ?></option>
			<option <?php echo $order == 'desc'? 'selected' : '' ; ?> value='desc'><?php echo $LANG['DESC']; ?></option>
		</select>
		
		<span  class="in-line"><?php echo $LANG['LAST_DAYS']; ?></span>
		<select name="last_days" class="content-input in-line">
			<option <?php echo $last_days == '1' ? 'selected' : '' ; ?> value='1'>1</option>
			<option <?php echo $last_days == '2' ? 'selected' : '' ; ?> value='2'>2</option>
			<option <?php echo $last_days == '3' ? 'selected' : '' ; ?> value='3'>3</option>
			<option <?php echo $last_days == '4' ? 'selected' : '' ; ?> value='4'>4</option>
			<option <?php echo $last_days == '5' ? 'selected' : '' ; ?> value='5'>5</option>
			<option <?php echo $last_days == '6' ? 'selected' : '' ; ?> value='6'>6</option>
			<option <?php echo $last_days == '7' ? 'selected' : '' ; ?> value='7'>7</option>
		</select>
		<button name="submit" type="submit" class="content-button in-line" value="SEARCH"><?php echo $LANG['SEARCH']; ?></button>
	</form>
</div>
<div class="publication-cats">
<span class="cat-title"><a href="/content/clear" title="<?php echo $LANG['ALL_PUBLICATIONS']; ?>" class="cat-link in-line" style="font-size:18px;"><?php echo $LANG['ALL_PUBLICATIONS']; ?></a></span>
<?php foreach($cats as $cat){ ?>

<span class="cat-title"><a href="/content/list/<?php echo $cat['search_tag']; ?>" title="<?php echo $cat['description']; ?>" class="cat-link in-line" style="font-size:<?php echo $cat['font_size'] ; ?>px"><?php echo $cat['title']; ?></a></span>	

<?php } ?>
</div>
<?php foreach($items as $item){  ?>

<div class="content-item in-line">
	<a class="content-title" href="/content/item/<?php echo $item['id']; ?>"><?php echo $item['title']; ?></a>
	<div class="content-img-div"><a href="/content/item/<?php echo $item['id']; ?>"><img class="content-img" src="<?php echo $item['image'];  ?>" /></a></div>
	<div class="content-desc"><?php echo $item['description']; ?></div>
</div>

<?php }

if(count($items) == 0){ ?>

	<div class="content-info-block"><?php echo $LANG['NO_PUBLICATIONS']; ?></br><a href="/content/clear"><?php echo $LANG['CONTENT_HOME']  ?></a></div>

<?php

}

?>
<?php if(count($items) < $items_count){ ?>

<div class="content-navigation"><a class="content-clear-filter"   href="/content/clear"><?php echo $LANG['CLEAR'] ; ?></a></div> 

<div class="content-navigation"><?php echo $pagination; ?></a></div> 

<?php } ?>
