<div class="content-item-read">
	<div class="content-title">
		<?php echo $data['item']['title']; ?>
	</div>
	<div class="left">
		<img class="content-img" src="<?php echo $data['item']['image'];  ?>" />
	</div>
	<div class="content-content">
		<?php echo $data['item']['content']; ?>
		<div class="pub-url">
			<?php echo $data['LANG']['SOURCE']; ?>
			<span class="url-span">
				<a href="<?php echo $data['item']['pub_url']; ?>">
					<?php echo $data['item']['title']; ?>
				</a>
			</span>
		</div>
	</div>
</div>
<div class="item-comments">
<?php $this->eventPopUp('comments',array('page_title' => $data['item']['title'] )); ?>
</div>