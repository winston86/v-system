<h1><?php echo $LANG['PROFILE_TITLE'].' '.$user['nickname']; ?></h1>


<table class="table content profile-info">
			<tr class="row">
				<td colspan="1" class="user-profile-value  in-line">
					<img width="200" src = "/images/users/small/<?php echo $user['image']; ?>" alt="<?php echo $user['nickname']; ?>" />
				</td>
				<td>
					<table class="table table-striped table-hover">
						<tr class="row">
							<td class="col-md-6 user-profile-field in-line"><?php echo $LANG['LOGIN']; ?>:</td>
							<td class="col-md-6 user-profile-value  in-line"><?php echo $user['login']; ?></td>
						</tr>
						<tr class="row">
							<td class="col-md-6 user-profile-field in-line"><?php echo $LANG['NICKNAME']; ?>:</td>
							<td class="col-md-6 user-profile-value  in-line"><?php echo $user['nickname']; ?></td>
						</tr>
						<tr class="row">
							<td class="col-md-6 user-profile-field in-line"><?php echo $LANG['FIRSTNAME']; ?>:</td>
							<td class="col-md-6 user-profile-value  in-line"><?php echo $user['firstname']; ?></td>
						</tr>
						<tr class="row">
							<td class="col-md-6 user-profile-field in-line"><?php echo $LANG['LASTNAME']; ?>:</td>
							<td class="col-md-6 user-profile-value  in-line"><?php echo $user['lastname']; ?></td>
						</tr>
						<tr class="row">
							<td class="col-md-6 user-profile-field in-line"><?php echo $LANG['EMAIL']; ?>:</td>
							<td class="col-md-6 user-profile-value  in-line"><?php echo $user['email']; ?></td>
						</tr>
						<tr class="row">
							<td class="col-md-6 user-profile-field in-line"><?php echo $LANG['GROUP']; ?>:</td>
							<td class="col-md-6 user-profile-value  in-line"><?php echo $user['group']; ?></td>
						</tr>
						<tr class="row">
							<td class="col-md-6 user-profile-field in-line"><?php echo $LANG['REG_DATE']; ?>:</td>
							<td class="col-md-6 user-profile-value  in-line"><?php echo $user['reg_date']; ?></td>
						</tr>
					</table>
				</td>
			</tr>
			
</table>

<div class="profile-settings">

	<ul class="profile-menu">
		<li>
			<a class="btn btn-success" href="/users/profile/<?php echo $user['login']; ?>/settings"><?php echo $LANG['SETTINGS']; ?></a>
		</li>
	</ul>

</div>