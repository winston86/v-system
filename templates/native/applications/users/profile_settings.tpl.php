<h1><?php echo $LANG['PROFILE_TITLE'].' '.$user['nickname']; ?></h1>
<div class="content profile-settings">
	<form method="POST" action="" enctype="multipart/form-data" >
			<div class="row">
				<div class="col-md-5 avatar-settings">
					<div class="input-group">
						<label class="pre-text user-profile-field"><img width="200" src = "/images/users/small/<?php echo $user['image']; ?>" alt="<?php echo $user['nickname']; ?>" /></label>
						<div class="user-profile-value"><input type="file" name="settings[image]" /></div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="input-group">
						<label class="pre-text input-group-addon user-profile-field"><?php echo $LANG['LOGIN']; ?></label>
						<div class="user-profile-value">
							<input class="mod-edit-input form-control" type="text" value="<?php echo $user['login']; ?>" disabled="disabled" />
						</div>
					</div>
					<div class="input-group">
						<label class="pre-text input-group-addon user-profile-field"><?php echo $LANG['NICKNAME']; ?></label>
						<div class="user-profile-value"><input class="mod-edit-input form-control" type="text" name="settings[nickname]" value="<?php echo $user['nickname']; ?>" /></div>
					</div>
					<div class="input-group">
						<label class="pre-text input-group-addon user-profile-field"><?php echo $LANG['FIRSTNAME']; ?></label>
						<div class="user-profile-value"><input class="mod-edit-input form-control" type="text" name="settings[firstname]" value="<?php echo $user['firstname']; ?>" /></div>
					</div>
					<div class="input-group">
						<label class="pre-text input-group-addon user-profile-field"><?php echo $LANG['LASTNAME']; ?></label>
						<div class="user-profile-value"><input class="mod-edit-input form-control" type="text" name="settings[lastname]" value="<?php echo $user['lastname']; ?>" /></div>
					</div>
					<div class="input-group">
						<label class="pre-text input-group-addon user-profile-field"><?php echo $LANG['EMAIL']; ?></label>
						<div class="user-profile-value"><input class="mod-edit-input form-control" type="text" name="settings[email]" value="<?php echo $user['email']; ?>" /></div>
					</div>
					<div class="input-group">
						<label class="pre-text input-group-addon user-profile-field"><?php echo $LANG['GROUP']; ?></label>
						<div class="user-profile-value">
							<input class="mod-edit-input form-control" type="text" value="<?php echo $user['group']; ?>" disabled="disabled" />
						</div>
					</div>
					<div class="input-group">
						<label class="pre-text input-group-addon user-profile-field"><?php echo $LANG['REG_DATE']; ?></label>
						<div class="user-profile-value">
							<input class="mod-edit-input form-control" type="text" value="<?php echo $user['reg_date']; ?>" disabled="disabled" />
						</div>
					</div>	
				</div>
			</div>
			<div>
                <label class="user-profile-field-description"><?php echo $LANG['DESCRIPTION']; ?></label>
                <div class="user-profile-value"><?php $this->eventPopUp('viewEditor',array('width'=>'100%', 'height'=>200,'name'=>'settings[description]','value'=>$user['description'])); ?></div>
            </div>
			
			<input type="submit" name="submit" value="<?php echo $LANG['SEND']; ?>">
	</form>

</div>
