<!DOCTYPE HTML  "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/html11/DTD/xhtml">
<head>
	<title><?php echo $this->site_name; ?></title>
    <base href="/">
	<meta name="keywords" content="<?php echo $this->site_keywords; ?>" />
	<meta name="description" content="<?php echo $this->site_description; ?>" />
	<meta name="generator" content="Victor-System" >
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="/templates/<?php echo $this->site_template ; ?>/css/reset.css"  type="text/css" />
	<link href="/templates/<?php echo $this->site_template ; ?>/css/text.css" rel="stylesheet" type="text/css" />
	<link href="/templates/<?php echo $this->site_template ; ?>/css/table.css" rel="stylesheet" type="text/css" />
	<link href="/vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="/templates/<?php echo $this->site_template ; ?>/css/styles.css" rel="stylesheet" type="text/css" />
	<script src="/vendor/bower-asset/jquery/dist/jquery.min.js" type="text/javascript"></script>
	<script src="/vendor/twbs/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
	{*additional_links*}
    <script src="/js/ui.js" type="text/javascript"></script>
</head>

<body>

<div id="wrapper">
	
		<div class="head">
		<div id="header">
		<a href="/" style="text-decoration:none"><<<-[-VICTOR-SYSTEM-]->>></a>
		</div>
			    {*app.top*}
		</div>

	<div class="main-content in-line <?php if(isset($this->apps_in_page['sidebar'])){ echo 's-650' ; } ?>">
	<div class="sys-info">
		{*sys_messages*}
	</div>
        {*main_content*}
	</div>
	<?php if(isset($this->apps_in_page['sidebar'])){ ?>
	<div class="sidebar in-line s-300">
        {*app.sidebar*}
	</div>
	<?php } ?>
	<?php if(isset($this->apps_in_page['bottom'])){ ?>
		<div class="bottom">
            {*app.bottom*}
        </div>
	<?php } ?>
</div>
</body>

</html>