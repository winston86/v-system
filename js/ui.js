ui = {
    jq: function( selector ){
            return selector.replace( /(:|\.|\[|\]|,)/g, "\\$1" );
        }
};

$(document).ready(function(){
    $('.sys-messages').find('.close').click(function(){
        $(this).closest('div').fadeOut();
    })
});