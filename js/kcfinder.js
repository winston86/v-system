	function openKCFinder_singleFile(el) {
			window.KCFinder = {};
			window.KCFinder.callBack = function(url) {
				// Actions with url parameter here
				el.parent().find('input.kcInput').val(url);
				window.KCFinder = null;
			};
			window.open('/extensions/ckeditor/kcfinder/browse.php?type=images&lng=uk&theme=dark&dir=upload', 'kcfinder_single'+el.parent().find('input.kcInput').attr('name'));
		}
 
	function openKCFinder_multipleFiles(el) {
			window.KCFinder = {};
			window.KCFinder.callBackMultiple = function(files) {
				if(imagesUrl.length > 0){imagesUrl += ","};
				for (var i = 0; i < files.length; i++) {
					// Actions with files[i] here
					if(i+1 < files.length){
						var sep = ',';
					}else{
						var sep = '';
					}
					imagesUrl += files[i]+sep;
					el.parent().find('.image-preview').append('<img src="'+files[i]+'" />');
				}
				el.parent().find('input.kcInput').val(imagesUrl);
				window.KCFinder = null;
			};
			window.open('/extensions/ckeditor/kcfinder/browse.php?type=images&lng=uk&theme=dark&dir=upload', 'kcfinder_multiple'+el.parent().find('input.kcInput').attr('name'));
		}