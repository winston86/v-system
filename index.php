<?php

session_start();

define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']);

include ROOT_DIR . '/config.inc.php';
include ROOT_DIR . '/security_config.inc.php';

////////////////s'ekvgnjado[svgn[ajdnbkaqeokfqwasgf
if ($_CONFIG['DISPLAY_ERRORS'] == 1) {
    ini_set('display_errors', 1);
    error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
} else {
    ini_set('display_errors', 0);
}
///////////////    a'legkmadl'gkkl'sdnmglkasdmgmksdg

include_once ROOT_DIR . "/vendor/autoload.php";

include_once ROOT_DIR . "/admin/Admin.exec.php";
include_once ROOT_DIR . "/admin/Application.php";
include_once ROOT_DIR . "/admin/Module.php";
include_once ROOT_DIR . "/admin/Extension.php";
include_once ROOT_DIR . "/classes/Logger.class.php";
include_once ROOT_DIR . "/classes/Mailer.class.php";
include_once ROOT_DIR . "/core/Db.class.php";
include_once ROOT_DIR . "/core/BaseModel.class.php";
include_once ROOT_DIR . "/core/AppBaseController.class.php";
include_once ROOT_DIR . "/core/MultiLang.class.php";
include_once ROOT_DIR . "/core/Render.class.php";
include_once ROOT_DIR . "/core/Page.class.php";
include_once ROOT_DIR . "/core/Users.class.php";

include_once ROOT_DIR . '/core/Core.class.php';

$Core = \core\Core::coreInit();

$Core->setHeaders(array(
	'Cache-Control' => 'no-store, no-cache, must-revalidate, max-age=0',
	'Cache-Control' => 'post-check=0, pre-check=0',
	'Pragma' => 'no-cache'
));


if (is_object($Core)) {

    define('CORE_ONLINE', 1);

}

$Core->execute();
