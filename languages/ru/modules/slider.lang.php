<?php

$lang = array(

'APP_TITLE'=>'Слайдер',
'NEXT' => 'Вперед',
'BACK' => 'Назад',
'NO_IMAGES' => 'Зображень не знайдено.',
'ROUT' => 'Привязка до сторінки',
'POSITION' => 'Позиція',
'SLIDER_TYPE' => 'Тип слайдера',
'IMAGE' => 'Зображення',
'IMAGE_TITLE' => 'Заголовок',
'IMAGE_LINK' => 'Ссилка'

);

?>