<?php

$lang = array(

'APP_TITLE' => 'Каталог товарів',
'TITLE_PARSING'=>'Парсинг товару',
'TITLE_NAME' => 'Назва',		
'CATALOG_URL' => 'Ссилка на каталог',		
'P_HREF_RULE' => 'Правило для ссилки на товар',
'P_TITLE_RULE' => 'Правило для назви товару',
'P_CONTENT_RULE' => 'Правило для опису',		
'P_IMAGE_RULE' => 'Правило для зображення',
'SOURCE' => 'Оригінальна сторінка товару: ',
'SEARCH_TAG' => 'Пошуковий тег',
'CONTENT' => 'Повний опис товару',
'IMAGE' => 'Адреса зображення',
'PRICE' => 'Ціна',
'ITEM_URL' => 'Адреса оригінального товару',
'ALL_ITEMS' => 'Всі товари',
'ON_PAGE_FROM' => 'Починаючи з',
'ON_PAGE_TO' => 'Закінчуючи',
'ORDER' => 'Сортування',
'ASC' => 'Зростання',
'DESC' => 'Спадання',
'RANDOM' => 'Випадково',
'LAST_DAYS' => 'Кількість днів',
'SEARCH' => 'Шукати',
'NEXT' => 'Наступні',
'PREV' => 'Попередні',
'BACK' => 'Назад',
'NO_ITEMS' => 'Немає інформації по заданому критерію.',
'SHOP_HOME' => 'Всі товари',
'SETTINGS'=>'Налаштування',
'PRODUCTS'=>'Товари',
'CATEGORIES'=>'Категорії',
'CATEGORY'=>'Категорія',
'BY_PRICE_ASC' => 'По зростанню ціни',
'BY_PRICE_DESC' => 'По спаданню ціни',
'BY_DATE' => 'По даті надходження',
'ITEMS' => 'Товари'

);

?>
