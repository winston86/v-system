<?php

$lang = array(

    'APP_TITLE' => 'Слайдер',
    'NEXT' => 'Вперед',
    'BACK' => 'Назад',
    'NO_IMAGES' => 'Зображень не знайдено.',
    'ROUTE' => 'Привязка до сторінки',
    'POSITION' => 'Позиція',
    'SLIDER_TYPE' => 'Тип слайдера',
    'IMAGE' => 'Зображення',
    'IMAGE_TITLE' => 'Заголовок',
    'IMAGE_LINK' => 'Ссилка',
    'ADD_IMAGE' => 'Додати зображення'

);

?>