<?php

$lang = [
    'THEME' => 'Тема',
    'SETSHOWPRINTMARGIN' => 'Видимість відступів',
    'SETHIGHLIGHTACTIVELINE' => 'Підсвічувати активний рядок',
    'FONTSIZE' => 'Розмір шрифта',
    'SETUSEWRAPMODE' => 'Виділяти слова',
    'SETUSESOFTTABS' => 'Використовувати таблиці',
    'SETTABSIZE' => 'Розмір таблиць'
];
