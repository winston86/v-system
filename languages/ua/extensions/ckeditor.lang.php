<?php

$lang = [
    //clipboard
    'Cut' => 'Cut',
    'Copy' => 'Copy',
    'Paste' => 'Paste',
    'PasteText' => 'PasteText',
    'PasteFromWord' => 'PasteFromWord',
    'Undo' => 'Undo',
    'Redo' => 'Redo',
    //editing
    'Find' => 'Find',
    'Replace' => 'Replace',
    'SelectAll' => 'SelectAll',
    'Scayt' => 'Scayt',
    //Links
    'Link' => 'Link',
    'Unlink' => 'Unlink',
    'Anchor' => 'Anchor',
    //Forms
    'Form' => 'Form',
    'Checkbox' => 'Checkbox',
    'Radio' => 'Radio',
    'TextField' => 'TextField',
    'Textarea' => 'Textarea',
    'Select' => 'Select',
    'Button' => 'Button',
    'ImageButton' => 'ImageButton',
    'HiddenField' => 'HiddenField',
    //Insert
    'Image' => 'Image',
    'Flash' => 'Flash',
    'Table' => 'Table',
    'Smiley' => 'Smiley',
    'HorizontalRule' => 'HorizontalRule',
    'SpecialChar' => 'SpecialChar',
    'PageBreak' => 'PageBreak',
    'Iframe' => 'Iframe',
    //Tools
    'Maximize' => 'Maximize',
    'ShowBlocks' => 'ShowBlocks',
    //Document
    'Source' => 'Source',
    'Save' => 'Save',
    'NewPage' => 'NewPage',
    'Preview' => 'Preview',
    'Print' => 'Print',
    'Templates' => 'Templates',
    //Basicstyles
    'Bold' => 'Bold',
    'Italic' => 'Italic',
    'Underline' => 'Underline',
    'Strike' => 'Strike',
    'Subscript' => 'Subscript',
    'Superscript' => 'Superscript',
    'RemoveFormat' => 'RemoveFormat',
    //Paragraph
    'NumberedList' => 'NumberedList',
    'BulletedList' => 'BulletedList',
    'CreateDiv' => 'CreateDiv',
    'JustifyLeft' => 'JustifyLeft',
    'JustifyCenter' => 'JustifyCenter',
    'JustifyRight' => 'JustifyRight',
    'JustifyBlock' => 'JustifyBlock',
    'BidiLtr' => 'BidiLtr',
    'BidiRtl' => 'BidiRtl',
    'Language' => 'Language',
    'Outdent' => 'Outdent',
    'Indent' => 'Indent',
    'Blockquote' => 'Blockquote',
    //Styles
    'Styles' => 'Styles',
    'Font' => 'Font',
    'FontSize' => 'FontSize',
    'Format' => 'Format',
    //colors
    'TextColor' => 'TextColor',
    'BGColor' => 'BGColor',
    //About
    'About' => 'About',
    //common
    '-' => '-',
];
