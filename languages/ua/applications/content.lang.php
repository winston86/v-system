<?php

$lang = array(

    'TITLE' => 'Публікації',
    'TITLE_PARSING' => 'Назва',
    'CATALOG_URL' => 'Ссилка на каталог',
    'P_HREF_RULE' => 'Правило для ссилки на статтю',
    'P_TITLE_RULE' => 'Правило для назви статті',
    'P_CONTENT_RULE' => 'Правило для вмісту',
    'P_IMAGE_RULE' => 'Правило для зображення',
    'SOURCE' => 'Оригінальна стаття: ',
    'SEARCH_TAG' => 'Пошуковий тег',
    'CONTENT' => 'Текcт статті',
    'IMAGE' => 'Адреса зображення',
    'PUB_URL' => 'Адреса оригінальної статті',
    'ALL_PUBLICATIONS' => 'Всі публікації',
    'PUB_CATS' => 'Категорії',
    'PUB_PARSING' => 'Парсинг',
    'ON_PAGE_FROM' => 'З публікації',
    'ON_PAGE_TO' => 'До публікації',
    'ORDER' => 'Сортування',
    'ASC' => 'Зростання',
    'DESC' => 'Спадання',
    'RANDOM' => 'Випадково',
    'LAST_DAYS' => 'Кількість днів',
    'SEARCH' => 'Шукати',
    'NEXT' => 'Наступні',
    'PREV' => 'Попередні',
    'BACK' => 'Назад',
    'NO_PUBLICATIONS' => 'Немає інформації по заданому критерію.',
    'CONTENT_HOME' => 'Всі публікації',
    'CLEAR' => 'Очистити фільтр',
    'ERROR_TITLE' => 'Введіть назву!',
    'ERROR_SEARCH_TAG' => 'Пошуковий тег обов`язковий для категорій.',
    'PARSING' => 'Парсинг',
    'PUBLICATIONS' => 'Публікації',
    'CATEGORIES' => 'Категорії'
);

?>