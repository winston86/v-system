<?php

$lang = array(
'TITLE' => 'Заголовок',
'ERROR_403' => 'Доступ заборонено. У вас не вистачає прав.',
'ERROR_404' => 'ERROR 404. Сторінку не знайдено',
'APPLICATIONS' => 'Програми',
'MODULES' => 'Модулі',
'EXTENSIONS' => 'Розширення',
'SYS_SETTINGS' => 'Налаштування',
'PUBLISHED'=>'Опубліковано',
'UNPUBLISHED' => 'Знято з публікації',
'NOT_PUBLISHED'=>'Не опубліковано',
'NOT_INSTALLED' => 'Компонент ще не встановлено',
'SUB_PAGES' => 'Показувати у підрядних сторінках',
'ATTACHE_TO_URL' => 'Закріпити за адресою',
'POSITION' => 'Позиція',
'ROUTE' => 'Шлях',
'PRIORITY' => 'Пріоритет',
'ENABLE' => 'Ввімкнути',
'DISABLE' => 'Вимкнути',
'SUBMIT' => 'Відправити',
'LANGUAGE' => 'мова',
'PAGES' => 'Сторінки',
'DELETE' => 'Видалити',
'ADD' => 'Додати',
'ACTIONS' => 'Дії',
'EDIT' => 'Редагувати',
'SAVED'	=>	'Налаштування збережено',
'SEND'=>'Відправити',
'BACK'=>'Назад',
'ID' => 'Ідентифікатор',
'DESCRIPTION' => 'Опис',
'DELETED' => 'Видалення пройшло успішно',
'ON_PAGE' => 'На сторінці',
'FROM' => 'Усіх',
'COMMENTS' => 'Коментарі',
'REQUIRED_FIELD' => 'Обовязко для заповнення',
'ADD_FEATURE_VARIANT' => 'Додати варінт параметра',
'SAVE' => 'Зберегти',
'CANCEL' => 'Відміна',
'FILE' => 'Файл',
'FILE_UPLOADED' => 'Файл завантажено',
'UPLOAD_ERR_INI_SIZE' => 'Розмір завантаженого файла перевищив допустимий розмір',
'UPLOAD_ERR_FORM_SIZE' => 'Розмір завантаженого файла перевищив очікуваний розмір',
'UPLOAD_ERR_PARTIAL' => 'Файл отриманий не повністю',
'UPLOAD_ERR_NO_FILE' => 'Файл не завантажено',
'UPLOAD_ERR_NO_TMP_DIR' => 'Проблеми з сервером. Відсутня тимчасова папка!',
'UPLOAD_ERR_CANT_WRITE' => 'Не вдалося записати на диск. Скоріше всього проблеми з правами на запис. Зверніться до адміністратора.',
'UPLOAD_ERR_EXTENSION' => 'Завантаження файлу зупинено... Зверніться до адміністратора',
'COPY' => 'Копіювати',
'SEARCH' => 'Шукати',
'ITEMS_NOT_FOUND' => 'Нічого не знайдено по заданим критеріям',
'YES' => 'Так',
'NO' => 'Ні',
'ALL' => 'Все',
'TOOLS' => 'Дії',
'LIST' => 'Список',
'MASSIVE_ACTION' => 'З відміченими',
'PUBLISH' => 'Опублікувати',
'UNPUBLISH' => 'Зняти з публікації',
'MOD_NOT_PUBLISHED' => 'Не опублікований',
'ID_SHORT' => 'ID',
'TEXT' => 'Текст',
'USER' => 'Користувач',
'ERROR_UPLOAD' => 'Помилка завантаження файла',
'ERROR_EMAIL' => 'Не вірно указано адресу електронної пошти',
'PAGE_ROUTE' => 'URL сторінки',
'' => '',
'' => '',
'' => '',
'' => '',
'' => '',
'' => '',
'' => '',
'' => '',
'' => '',
'' => '',
'' => '',

);

?>