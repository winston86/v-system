<?php

$_ALLOWED_SCRIPTS = [
    '/var/www/victor.loc/extensions/ckeditor/',
];

$_FORBIDDEN_SCRIPTS = [

];

$_ALLOWED_MIMES = ['video/3gpp', 'video/3gpp2', 'application/x-7z-compressed', 'application/x-shockwave-flash', 'application/pdf', 'audio/x-aac', 'application/vnd.android.package-archive', 'video/x-msvideo', 'image/bmp', 'application/x-bittorrent', 'application/x-bzip', 'application/x-bzip2', 'text/css', 'image/vnd.dvb.subtitle', 'text/csv', 'application/x-debian-package', 'image/vnd.djvu', 'video/x-flv', 'image/gif', 'image/x-icon', 'text/vnd.sun.j2me.app-descriptor', 'application/java-archive', 'application/javascript', 'text/x-java-source,java', 'application/java-vm', 'application/json', 'image/jpeg', 'image/x-citrix-jpeg', 'video/jpeg', 'audio/x-mpegurl', 'video/x-m4v', 'application/x-msdownload', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.openxmlformats-officedocument.spreadsheetml.template', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.wordprocessingml.template', 'audio/x-ms-wma', 'video/x-ms-wm', 'video/x-ms-wmv', 'application/msword', 'application/vnd.ms-word.document.macroenabled.12', 'application/vnd.ms-word.template.macroenabled.12', 'video/mpeg', 'audio/mp4', 'video/mp4', 'application/mp4', 'image/vnd.adobe.photoshop', 'image/png', 'image/x-citrix-png', 'image/x-png', 'application/x-rar-compressed', 'application/rss+xml', 'application/x-tar', 'text/plain', 'application/x-font-ttf', 'text/turtle', 'audio/x-wav', 'application/x-font-woff', 'application/xslt+xml', 'application/zip', 'text/x-asm', 'text/x-php', 'text/html'];
