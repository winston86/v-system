<?php
/**
 * Class and Function List:
 * Function list:
 * - log()
 * Classes list:
 * - Logger
 */
namespace classes;

class Logger
{

    public function log($data)
    {

        global $_CONFIG;

        if ($_CONFIG['ENABLE_LOGS'] != 1) {
            return;
        }

        $file = ROOT_DIR . '/log.txt';
        $log  = array_filter(file($file));
        $tail = implode("", array_slice($log, max(count($log) - 100, 0), 100));

        file_put_contents($file, $tail . date("Y-m-d H:i:s") . ' ::::: ' . $data . "\n");

    }

}
