<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - to()
 * - from()
 * - theme()
 * - message()
 * - charset()
 * - type()
 * - headers()
 * - send()
 * Classes list:
 * - Mailer
 */
namespace classes;

class Mailer
{

    public function __construct()
    {

        $this->to = '';

        $this->from = '';

        $this->theme = '';

        $this->message = '';

        $this->type = '';

        $this->headers = '';

        $this->charset = 'utf-8';

    }

    public function to($to)
    {
        if (empty($to)) {
            return;
        }
        if (!is_array($to)) {
            $to = [$to];
        }
        foreach ($to as $recipient) {
            $this->to .= $recipient . ",";
        }

    }

    public function from($from)
    {

        $this->headers .= "From: {$from}\r\n";

    }

    public function theme($theme)
    {

        $this->theme = $theme;

    }

    public function message($message)
    {

        $this->message = $message;

    }

    public function charset($charset)
    {

        $this->charset = $charset;

    }

    public function type($type = 'plain')
    {

        $this->headers .= "MIME-Version: 1.0\r\n";
        $this->headers .= "Content-type: text/{$type}; charset={$this->charset}\r\n";

    }

    public function headers($header)
    {

        $this->headers .= $header;

    }

    public function send()
    {

        return mail($this->to, $this->theme, $this->message, $this->headers);

    }

}
