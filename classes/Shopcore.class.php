<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - getProductsByCats()
 * - getProducts()
 * - getProductsCount()
 * - getProductsCountByCats()
 * - getProductVariants()
 * - getProductManufacturers()
 * - getProductWarehouses()
 * - getProductRatings()
 * - getProductVisits()
 * - getProductImages()
 * - getProductPrices()
 * - getProductCategories()
 * - getProductDescriptions()
 * - getProductTitles()
 * - addProduct()
 * - updateProduct()
 * - deleteProduct()
 * - getWarehousesSelect()
 * - getWarehouses()
 * - getWarehousesCount()
 * - addWarehouse()
 * - updateWarehouses()
 * - deleteWarehouses()
 * - getManufacturersSelect()
 * - getManufacturers()
 * - getManufacturersCount()
 * - addManufacturer()
 * - updateManufacturer()
 * - deleteManufacturers()
 * - getCatSelect()
 * - getCategoriesByParentId()
 * - getCategoriesCountByParentId()
 * - getCategories()
 * - getCatParents()
 * - getCategoriesRatings()
 * - addCategory()
 * - updateCategory()
 * - deleteCategories()
 * - changeCurrency()
 * - getCurrentCurrency()
 * - getCurrentCurrencyId()
 * - getCurrencyRate()
 * - getSumByCurrency()
 * - countSum()
 * - getOrderSum()
 * - getVariants()
 * - getGroupSales()
 * - getProductSales()
 * - getCurrencies()
 * - addCurrency()
 * - updateCurrencies()
 * - deleteCurrencies()
 * - getCurSelect()
 * - getCurrenciesCount()
 * - getCartProducts()
 * - addToCart()
 * - removeFromCart()
 * - editQuantity()
 * - addOrder()
 * - addOrderItems()
 * - makeOrderInfoShot()
 * - updateOrder()
 * - updateOrderItems()
 * - getOrders()
 * - deleteOrders()
 * - getOrderItems()
 * - getOrdersCount()
 * - sendNotification()
 * - getPayments()
 * - getPaymentsCount()
 * - addPayment()
 * - updatePayments()
 * - deletePayments()
 * - getDeliveryServices()
 * - getDeliveryServicesCount()
 * - updateDeliveryServices()
 * - addDeliveryService()
 * - deleteDeliveryServices()
 * - addFeature()
 * - updateFeature()
 * - deleteFeatures()
 * - getFeatures()
 * - getFeaturesCount()
 * - getProductFeatures()
 * - addFeatureVariant()
 * - updateFeatureVariant()
 * - deleteFeatureVariants()
 * - getFeatureVariants()
 * - getFeatureVariantsCount()
 * - getDiscounts()
 * - getDiscountsCount()
 * - addDiscount()
 * - updateDiscounts()
 * - deleteDiscounts()
 * - getSales()
 * - getSaleAttrs()
 * - getSalesCount()
 * - addSale()
 * - addSaleAttrs()
 * - updateSaleAttrs()
 * - updateSales()
 * - deleteSales()
 * - getOrderStatuses()
 * - getOrderStatusesCount()
 * - addOrderStatus()
 * - updateOrderStatuses()
 * - deleteOrderStatuses()
 * Classes list:
 * - ShopCore extends BaseModel
 */
namespace classes;

use classes\Mailer;
use core\BaseModel;
use core\Core;

class ShopCore extends BaseModel
{

    public function __construct()
    {

        global $Core;

        $this->settings = $Core->getAppSettings('shop');

        parent::__construct();

    }

    ////////products
    public function getProductsByCats($cat_ids)
    {

        $sql = "SELECT * FROM {$this->getDbPrefix()}shop_products WHERE";

        if (!is_array($cat_ids)) {
            $cat_ids = array(
                'cat_ids' => $cat_ids,
            );
        }

        foreach ($cat_ids as $cat_id) {

            $sql .= " {(int)$cat_id} IN(categories) OR";

        }

        $sql = trim($sql, "OR");

        if ($products = $this->query($sql)) {

            return $products;

        }

        return false;

    }

    public function getProducts($params = array(), $limit = null)
    {

        if ($products = $this->selectRows('shop_products', '*', $params, $limit)) {

            foreach ($products as $key => $product) {

                $products[$key]['categories']    = $this->getProductCategories(['product_id' => $product['id']]);
                $products[$key]['warehouses']    = $this->getProductWarehouses(['product_id' => $product['id']]);
                $products[$key]['manufacturers'] = $this->getProductManufacturers(['product_id' => $product['id']]);
                $products[$key]['variants']      = $this->getProductVariants(['product_id' => $product['id']]);

            }

            return $limit == 1 ? $products[0] : $products;

        }

        return false;

    }

    public function getProductsCount($params)
    {

        if ($count_products = $this->countRows('shop_products', $params)) {
            $this->products_count = $count_products;
            return $count_products;

        }

        return false;

    }

    public function getProductsCountByCats($cat_ids)
    {

        $sql = "SELECT COUNT(id) as count FROM {$this->getDbPrefix()}shop_products WHERE";

        if (!is_array($cat_ids)) {
            $cat_ids = array(
                'cat_ids' => $cat_ids,
            );
        }

        foreach ($cat_ids as $cat_id) {

            $sql .= " {(int)$cat_id} IN(categories) OR";

        }

        $sql = trim($sql, "OR");

        if ($res = $this->query($sql)) {

            return $res['count'];

        }

        return false;

    }

    public function getProductVariants($params)
    {

        if (empty($params['product_id'])) {
            return [];
        }

        if (isset($params['published']) && $params['published'] == 1) {
            $published = 1;
        } else {
            $published = array(
                'operator' => '>=',
                'value'    => 0,
            );
        }

        $params['published'] = $published;

        $this->setJoin(array(
            'v' => array(
                'shop_feature_variants',
                '*',
                array(
                    'id'        => 'main.variant_id',
                    'published' => $published,
                ),
            ),
            'f' => array(
                'shop_features',
                'type,title as feature_title,id  as feature_id',
                array(
                    'id'        => 'main.feature_id',
                    'published' => $published,
                ),
            ),
        ), 'LEFT');

        if ($variants = $this->selectRows('shop_product_variants', '*', $params, false, 'main.feature_id,main.variant_id')) {
            return $variants;
        }

        return false;

    }

    public function getProductManufacturers($params)
    {

        if (isset($params['id']) && !isset($params['product_id'])) {
            $params['product_id'] = $params['id'];
        }

        if ($manufacturers = $this->selectRows('shop_product_into_manufacturer', '*', $params)) {

            return $manufacturers;

        }

        return false;

    }

    public function getProductWarehouses($params)
    {

        if (isset($params['id']) && !isset($params['product_id'])) {
            $params['product_id'] = $params['id'];
        }

        if ($warehouses = $this->selectRows('shop_product_into_warehouse', '*', $params)) {

            return $warehouses;

        }

        return false;

    }

    public function getProductRatings($prod_ids)
    {

        $params = array();

        if (isset($prod_ids)) {
            $params = array(
                'id' => $prod_ids,
            );
        }

        if ($products = $this->selectRows('shop_products', 'id,votes,rating', $params)) {

            $ratings = array();

            foreach ($products as $product) {

                $ratings[$product['id']] = array(
                    'votes'  => $product['votes'],
                    'rating' => $product['rating'],
                );

            }

            return count($ratings) > 1 ? $ratings : $ratings[0];

        }

        return false;

    }

    public function getProductVisits($prod_ids)
    {

        $params = array();

        if (isset($prod_ids)) {
            $params = array(
                'id' => $prod_ids,
            );
        }

        if ($products = $this->selectRows('shop_products', 'id,visits', $params)) {

            $visits = array();

            foreach ($products as $product) {

                $visits[$product['id']] = $product['description'];

            }

            return count($visits) > 1 ? $visits : $visits[0];

        }

        return false;

    }

    public function getProductImages($prod_ids, $size = false)
    {

        $params = array();

        if (isset($prod_ids)) {
            $params = array(
                'id' => $prod_ids,
            );
        }

        if ($products = $this->selectRows('shop_products', 'id,images', $params)) {

            $images = array();

            foreach ($products as $product) {
                if (empty($product['images'])) {
                    continue;
                }
                $images[$product['id']] = explode(",", $product['images']);

            }

        }

        return !empty($images) ? $images : false;

    }

    public function getProductPrices($prod_ids)
    {

        $params = array();

        if (isset($prod_ids)) {
            $params = array(
                'id' => $prod_ids,
            );
        }

        if ($products = $this->selectRows('shop_products', 'id,price,sale_price,purchase_price', $params)) {

            $prices = array();

            foreach ($products as $product) {

                $prices[$product['id']] = array(
                    'price'          => $product['price'],
                    'sale_price'     => $product['sale_price'],
                    'purchase_price' => $product['purchase_price'],
                );

            }

            return count($prices) > 1 ? $prices : $prices[0];

        }

        return false;

    }

    public function getProductCategories($params)
    {

        if ($categories = $this->selectRows('shop_product_into_category', '*', $params)) {

            return $categories;

        }

        return false;

    }

    public function getProductDescriptions($prod_ids)
    {

        $params = array();

        if (isset($prod_ids)) {
            $params = array(
                'id' => $prod_ids,
            );
        }

        if ($products = $this->selectRows('shop_products', 'id,description', $params)) {

            $descriptions = array();

            foreach ($products as $product) {

                $descriptions[$product['id']] = $product['description'];

            }

            return $descriptions;

        }

        return false;

    }

    public function getProductTitles($prod_ids)
    {

        $params = array();

        if (isset($prod_ids)) {
            $params = array(
                'id' => $prod_ids,
            );
        }

        if ($products = $this->selectRows('shop_products', 'id,title', $params)) {

            $titles = array();

            foreach ($products as $product) {

                $titles[$product['id']] = $product['title'];

            }

            return $titles;

        }

        return false;

    }

    public function addProduct($product)
    {

        if ($prod_id = $this->insert('shop_products', $product)) {

            foreach ($product['manufacturers'] as $key => $value) {
                $this->insert('shop_product_into_manufacturer', array(
                    'product_id'      => $prod_id,
                    'manufacturer_id' => $value,
                ));
            }

            foreach ($product['categories'] as $key => $value) {
                $this->insert('shop_product_into_category', array(
                    'product_id'  => $prod_id,
                    'category_id' => $value,
                ));
            }

            foreach ($product['warehouses'] as $key => $value) {
                $this->insert('shop_product_into_warehouse', array(
                    'product_id'   => $prod_id,
                    'warehouse_id' => $value,
                ));
            }

            foreach ($product['variants'] as $v_id => $q) {
                $variant['quantity']   = $q;
                $variant['product_id'] = $prod_id;
                $variant['variant_id'] = $v_id;
                $variant['feature_id'] = $this->selectField('shop_feature_variants', 'feature_id', array(
                    'id' => $v_id,
                ));

                $this->insert('shop_product_variants', $variant);
            }

            return $prod_id;

        }

        return false;

    }

    public function updateProduct($product, $params = array())
    {

        if (empty($params) && isset($product['id'])) {

            $params = array(
                'id' => $product['id'],
            );

        }

        if ($this->update('shop_products', $product, $params)) {
            $sql = "DELETE FROM {$this->prefix}shop_product_into_manufacturer WHERE product_id = '{$params['id']}' AND manufacturer_id NOT IN(" . $this->placeArray($product['manufacturers']) . ")";
            $this->query($sql);

            foreach ($product['manufacturers'] as $key => $value) {

                $this->insert('shop_product_into_manufacturer', array(
                    'product_id'      => $params['id'],
                    'manufacturer_id' => $value,
                ));
            }

            $sql = "DELETE FROM {$this->prefix}shop_product_into_category WHERE product_id = '{$params['id']}' AND category_id NOT IN(" . $this->placeArray($product['categories']) . ")";
            $this->query($sql);

            foreach ($product['categories'] as $key => $value) {
                $this->insert('shop_product_into_category', array(
                    'product_id'  => $params['id'],
                    'category_id' => $value,
                ));
            }

            $sql = "DELETE FROM {$this->prefix}shop_product_into_warehouse WHERE product_id = '{$params['id']}' AND warehouse_id NOT IN(" . $this->placeArray($product['warehouses']) . ")";
            $this->query($sql);

            foreach ($product['warehouses'] as $key => $value) {
                $this->insert('shop_product_into_warehouse', array(
                    'product_id'   => $params['id'],
                    'warehouse_id' => $value,
                ));
            }

            foreach ($product['variants'] as $v_id => $q) {
                $feature_id = $this->selectField('shop_feature_variants', 'feature_id', array(
                    'id' => $v_id,
                ));
                $sql = "INSERT INTO {$this->prefix}shop_product_variants (product_id,variant_id,feature_id,quantity) VALUES({$params['id']},$v_id,$feature_id,$q) ON DUPLICATE KEY UPDATE quantity = '{$q}' ";
                $this->query($sql);
                $v_ids[] = $v_id;
            }

            $v_ids = $this->placeArray($v_ids);

            $sql = "DELETE FROM {$this->prefix}shop_product_variants WHERE product_id = '{$params['id']}' AND variant_id NOT IN($v_ids)";
            $this->query($sql);

            return true;

        }

        return false;

    }

    public function deleteProduct($params = array())
    {

        $product_id = $this->selectField('shop_products', 'id', $params);

        if ($this->delete('shop_products', array(
            'id' => $product_id,
        ))) {
            $this->delete('shop_product_into_warehouse', array(
                'id' => $product_id,
            ));
            $this->delete('shop_product_into_category', array(
                'id' => $product_id,
            ));
            $this->delete('shop_product_into_manufacturer', array(
                'id' => $product_id,
            ));
        }

    }

    ///////warehouses
    public function getWarehousesSelect($params = array())
    {

        $warehouses_select = array();

        foreach ($this->getWarehouses() as $wh) {
            $warehouses_select[$wh['id']] = $wh['title'];
        }

        return $warehouses_select;

    }

    public function getWarehouses($params = array(), $limit = null)
    {

        if ($warehouses = $this->selectRows('shop_warehouses', '*', $params, $limit)) {

            return $limit == 1 ? $warehouses[0] : $warehouses;

        }

        return array();

    }

    public function getWarehousesCount($params = array())
    {

        return $this->countRows('shop_warehouses', $params);

    }

    public function addWarehouse($data)
    {

        if ($warehouse_id = $this->insert('shop_warehouses', $data)) {

            return $warehouse_id;

        }

        return false;

    }

    public function updateWarehouses($data, $params)
    {

        if (empty($params) && isset($data['id'])) {

            $params = array(
                'id' => $data['id'],
            );

        }

        if ($this->update('shop_warehouses', $data, $params)) {

            return true;

        }

        return false;

    }

    public function deleteWarehouses($params)
    {

        if ($this->delete('shop_warehouses', $params)) {

            return true;

        }

        return false;

    }

    ///////manufacturers
    public function getManufacturersSelect($params = array())
    {
        $manufacturers_select = array();

        foreach ($this->getManufacturers() as $mf) {
            $manufacturers_select[$mf['id']] = $mf['title'];
        }

        return $manufacturers_select;

    }

    public function getManufacturers($params = array(), $limit = null)
    {

        if ($manufacturers = $this->selectRows('shop_manufacturers', '*', $params)) {

            return $limit == 1 ? $manufacturers[0] : $manufacturers;

        }

        return array();

    }

    public function getManufacturersCount($params = array())
    {

        return $this->countRows('sys_manufacturers', $params);

    }

    public function addManufacturer($data)
    {

        if ($manufacturer_id = $this->insert('shop_manufacturers', $data)) {

            return $manufacturer_id;

        }

        return false;

    }

    public function updateManufacturer($data, $params)
    {

        if (empty($params) && isset($data['id'])) {

            $params = array(
                'id' => $data['id'],
            );

        }

        if ($this->update('shop_manufacturers', $data, $params)) {

            return true;

        }

        return false;

    }

    public function deleteManufacturers($params)
    {

        if ($this->delete('shop_manufacturers', $params)) {

            return true;

        }

        return false;

    }

    ///////categories
    public function getCatSelect($params = array())
    {

        $cat_select = array();

        foreach ($this->getCategories() as $cat) {
            $cat_select[$cat['id']] = $cat['title'];
        }

        return $cat_select;

    }

    public function getCategoriesByParentId($cat_id = 0, $limit = 10)
    {

        $cat_id = (int) $cat_id;

        $sql = "SELECT * FROM {$this->getDbPrefix()}shop_categories WHERE '$cat_id' IN(parent_id) LIMIT $limit";

        if ($categories = $this->query($sql)) {

            return $categories;

        }

        return array();

    }

    public function getCategoriesCountByParentId($cat_id = 0)
    {

        $sql = "SELECT COUNT(id) as count FROM {$this->getDbPrefix()}shop_categories WHERE {(int)$cat_id} IN(parent_ids)";

        if ($res = $this->query($sql)) {

            return $res['count'];

        }

        return false;

    }

    public function getCategories($params = array(), $limit = null)
    {

        /* check params (parent_ids IN(parent_ids)) */

        $condition = '';
        foreach ($params as $key => $param) {
            if ($key == 'parent_ids') {
                foreach (explode(",", $param) as $p_id) {
                    $condition .= " AND " . $this->placeParams(" parent_ids = :p_id", array(
                        'p_id' => $p_id,
                    ));
                }
            } else {
                $condition .= " AND " . $this->placeParams(" $key = :param", array(
                    'param' => $param,
                ));
            }
        }

        $sql = "SELECT * FROM {$this->getDbPrefix()}shop_categories WHERE 1 $condition " . (!empty($limit) ? "LIMIT $limit" : '');

        if ($categories = $this->query($sql)) {

            $this->categories_count = count($categories);
            return $limit == 1 ? $categories[0] : $categories;

        }

        return false;

    }

    public function getCatParents($cat_id, $c_p = array())
    {

        $cat_p = $this->selectField('shop_categories', 'parent_id', array(
            'id' => $cat_id,
        ));

        if ($cat_p != 0) {

            $c_p[] = $cat_p;

            $this->getCatParents($cat_p, $c_p);

        }

        return $c_p;

    }

    public function getCategoriesRatings($cat_ids)
    {

        $params = array(
            'id' => $cat_ids,
        );

        if ($categories = $this->selectRows('shop_categories', 'id,votes,rating', $params)) {

            $rating = array();

            foreach ($categories as $category) {

                $rating[$category['id']] = array(
                    'votes'  => $category['votes'],
                    'rating' => $category['rating'],
                );

            }

            return $rating;

        }

        return false;

    }

    public function addCategory($data)
    {

        if ($cat_id = $this->insert('shop_categories', $data)) {

            return $cat_id;

        }

        return false;

    }

    public function updateCategory($data, $params)
    {

        if (empty($params) && isset($data['id'])) {

            $params = array(
                'id' => $data['id'],
            );

        }

        if ($this->update('shop_categories', $data, $params)) {

            return true;

        }

        return false;

    }

    public function deleteCategories($params = array())
    {

        if ($this->delete('shop_categories', $params)) {

            return true;

        }

        return false;

    }

    //////curency
    public function changeCurrency($curency_id)
    {

        global $Core;

        $val = array(
            'currency' => $curency_id,
        );

        $Core->sessionPut($val);

    }

    public function getCurrentCurrency()
    {

        global $Core;

        $currency_id = $Core->inSession('currency') ? $Core->sessionGet('currency') : $this->settings['currency'];

        $params = array(
            'id' => $currency_id,
        );

        if ($currency = $this->selectRows('shop_currencies', '*', $params)) {

            return $currency;

        }

        return false;

    }

    public function getCurrentCurrencyId()
    {

        global $Core;

        return $Core->inSession('currency') ? $Core->sessionGet('currency') : $this->settings['currency'];

    }

    public function getCurrencyRate()
    {

        global $Core;

        if (!$Core->inSession('currency') || $Core->sessionGet('currency') == $this->settings['currency']) {

            return 1;

        } else {

            $params = array(
                'id' => $this->getCurrentCurrencyId(),
            );

            return $this->selectFields('shop_currencies', 'rate', $params);

        }

    }

    public function getSumByCurrency($sum = 0)
    {

        return $sum * $this->getCurrencyRate();

    }

    public function countSum($items, $order)
    {
        global $Core;
        $sum = 0;
        foreach ($items as $key => $item) {
            $product = $this->getProducts(array(
                'id' => $item['product_id'],
            ), 1);
            $base_sum = $product['price'];
            $item_sum = 0;

            if (!is_array($item['variants_id'])) {
                $product['variants_id'] = explode(",", $item['variants_id']);
            }

            foreach ($product['variants_id'] as $key => $var) {
                $params = array(
                    'id'        => $var,
                    'published' => 1,
                );
                $variant = $this->getVariants($params, 1);
                if ($variant['mod_type'] == 'currency') {
                    $item_sum += $base_sum + $variant['mod_value'];
                } elseif ($variant['mod_type'] == '%') {
                    $item_sum += $base_sum / 100 * $variant['mod_value'];
                }
            }

            if ($item_sum <= 0) {
                $item_sum = $base_sum;
            }

            $params = array(
                'user_id'   => $order['user_id'],
                'published' => 1,
            );

            $diff = 0;

            $discounts = $this->getDiscounts($params, null, "value ", "type");

            foreach ($discounts as $key => $dis) {
                if ($dis['type'] == '%') {
                    $tmp_diff = $base_sum / 100 * $dis['value'];
                } elseif ($dis['type'] == 'currency') {
                    $tmp_diff = $dis['value'];
                }
                $diff = max($diff, $tmp_diff);
            }

            $sales = $this->getProductSales($product['id']);

            $groups = $Core->users->getUserGroupId($order['user_id']);
            $sales += $this->getGroupSales($groups);

            foreach ($sales as $key => $sale) {
                if ($sale['type'] == '%') {
                    $tmp_diff = $base_sum / 100 * $sale['value'];
                } elseif ($sale['type'] == 'currency') {
                    $tmp_diff = $sale['value'];
                }
                $diff = max($diff, $tmp_diff);
            }

            $item_sum -= $diff;

            if (!empty($product['sale_price'])) {
                $sum += min($item_sum, $product['sale_price']);
            }

            $sum = $sum * $item['quantity'];
            $sum = $this->getSumByCurrency($sum);

        }

        return $sum;

    }

    public function getOrderSum($o_id)
    {
        $items = $this->getOrderItems(array(
            'order_id' => $o_id,
        ));
        $sum = 0;
        foreach ($items as $key => $item) {
            $sum += $item['sum'];
        }
        $sum = $this->getSumByCurrency($sum);
        return $sum;
    }

    public function getVariants($params, $limit = null)
    {
        global $Core;
        $this->setJoin(array(
            'f' => array(
                'shop_features',
                'published',
                array(
                    'id'        => 'main.feature_id',
                    'published' => 1,
                ),
            ),
        ), 'INNER');
        $res = $this->selectRows('shop_feature_variants', '*', $params, $limit);
        return $limit == 1 ? $res[0] : $res;
    }

    public function getGroupSales($groups)
    {

        $sales = array();
        if (!is_array($groups)) {
            $groups = array(
                $groups,
            );
        }
        foreach ($groups as $key => $group) {
            $params = array(
                'group_id' => $group['group_id'],
            );
            $this->setJoin(array(
                's' => array(
                    'shop_sales',
                    '*',
                    array(
                        'id'        => 'main.sale_id',
                        'published' => 1,
                        'start'     => array(
                            'operator' => '<=',
                            'value'    => $this->escapeStr(date("Y-m-d H:i:s")),
                        ),
                        'end'       => array(
                            'operator' => '>=',
                            'value'    => $this->escapeStr(date("Y-m-d H:i:s")),
                        ),
                    ),
                ),
            ), "INNER");
            $sales += $this->selectRows('shop_sale_groups', 'sale_id', $params, null, '', ' id');
        }
        return $sales;
    }

    public function getProductSales($p_id)
    {

        $cats = $this->getProductCategories(array(
            'product_id' => $p_id,
        ));
        $mans = $this->getProductManufacturers(array(
            'product_id' => $p_id,
        ));
        $wars = $this->getProductWarehouses(array(
            'product_id' => $p_id,
        ));

        $sales = array();

        foreach ($cats as $key => $cat) {
            $params = array(
                'target'    => 'category',
                'target_id' => $cat['category_id'],
            );
            $this->setJoin(array(
                's' => array(
                    'shop_sales',
                    '*',
                    array(
                        'id'        => 'main.sale_id',
                        'published' => 1,
                        'start'     => array(
                            'operator' => '<=',
                            'value'    => $this->escapeStr(date("Y-m-d H:i:s")),
                        ),
                        'end'       => array(
                            'operator' => '>=',
                            'value'    => $this->escapeStr(date("Y-m-d H:i:s")),
                        ),
                    ),
                ),
            ), "INNER");
            $sales += $this->selectRows('shop_sale_targets', 'sale_id', $params);

        }

        foreach ($mans as $key => $man) {
            $params = array(
                'target'    => 'manufacturer',
                'target_id' => $man['manufacturer_id'],
            );
            $this->setJoin(array(
                's' => array(
                    'shop_sales',
                    '*',
                    array(
                        'id'        => 'main.sale_id',
                        'published' => 1,
                        'start'     => array(
                            'operator' => '<=',
                            'value'    => $this->escapeStr(date("Y-m-d H:i:s")),
                        ),
                        'end'       => array(
                            'operator' => '>=',
                            'value'    => $this->escapeStr(date("Y-m-d H:i:s")),
                        ),
                    ),
                ),
            ), "INNER");
            $sales += $this->selectRows('shop_sale_targets', 'sale_id', $params);
        }

        foreach ($wars as $key => $war) {
            $params = array(
                'target'    => 'warehouse',
                'target_id' => $war['warehouse_id'],
            );
            $this->setJoin(array(
                's' => array(
                    'shop_sales',
                    '*',
                    array(
                        'id'        => 'main.sale_id',
                        'published' => 1,
                        'start'     => array(
                            'operator' => '<=',
                            'value'    => $this->escapeStr(date("Y-m-d H:i:s")),
                        ),
                        'end'       => array(
                            'operator' => '>=',
                            'value'    => $this->escapeStr(date("Y-m-d H:i:s")),
                        ),
                    ),
                ),
            ), "INNER");
            $sales += $this->selectRows('shop_sale_targets', 'sale_id', $params);
        }

        return $sales;

    }

    public function getCurrencies($params = array(), $limit = null)
    {

        $currs = $this->selectRows('shop_currencies', '*', $params, $limit);

        return $limit == 1 ? $currs[0] : $currs;

    }

    public function addCurrency($data)
    {

        if ($currency_id = $this->insert('shop_currencies', $data)) {

            return $currency_id;

        }

        return false;

    }

    public function updateCurrencies($data, $params = array())
    {

        if (empty($params) && isset($data['id'])) {

            $params = array(
                'id' => $data['id'],
            );

        }

        unset($data['id']);

        if ($this->update('shop_currencies', $data, $params)) {

            return true;

        }

        return false;

    }

    public function deleteCurrencies($curency_ids)
    {

        $params = array(
            'id' => $curency_ids,
        );

        if ($this->delete('shop_currencies', $params)) {

            return true;

        }

        return false;

    }

    public function getCurSelect($params = array())
    {

        foreach ($this->getCurrencies($params) as $key => $value) {
            $cur_select[$value['id']] = $value['title'];
        }

        return $cur_select;
    }

    public function getCurrenciesCount($params = array())
    {

        return $this->countRows('shop_currencies', $params);

    }

    ///////cart
    public function getCartProducts()
    {

        global $Core;

        $cart = $Core->inSession('cart') ? $Core->sessionGet('cart') : false;

        if (!$cart) {
            return false;
        }

        return $this->getProductsById($cart['products']);

    }

    public function addToCart($prod_id)
    {

        global $Core;

        $cart = $Core->inSession('cart') ? $Core->sessionGet('cart') : array();

        $cart['products'][] = $prod_id;

        $Core->sessionPut(array(
            'cart' => $cart,
        ));

    }

    public function removeFromCart($prod_ids)
    {

        global $Core;

        foreach ($prod_ids as $prod_id) {

            $cart = $Core->inSession('cart') ? $Core->sessionGet('cart') : array();

            if (count($cart) < 1) {
                return;
            }

            unset($cart['products'][array_search($prod_id, $cart['products'])]);

            $Core->sessionPut(array(
                'cart' => $cart,
            ));

        }

    }

    public function editQuantity($prod_id, $quantity)
    {

        global $Core;

        $cart = $Core->inSession('cart') ? $Core->sessionGet('cart') : array();

        if (count($cart) < 1) {
            return;
        }

        $cart['quantity'][$prod_id] = $quantity;

        $Core->sessionPut(array(
            'cart' => $cart,
        ));

    }

    ///////orders
    public function addOrder($data)
    {

        global $Core;

        $data['info'] = json_encode($data['info']);

        if (is_array($data['user_id'])) {
            list($data['user_id']) = explode("-", $data['user_id'][0]);
        }
        
        if ($order_id = $this->insert('shop_orders', $data)) {

            $Core->sessionUnset('cart');

            if (isset($this->settings['user_mail_notification'])) {

                $tpl = 'order_user.tpl.php';

                $this->sendNotification('user', $order_id, $tpl);

            }

            if (isset($this->settings['admin_mail_notification'])) {

                $tpl = 'order_admin.tpl.php';

                $this->sendNotification('admin', $order_id, $tpl);

            }

            return $order_id;

        }

        return false;

    }

    public function addOrderItems($order, $order_items)
    {

        foreach ($order_items as $hashed_id => $item) {

            list($i_id, $hash) = explode("-", $hashed_id);

            $item['hashed_id'] = $hashed_id;

            $item['order_id'] = $order['id'];

            $item['product_id'] = $i_id;

            $var_arr = [];

            if (isset($item['feature_variant'])) {
                foreach ($item['feature_variant'] as $f_id => $v) {
                    $var_arr[] = $v;
                }
            }

            if (isset($item['variants'])) {
                $var_arr = array_merge(array_keys($item['variants']), $var_arr);
            }

            $item['variants_id'] = implode(",", $var_arr);

            $item['discount_id'] = $item['discount_id'];

            $item['sale_id'] = $item['sale_id'];

            $item['sum'] = $this->countSum(array(
                $item,
            ), $order);

            $item['manager_user_id'] = (int) $item['manager_user_id'];

            if ($item_id = $this->insert('shop_order_items', $item)) {

                $item_ids[] = $item_id;

            }
        }

        return $item_ids;

    }

    public function makeOrderInfoShot($o_id, $order, $order_items)
    {

        $order['id'] = $o_id;
        $shot        = json_encode(array(
            'order'       => $order,
            'order_items' => $order_items,
        ));
        return $this->update('shop_orders', array(
            'order_infoshot' => $shot,
        ), array(
            'id' => $o_id,
        ));
    }

    public function updateOrder($data, $params = array())
    {

        if (empty($params) && isset($data['id'])) {

            $params = array(
                'id' => $data['id'],
            );

        } elseif (empty($params) && !isset($data['id'])) {
            return false;
        }

        if (is_array($data['user_id'])) {
            $data['user_id'] = $data['user_id'][0];
        }

        if ($this->update('shop_orders', $data, $params)) {

            if (isset($this->settings['user_mail_notification'])) {

                $tpl = 'order_user.tpl.php';

                $this->sendNotification('user', $data['id'], $tpl);

            }

            if (isset($this->settings['admin_mail_notification'])) {

                $tpl = 'order_admin.tpl.php';

                $this->sendNotification('admin', $data['id'], $tpl);

            }

            return true;

        }

        return false;

    }

    public function updateOrderItems($order, $order_items)
    {

        $this->delete('shop_order_items', array(
            'order_id' => $order['id'],
        ));

        foreach ($order_items as $hashed_id => $item) {

            list($i_id, $hash) = explode("-", $hashed_id);

            $item['hashed_id'] = $hashed_id;

            $item['order_id'] = $order['id'];

            $item['product_id'] = $i_id;

            $var_arr = [];

            if (isset($item['feature_variant'])) {
                foreach ($item['feature_variant'] as $f_id => $v) {
                    $var_arr[] = $v;
                }
            }

            if (isset($item['variants'])) {
                $var_arr = array_merge(array_keys($item['variants']), $var_arr);
            }

            $item['variants_id'] .= implode(",", $var_arr);

            $item['discount_id'] = $item['discount_id'];

            $item['sale_id'] = $item['sale_id'];

            $item['sum'] = $this->countSum(array(
                $item,
            ), $order);

            $item['manager_user_id'] = (int) $item['manager_user_id'];

            if ($item_id = $this->insert('shop_order_items', $item)) {

                $item_ids[] = $item_id;

            }
        }

        return $item_ids;
    }

    /*
    public function getOrderFields(){}

    public function addOrderFields(){}

    public function deleteOrders(){}
     */

    public function getOrders($params, $limit = false, $order = 'id ASC')
    {

        if ($orders = $this->selectRows('shop_orders', '*', $params, $limit, $order)) {

            return $limit == 1 ? $orders[0] : $orders;

        }

        return false;

    }

    public function deleteOrders($params)
    {
        $this->delete('shop_orders', $params);
        $this->delete('shop_order_items', ['order_id' => $params['id']]);
        return true;
    }

    public function getOrderItems($params, $limit = 0)
    {

        $this->setJoin(array(
            'p' => array(
                'shop_products',
                'title',
                array(
                    'id' => 'main.product_id',
                ),
            ),
        ));

        if ($products = $this->selectRows('shop_order_items', '*', $params)) {

            return $limit == 1 ? $products[0] : $products;

        }

    }

    public function getOrdersCount($params)
    {

        return $this->countRows('shop_orders', $params);

    }

    public function sendNotification($type, $order_id, $tpl)
    {

        global $Core;

        if ($type == 'user' && empty($this->settings['user_mail_notification'])) {
            return false;
        }

        if ($type == 'admin' && empty($this->settings['admin_mail_notification'])) {
            return false;
        }

        $order = $this->getOrders($order_id);

        if (!$order) {
            return false;
        }

        $order['info'] = json_decode($order['info']);

        $to = array();

        $from = $this->settings['shop_owner_email'];

        $theme = $this->lang['NOTIFICATION_' . $type] . $_SERVER['SERVER_NAME'];

        $message = $Core->render->renderTpl("/templates/native/mail/" . $tpl, $order);

        $mailer = new Mailer();

        $mailer->to($to);

        $mailer->from($from);

        $mailer->theme($theme);

        $mailer->message($message);

        $mailer->type('html');

        $mailer->send();
    }

    //////payments
    public function getPayments($params = array(), $limit = null)
    {

        if ($payments = $this->selectRows('shop_payments', '*', $params, $limit)) {

            return $limit == 1 ? $payments[0] : $payments;

        }

        return false;

    }

    public function getPaymentsCount($params = array())
    {

        return $this->countRows('shop_payments', $params);

    }

    public function addPayment($data)
    {

        /*if(isset($data['file'])){

        if($file = $Core->saveFile($data['file'],'/upload/files'))
        $data['file'] = '/upload/files'.$file['name'];

        }*/

        if ($payment_id = $this->insert('shop_payments', $data)) {

            return $payment_id;

        }

        return false;

    }

    public function updatePayments($data, $params)
    {

        if (empty($params) && isset($data['id'])) {

            $params = array(
                'id' => $data['id'],
            );

        }

        /*if(isset($data['file'])){

        if($file = $Core->saveFile($data['file'],'/uploads/files')){

        $data['file'] = '/upload/files/'.$file['name'];

        $c_file = $this->selectField('shop_payments','file',$params);

        if(file_exists($c_file)){

        unlink($c_file);

        }

        }

        }*/

        if ($this->update('shop_payments', $data, $params)) {

            return true;

        }

        return false;

    }

    public function deletePayments($params)
    {

        if ($this->delete('shop_payments', $params)) {

            return true;

        }

        return false;

    }

    //////deliverys
    public function getDeliveryServices($params = [], $limit = null)
    {

        if ($Dservices = $this->selectRows('shop_delivery_services', '*', $params, $limit)) {

            return $limit == 1 ? $Dservices[0] : $Dservices;

        }

        return false;

    }

    public function getDeliveryServicesCount($params)
    {

        return $this->countRows('delivery_services', $params);

    }

    public function updateDeliveryServices($data, $params = array())
    {

        if (empty($params) && isset($data['id'])) {

            $params = array(
                'id' => $data['id'],
            );

        }

        if ($this->update('shop_delivery_services', $data, $params)) {

            return true;

        }

        return false;

    }

    public function addDeliveryService($data)
    {

        if ($del_id = $this->insert('shop_delivery_services', $data)) {

            return $del_id;

        }

        return false;

    }

    public function deleteDeliveryServices($del_ids)
    {

        $params = array(
            'id' => $del_ids,
        );

        if ($this->delete('shop_delivery_services', $params)) {

            return true;

        }

        return false;

    }
    //////Features
    public function addFeature($feature)
    {

        foreach ($feature as $key => $value) {
            if (is_array($value)) {
                $feature[$key] = implode($feature[$key], ",");
            }
        }

        if ($feat_id = $this->insert('shop_features', $feature)) {

            return $feat_id;

        }

        return false;

    }

    public function updateFeature($feature, $params = array())
    {

        if (empty($params) && isset($feature['id'])) {

            $params = array(
                'id' => $feature['id'],
            );

        }

        foreach ($feature as $key => $value) {
            if (is_array($value)) {
                $feature[$key] = implode($feature[$key], ",");
            }
        }

        if ($this->update('shop_features', $feature, $params)) {

            return true;

        }

        return false;

    }

    public function deleteFeatures($params = array())
    {

        if ($this->delete('shop_features', $params)) {

            return true;

        }

        return false;

    }

    public function getFeatures($params = array(), $limit = null)
    {

        if ($feats = $this->selectRows('shop_features', '*', $params, $limit)) {

            return $limit == 1 ? $feats[0] : $feats;

        }

        return array();

    }

    public function getFeaturesCount($params)
    {

        if ($count = $this->countRows('shop_features', $params)) {

            return $count;

        }

        return false;

    }

    public function getProductFeatures($id)
    {
        $join['f'] = array(
            'shop_features',
            'type,title',
            array(
                'id' => 'main.feature_id',
            ),
        );
        $this->setJoin($join);
        return $this->selectRows('shop_product_variants', 'feature_id as id', ['product_id' => $id], false, '', 'id');
    }

    //////Features variants
    public function addFeatureVariant($data)
    {

        if ($var_id = $this->insert('shop_feature_variants', $data)) {

            return $var_id;

        }

        return false;

    }

    public function updateFeatureVariant($data, $params = array())
    {

        if (empty($params) && isset($data['id'])) {

            $params = array(
                'id' => $data['id'],
            );

        }

        unset($data['id']);

        if ($this->update('shop_feature_variants', $data, $params)) {

            return true;

        }

        return false;

    }

    public function deleteFeatureVariants($params = array())
    {

        if ($this->delete('shop_feature_variants', $params)) {

            return true;

        }

        return false;

    }

    public function getFeatureVariants($params = array(), $limit = null)
    {

        $join['f'] = array(
            'shop_features',
            'title as feature,type',
            array(
                'id' => 'main.feature_id',
            ),
        );

        $this->setJoin($join);

        if ($vars = $this->selectRows('shop_feature_variants', '*', $params, $limit, 'feature ASC')) {

            //return variants info
            return $limit == 1 ? $vars[0] : $vars;

        }

        return array();

    }

    public function getFeatureVariantsCount($params)
    {

        if ($count = $this->countRows('shop_feature_variants', $params)) {

            return $count;

        }

        return false;

    }

    public function getDiscounts($params = array(), $limit = null, $order = '', $group = '')
    {

        $join['u'] = array(
            'users',
            'nickname, email, login',
            array(
                'id' => 'main.user_id',
            ),
        );

        $this->setJoin($join);

        if ($discs = $this->selectRows('shop_discounts', '*', $params, $limit, $order, $group)) {

            return $limit == 1 ? $discs[0] : $discs;

        }

        return false;

    }

    public function getDiscountsCount($params)
    {

        return $this->countRows('shop_discounts', $params);

    }

    public function addDiscount($data)
    {

        global $Core;

        foreach ($Core->getRequest('items_checked') as $key => $value) {

            $data['user_id'] = $value;

            $this->insert('shop_discounts', $data);

        }

        return;

    }

    public function updateDiscounts($data, $params = array())
    {

        return $this->update('shop_discounts', $data, $params);

    }

    public function deleteDiscounts($params = array())
    {

        return $this->delete('shop_discounts', $params);

    }

    public function getSales($params = array(), $limit = null)
    {

        if ($sales = $this->selectRows('shop_sales', '*', $params, $limit)) {

            foreach ($sales as $key => $value) {

                $sales[$key] = $sales[$key] + $this->getSaleAttrs($value['id']);

            }

            return $limit == 1 ? $sales[0] : $sales;

        }

        return false;

    }

    public function getSaleAttrs($sale_id)
    {

        $cats = $this->selectField('shop_sale_targets', 'target_id', array(
            'sale_id' => $sale_id,
            'target'  => 'category',
        ));
        $sale['categories'] = is_array($cats) ? $cats : array(
            $cats,
        );
        $mans = $this->selectField('shop_sale_targets', 'target_id', array(
            'sale_id' => $sale_id,
            'target'  => 'manufacturer',
        ));
        $sale['manufacturers'] = is_array($mans) ? $mans : array(
            $mans,
        );
        $war = $this->selectField('shop_sale_targets', 'target_id', array(
            'sale_id' => $sale_id,
            'target'  => 'warehouse',
        ));
        $sale['warehouses'] = is_array($war) ? $war : array(
            $war,
        );
        $grps = $this->selectField('shop_sale_groups', 'group_id', array(
            'sale_id' => $sale_id,
        ));
        $sale['groups'] = is_array($grps) ? $grps : array(
            $grps,
        );

        return $sale;

    }

    public function getSalesCount($params)
    {

        return $this->countRows('shop_sales', $params);

    }

    public function addSale($data)
    {

        if ($sale_id = $this->insert('shop_sales', $data)) {
            $data['id'] = $sale_id;
            $this->addSaleAttrs($data);

        }

        return $sale_id;

    }

    public function addSaleAttrs($data)
    {

        foreach ($data['categories'] as $key => $value) {
            $sc['target']    = 'category';
            $sc['target_id'] = $value;
            $sc['sale_id']   = $data['id'];
            $this->insert('shop_sale_targets', $sc);
        }

        foreach ($data['manufacturers'] as $key => $value) {
            $sm['target']    = 'manufacturer';
            $sm['target_id'] = $value;
            $sm['sale_id']   = $data['id'];
            $this->insert('shop_sale_targets', $sm);
        }

        foreach ($data['warehouses'] as $key => $value) {
            $sw['target']    = 'warehouse';
            $sw['target_id'] = $value;
            $sw['sale_id']   = $data['id'];
            $this->insert('shop_sale_targets', $sw);
        }

        foreach ($data['groups'] as $key => $value) {
            $sg['group_id'] = $value;
            $sg['sale_id']  = $data['id'];
            $this->insert('shop_sale_groups', $sg);
        }

        return;

    }

    public function updateSaleAttrs($data, $params)
    {

        $params['sale_id'] = $params['id'];

        $params['target_id']['operator'] = ' NOT IN ';
        $params['target_id']['value']    = $data['categories'];

        $this->delete('shop_sale_targets', $params);

        $params['target_id']['operator'] = ' NOT IN ';
        $params['target_id']['value']    = $data['manufacturers'];

        $this->delete('shop_sale_targets', $params);

        $params['target_id']['operator'] = ' NOT IN ';
        $params['target_id']['value']    = $data['warehouses'];

        $this->delete('shop_sale_targets', $params);

        $params['group_id']['operator'] = ' NOT IN ';
        $params['group_id']['value']    = $data['groups'];

        $this->delete('shop_sale_groups', $params);

        $data['id'] = $params['id'];

        $this->addSaleAttrs($data);

        return;

    }

    public function updateSales($data, $params)
    {

        if ($this->update('shop_sales', $data, $params)) {

            $this->updateSaleAttrs($data, $params);

            return true;

        }

        return false;

    }

    public function deleteSales($params)
    {

        $this->delete('shop_sales', $params);
        $this->delete('shop_sale_targets', array(
            'sale_id' => $params['id'],
        ));
        $this->delete('shop_sale_groups', array(
            'sale_id' => $params['id'],
        ));
        return;

    }

    public function getOrderStatuses($params = array(), $limit = null)
    {

        if ($statuses = $this->selectRows('shop_order_statuses', '*', $params, $limit)) {

            return $limit == 1 ? $statuses[0] : $statuses;

        }

        return false;

    }

    public function getOrderStatusesCount($params)
    {

        return $this->countRows('shop_order_statuses', $params);

    }

    public function addOrderStatus($order_status)
    {

        return $this->insert('shop_order_statuses', $order_status);

    }

    public function updateOrderStatuses($order_status, $params)
    {

        return $this->update('shop_order_statuses', $order_status, $params);

    }

    public function deleteOrderStatuses($params)
    {

        return $this->delete('shop_order_statuses', $params);

    }

}
